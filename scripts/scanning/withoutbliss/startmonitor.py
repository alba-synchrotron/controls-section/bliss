#!/usr/bin/env python
import math
import time
import logging
from datetime import datetime, timedelta
from typing import Dict, Tuple, Generator, Sequence, Union

import numpy

from blissdata.redis_engine.scan import Scan
from blissdata.redis_engine.scan import ScanState
from blissdata.redis_engine.store import DataStore
from blissdata.redis_engine.exceptions import NoScanAvailable
from blissdata.redis_engine.stream import Stream
from blissdata.redis_engine.stream import StreamingClient
from blissdata.redis_engine.exceptions import EndOfStream


YIELD_PERIOD = 1


def main(redis_url: str, session_name: str) -> None:
    data_store = DataStore(redis_url)
    print(f"Connected to Redis at {redis_url}")
    print(f"Monitoring session '{session_name}'")
    for scan in _iterate_new_scans(data_store, session_name):

        # Wait until all scan start metadata has been published
        while scan.state < ScanState.PREPARED:
            scan.update(timeout=YIELD_PERIOD)
        _print_scan_info(scan.info)

        # Monitor the scan (could be done in a different thread or process)
        # Here we simply print the number of data points recieved per channel
        _process_scan_data(scan)

        # Wait until all scan end metadata has been published
        while scan.state < ScanState.CLOSED:
            scan.update(timeout=YIELD_PERIOD)
        _print_finished(scan.info)


def _iterate_new_scans(
    data_store: DataStore, session: str
) -> Generator[Scan, None, None]:
    """Yield new scans of a session indefinitely"""
    since = data_store.get_last_scan_timetag()
    while True:
        try:
            since, key = data_store.get_next_scan(since=since, timeout=YIELD_PERIOD)
            scan = data_store.load_scan(key, scan_cls=Scan)
            if scan.session == session:
                yield scan
        except NoScanAvailable:
            pass


def _process_scan_data(scan: Scan) -> None:
    """Handle scan data: print statistics in this example"""
    client = StreamingClient(scan.streams)
    nlast_lines = 0
    start_time = time.time()
    count_time = scan.info["count_time"]
    channels = dict()
    while True:
        try:
            data = client.read(timeout=YIELD_PERIOD)
        except EndOfStream:
            break
        seconds_since_start = time.time() - start_time
        channels.update(_extract_channel_info(data, seconds_since_start, count_time))
        nlast_lines = _print_scan_progress(channels, nlast_lines)


def _extract_channel_info(
    data: Dict[Stream, Tuple[int, Sequence[Union[numpy.ndarray, dict]]]],
    seconds_since_start: float,
    count_time: float,
) -> Dict[str, Dict[str, str]]:
    """Extract channel data statistics"""
    channels = dict()
    for stream, (first_index, stream_data) in data.items():
        data_type = stream.encoding["type"]
        if data_type == "numeric":
            dtype = numpy.dtype(stream.encoding["dtype_str"])
            shape = tuple(stream.encoding["shape"])
            ndim = len(shape)
            npoints = first_index + len(stream_data)
        elif data_type == "json":
            json_format = stream.info["format"]
            if json_format == "lima_v1":
                dtype = numpy.dtype(stream.info["dtype"])
                ndim = stream.info["dim"]
                shape = tuple(stream.info["shape"])
                last_status = stream_data[-1]
                npoints = last_status["last_index"] + 1
            else:
                raise ValueError(f"unknown JSON stream format '{data_type}'")
        else:
            raise ValueError(f"unknown stream data type '{data_type}'")

        point_byte_size = dtype.itemsize
        if shape:
            point_byte_size *= numpy.prod(shape, dtype=int)

        transferred_volume_per_sec = point_byte_size * npoints / seconds_since_start

        scan_volume_per_sec = point_byte_size / count_time

        chaninfo = {
            "Channel": stream.name,
            "Dim": str(ndim),
            "Shape": str(shape),
            "Dtype": str(dtype),
            "Points": str(npoints),
            "Volume": _pretty_print_byte_size(point_byte_size),
            "Data Rate": _pretty_print_byte_size(transferred_volume_per_sec) + "/s",
            "Scan Rate": _pretty_print_byte_size(scan_volume_per_sec) + "/s",
        }

        channels[stream.name] = chaninfo
    return channels


def _print_scan_info(info: dict) -> None:
    title = info["title"]
    filename = info["filename"]
    npoints = info["npoints"]
    count_time = info["count_time"]
    scan_number = info["scan_nb"]

    print(f"\n{title} (#{scan_number})")
    print(f" File URL: silx://{filename}?path={scan_number}.1")
    duration = timedelta(seconds=npoints * count_time)
    print(f" Scan duration: {duration}")


def _print_finished(info: dict) -> None:
    end_reason = info.get("end_reason")
    if end_reason:
        start_time = datetime.fromisoformat(info["start_time"])
        end_time = datetime.fromisoformat(info["end_time"])
        print("Scan ended with", end_reason, "in", end_time - start_time)
    else:
        print("Scan did not start")


def _pretty_print_byte_size(byte_size: int) -> str:
    if byte_size == 0:
        return "0B "
    size_name = ("B ", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(byte_size, 1024)))
    value = byte_size / math.pow(1024, i)
    unit = size_name[i]
    return f"{value:5.1f}{unit}"


_COLUMN_FMT = {
    "Channel": "{:<15}",
    "Dim": "{:>5}",
    "Shape": "{:>12}",
    "Dtype": "{:>8}",
    "Points": "{:>8}",
    "Volume": "{:>8}",
    "Data Rate": "{:>8}",
    "Scan Rate": "{:>8}",
}


def _print_scan_progress(channels: Dict[str, Dict[str, str]], nlast_lines: int) -> int:
    if not channels:
        return nlast_lines

    up = f"\x1B[{nlast_lines}A"
    down = "\x1B[0K"
    sep = " "

    lines = [
        sep
        + sep.join([fmt.format(row[colname]) for colname, fmt in _COLUMN_FMT.items()])
        for _, row in sorted(channels.items())
    ]

    ncurrent_lines = len(lines)

    if nlast_lines == 0:
        header = [fmt.format(colname) for colname, fmt in _COLUMN_FMT.items()]
        separator = ["-" * len(s) for s in header]
        print(sep + sep.join(header))
        print(sep + sep.join(separator))

        print("\n" * (ncurrent_lines - 1))
        up = f"\x1B[{ncurrent_lines}A"
    elif nlast_lines > ncurrent_lines:
        print(f"{up}{down}")
        for _ in range(1, nlast_lines):
            print(down)

    print(f"{up}{lines[0]}{down}")
    for r in range(1, ncurrent_lines):
        print(f"{lines[r]}{down}")

    return ncurrent_lines


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Start Data Monitor")

    parser.add_argument(
        "--redis", default="redis://localhost:6379", type=str, help="Redis URL"
    )

    parser.add_argument(
        "--session", default="test_session", type=str, help="Session name"
    )    

    parser.add_argument(
        "--log",
        default="INFO",
        type=str.upper,
        dest="level",
        help="Log level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"],
    )

    args = parser.parse_args()
    logging.basicConfig(level=args.level)

    main(args.redis, args.session)
