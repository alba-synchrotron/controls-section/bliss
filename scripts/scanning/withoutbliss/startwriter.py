#!/usr/bin/env python
import logging

from blisswriter.subscribers.session_subscriber import NexusSessionSubscriber


def main(redis_url: str, writer_name: str, session_name: str) -> None:
    # Start listening to Redis data
    subscriber = NexusSessionSubscriber(
        session_name=session_name,
        writer_name=writer_name,
        redis_url=redis_url,
        configurable=True,
    )
    subscriber.start()
    subscriber.join()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Start NeXus writer")

    parser.add_argument(
        "--redis", default="redis://localhost:6379", type=str, help="Redis URL"
    )

    parser.add_argument(
        "--name", default="nexus", type=str, help="Writer name"
    )

    parser.add_argument(
        "--session", default="test_session", type=str, help="Session name"
    )    

    parser.add_argument(
        "--log",
        default="INFO",
        type=str.upper,
        dest="level",
        help="Log level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"],
    )

    args = parser.parse_args()
    logging.basicConfig(level=args.level)

    main(args.redis, args.name, args.session)
