#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <session_name>"
    echo "No session_name provided, default=test_session"
    session_name="test_session"
else
    session_name="$1"
    echo session_name=$session_name
fi

export REDIS_DATA_HOST=redis://localhost:6379
exec flint -s "$session_name" --no-rpc --no-persistence
