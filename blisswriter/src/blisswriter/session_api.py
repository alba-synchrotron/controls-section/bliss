"""API available in bliss session

.. code:: python

    from blisswriter import *
"""

from .writer import scan_url_info  # noqa F401
