# -*- coding: utf-8 -*-
#
# This file is part of the mechatronic project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.controller.speedgoat import xpc


def test_lib():
    assert xpc.get_api_version() is not None
    assert xpc.get_last_error() is xpc.xpc.ENOERR
