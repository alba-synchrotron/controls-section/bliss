# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import numpy
from unittest import mock
from contextlib import contextmanager

from bliss.common.axis import Axis
from bliss.config import channels
from bliss.common.standard import ascan, dmesh
from bliss.common.motor_group import Group
from bliss.controllers.motor import CalcController
from bliss.common import scans
from bliss.common import event
from bliss.common.standard import mv, mvr, _move


def test_tags(s1ho):
    controller = s1ho.controller
    for tag, axis_name in {
        "front": "s1f",
        "back": "s1b",
        "up": "s1u",
        "down": "s1d",
        "hgap": "s1hg",
        "hoffset": "s1ho",
        "vgap": "s1vg",
        "voffset": "s1vo",
    }.items():
        assert controller._tagged[tag][0].name == axis_name


def test_real_tags(s1ho):
    controller = s1ho.controller
    assert [x.name for x in controller._tagged["real"]] == ["s1f", "s1b", "s1u", "s1d"]


def test_has_tag(s1ho, s1vg, s1u):
    assert s1ho.has_tag("hoffset")
    assert not s1ho.has_tag("vgap")
    assert not s1vg.has_tag("real")


def test_reals_list(s1ho):
    controller = s1ho.controller
    assert len(controller.reals) == 4
    assert all([isinstance(x, Axis) for x in controller.reals])


def test_pseudos_list(s1ho):
    controller = s1ho.controller
    assert len(controller.pseudos) == 4
    assert all([isinstance(x, Axis) for x in controller.pseudos])


def test_exported_pseudo_axes(s1vg, s1vo, s1hg, s1ho):
    assert all((s1vg, s1vo, s1hg, s1ho))
    controller = s1vg.controller
    assert all((axis.controller == controller for axis in (s1vg, s1vo, s1hg, s1ho)))
    assert all(["READY" in axis.state for axis in controller.pseudos])


def test_real_axis_is_right_object(s1f, s1ho, m1):
    controller = s1ho.controller
    assert s1f == controller.axes["s1f"]
    assert s1f.controller == m1.controller


def test_calc_with_init(roby, calc_mot1):
    # see issue #488
    # calc_mot1 defines some attribute in 'initialize_axis',
    # the next two lines should pass without exception:
    roby.position
    roby.sync_hard()


def test_limits(default_session, s1hg):
    # ======== Test a simple calc (reals are real axes) ================
    with pytest.raises(ValueError):
        s1hg.move(40)
    with pytest.raises(ValueError):
        s1hg.move(-16)

    # ======== Test a calc of calc (reals are pseudo axes) ============
    robz = default_session.config.get("robz")
    theta = default_session.config.get("theta")
    twotheta = default_session.config.get("twotheta")

    robz_backlash = robz.backlash
    robz_limits = robz.limits

    robz.backlash = 0
    robz.limits = [-1, 1]
    theta.limits = [0, 180]
    twotheta.limits = [0, 360]

    # === generated positions
    # twotheta  0  180  360
    # theta     0  90  180
    # robz      1  0  -1

    twotheta_positions = numpy.array([0, 180, 360])

    theta_positions = twotheta.controller.calc_to_real(
        {"twotheta": twotheta_positions}
    )["theta"]
    assert theta_positions == pytest.approx([0, 90, 180])

    robz_positions = theta.controller.calc_to_real({"angle": theta_positions})["px"]
    assert robz_positions == pytest.approx([1, 0, -1])

    # ========== It should not raise an error ==========================
    for i, pos in enumerate(twotheta_positions):
        twotheta.move(pos)
        assert pytest.approx(twotheta.position) == pos
        assert pytest.approx(theta.position) == theta_positions[i]
        assert pytest.approx(robz.position) == robz_positions[i]

    twotheta.controller.check_limits((twotheta, twotheta_positions))

    # ========== It should raise an error because of robz limits =======
    robz.limits = [0, 0.5]
    for pos in [0]:
        with pytest.raises(ValueError) as excinfo:
            twotheta.move(pos)
        assert excinfo.value.args[0].startswith("robz:")
    with pytest.raises(ValueError) as excinfo:
        twotheta.controller.check_limits((twotheta, twotheta_positions))
    assert excinfo.value.args[0].startswith("robz:")
    with pytest.raises(ValueError) as excinfo:
        ascan(twotheta, 0, 360, 2, 0.1, run=False)
    assert excinfo.value.args[0].startswith("robz:")
    robz.limits = [-1, 1]

    # ========== It should raise an error because of theta limits ======
    theta.limits = [10, 170]
    for pos in [0, 360]:
        with pytest.raises(ValueError) as excinfo:
            twotheta.move(pos)
        assert excinfo.value.args[0].startswith("theta:")
    with pytest.raises(ValueError) as excinfo:
        twotheta.controller.check_limits((twotheta, twotheta_positions))
    assert excinfo.value.args[0].startswith("theta:")
    theta.limits = [0, 180]

    # ========== It should raise an error because of twotheta limits ===
    twotheta.limits = [100, 260]
    for pos in [0, 360]:
        with pytest.raises(ValueError) as excinfo:
            twotheta.move(pos)
        assert excinfo.value.args[0].startswith("twotheta:")
    with pytest.raises(ValueError) as excinfo:
        twotheta.controller.check_limits((twotheta, twotheta_positions))
    assert excinfo.value.args[0].startswith("twotheta:")
    twotheta.limits = [0, 360]
    # ===================================================================

    robz.limits = robz_limits
    robz.backlash = robz_backlash


def test_hw_limits_and_set_pos(s1f, s1b, s1hg):
    try:
        s1f.controller.set_hw_limits(s1f, -2, 2)
        s1b.controller.set_hw_limits(s1b, -2, 2)
        with pytest.raises(RuntimeError):
            s1hg.move(6)
        assert s1hg._set_position == pytest.approx(s1hg.position)
    finally:
        s1f.controller.set_hw_limits(s1f, None, None)
        s1b.controller.set_hw_limits(s1b, None, None)


def test_real_move_and_set_pos(s1f, s1b, s1hg):
    s1hg.move(0.5)
    s1f.rmove(1)
    s1b.rmove(1)
    assert s1f._set_position == pytest.approx(1.25)
    assert s1b._set_position == pytest.approx(1.25)
    assert s1hg.position == pytest.approx(2.5)
    assert s1hg._set_position == pytest.approx(2.5)


def test_set_dial(roby, calc_mot1):
    calc_mot1.move(4)
    assert roby.position == pytest.approx(2)
    calc_mot1.dial = 0
    assert calc_mot1.position == pytest.approx(4)
    assert calc_mot1.dial == pytest.approx(0)
    assert roby.position == pytest.approx(0)


def test_set_position(roby, calc_mot1):
    calc_mot1.move(1)
    assert calc_mot1.offset == pytest.approx(0)
    assert roby.position == pytest.approx(0.5)
    calc_mot1.position = 0
    assert calc_mot1.offset == pytest.approx(-1)
    assert calc_mot1.position == pytest.approx(0)
    assert calc_mot1.dial == pytest.approx(1)
    assert roby.position == pytest.approx(0.5)


def test_offset_set_position(calc_mot1):
    calc_mot1.dial = 0
    calc_mot1.position = 1
    assert calc_mot1._set_position == pytest.approx(1)
    calc_mot1.move(0.1)
    assert calc_mot1._set_position == pytest.approx(0.1)


def test_calc_in_calc(roby, calc_mot1, calc_mot2):
    calc_mot1.move(1)
    assert calc_mot1.position == pytest.approx(1)
    assert roby.position == pytest.approx(0.5)
    calc_mot2.move(1)
    assert calc_mot1.position == pytest.approx(0.5)
    assert calc_mot2.position == pytest.approx(1)
    assert roby.position == pytest.approx(0.25)


def test_ascan_limits(session, s1hg, s1f, s1b):
    s1hg.position = 0
    s1hg.dial = 0
    s1f.limits = -1, 1
    s1b.limits = -1, 1
    with pytest.raises(ValueError) as out_of_range_exc:
        s1hg.move(2.1)
    assert "would exceed high limit" in str(out_of_range_exc.value)
    with pytest.raises(ValueError) as out_of_range_exc:
        ascan(s1hg, -1, 2.1, 10, 0.1, run=False)
    assert "would exceed high limit" in str(out_of_range_exc.value)


def test_same_calc_real_grp_move(s1hg, s1f, roby, calc_mot2):
    # test for issue 481
    with pytest.raises(RuntimeError) as exc:
        Group(s1hg, s1f)

    assert (
        "Virtual axis 's1hg` cannot be present in group with any of its corresponding real axes: ['s1f']"
        in str(exc.value)
    )

    with pytest.raises(RuntimeError) as exc:
        Group(roby, calc_mot2)

    assert (
        "Virtual axis 'calc_mot1` cannot be present in group with any of its corresponding real axes: ['roby']"
        in str(exc.value)
    )


def test_calc_motor_publishing(session, calc_mot2):
    diode = session.config.get("diode")
    m0 = session.config.get("m0")

    s = scans.a2scan(calc_mot2, 0, 1, m0, 0, 1, 3, 0.1, diode, save=False)
    pub_motors = s.scan_info["acquisition_chain"]["axis"]["master"]["scalars"]

    assert "axis:calc_mot2" in pub_motors
    assert "axis:m0" in pub_motors
    assert "axis:calc_mot1" in pub_motors
    assert "axis:roby" in pub_motors


def test_calc_motor_energy(beacon):
    # calculate energy form the position of roby
    mono = beacon.get("mono")
    mono.position = 9.1

    energy = beacon.get("energy")
    assert abs(energy.position - 12.5011) < 0.001

    wavelength = beacon.get("wavelength")
    assert abs(wavelength.position - 0.991784) < 0.001

    energy.move(6.56)
    assert abs(mono.position - 17.54141) < 0.001
    energy.controller.close()


def test_calc_motor_no_settings_axis(beacon, calc_mot3, nsa):
    # calc_mot3.position == 2 * nsa.position
    nsa.move(1)
    assert calc_mot3.position == pytest.approx(2)
    calc_mot3.move(1)
    assert nsa.position == pytest.approx(0.5)


def test_issue_1909(session):
    llbragg1 = session.config.get("llbragg1")
    llbeamy1 = session.config.get("llbeamy1")
    diode = session.config.get("diode")

    dmesh(llbragg1, -0.005, 0.005, 3, llbeamy1, -1, 1, 2, 0.01, diode)


def test_calc_sync_hard(session, calc_mot1, roby):
    assert roby.position == 0

    # change roby position directly in controller
    # (like moving motor manually),
    # then check that pseudo pos is updated when
    # calling sync_hard on real axis
    roby.controller.set_hw_position(roby, 1 * roby.steps_per_unit)
    roby.sync_hard()
    assert roby.position == 1
    assert calc_mot1.position == 2

    # test issue #3091
    # change roby position, without calc_mot1 noticing
    event.disconnect(
        roby, "internal_position", calc_mot1.controller._real_position_update
    )
    event.disconnect(
        roby, "internal__set_position", calc_mot1.controller._real_setpos_update
    )
    roby.position = 2
    roby.dial = 2  # make no offset
    event.connect(roby, "internal_position", calc_mot1.controller._real_position_update)
    event.connect(
        roby, "internal__set_position", calc_mot1.controller._real_setpos_update
    )

    assert roby.position == 2
    assert calc_mot1.position == 2
    calc_mot1.sync_hard()
    assert calc_mot1.position == 4

    # change roby position directly in controller
    # (like moving motor manually),
    # then do sync_hard on pseudo axis and check
    # real axis is updated
    roby.controller.set_hw_position(roby, 1 * roby.steps_per_unit)
    assert roby.position == 2
    assert calc_mot1.position == 4
    calc_mot1.sync_hard()
    assert roby.position == 1
    assert calc_mot1.position == 2


def test_calc_with_bad_real(default_session):
    bad = default_session.config.get("bad")
    calc = default_session.config.get("coupled_calc2")
    assert calc.state == "READY"
    calc.sync_hard()
    assert calc.state == "READY"

    bad.controller.fault_state = True
    calc.sync_hard()
    assert calc.state == "FAULT"


def test_calc_pseudo_sync(default_session):
    """Check there is no discrepancy between pseudo.position and pseudo._set_position after
    a parameter used in calc_from_real has been changed (the parameter is not an axis).
    """
    xpos = default_session.config.get("rcosx")  # pseudo
    omega = default_session.config.get("omega")  # real
    ctrl = xpos.controller  # calc_controller

    # set radius, move and check no discrepancy
    ctrl.radius = 2
    omega.move(0)
    assert xpos.position == 2
    assert xpos._set_position == 2

    # set radius and check no discrepancy
    ctrl.radius = 1
    assert xpos.position == 1
    assert xpos._set_position == 1

    # rmove and check no discrepancy
    omega.rmove(180)
    assert xpos.position == -1
    assert xpos._set_position == -1

    # set radius and check no discrepancy
    ctrl.radius = 2
    assert xpos.position == -2
    assert xpos._set_position == -2

    # rmove and check no discrepancy
    omega.rmove(-180)
    assert xpos.position == 2
    assert xpos._set_position == 2


def test_calcmot_w_offset_scan_check_limits_issue_3239(session, calc_mot1):
    diode = session.config.get("diode")
    calc_mot1.offset = 2

    with mock.patch.object(
        calc_mot1.controller, "check_limits", wraps=calc_mot1.controller.check_limits
    ) as mocked_check_limits:
        ascan(calc_mot1, 0, 1, 5, 0.01, diode)
        mocked_check_limits.assert_called_once()
        # cannot use 'assert_called_with' because of numpy array comparison
        call_items = mocked_check_limits.call_args[0][0]
        axis, pos = call_items
        assert axis is calc_mot1
        numpy.testing.assert_array_equal(
            pos, numpy.array([-2, -1.8, -1.6, -1.4, -1.2, -1])
        )


def test_issue_3285(session, robz):
    diode = session.config.get("diode")
    robz.offset = -35
    robz.limits = -5, 5
    scans.ascan(robz, -4, -3, 3, 0.1, diode)
    with pytest.raises(ValueError) as exc:
        scans.ascan(robz, -6, -3, 3, 0.1, diode)
    assert "would exceed low limit" in str(exc)
    with pytest.raises(ValueError) as exc:
        scans.ascan(robz, -3, 6, 3, 0.1, diode)
    assert "would exceed high limit" in str(exc)


def test_check_limits(session, simulation_monochromator):
    mono = simulation_monochromator
    sim_mono1 = mono.motors.bragg
    sim_mono1e = mono.motors.energy
    ### put same conditions as on ID09
    sim_mono1.sign = -1
    sim_mono1.dial = 44.3499
    sim_mono1.offset = 51.4479
    sim_mono1.limits = 1.3, 29.8
    ###
    scans.dscan(sim_mono1e, -3, 3, 1, 0)


def test_issue_3454(session, s1u, s1d, s1vg, s1vo):
    """
    3454-problem-in-limit-check-of-calc-motors-when-scalars-and-arrays-are-mixed-in-positions-dict

    This test is to ensure that positions dict passed to a calc controller
    contains only numpy array in limit check.
    """
    diode = session.config.get("diode")

    with mock.patch.object(
        s1vg.controller, "calc_to_real", wraps=s1vg.controller.calc_to_real
    ) as mocked_calc_to_real:
        scans.dscan(s1vg, 0, 1, 3, 0.1, diode)
        # check first call (= limits check) is done with numpy arrays
        limit_check_call_args = mocked_calc_to_real.call_args_list[0]
        pos_dict = limit_check_call_args[0][0]
        for axis_name in pos_dict:
            position = pos_dict[axis_name]
            # print("axis_name=", axis_name, "pos=", pos_dict[axis_name])
            assert isinstance(position, numpy.ndarray)


def clear_motor_from_config(session, mot, cleared_motor_list=None):
    if cleared_motor_list is None:
        cleared_motor_list = []
    if isinstance(mot.controller, CalcController):
        for real_mot in mot.controller.reals:
            clear_motor_from_config(session, real_mot, cleared_motor_list)
    del session.config._name2instance[mot.name]
    channels.clear_cache(mot, mot.controller)
    mot.controller._Controller__initialized_axis[mot] = False
    cleared_motor_list.append(mot.name)
    return cleared_motor_list


@contextmanager
def motor_from_another_session(session, motor_name):
    mot = session.config.get(motor_name)

    cleared_motors = clear_motor_from_config(session, mot)

    try:
        yield session.config.get(motor_name)
    finally:
        for name in cleared_motors:
            session.config._name2instance[name].__close__()


def test_issue_3789(session, calc_mot2):
    cm2 = calc_mot2
    with motor_from_another_session(session, "calc_mot2") as cm22:
        cm2.settings.disable_cache("_set_position")
        cm2.settings.disable_cache("position")
        cm2.settings.disable_cache("state")

        assert cm2.position == 0
        assert cm22.position == 0

        cm2.move(1)

        assert cm22.position == 1


def test_issue_3822(session, s2vg, s2hg, s2vo, s2ho):
    assert s2vg.controller != s2hg.controller
    # ensure the real axes are all on the same controller, and distinct
    all_reals = s2vg.controller.reals + s2hg.controller.reals
    assert len(set(all_reals)) == len(all_reals)
    real_motor_controllers = [mot.controller for mot in all_reals]
    assert len(set(real_motor_controllers)) == 1
    # finally get the real motors controller
    real_motor_controller = real_motor_controllers.pop()

    s1f = s2hg.controller.reals[0]
    s1b = s2hg.controller.reals[1]
    s1u = s2vg.controller.reals[0]
    s1d = s2vg.controller.reals[1]

    assert s1f.name == "s1f"
    assert s1b.name == "s1b"
    assert s1u.name == "s1u"
    assert s1d.name == "s1d"

    def check_pos_and_set_pos_aligned(*axes):
        for x in axes:
            print(x.name, x.position, x._set_position)
            assert x.position == x._set_position

    with mock.patch.object(
        real_motor_controller, "start_all", wraps=real_motor_controller.start_all
    ) as start_all:

        # check pos and set_pos are aligned before moving
        check_pos_and_set_pos_aligned(s2vg, s2vo, s2hg, s2ho, s1f, s1b, s1u, s1d)

        mv(s2hg, 1, s2vg, 2)

        # check all axes positions are as expected
        s2hg_pos = s2hg.position
        s2vg_pos = s2vg.position
        s2ho_pos = s2ho.position
        s2vo_pos = s2vo.position
        s1f_pos = s1f.position
        s1b_pos = s1b.position
        s1u_pos = s1u.position
        s1d_pos = s1d.position

        assert s2hg_pos == 1
        assert s2vg_pos == 2
        assert s2vo_pos == 0
        assert s2ho_pos == 0

        assert s1f_pos == s2hg_pos / 2.0 + s2ho_pos
        assert s1b_pos == s2hg_pos / 2.0 - s2ho_pos
        assert s1u_pos == s2vg_pos / 2.0 + s2vo_pos
        assert s1d_pos == s2vg_pos / 2.0 - s2vo_pos

        assert s1f_pos == 0.5  # 1/2 + 0
        assert s1b_pos == 0.5  # 1/2 - 0
        assert s1u_pos == 1.0  # 2/2 + 0
        assert s1d_pos == 1.0  # 2/2 + 0

        # check pos and set_pos are aligned after moving
        # for all axes even the complementary pseudos of (s2hg, s2vg) i.e (s2vo, s2ho)
        check_pos_and_set_pos_aligned(s2vg, s2vo, s2hg, s2ho, s1f, s1b, s1u, s1d)

        start_all.assert_called_once()

        start_all_args = start_all.call_args_list[0]
        all_motions_involved = start_all_args.args
        all_real_axes_involved = set(
            [motion.axis.name for motion in all_motions_involved]
        )
        assert set(("s1f", "s1b", "s1u", "s1d")) == all_real_axes_involved

    with mock.patch.object(
        real_motor_controller, "start_all", wraps=real_motor_controller.start_all
    ) as start_all:
        calc_mot = session.config.get("calc_mot_issue_3822")

        assert calc_mot.position == pytest.approx(1.5)
        assert calc_mot.position == (s2vg.position + s2hg.position) / 2.0

        check_pos_and_set_pos_aligned(
            calc_mot, s2vg, s2vo, s2hg, s2ho, s1f, s1b, s1u, s1d
        )
        calc_mot.move(3)

        # we ask to move calc-mot pseudo to 3, so it computes where its reals should go
        # s2vg, s2hg should go to => 3, 3 (but nothing is done yet)
        # then it computes where s1f, s1b, s1u, s1d should go with
        # s2vg=3, s2hg=3 and with current position of s2vo=0 and s2ho=0
        # => s1f = s2hg/2 + s2ho = 3/2 + 0 = 1.5
        # => s1b = s2hg/2 - s2ho = 3/2 - 0 = 1.5
        # => s1u = s2vg/2 + s2vo = 3/2 + 0 = 1.5
        # => s1d = s2vg/2 - s2vo = 3/2 - 0 = 1.5
        # Now it can send the grouped move command to the reals with computed position above.
        # During motion of the hw-reals the pseudos positions are recomputed
        # Once hw-reals have reached the final positions we expect to find the pseudo at
        # s2ho = (s1f - s1b)/2  = (1.5 - 1.5)/2 = 0
        # s2hg = s1b + s1f      = 1.5 + 1.5     = 3
        # s2vo = (s1u - s1d)/2  = (1.5 - 1.5)/2 = 0
        # s2vg = s1u + s1d      = 1.5 + 1.5     = 3
        # calc_mot = (s2hg + s2vg)/2 = (3 + 3) / 2 = 3

        # check all axes positions are as expected
        assert calc_mot.position == 3
        assert s2vg.position == calc_mot.position
        assert s2hg.position == calc_mot.position
        assert s1f.position == s2hg.position / 2.0 + s2ho.position
        assert s1b.position == s2hg.position / 2.0 - s2ho.position
        assert s1u.position == s2vg.position / 2.0 + s2vo.position
        assert s1d.position == s2vg.position / 2.0 - s2vo.position
        assert s2vo.position == (s1u.position - s1d.position) / 2.0
        assert s2ho.position == (s1f.position - s1b.position) / 2.0

        assert s1f.position == 1.5
        assert s1b.position == 1.5
        assert s1u.position == 1.5
        assert s1d.position == 1.5
        assert s2vo.position == 0
        assert s2ho.position == 0
        assert s2vg.position == 3
        assert s2hg.position == 3
        assert calc_mot.position == 3

        # check all reals axes have moved during last move
        assert s1f.position != s1f_pos
        assert s1b.position != s1b_pos
        assert s1u.position != s1u_pos
        assert s1d.position != s1d_pos

        # check pos and set_pos are aligned after moving
        # for all axes even the complementary pseudos of (s2hg, s2vg) i.e (s2vo, s2ho)
        check_pos_and_set_pos_aligned(
            calc_mot, s2vg, s2vo, s2hg, s2ho, s1f, s1b, s1u, s1d
        )

        start_all_args = start_all.call_args_list[0]
        all_motions_involved = start_all_args.args
        all_real_axes_involved = set(
            [motion.axis.name for motion in all_motions_involved]
        )
        assert set(("s1f", "s1b", "s1u", "s1d")) == all_real_axes_involved


def test_pseudo_pos_update_for_same_resulting_reals_positions(default_session):
    """
    Different combinations of (pseudo pos, calc params) can lead to the same position of reals
    In that case, because the reals positions do not change between the 2 situations,
    the update of the pseudos positions is not triggered as usual by the motion of the reals.
    Therefore, a special treatment is done in motor_group._Group.move()
    (see event.send(axis, "internal_position", axis.position) )
    """
    spectro = default_session.config.get("spectro")
    spectro.referential_origin = 0, 0, 0

    # First case (bragg=60, miscut=-5)
    for ana in spectro._active_analysers:
        ana.miscut = -5
    spectro.bragg_axis.move(60)
    print(spectro.__info__())
    assert spectro.is_aligned == (True, True)

    # Jump to second case (bragg=65, miscut=0)
    for ana in spectro._active_analysers:
        ana.miscut = 0

    spectro.bragg_axis.move(65)
    print(spectro.__info__())
    assert spectro.is_aligned == (True, True)
    assert spectro.bragg_axis.position == 65


def test_issue_4198(default_session):

    spectro = default_session.config.get("spectro")
    spectro.freeze("ana_1")

    mv(spectro.bragg_axis, 65)
    assert spectro.bragg_axis.is_moving is False
    assert spectro.energy_axis.is_moving is False

    mv(spectro.bragg_axis, 65)
    assert spectro.bragg_axis.is_moving is False
    assert spectro.energy_axis.is_moving is False


def test_issue_4191(default_session):
    # === MR !6058
    # Issue occurs with this CalcAxis Architecture (3 layers of calc at least on one branch and one moving physical axis in another branch)
    #
    #                CalcAxis(track_undu)
    #                   |            |
    #       CalcAxis(twotheta)    Axis(undu)
    #               |
    #         CalcAxis(theta)
    #               |
    #            Axis(robz)
    #

    track_undu = default_session.config.get("track_undu")  # CalcAxis
    twotheta = default_session.config.get("twotheta")  # CalcAxis
    theta = default_session.config.get("theta")  # CalcAxis
    robz = default_session.config.get("robz")  # Axis
    undu = default_session.config.get("simx")  # Axis

    # go to a position
    mv(track_undu, 8.000)
    assert track_undu.is_moving is False
    assert twotheta.is_moving is False
    assert theta.is_moving is False
    assert robz.is_moving is False
    assert undu.is_moving is False

    # move undu independently to simulate
    # a unprecise positioning after the previous move,
    # like an undulator or a close loop regulated axis
    mvr(undu, 0.1)

    # go to same position again
    mv(track_undu, 8.000)
    assert track_undu.is_moving is False
    assert twotheta.is_moving is False
    assert theta.is_moving is False
    assert robz.is_moving is False
    assert undu.is_moving is False


def test_diamond_calc_issue_4201(default_session):
    # === MR !6082
    # Issue occurs with this CalcAxis Architecture
    # (one axis shared in 2 branches of a CalcAxis tree)
    #
    #                  CalcAxis(c5)
    #                   |        |
    #         CalcAxis(c1)    CalcAxis(c4)
    #          |     |         |        |
    #    Axis(roby) Axis(m0) Axis(roby) Axis(robu)
    #
    #
    c5 = default_session.config.get("coupled_calc5")
    c4 = default_session.config.get("coupled_calc4")
    c1 = default_session.config.get("coupled_calc1")
    roby = default_session.config.get("roby")
    robu = default_session.config.get("robu")
    m0 = default_session.config.get("m0")

    assert [x for x in c5.controller.reals] == [c1, c4]
    assert [x for x in c4.controller.reals] == [roby, robu]
    assert [x for x in c1.controller.reals] == [roby, m0]

    assert c5.controller.factor == 1
    assert c4.controller.factor == 1
    assert c1.controller.factor == 1

    # check that if both motion of roby are the same
    # duplicated motion is filtered silently
    pos = 1
    group, _ = _move({c5: pos})
    md = group._group_move._motions_dict
    ctrl_motions = md[roby.controller]
    assert [m.axis for m in ctrl_motions] == [roby, m0, robu]
    assert ctrl_motions[0].axis is roby
    assert ctrl_motions[0].user_target_pos == pos

    # check that if both motion of roby are different
    # a RuntimeError is raised
    c1.controller.factor = (
        2  # so c4 will ask to move roby at a different position than c1
    )
    pos = 2
    with pytest.raises(RuntimeError) as exc:
        group, _ = _move({c5: pos})
    assert "Found different motions for same axis roby" in str(exc)
