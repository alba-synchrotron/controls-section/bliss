import pytest
import numpy
from bliss.config.static import ObjectCreationFailed
from .conftest import motor_fixture
from bliss.common.motor_group import Group
from bliss.common.scans import dscan

xy_robx = motor_fixture("xy_robx")
xy_roby = motor_fixture("xy_roby")
sampx = motor_fixture("sampx")
sampy = motor_fixture("sampy")
sampx2 = motor_fixture("sampx2")
sampy2 = motor_fixture("sampy2")
sampx3 = motor_fixture("sampx3")
sampy3 = motor_fixture("sampy3")


def test_setup(xy_robx, xy_roby, sampx, sampy, omega):
    """Calc motors will have the same units as real motors"""
    assert xy_roby.unit == xy_robx.unit
    assert sampx.unit == xy_robx.unit
    assert sampy.unit == xy_robx.unit


def test_rot_to_calc(xy_robx, xy_roby, sampx, sampy, omega):
    xy_robx.move(1)
    xy_roby.move(0)
    omega.move(0)
    assert (sampx.position, sampy.position) == pytest.approx((1, 0), abs=0.01)
    omega.move(90)
    assert (sampx.position, sampy.position) == pytest.approx((0, 1), abs=0.01)
    omega.move(180)
    assert (sampx.position, sampy.position) == pytest.approx((-1, 0), abs=0.01)
    omega.move(270)
    assert (sampx.position, sampy.position) == pytest.approx((0, -1), abs=0.01)


def test_real_to_calc(xy_robx, xy_roby, sampx, sampy, omega):
    xy_robx.move(1)
    xy_roby.move(0)
    omega.move(90)
    assert (sampx.position, sampy.position) == pytest.approx((0, 1), abs=0.01)
    xy_robx.move(2)
    assert (sampx.position, sampy.position) == pytest.approx((0, 2), abs=0.01)


def test_calc_to_real_bliss_1_10(xy_robx, xy_roby, sampx3, sampy3, omega):
    """Some beamline config could use `real` instead of `real_param`"""
    omega.move(90)
    sampx3.move(0)
    sampy3.move(1)
    assert (xy_robx.position, xy_roby.position) == pytest.approx((1, 0), abs=0.01)
    sampy3.move(2)
    assert (xy_robx.position, xy_roby.position, sampx3.position) == pytest.approx(
        (2, 0, 0), abs=0.01
    )
    sampy3.move(1)
    assert (xy_robx.position, xy_roby.position, sampx3.position) == pytest.approx(
        (1, 0, 0), abs=0.01
    )


def test_calc_to_real(xy_robx, xy_roby, sampx, sampy, omega):
    omega.move(90)
    sampx.move(0)
    sampy.move(1)
    assert (xy_robx.position, xy_roby.position) == pytest.approx((1, 0), abs=0.01)
    sampy.move(2)
    assert (xy_robx.position, xy_roby.position, sampx.position) == pytest.approx(
        (2, 0, 0), abs=0.01
    )
    sampy.move(1)
    assert (xy_robx.position, xy_roby.position, sampx.position) == pytest.approx(
        (1, 0, 0), abs=0.01
    )


def test_calc_to_real_45deg(xy_robx, xy_roby, sampx, sampy, omega):
    """Test a rotation angle impacting the whole real motors"""
    omega.move(45)
    sampx.move(0)
    sampy.move(0)
    assert (xy_robx.position, xy_roby.position) == pytest.approx((0, 0), abs=0.01)
    sampx.move(0.707)
    sampy.move(0.707)
    assert (xy_robx.position, xy_roby.position) == pytest.approx((1, 0), abs=0.01)
    # Moving sampy do not move sampx
    sampy.move(0)
    assert (sampx.position,) == pytest.approx((0.707,), abs=0.01)


def test_rot_on_dial(xy_robx, xy_roby, sampx2, sampy2, omega):
    """Test xyonrotation with a ref on the rotation"""
    omega.move(45)
    omega.position = 0

    omega.move(45)
    sampx2.move(1)
    sampy2.move(1)
    assert (xy_robx.position, xy_roby.position) == pytest.approx((1, -1), abs=0.01)
    omega.rmove(45)
    older = sampx2.position
    assert (xy_robx.position, xy_roby.position) == pytest.approx((1, -1), abs=0.01)
    assert (sampx2.position, sampy2.position) == pytest.approx((0, 1.4142), abs=0.01)
    # Moving sampy do not move sampx
    sampy2.move(0)
    assert (sampx2.position,) == pytest.approx((older,), abs=0.01)


def test_wrong_units(default_session):
    """Real motors must have the same units"""
    config = default_session.config
    config.get_config("xy_roby")["unit"] = "um"
    with pytest.raises(ObjectCreationFailed):
        config.get("calc_xyrotation")


@pytest.fixture
def omega_in_rad(default_session):
    config = default_session.config
    config.get_config("omega")["unit"] = "rad"
    yield


def test_rotation_in_rad(omega_in_rad, xy_robx, xy_roby, sampx, sampy, omega):
    """Move rotation at pi/2 rad

    Check a vector position
    """
    xy_robx.move(1)
    xy_roby.move(0)
    omega.move(numpy.pi * 0.5)
    assert (sampx.position, sampy.position) == pytest.approx((0, 1), abs=0.01)


def test_move_real_param_and_virtual(xy_robx, xy_roby, sampx, sampy, omega):
    """Test to move real param and virtual at the same time

    The result is the same as moving first the real param, then the virtual
    """
    if False:
        # The move have to be the same as this sequence
        omega.move(45)
        sampx.move(0.707)
        sampy.move(0.707)
        result = (xy_robx.position, xy_roby.position)
        omega.move(0)
        sampx.move(0)
        sampy.move(0)
        print(result)
    else:
        result = (1, 0)

    g = Group(omega, sampx, sampy)
    g.move({omega: 45, sampx: 0.707, sampy: 0.707})

    assert (xy_robx.position, xy_roby.position) == pytest.approx(result, abs=0.01)


def test_move_real_param_and_real(xy_robx, xy_roby, sampx, sampy, omega):
    """Test to move real param and real at the same time"""
    if False:
        # The move have to be the same as this sequence
        omega.move(45)
        xy_robx.move(0.707)
        xy_roby.move(0.707)
        result = (sampx.position, sampy.position)
        omega.move(0)
        xy_robx.move(0)
        xy_roby.move(0)
        print(result)
    else:
        result = (0, 1)

    g = Group(omega, xy_robx, xy_roby)
    g.move({omega: 45, xy_robx: 0.707, xy_roby: 0.707})

    assert (sampx.position, sampy.position) == pytest.approx(result, abs=0.01)


def test_inconsistent_move(xy_robx, sampx):
    """Check that inconsistent motors can't be moved at the same time."""
    with pytest.raises(RuntimeError) as exc_info:
        Group(xy_robx, sampx)

    assert "sampx" in exc_info.value.args[0]
    assert "xy_robx" in exc_info.value.args[0]


def test_xyrot_scanning(default_session, xy_robx, xy_roby, sampx, sampy, omega):
    diode = default_session.config.get("diode")
    dscan(sampx, -0.1, 0.1, 2, 0.01, diode)
    dscan(xy_robx, -0.1, 0.1, 2, 0.01, diode)
    dscan(omega, -0.1, 0.1, 2, 0.01, diode)
