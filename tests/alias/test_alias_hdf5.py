# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import h5py

from bliss import global_map
from bliss.common import scans
from bliss.scanning.scan import Scan
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.acquisition.motor import SoftwarePositionTriggerMaster
from bliss.scanning.acquisition.counter import SamplingCounterAcquisitionSlave


def test_alias_hdf5_file_items(alias_session):
    env_dict = alias_session.env_dict

    scan = scans.a3scan(
        env_dict["robyy"],
        0,
        5,
        env_dict["robzz"],
        0,
        5,
        env_dict["m1"],
        0,
        5,
        5,
        0.001,
        env_dict["simu1"],
        env_dict["dtime"],
        env_dict["myroi"],
        env_dict["lima_simulator"],
        env_dict["myroi3"],
        save=True,
        return_scan=True,
    )

    measurement = [
        "elapsed_time",
        "epoch",
        "lima_simulator_myroi",
        "lima_simulator_myroi3",
        "lima_simulator_r1_avg",
        "lima_simulator_r1_max",
        "lima_simulator_r1_min",
        "lima_simulator_r1_std",
        "lima_simulator_r2",
        "lima_simulator_r2_avg",
        "lima_simulator_r2_max",
        "lima_simulator_r2_min",
        "lima_simulator_r2_std",
        "lima_simulator_r3_avg",
        "lima_simulator_r3_max",
        "lima_simulator_r3_min",
        "lima_simulator_r3_std",
        "m1",
        "robenc",
        "robyy",
        "robzz",
        "simu1_det0",
        "simu1_det0_event_count_rate",
        "simu1_det0_events",
        "simu1_det0_trigger_count_rate",
        "simu1_det0_trigger_live_time",
        "simu1_det0_triggers",
        "simu1_det1",
        "simu1_det1_elapsed_time",
        "simu1_det1_event_count_rate",
        "simu1_det1_events",
        "simu1_det1_live_time",
        "simu1_det1_trigger_count_rate",
        "simu1_det1_trigger_live_time",
        "simu1_det1_triggers",
        "simu1_det2",
        "simu1_det2_elapsed_time",
        "simu1_det2_event_count_rate",
        "simu1_det2_events",
        "simu1_det2_live_time",
        "simu1_det2_trigger_count_rate",
        "simu1_det2_trigger_live_time",
        "simu1_det2_triggers",
        "simu1_det3",
        "simu1_det3_elapsed_time",
        "simu1_det3_event_count_rate",
        "simu1_det3_events",
        "simu1_det3_fractional_dead_time",
        "simu1_det3_live_time",
        "simu1_det3_trigger_count_rate",
        "simu1_det3_trigger_live_time",
        "simu1_det3_triggers",
        "simu1_sum_dtime",
        "simu1_sum_dtime1",
        "simu1_sum_dtime2",
        "simu1_sum_ltime",
        "simu1_sum_rtime",
    ]
    positioners = ["m1", "m2", "mot0", "robu", "robyy", "robzz"]
    instrument = [
        "elapsed_time",
        "epoch",
        "lima_simulator",
        "lima_simulator_",
        "lima_simulator_r1",
        "lima_simulator_r2",
        "lima_simulator_r3",
        "m1",
        "name",
        "positioners",
        "positioners_dial_end",
        "positioners_dial_start",
        "positioners_end",
        "positioners_start",
        "robenc",
        "robyy",
        "robzz",
        "simu1_det0",
        "simu1_det1",
        "simu1_det2",
        "simu1_det3",
        "simu1_sum",
    ]
    _assert_aliases(scan.writer.get_filename(), measurement, positioners, instrument)


def test_alias_hdf5_continuous_scan(alias_session):
    env_dict = alias_session.env_dict

    diode = alias_session.config.get("diode")
    global_map.aliases.add("myDiode", diode)

    robyy = env_dict["robyy"]
    counter = env_dict["myDiode"]
    master = SoftwarePositionTriggerMaster(robyy, 0, 1, 10, time=1)
    acq_dev = SamplingCounterAcquisitionSlave(counter, count_time=0.01, npoints=10)
    chain = AcquisitionChain()
    chain.add(master, acq_dev)

    scan = Scan(chain)
    scan.run()

    measurement = ["myDiode", "robyy"]
    positioners = ["m1", "m2", "mot0", "robu", "robyy", "robzz"]
    instrument = [
        "myDiode",
        "name",
        "positioners",
        "positioners_dial_end",
        "positioners_dial_start",
        "positioners_end",
        "positioners_start",
        "robyy",
    ]
    _assert_aliases(scan.writer.get_filename(), measurement, positioners, instrument)


def _assert_aliases(filename, emeasurement, epositioners, einstrument):
    with h5py.File(filename, mode="r") as f:
        measurement = list(f["1.1/measurement"])
        positioners = list(f["1.1/instrument/positioners"])
        instrument = list(f["1.1/instrument"])

    assert set(measurement) == set(emeasurement)
    assert set(positioners) == set(epositioners)
    assert set(instrument) == set(einstrument)
