# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import pytest
from silx.io.h5py_utils import top_level_names

from bliss.common import scans
from bliss.scanning.scan import Scan


def test_unsaved_scan_number(session):
    Scan.UNSAVED_SCAN_NUMBER = 0
    diode = session.env_dict["diode"]

    scan = scans.loopscan(1, 0.001, diode, save=False)
    assert scan.scan_info["scan_nb"] == 1

    scan = scans.loopscan(1, 0.001, diode, save=False)
    assert scan.scan_info["scan_nb"] == 2

    scan = scans.loopscan(1, 0.001, diode, save=True)
    assert scan.scan_info["scan_nb"] == 1

    scan = scans.loopscan(1, 0.001, diode, save=False)
    assert scan.scan_info["scan_nb"] == 3

    scan = scans.loopscan(1, 0.001, diode, save=True)
    assert scan.scan_info["scan_nb"] == 2


def test_scan_number_esrf_policy(session, nexus_writer_service, esrf_data_policy):
    diode = session.env_dict["diode"]
    scan_saving = session.scan_saving

    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 1
    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 2

    scan_saving.newdataset("new_dataset")

    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 1
    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 2

    scan_saving.newcollection("new_collection")

    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 1
    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 2

    scan_saving.newproposal("new_proposal")

    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 1
    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 2


def test_scan_number_no_policy(session, nexus_writer_service):
    diode = session.env_dict["diode"]
    scan_saving = session.scan_saving

    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 1
    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 2

    scan_saving.base_path += "_2"

    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 1
    scan = scans.loopscan(1, 0.01, diode, save=True)
    assert scan.scan_info["scan_nb"] == 2


@pytest.mark.parametrize("writer", ["hdf5", "nexus", "null"])
def test_writer_scan_number(writer, session, nexus_writer_service):
    diode = session.env_dict["diode"]
    scan_saving = session.scan_saving
    scan_saving.writer = writer
    if scan_saving.filename:
        try:
            os.remove(scan_saving.filename)
        except FileNotFoundError:
            pass

    scan = scans.loopscan(1, 0.1, diode, save=True)
    assert scan.scan_info["scan_nb"] == 1

    scan = scans.loopscan(1, 0.1, diode, save=False)

    scan = scans.loopscan(1, 0.1, diode, save=True)
    assert scan.scan_info["scan_nb"] == 2

    scan = scans.loopscan(1, 0.1, diode, save=False)

    if writer == "null":
        assert scan_saving.filename is None
    else:
        assert top_level_names(scan_saving.filename) == ["1.1", "2.1"]
