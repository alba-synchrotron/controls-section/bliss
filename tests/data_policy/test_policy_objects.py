from bliss.common.standard import loopscan
from bliss.common.data_store import get_default_data_store

from . import icat_test_utils


def test_data_policy_objects(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    # Prepare for scanning without the Nexus writer
    diode = session.env_dict["diode"]
    scan_saving.writer = "hdf5"

    proposal = scan_saving.proposal
    collection = scan_saving.collection
    dataset = scan_saving.dataset

    assert str(proposal) == scan_saving.proposal_name
    assert proposal.name == scan_saving.proposal_name
    assert len(list(proposal.children)) == 1

    assert str(collection) == scan_saving.collection_name
    assert collection.proposal.name == scan_saving.proposal_name
    assert collection.name == scan_saving.collection_name
    assert len(list(collection.children)) == 1
    assert collection.proposal.name == proposal.name

    assert str(dataset) == scan_saving.dataset_name
    assert dataset.proposal.name == scan_saving.proposal_name
    assert dataset.collection.name == scan_saving.collection_name
    assert dataset.name == scan_saving.dataset_name
    assert not dataset.has_scans
    assert len(list(dataset.children)) == 0
    assert dataset.proposal.name == proposal.name
    assert dataset.collection.name == collection.name

    loopscan(3, 0.01, diode)

    assert dataset.has_scans
    assert len(list(dataset.scans)) == 1

    assert dataset.path.startswith(proposal.path)
    assert dataset.path.startswith(collection.path)
    assert collection.path.startswith(proposal.path)
    assert len(dataset.path) > len(proposal.path)
    assert len(dataset.path) > len(collection.path)
    assert len(collection.path) > len(proposal.path)


def test_dataset_object(session, esrf_data_policy, icat_mock_client):
    data_store = get_default_data_store()
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    # Prepare for scanning without the Nexus writer
    diode = session.env_dict["diode"]
    scan_saving.writer = "hdf5"

    # First dataset
    s = loopscan(3, 0.01, diode)

    dataset = scan_saving.dataset
    assert dataset.has_scans
    assert dataset.has_data
    assert [s.key for s in dataset.scans] == [s._scan_data.key]
    assert not dataset.is_closed

    # Second dataset
    info = icat_test_utils.dataset_info(scan_saving)
    scan_saving.dataset_name = None
    assert dataset.is_closed
    assert scan_saving._dataset_object is None
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    s = loopscan(3, 0.01, diode)
    assert scan_saving.dataset.name != dataset.name

    # Third dataset
    info = icat_test_utils.dataset_info(scan_saving)
    scan_saving.dataset_name = None
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    # Second dataset not in Redis yet
    _, keys = data_store.search_existing_scans(session=session.name)
    dataset_names = {data_store.load_scan(key).dataset for key in keys}
    assert len(dataset_names) == 2

    s = loopscan(3, 0.01, diode, save=False)

    # Third dataset in Redis
    _, keys = data_store.search_existing_scans(session=session.name)
    dataset_names = {data_store.load_scan(key).dataset for key in keys}
    assert len(dataset_names) == 3

    # Third dataset object does not exist yet
    assert scan_saving._dataset_object is None

    # Third dataset created upon using it
    dataset = scan_saving.dataset
    assert not dataset.is_closed
    assert dataset.has_scans
    assert not dataset.has_data
    assert [s.key for s in dataset.scans] == [s._scan_data.key]

    # Does not go to a new dataset (because the current one has no data)
    scan_saving.dataset_name = None

    # Still in third dataset
    assert scan_saving.dataset.name == dataset.name
    assert not dataset.is_closed
    assert dataset.has_scans
    assert not dataset.has_data
    assert [s.key for s in dataset.scans] == [s._scan_data.key]
    assert icat_mock_client.return_value.store_dataset.call_count == 0

    _, keys = data_store.search_existing_scans(session=session.name)
    dataset_names = {data_store.load_scan(key).dataset for key in keys}
    assert len(dataset_names) == 3
