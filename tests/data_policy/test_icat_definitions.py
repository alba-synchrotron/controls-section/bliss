from bliss.icat.metadata import icat_metadata_from_device
from pyicat_plus.metadata.definitions import load_icat_fields


def test_metadata_from_device():
    icat_fields = load_icat_fields()

    icat_expected = {
        "InstrumentSlitPrimary_name": "ps",
        "InstrumentSlitPrimary_vertical_gap": 1,
    }

    from_device = {"instrument": {"primary_slit": {"name": "ps", "vertical_gap": 1}}}
    icat_metadata = icat_metadata_from_device(icat_fields, from_device)
    assert icat_expected == icat_metadata

    from_device = {"name": "ps", "vertical_gap": 1}
    group = icat_fields["instrument", "primary_slit"]
    icat_metadata = icat_metadata_from_device(group, from_device)
    assert icat_expected == icat_metadata
