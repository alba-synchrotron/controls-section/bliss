import pytest

from bliss.common.standard import loopscan
from bliss.scanning.scan import Scan
from bliss.scanning.scan_saving import ScanSaving

from . import icat_test_utils

DEFAULT_METADATA = {
    "InstrumentVariables_name": ["roby", "robz"],
    "InstrumentVariables_value": [0.0, 0.0],
    "InstrumentDetector01_name": "diode1",
    "InstrumentDetector02_name": "diode2",
    "InstrumentSlitPrimary_name": "primary_slit",
    "InstrumentSlitPrimary_vertical_offset": 0.0,
    "InstrumentSlitPrimary_horizontal_offset": 0.0,
    "InstrumentSlitPrimary_horizontal_gap": 0.0,
    "InstrumentSlitPrimary_vertical_gap": 0.0,
    "InstrumentInsertionDevice_gap_value": [0.0, 0.0],
    "InstrumentInsertionDevice_gap_name": ["roby", "robz"],
    "SamplePositioners_name": "roby",
    "SamplePositioners_value": 0.0,
    "Sample_name": "sample",
}


def test_icat_metadata(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    # Prepare for scanning without the Nexus writer
    scan_saving.writer = "hdf5"
    diode = session.env_dict["diode"]
    att1 = session.env_dict["att1"]
    att1.Al200()

    expected = dict(DEFAULT_METADATA)
    expected["InstrumentAttenuator01_status"] = "in"
    expected["InstrumentAttenuator01_thickness"] = 200
    expected["InstrumentAttenuator01Positioners_value"] = 2.5
    expected["InstrumentAttenuator01Positioners_name"] = "att1z"
    expected["InstrumentAttenuator01_type"] = "Al"

    loopscan(3, 0.01, diode)

    # Check metadata gathering
    icatfields = dict(scan_saving.dataset.get_current_icat_metadata())
    info = icat_test_utils.dataset_info(scan_saving)
    info["metadata"] = dict(expected)
    info["metadata"]["startDate"] = icatfields.pop("startDate")
    info["metadata"]["endDate"] = None
    # no endDate because dataset is not closed yet
    assert icatfields == expected

    scan_saving.dataset_name = None

    # test reception of metadata on icat server side
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)

    loopscan(3, 0.01, diode)
    # Check metadata gathering
    icatfields = dict(scan_saving.dataset.get_current_icat_metadata())
    icatfields.pop("startDate")
    assert icatfields == expected

    # Check metadata after dataset closing
    dataset = scan_saving.dataset
    scan_saving.enddataset()
    icatfields = dict(dataset.get_current_icat_metadata())
    icatfields.pop("startDate")
    icatfields.pop("endDate")
    expected["definition"] = "UNKNOWN"
    assert icatfields == expected

    # check registered datasets
    datasets = [dset for dset in scan_saving.collection.children]
    assert len(datasets) == 2, datasets


def test_icat_metadata_custom(session, esrf_data_policy, icat_mock_client):
    scan_saving = session.scan_saving
    icat_test_utils.assert_initial_proposal(icat_mock_client, scan_saving)

    # Prepare for scanning without the Nexus writer
    diode = session.env_dict["diode"]
    scan_saving.writer = "hdf5"

    # do a scan in the 'normal' dataset
    loopscan(2, 0.1, diode)

    # create custom dataset
    scan_saving = ScanSaving("my_custom_scansaving")
    scan_saving.writer = "hdf5"
    ds_name = session.scan_saving.dataset_name
    ds_name += "_b"
    scan_saving.dataset_name = ds_name

    # scan in custom dataset with custom metadata fields
    # set before and after the scan
    scan_saving.dataset.add_techniques("FLUO")
    ls = loopscan(3, 0.1, diode, run=False)
    s = Scan(ls.acq_chain, scan_saving=scan_saving)
    scan_saving.dataset.write_metadata_field("FLUO_i0", 17.1)
    s.run()
    scan_saving.dataset["FLUO_it"] = 18.2

    info = icat_test_utils.dataset_info(scan_saving)
    info["metadata"] = dict(DEFAULT_METADATA)
    info["metadata"]["startDate"] = None
    info["metadata"]["endDate"] = None
    info["metadata"]["FLUO_i0"] = 17.1
    info["metadata"]["FLUO_it"] = 18.2

    # close the custom dataset
    scan_saving.enddataset()

    # check whether all metadata is sent to ICAT
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    # coming back to a previous dataset is not allowed
    with pytest.raises(RuntimeError) as excinfo:
        loopscan(3, 0.1, diode)
    assert "Scan 1.1 already exists" in str(excinfo)

    info = icat_test_utils.dataset_info(session.scan_saving)
    info["metadata"] = dict(DEFAULT_METADATA)
    info["metadata"]["startDate"] = None
    info["metadata"]["endDate"] = None

    # close the 'normal' dataset
    session.scan_saving.enddataset()

    # check whether all metadata is sent to ICAT
    icat_test_utils.assert_dataset_icat(icat_mock_client, **info)
    icat_mock_client.reset_mock()

    # see if things in redis are correct
    datasets = []
    for dataset in scan_saving.collection.children:
        if dataset.is_registered:
            datasets.append(dataset)
    assert all(d.is_closed for d in datasets)
    datasets = {d.name: d for d in datasets}
    assert datasets.keys() == {"0001", "0001_b"}

    metadata_0001 = datasets["0001"].get_current_icat_metadata()
    assert len(metadata_0001) == 17, metadata_0001.keys()
    metadata_0001b = datasets["0001_b"].get_current_icat_metadata()
    assert len(metadata_0001b) == 19, metadata_0001b.keys()  # + FLUO_i0, FLUO_it

    assert "startDate" in metadata_0001
    assert "startDate" in metadata_0001b
    assert "endDate" in metadata_0001
    assert "endDate" in metadata_0001b
    assert "FLUO_i0" in metadata_0001b
    assert metadata_0001["Sample_name"] == metadata_0001b["Sample_name"]
    assert metadata_0001b["definition"] == "FLUO"


def test_icat_metadata_namespaces(session, esrf_data_policy):
    scan_saving = session.scan_saving

    # Prepare for scanning without the Nexus writer
    diode = session.env_dict["diode"]
    scan_saving.writer = "hdf5"

    scan_saving.newdataset("toto")

    existing = {x for x in dir(scan_saving.dataset.existing) if not x.startswith("_")}
    assert scan_saving.dataset.get_current_icat_metadata_fields() == existing

    scan_saving.dataset.add_techniques("FLUO")

    actual = {x for x in dir(scan_saving.dataset.expected) if not x.startswith("_")}
    expected = {"Sample_name", "Sample_description"}
    expected.update(
        field.field_name
        for field in scan_saving.dataset._icat_fields["FLUO"].iter_fields()
    )
    assert actual == expected

    # check that the expected keys do not move into existing
    existing = {x for x in dir(scan_saving.dataset.existing) if not x.startswith("_")}
    assert scan_saving.dataset.get_current_icat_metadata_fields() == existing

    loopscan(1, 0.1, diode)
    scan_saving.newdataset("toto1")

    # create a new dataset and see that the old technique is gone
    scan_saving.dataset.add_techniques("EM")
    actual = {x for x in dir(scan_saving.dataset.expected) if not x.startswith("_")}

    expected = {"Sample_name", "Sample_description"}
    expected.update(
        field.field_name
        for field in scan_saving.dataset._icat_fields["EM"].iter_fields()
    )
    assert actual == expected

    # add a key through .expected and see if it pops up in existing
    scan_saving.dataset.expected.EM_images_count = 24
    assert "EM_images_count" in dir(scan_saving.dataset.existing)
    assert scan_saving.dataset.existing.EM_images_count == 24
    assert scan_saving.dataset.metadata.EM.images_count == 24

    # see if setting a value to None removes it from existing
    scan_saving.dataset.existing.EM_images_count = None
    assert "EM_images_count" not in dir(scan_saving.dataset.existing)
    assert scan_saving.dataset.metadata.EM.images_count is None


def test_icat_metadata_inheritance(session, esrf_data_policy):
    scan_saving = session.scan_saving
    assert "Sample_name" in scan_saving.dataset.expected_fields
    assert "Sample_name" in scan_saving.dataset.existing_fields
    assert "Sample_description" in scan_saving.dataset.expected_fields
    assert "Sample_description" not in scan_saving.dataset.existing_fields
    assert scan_saving.collection.expected_fields.issubset(
        scan_saving.dataset.expected_fields
    )
    assert scan_saving.collection.existing_fields.issubset(
        scan_saving.dataset.existing_fields
    )
    assert scan_saving.collection["Sample_name"] == scan_saving.collection_name
    assert scan_saving.dataset["Sample_name"] == scan_saving.collection_name

    scan_saving.dataset["Sample_name"] += "_suffix"
    assert scan_saving.collection["Sample_name"] == scan_saving.collection_name
    assert scan_saving.dataset["Sample_name"] == scan_saving.collection_name + "_suffix"
    assert scan_saving.collection.sample_description is None
    assert scan_saving.dataset.description is None
    assert not scan_saving.collection.metadata_is_complete
    assert not scan_saving.dataset.metadata_is_complete

    scan_saving.collection.sample_description = "sample description"
    assert scan_saving.collection.sample_description == "sample description"
    assert scan_saving.dataset.description == "sample description"
    assert scan_saving.collection.metadata_is_complete
    assert scan_saving.dataset.metadata_is_complete

    scan_saving.dataset.description = "dataset description"
    assert scan_saving.collection.sample_description == "sample description"
    assert scan_saving.dataset.description == "sample description (dataset description)"
    assert scan_saving.collection.metadata_is_complete
    assert scan_saving.dataset.metadata_is_complete

    icat_test_utils.create_dataset(scan_saving)
    scan_saving.newdataset(None)

    assert scan_saving.collection["Sample_name"] == scan_saving.collection_name
    assert scan_saving.dataset["Sample_name"] == scan_saving.collection_name
    assert scan_saving.collection.sample_description == "sample description"
    assert scan_saving.dataset.description == "sample description"
    assert scan_saving.collection.metadata_is_complete
    assert scan_saving.dataset.metadata_is_complete

    scan_saving.newcollection("toto")

    assert scan_saving.collection["Sample_name"] == "toto"
    assert scan_saving.dataset["Sample_name"] == "toto"
    assert scan_saving.collection.sample_description is None
    assert scan_saving.dataset.description is None
    assert not scan_saving.collection.metadata_is_complete
    assert not scan_saving.dataset.metadata_is_complete

    scan_saving.dataset.description = "dataset description"
    assert scan_saving.collection.sample_description is None
    assert scan_saving.dataset.description == "dataset description"
    assert not scan_saving.collection.metadata_is_complete
    assert scan_saving.dataset.metadata_is_complete

    scan_saving.collection.sample_description = "sample description"
    assert scan_saving.collection.sample_description == "sample description"
    assert scan_saving.dataset.description == "dataset description"
    assert scan_saving.collection.metadata_is_complete
    assert scan_saving.dataset.metadata_is_complete

    scan_saving.dataset.description = "dataset description"
    assert scan_saving.collection.sample_description == "sample description"
    assert scan_saving.dataset.description == "sample description (dataset description)"
    assert scan_saving.collection.metadata_is_complete
    assert scan_saving.dataset.metadata_is_complete


def test_icat_metadata_freezing(session, esrf_data_policy):
    scan_saving = session.scan_saving

    mdatafield = "Sample_name"
    scan_saving.collection[mdatafield] = "value1"
    assert scan_saving.dataset[mdatafield] == "value1"
    scan_saving.collection[mdatafield] = "value2"
    assert scan_saving.dataset[mdatafield] == "value2"
    scan_saving.dataset.freeze_inherited_icat_metadata()
    scan_saving.collection[mdatafield] = "value3"
    assert scan_saving.dataset[mdatafield] == "value2"
    scan_saving.dataset.unfreeze_inherited_icat_metadata()
    assert scan_saving.dataset[mdatafield] == "value2"
    scan_saving.dataset[mdatafield] = None
    assert scan_saving.dataset[mdatafield] == "value3"


def test_icat_metadata_disable(session, esrf_data_policy):
    att1 = session.env_dict["att1"]
    att1.Al200()

    expected = {
        "InstrumentVariables_name": ["roby", "robz"],
        "InstrumentVariables_value": [0.0, 0.0],
        "InstrumentDetector01_name": "diode1",
        "InstrumentDetector02_name": "diode2",
        "InstrumentSlitPrimary_name": "primary_slit",
        "InstrumentSlitPrimary_vertical_offset": 0.0,
        "InstrumentSlitPrimary_horizontal_offset": 0.0,
        "InstrumentSlitPrimary_horizontal_gap": 0.0,
        "InstrumentSlitPrimary_vertical_gap": 0.0,
        "InstrumentAttenuator01_status": "in",
        "InstrumentAttenuator01_thickness": 200,
        "InstrumentAttenuator01Positioners_value": 2.5,
        "InstrumentAttenuator01Positioners_name": "att1z",
        "InstrumentAttenuator01_type": "Al",
        "InstrumentInsertionDevice_gap_value": [0.0, 0.0],
        "InstrumentInsertionDevice_gap_name": ["roby", "robz"],
        "SamplePositioners_name": "roby",
        "SamplePositioners_value": 0.0,
    }
    icatfields = dict(session.icat_metadata.get_metadata())
    assert icatfields == expected

    att1.disable_dataset_metadata()
    try:
        expected = {
            "InstrumentVariables_name": ["roby", "robz"],
            "InstrumentVariables_value": [0.0, 0.0],
            "InstrumentDetector01_name": "diode1",
            "InstrumentDetector02_name": "diode2",
            "InstrumentSlitPrimary_name": "primary_slit",
            "InstrumentSlitPrimary_vertical_offset": 0.0,
            "InstrumentSlitPrimary_horizontal_offset": 0.0,
            "InstrumentSlitPrimary_horizontal_gap": 0.0,
            "InstrumentSlitPrimary_vertical_gap": 0.0,
            "InstrumentInsertionDevice_gap_value": [0.0, 0.0],
            "InstrumentInsertionDevice_gap_name": ["roby", "robz"],
            "SamplePositioners_name": "roby",
            "SamplePositioners_value": 0.0,
        }
        icatfields = dict(session.icat_metadata.get_metadata())
        assert icatfields == expected
    finally:
        att1.enable_dataset_metadata()


def test_icat_metadata_technique(session, esrf_data_policy):
    att1 = session.env_dict["att1"]
    att1.Al200()

    expected = {
        "InstrumentVariables_name": ["roby", "robz"],
        "InstrumentVariables_value": [0.0, 0.0],
        "InstrumentDetector01_name": "diode1",
        "InstrumentDetector02_name": "diode2",
        "InstrumentSlitPrimary_name": "primary_slit",
        "InstrumentSlitPrimary_vertical_offset": 0.0,
        "InstrumentSlitPrimary_horizontal_offset": 0.0,
        "InstrumentSlitPrimary_horizontal_gap": 0.0,
        "InstrumentSlitPrimary_vertical_gap": 0.0,
        "InstrumentAttenuator01_status": "in",
        "InstrumentAttenuator01_thickness": 200,
        "InstrumentAttenuator01Positioners_value": 2.5,
        "InstrumentAttenuator01Positioners_name": "att1z",
        "InstrumentAttenuator01_type": "Al",
        "InstrumentInsertionDevice_gap_value": [0.0, 0.0],
        "InstrumentInsertionDevice_gap_name": ["roby", "robz"],
        "SamplePositioners_name": "roby",
        "SamplePositioners_value": 0.0,
    }
    icatfields = dict(session.icat_metadata.get_metadata())
    assert icatfields == expected

    dataset = session.scan_saving.dataset

    dataset.gather_metadata(on_exists="overwrite")
    icatfields = dataset.get_current_icat_metadata()
    icatfields.pop("Sample_name")
    icatfields.pop("startDate")
    assert icatfields == expected

    expected["InstrumentDetector03_name"] = "diode3"

    icatfields = dict(session.icat_metadata.get_metadata(techniques=["exafs"]))
    assert icatfields == expected

    session.scan_saving.dataset.gather_metadata(on_exists="overwrite")
    icatfields = dataset.get_current_icat_metadata()
    icatfields.pop("Sample_name")
    icatfields.pop("startDate")
    assert icatfields != expected

    session.scan_saving.dataset.add_techniques("exafs")
    assert session.scan_saving.dataset.existing.definition == "EXAFS"
    expected["definition"] = "EXAFS"

    session.scan_saving.dataset.gather_metadata(on_exists="overwrite")
    icatfields = dataset.get_current_icat_metadata()
    icatfields.pop("Sample_name")
    icatfields.pop("startDate")
    assert icatfields == expected
