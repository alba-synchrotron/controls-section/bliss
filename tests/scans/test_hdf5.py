# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import h5py

from silx.io.utils import h5py_read_dataset

from bliss import global_map, current_session
from bliss.common import scans
from bliss.scanning.scan import Scan
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.acquisition import timer
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster


def test_hdf5_metadata(session):
    all_motors = dict(
        [(axis.name, axis.position) for axis in global_map.get_positioners_iter()]
    )

    roby = session.config.get("roby")
    diode = session.config.get("diode")

    scan = scans.ascan(roby, 0, 10, 10, 0.01, diode, save=True, return_scan=True)
    assert scan is current_session.scans[-1]

    iso_start_time = scan.scan_info["start_time"]

    with h5py.File(scan.writer.get_filename()) as h5file:
        dataset = h5file["/1.1"]
        assert (
            h5py_read_dataset(dataset["title"])
            == "ascan roby 0.000000 10.000000 10 0.01"
        )
        assert h5py_read_dataset(dataset["start_time"]).startswith(iso_start_time)
        assert dataset["measurement"]
        assert dataset["instrument"]
        positioners = dataset["instrument/positioners_start"]
        assert set(positioners) == set(all_motors)
        for name, pos in positioners.items():
            assert all_motors[name] == pos[()], name


def test_hdf5_file_items(session):
    roby = session.config.get("roby")
    diode = session.config.get("diode")
    simu1 = session.config.get("simu1")

    scan = scans.ascan(
        roby,
        0,
        5,
        5,
        0.001,
        diode,
        simu1.counters.spectrum_det0,
        save=True,
        return_scan=True,
    )

    scan_file = scan.writer.get_filename()
    with h5py.File(scan_file) as h5file:
        expected = {"roby", "diode", "elapsed_time", "simu1_det0", "epoch"}
        assert set(h5file["/1.1/measurement"]) == expected
        expected = {
            "name",
            "positioners",
            "positioners_dial_start",
            "positioners_dial_end",
            "positioners_start",
            "positioners_end",
            "epoch",
            "simu1_det0",
            "elapsed_time",
            "roby",
            "slit2",
            "diode",
            "primary_slit",
            "transfocator_simulator",
        }
        assert set(h5file["/1.1/instrument"]) == set(expected)


def test_hdf5_values(session):
    roby = session.config.get("roby")
    diode = session.config.get("diode")
    scan = scans.ascan(roby, 0, 10, 3, 0.01, diode, save=True, return_scan=True)
    scan_file = scan.writer.get_filename()
    data = scan.get_data()["diode"]
    with h5py.File(scan_file) as h5file:
        dataset = h5file["/1.1/measurement/diode"]
        assert list(dataset) == list(data)


def test_subscan_in_hdf5(
    session, scan_saving, lima_simulator, dummy_acq_master, dummy_acq_device
):
    chain = AcquisitionChain()
    master1 = timer.SoftwareTimerMaster(0.1, npoints=2, name="timer1")
    dummy1 = dummy_acq_device.get(None, name="dummy1", npoints=1)
    master2 = timer.SoftwareTimerMaster(0.001, npoints=50, name="timer2")
    lima_sim = session.config.get("lima_simulator")
    lima_master = LimaAcquisitionMaster(lima_sim, acq_nb_frames=1, acq_expo_time=0.001)
    dummy2 = dummy_acq_device.get(None, name="dummy2", npoints=1)
    chain.add(lima_master, dummy2)
    chain.add(master2, lima_master)
    chain.add(master1, dummy1)
    master1.terminator = False

    scan_saving.base_path = session.scan_saving.base_path
    scan = Scan(chain, "test", scan_saving=scan_saving)
    scan.run()

    scan_file = scan.writer.get_filename()
    with h5py.File(scan_file) as h5file:
        assert h5file["/1.1/measurement/elapsed_time"]
        assert h5file["1.2/measurement/elapsed_time"]
        assert h5file["/1.1/measurement/nb"]
        assert h5file["1.2/measurement/nb"]


def test_image_reference_in_hdf5(alias_session):
    env_dict = alias_session.env_dict

    scan = scans.ascan(env_dict["robyy"], 0, 1, 2, 0.1, env_dict["lima_simulator"])

    with h5py.File(scan.writer.get_filename()) as h5file:
        refs = h5py_read_dataset(h5file["/1.1/instrument/lima_simulator/data"])
    expected = [f"scan0001/lima_simulator_000{i}.edf::0" for i in range(3)]
    assert refs.tolist() == expected


def test_lima_instrument_entry(alias_session):
    env_dict = alias_session.env_dict

    scan = scans.ascan(env_dict["robyy"], 0, 1, 3, 0.1, env_dict["lima_simulator"])

    with h5py.File(scan.writer.get_filename()) as h5file:
        assert "lima_simulator" in h5file["/1.1/instrument"]
        assert "acq_mode" in h5file["/1.1/instrument/lima_simulator/acq_parameters"]
        assert "height" in h5file["/1.1/instrument/lima_simulator_r1/selection"]


def test_NXclass_of_scan_meta(session, lima_simulator):
    lima_sim = session.config.get("lima_simulator")

    scan = scans.loopscan(3, 0.1, lima_sim)
    with h5py.File(scan.writer.get_filename()) as h5file:
        assert h5file["/1.1/instrument"].attrs["NX_class"] == "NXinstrument"
        assert (
            h5file["/1.1/instrument/lima_simulator"].attrs["NX_class"] == "NXdetector"
        )
        assert h5file["/1.1/instrument/positioners"].attrs["NX_class"] == "NXcollection"


def test_scan_info_cleaning(alias_session):
    env_dict = alias_session.env_dict
    lima_simulator = env_dict["lima_simulator"]
    robyy = env_dict["robyy"]
    diode = alias_session.config.get("diode")

    # test that positioners are remaining in for a simple counter
    # that does not update 'scan_info'
    s1 = scans.ascan(robyy, 0, 1, 3, 0.1, diode)
    with h5py.File(s1.writer.get_filename()) as h5file:
        assert "lima_simulator" not in h5file["/1.1/instrument"]

    # test that positioners are remaining in for a counter that
    # updates 'scan_info'
    s2 = scans.ascan(robyy, 0, 1, 3, 0.1, lima_simulator)
    assert "positioners" in s2.scan_info
    with h5py.File(s2.writer.get_filename()) as h5file:
        assert "lima_simulator" in h5file["/2.1/instrument"]

    # test that 'lima_simulator' does not remain in 'scan_info'
    # for a scan that it is not involved in
    s3 = scans.ascan(robyy, 0, 1, 3, 0.1, diode)
    with h5py.File(s3.writer.get_filename()) as h5file:
        assert "lima_simulator" not in h5file["/3.1/instrument"]


def test_fill_meta_mechanisms(alias_session, lima_simulator):
    lima_sim = alias_session.config.get("lima_simulator")
    _ = alias_session.config.get("transfocator_simulator")

    scan = scans.loopscan(3, 0.1, lima_sim)
    with h5py.File(scan.writer.get_filename()) as h5file:
        assert "lima_simulator" in h5file["/1.1/instrument"]
        assert "acq_mode" in h5file["/1.1/instrument/lima_simulator/acq_parameters"]
        assert "height" in h5file["/1.1/instrument/lima_simulator_r1/selection"]
        assert "transfocator_simulator" in h5file["/1.1/instrument/"]
        assert "L1" in h5file["/1.1/instrument/transfocator_simulator"]
