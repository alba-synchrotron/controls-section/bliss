# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import pytest

import bliss
from bliss.common.standard import info
from bliss.common.scans import ct, sct
from bliss.config.settings import OrderedHashSetting
from bliss.scanning import scan_saving as scan_saving_module


@pytest.mark.parametrize("writer", ["hdf5", "nexus", "null", "external"])
def test_scan_saving_path(writer, session):
    scan_saving = session.scan_saving
    scan_saving.template = "{session}/{scan_name}/{scan_number}"
    scan_saving.data_filename = "{session}_{scan_name}_data"
    scan_saving.writer = writer
    base_path = os.path.join(scan_saving.base_path, "subdir")
    scan_saving.base_path = base_path

    assert scan_saving.base_path == base_path
    assert scan_saving.template == "{session}/{scan_name}/{scan_number}"
    assert scan_saving.data_filename == "{session}_{scan_name}_data"

    # Test all path related methods and properties
    root_path = f"{base_path}/{session.name}/{{scan_name}}/{{scan_number}}"
    assert scan_saving.get_path() == root_path
    assert scan_saving.root_path == root_path
    images_path = f"{base_path}/{session.name}/{{scan_name}}/{{scan_number}}/scan{{scan_number}}/{{img_acq_device}}_"
    assert scan_saving.images_path == images_path
    data_path = f"{base_path}/{session.name}/{{scan_name}}/{{scan_number}}/{session.name}_{{scan_name}}_data"
    assert scan_saving.data_path == data_path
    if writer == "null":
        data_fullpath = data_path + "."
    else:
        data_fullpath = data_path + ".h5"
    assert scan_saving.data_fullpath == data_fullpath
    assert scan_saving.eval_data_filename == f"{session.name}_{{scan_name}}_data"
    if writer == "null":
        filename = None
    else:
        filename = data_fullpath
    assert scan_saving.filename == filename

    getdict = scan_saving.get()
    getdict.pop("writer")
    getdict.pop("db_path_items")
    expected = {
        "root_path": root_path,
        "data_path": data_path,
        "images_path": images_path,
    }
    assert getdict == expected

    scan_saving_repr = """Parameters (default) -

  .base_path            = '{base_path}'
  .data_filename        = '{{session}}_{{scan_name}}_data'
  .template             = '{{session}}/{{scan_name}}/{{scan_number}}'
  .images_path_relative = True
  .images_path_template = 'scan{{scan_number}}'
  .images_prefix        = '{{img_acq_device}}_'
  .date_format          = '%Y%m%d'
  .scan_number_format   = '%04d'
  .session              = '{session}'
  .date                 = '{date}'
  .user_name            = '{user_name}'
  .scan_name            = '{{scan_name}}'
  .scan_number          = '{{scan_number}}'
  .img_acq_device       = '<images_* only> acquisition device name'
  .writer               = '{writer}'
  .data_policy          = 'None'
  .creation_date        = '{creation_date}'
  .last_accessed        = '{last_accessed}'
--------------  ---------  -----------
does not exist  filename   {filename}
does not exist  directory  {root_path}
--------------  ---------  -----------""".format(
        base_path=base_path,
        session=session.name,
        user_name=scan_saving.user_name,
        date=scan_saving.date,
        writer=writer,
        creation_date=scan_saving.creation_date,
        last_accessed=scan_saving.last_accessed,
        filename=filename,
        root_path=root_path,
    )

    expected = scan_saving_repr.split("\n")
    actual = info(scan_saving).split("\n")
    expected[19] = actual[19]  # last_accessed for read/write
    if writer == "null":
        expected = expected[:-1]
        expected[-3:] = ["---------", "NO SAVING", "---------"]
    else:
        add = "-" * (len(actual[-1]) - len(expected[-1]))
        expected[-4] += add
        expected[-1] += add
    assert actual == expected

    # Test scan saving related things after a scan
    if writer in ("hdf5", "null"):
        scan = sct(0.1, session.env_dict["diode"], save=True, name="sname")
    else:
        scan = ct(0.1, session.env_dict["diode"])
        assert scan.scan_info["filename"] is None

    assert scan.scan_info["data_policy"] == "None"
    assert scan.scan_info["data_writer"] == writer


def test_session_scan_saving_config(beacon):
    class TestESRFScanSaving(scan_saving_module.ESRFScanSaving):
        def _icat_set_proposal(self, proposal, **kw):
            return

    scan_saving_module.TestESRFScanSaving = TestESRFScanSaving

    scan_saving_test_session = beacon.get("scan_saving_test_session")
    scan_saving_test_session.setup()

    try:
        scan_saving = scan_saving_test_session.scan_saving
        assert isinstance(scan_saving, scan_saving_module.TestESRFScanSaving)
        scan_saving.newproposal("ihr0000")
        assert scan_saving.base_path == "/tmp/scans/visitor_test_scan_saving"
    finally:
        scan_saving_test_session.close()


def test_default_session_scan_saving(session):
    session.enable_esrf_data_policy()
    scan_saving = session.scan_saving
    scan_saving.beamline


class CustomWardrobe(scan_saving_module.EvalParametersWardrobe):
    PROPERTY_ATTRIBUTES = ["p1"]
    PROPS = {"p1": "p1", "p2": "p2"}

    def __init__(self, name):
        default_values = {
            "a": "va",
            "b": "vb",
            "c": "vc",
            "d": "{a}+{b}+{p1}",
            "e": "{d}+{d}+{b}+{p2}",
            "f": "{f}",
            "g": "{f}_{f}",
            "circlea1": "{a}_{circlea1}",
            "circleb1": "{b}_{circleb2}",
            "circleb2": "{circleb1}_{b}",
            "circlec1": "{c}_{circlec2}",
            "circlec2": "{circlec3}_{c}",
            "circlec3": "{c}_{circlec1}",
            "circled1": "{circled1}_{a}",
        }
        super().__init__(name, default_values=default_values)

    @scan_saving_module.property_with_eval_dict
    def p1(self, eval_dict=None):
        return self.PROPS["p1"]

    @scan_saving_module.property_with_eval_dict
    def p2(self, eval_dict=None):
        return self.PROPS["p2"]

    def func1(self):
        return "func1" + self.p1 + self.func2()

    def func2(self):
        return "func2" + self.p2

    def func3(self):
        return "{e}"


def test_scan_saving_eval(session):

    wr = CustomWardrobe("test_scan_saving_eval")
    wr.add("pfunc1", CustomWardrobe.func1)
    wr.add("pfunc2", CustomWardrobe.func2)
    wr.add("pfunc3", CustomWardrobe.func3)
    wr.add("afuncs", "{a}_{pfunc1}_{pfunc2}_{pfunc3}")

    # Check template evaluation

    eval_dict = {}
    for _ in range(2):
        assert wr.eval_template(wr.a, eval_dict=eval_dict) == "va"
        assert wr.eval_template(wr.b, eval_dict=eval_dict) == "vb"
        assert wr.eval_template(wr.c, eval_dict=eval_dict) == "vc"

        assert wr.eval_template(wr.d, eval_dict=eval_dict) == "va+vb+p1"
        assert wr.eval_template(wr.d, eval_dict=eval_dict) == "va+vb+p1"

        assert wr.eval_template(wr.e, eval_dict=eval_dict) == "va+vb+p1+va+vb+p1+vb+p2"
        assert wr.eval_template(wr.e, eval_dict=eval_dict) == "va+vb+p1+va+vb+p1+vb+p2"
        assert wr.eval_template(wr.f, eval_dict=eval_dict) == "{f}"
        assert wr.eval_template(wr.f, eval_dict=eval_dict) == "{f}"

        assert wr.eval_template(wr.g, eval_dict=eval_dict) == "{f}_{f}"
        assert wr.eval_template(wr.g, eval_dict=eval_dict) == "{f}_{f}"

        assert wr.eval_template(wr.pfunc1, eval_dict=eval_dict) == "func1p1func2p2"
        assert wr.eval_template(wr.pfunc1, eval_dict=eval_dict) == "func1p1func2p2"

        assert wr.eval_template(wr.pfunc2, eval_dict=eval_dict) == "func2p2"
        assert wr.eval_template(wr.pfunc2, eval_dict=eval_dict) == "func2p2"

        assert (
            wr.eval_template(wr.pfunc3, eval_dict=eval_dict)
            == "va+vb+p1+va+vb+p1+vb+p2"
        )
        assert (
            wr.eval_template(wr.pfunc3, eval_dict=eval_dict)
            == "va+vb+p1+va+vb+p1+vb+p2"
        )

        assert (
            wr.eval_template(wr.afuncs, eval_dict=eval_dict)
            == "va_func1p1func2p2_func2p2_va+vb+p1+va+vb+p1+vb+p2"
        )
        assert (
            wr.eval_template(wr.afuncs, eval_dict=eval_dict)
            == "va_func1p1func2p2_func2p2_va+vb+p1+va+vb+p1+vb+p2"
        )

        assert (
            wr.eval_template(wr.pfunc3, eval_dict=eval_dict)
            == "va+vb+p1+va+vb+p1+vb+p2"
        )
        assert (
            wr.eval_template(wr.pfunc3, eval_dict=eval_dict)
            == "va+vb+p1+va+vb+p1+vb+p2"
        )

        assert wr.eval_template(wr.circlea1, eval_dict=eval_dict) == "va_{circlea1}"
        assert wr.eval_template(wr.circlea1, eval_dict=eval_dict) == "va_{circlea1}"

        assert wr.eval_template(wr.circled1, eval_dict=eval_dict) == "{circled1}_va"
        assert wr.eval_template(wr.circled1, eval_dict=eval_dict) == "{circled1}_va"

        assert wr.eval_template(wr.circleb1, eval_dict=eval_dict) == "vb_{circleb1}_vb"
        assert wr.eval_template(wr.circleb1, eval_dict=eval_dict) == "vb_{circleb1}_vb"

        assert wr.eval_template(wr.circleb2, eval_dict=None) == "vb_{circleb2}_vb"
        assert wr.eval_template(wr.circleb2, eval_dict=None) == "vb_{circleb2}_vb"

        assert wr.eval_template(wr.circlec1, eval_dict=eval_dict) == "vc_{circlec3}_vc"
        assert wr.eval_template(wr.circlec1, eval_dict=eval_dict) == "vc_{circlec3}_vc"

        assert wr.eval_template(wr.circlec2, eval_dict=None) == "vc_{circlec1}_vc"
        assert wr.eval_template(wr.circlec2, eval_dict=None) == "vc_{circlec1}_vc"

        assert wr.eval_template(wr.circlec3, eval_dict=None) == "vc_vc_{circlec2}"
        assert wr.eval_template(wr.circlec3, eval_dict=None) == "vc_vc_{circlec2}"

    # Check caching

    eval_dict = {}
    assert wr.func1() == "func1p1func2p2"
    assert wr.func1() == "func1p1func2p2"
    wr.PROPS["p1"] = "v1"
    wr.PROPS["p2"] = "v2"
    assert wr.func1() == "func1v1func2v2"
    assert wr.func1() == "func1v1func2v2"

    eval_dict = {}
    assert wr.eval_template(wr.a, eval_dict=eval_dict) == "va"
    assert wr.eval_template(wr.b, eval_dict=eval_dict) == "vb"
    assert wr.eval_template(wr.c, eval_dict=eval_dict) == "vc"

    wr.a = wr.b = wr.c = "modify"
    assert wr.eval_template(wr.d, eval_dict=eval_dict) == "va+vb+v1"
    assert wr.eval_template(wr.d, eval_dict=eval_dict) == "va+vb+v1"

    wr.d = "modify"
    assert wr.eval_template(wr.e, eval_dict=eval_dict) == "va+vb+v1+va+vb+v1+vb+v2"
    assert wr.eval_template(wr.e, eval_dict=eval_dict) == "va+vb+v1+va+vb+v1+vb+v2"


@pytest.mark.parametrize("writer", ["null", "hdf5", "nexus", "external"])
def test_scan_saving_writer_options(session, writer):
    scan_saving = session.scan_saving

    scan_saving.writer = writer
    if writer == "null":
        expected = dict()
    else:
        expected = {
            "chunk_options": dict(),
            "separate_scan_files": None,
        }
    assert scan_saving.get_writer_options() == expected

    if writer == "null":
        return

    writer = scan_saving.writer_object
    writer.compression_limit = 1
    writer.chunk_size = 1
    writer.compression_scheme = "gzip"
    writer.chunk_split = 4
    writer.separate_scan_files = True
    assert scan_saving.get_writer_options() == {
        "chunk_options": {
            "chunk_nbytes": 1 << 20,
            "compression_scheme": "gzip",
            "compression_limit_nbytes": 1 << 20,
            "chunk_split": 4,
        },
        "separate_scan_files": True,
    }


def test_scan_saving_warning_tmpdir(session, capsys, log_shell_mode):
    bliss.set_bliss_shell_mode()
    try:
        sct(session.env_dict["diode"])
    finally:
        bliss.set_bliss_shell_mode(False)
    expected = (
        "Warning: scan data are currently saved under /tmp, where files are volatile.\n"
    )
    assert expected in capsys.readouterr().out


class CustomScanSaving(scan_saving_module.BasicScanSaving):
    SLOTS = scan_saving_module.BasicScanSaving.SLOTS + [
        "slot1",
        "slot2",
    ]
    DEFAULT_VALUES = {
        **scan_saving_module.BasicScanSaving.DEFAULT_VALUES,
        "default_key1": "default_key1_value",
        "default_key2": "default_key2_value",
        "default_property": "default_property_value",
        "default_slot": "default_slot_value",
        "default_none_slot": "default_none_slot_value",
        "default_rename": "default_rename_value",
    }
    PROPERTY_ATTRIBUTES = scan_saving_module.BasicScanSaving.PROPERTY_ATTRIBUTES + [
        "property1",
        "property2",
        "property_no_eval",
    ]

    def __init__(self, name, session_name=None):
        self.slot1 = "slot1_value"
        self.slot2 = "slot2_value"
        super().__init__(name, session_name)
        self._remove_deprecated()

    def _remove_deprecated(self):
        pass

    @property
    def property1(self):
        return "property1_value"

    @property
    def property2(self):
        return "property2_value"

    @property
    def property_no_eval(self):
        return "property_no_eval_value"


class CustomScanSavingUpgrade(scan_saving_module.BasicScanSaving):
    SLOTS = scan_saving_module.BasicScanSaving.SLOTS + [
        "slot1",
        "slot3",
        "default_slot",
        "default_none_slot",
        "removed_names",
    ]
    DEFAULT_VALUES = {
        **scan_saving_module.BasicScanSaving.DEFAULT_VALUES,
        "default_key1": "default_key1_value",
        "default_key3": "default_key3_value",
        "default_renamed": "default_renamed_value",
    }
    PROPERTY_ATTRIBUTES = scan_saving_module.BasicScanSaving.PROPERTY_ATTRIBUTES + [
        "property1",
        "property3",
        "default_property",
        "property_no_eval",
    ]
    NO_EVAL_PROPERTIES = scan_saving_module.BasicScanSaving.NO_EVAL_PROPERTIES | {
        "property_no_eval"
    }

    def __init__(self, name, session_name=None):
        super().__init__(name, session_name)
        self.removed_names = set()
        self.slot1 = "slot1_value"
        self.slot3 = "slot3_value"
        self.default_slot = "default_slot_value_upgrade"
        self.default_none_slot = None
        assert getattr(self, "default_none_slot") is None, "issue #4050"
        self._remove_deprecated_defaults()

    def _remove_deprecated_defaults(self):
        redis_keys = set(self._proxy.keys())

        # Example of moving a parameter to another attribute (slot or property)
        for deprecated in (
            "default_key2",
            "default_property",
            "default_slot",
            "default_none_slot",
        ):
            if deprecated in redis_keys:
                print(f"Remove '{deprecated}'")
                self.remove(f".{deprecated}")
                self.removed_names.add(deprecated)

        # Example of renaming a default parameter
        if "default_rename" in redis_keys:
            value = self._proxy["default_rename"]
            print("Rename 'default_rename'")
            self.remove(".default_rename")
            self.default_renamed = value
            self.removed_names.add("default_rename")

    @property
    def property1(self):
        return "property1_value"

    @property
    def property3(self):
        return "property3_value"

    @property
    def property_no_eval(self):
        return "property_no_eval_value_upgrade"

    @property
    def default_property(self):
        return "default_property_value_upgrade"


def test_changing_scan_saving(session):
    base_expected = {
        "base_path": "/tmp/scans",
        "data_filename": "data",
        "template": "{session}/",
        "images_path_relative": True,
        "images_path_template": "scan{scan_number}",
        "images_prefix": "{img_acq_device}_",
        "date_format": "%Y%m%d",
        "scan_number_format": "%04d",
        "_writer_module": "hdf5",
        "session": "test_session",
        "scan_name": "{scan_name}",
        "scan_number": "{scan_number}",
        "img_acq_device": "{img_acq_device}",
        "writer": "hdf5",
        "data_policy": "None",
    }

    expected = {
        **base_expected,
        "default_key1": "default_key1_value",
        "default_key2": "default_key2_value",
        "default_rename": "default_rename_value",
        "default_property": "default_property_value",
        "default_slot": "default_slot_value",
        "default_none_slot": "default_none_slot_value",
        "property1": "property1_value",
        "property2": "property2_value",
        "property_no_eval": "property_no_eval_value",
    }

    # Instantiate and check the original class version
    scan_saving = CustomScanSaving("myname")
    _assert_scan_saving_redis(scan_saving)
    _assert_scan_saving(scan_saving, expected)

    # Instantiate the class after an upgrade
    expected_removed_names = {"default_key2", "default_rename"}
    expected = {
        **base_expected,
        "default_key1": "default_key1_value",
        "default_key3": "default_key3_value",
        "default_renamed": "default_rename_value",
        "property1": "property1_value",
        "property3": "property3_value",
        "default_property": "default_property_value_upgrade",
    }

    scan_saving = CustomScanSavingUpgrade("myname")
    _assert_scan_saving_redis(scan_saving)
    assert scan_saving.removed_names == expected_removed_names
    _assert_scan_saving(scan_saving, expected)
    assert scan_saving.default_slot == "default_slot_value_upgrade"
    assert scan_saving.default_none_slot is None
    assert scan_saving.property_no_eval == "property_no_eval_value_upgrade"

    scan_saving = CustomScanSavingUpgrade("myname")
    _assert_scan_saving_redis(scan_saving)
    assert not scan_saving.removed_names
    _assert_scan_saving(scan_saving, expected)
    assert scan_saving.default_slot == "default_slot_value_upgrade"
    assert scan_saving.default_none_slot is None
    assert scan_saving.property_no_eval == "property_no_eval_value_upgrade"


def _assert_scan_saving_redis(scan_saving: scan_saving_module.BasicScanSaving):
    expected = set(scan_saving.DEFAULT_VALUES) | {"_creation_date"}
    pr = OrderedHashSetting(scan_saving._hash("default"))
    assert set(pr.keys()) == expected


def _assert_scan_saving(
    scan_saving: scan_saving_module.BasicScanSaving, expected: dict
) -> None:
    variables = "creation_date", "last_accessed", "date", "user_name"
    adict = dict()
    scan_saving._update_eval_dict(adict)
    for variable in variables:
        adict.pop(variable)
    assert adict == expected
