# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import mock
import pytest
import numpy
import gevent

from bliss.common import scans, event
from bliss.common.data_store import get_default_data_store
from bliss.scanning.scan_state import ScanState
from bliss.controllers.counter import CalcCounterController
from bliss.scanning.acquisition.calc import CalcCounterAcquisitionSlave
from blissdata.stream import LimaStream


def test_ascan(session):
    robz2 = session.env_dict["robz2"]
    simul_counter = session.env_dict["sim_ct_gauss"]
    s = scans.ascan(
        robz2, 0.0, 0.1234567, 2, 0, simul_counter, return_scan=True, save=False
    )
    assert pytest.approx(robz2.position, abs=0.0001) == 0.1234567
    # test for issue #1079
    # use tolerance to get axis precision
    assert pytest.approx(robz2.tolerance) == 1e-4
    assert s.scan_info["title"].startswith("ascan robz2 0.000000 0.123457 ")
    scan_data = s.get_data()
    assert numpy.array_equal(scan_data["sim_ct_gauss"], simul_counter.data)

    # A default plot is expected, displaying the motor as x-axis
    plot = s.scan_info["plots"][0]
    assert plot["items"][0]["x"] == "axis:robz2"


def test_loopscan_sleep_time(session):
    """
    Test that:
    * sleep_time parameter is well passed to the timer of top master.
    * _sleep() is called once and only once.
    """
    simul_counter = session.env_dict["sim_ct_gauss"]

    test_scan = scans.loopscan(
        2, 0.2, simul_counter, sleep_time=1, save=False, run=False
    )

    timer = test_scan.acq_chain.top_masters[0]
    assert timer.sleep_time == 1

    with mock.patch.object(
        timer, "_sleep", wraps=timer._sleep  # pylint: disable=protected-access
    ) as timer_sleep:
        test_scan.run()  # pylint: disable=not-callable
        assert timer_sleep.call_count == 1

    # A default plot is expected, displaying the time as x-axis
    plot = test_scan.scan_info["plots"][0]
    assert plot["items"][0]["x"] == "timer:elapsed_time"


def test_loopscan_stab_time(session):
    """
    Test that:
    * stab_time parameter is well passed to the VariableStepTriggerMaster top master.
    * _sleep_stab_time() function is called the good number of times.
    """
    simul_counter = session.env_dict["sim_ct_gauss"]
    simul_mot = session.env_dict["robz2"]

    test_scan = scans.ascan(
        simul_mot, 1, 3, 3, 0.1, simul_counter, stab_time=0.123, save=False, run=False
    )

    top_master = test_scan.acq_chain.top_masters[0]
    assert top_master.stab_time == 0.123

    with mock.patch.object(
        top_master,
        "_sleep_stab_time",
        wraps=top_master._sleep_stab_time,  # pylint: disable=protected-access
    ) as timer_sleep:
        test_scan.run()  # pylint: disable=not-callable
        assert timer_sleep.call_count == 4


def test_ascan_gauss2(session):
    robz2 = session.env_dict["robz2"]
    simul_counter = session.env_dict["sim_ct_gauss"]
    s = scans.ascan(robz2, 0, 0.1, 2, 0, simul_counter, return_scan=True, save=False)
    assert robz2.position == 0.1
    scan_data = s.get_data()
    assert numpy.array_equal(scan_data["sim_ct_gauss"], simul_counter.data)


def test_ascan_encoder_in_counters(session):
    # issue #1990
    m1 = session.env_dict["m1"]
    simul_counter = session.env_dict["sim_ct_gauss"]
    s = scans.ascan(m1, 0, 0.1, 2, 0, simul_counter, return_scan=True, save=False)
    scan_data = s.get_data()
    assert "encoder:m1enc" in scan_data


def test_dscan(session):
    simul_counter = session.env_dict["sim_ct_gauss"]
    robz2 = session.env_dict["robz2"]
    robz2.position = 0.1265879
    # contrary to ascan, dscan returns to start pos
    start_pos = robz2.position
    s = scans.dscan(robz2, -0.2, 0.2, 1, 0, simul_counter, return_scan=True, save=False)
    # test for issues #1080
    # use tolerance to get axis precision
    assert pytest.approx(robz2.tolerance) == 1e-4
    assert s.scan_info["title"].startswith(
        f"ascan robz2 {start_pos - 0.2:.6f} {start_pos + 0.2:.6f}"
    )
    #
    assert robz2.position == start_pos
    scan_data = s.get_data()
    assert numpy.allclose(
        scan_data["robz2"],
        numpy.linspace(start_pos - 0.2, start_pos + 0.2, 2),
        atol=5e-4,
    )
    assert numpy.array_equal(scan_data["sim_ct_gauss"], simul_counter.data)


def test_d2scan(session):
    simul_counter = session.env_dict["sim_ct_gauss"]
    robz = session.env_dict["robz"]
    robz.position = 1
    robz2 = session.env_dict["robz2"]
    robz2.position = -1
    # contrary to ascan, dscan returns to start pos
    start_pos = robz2.position
    s = scans.d2scan(
        robz2,
        -0.2,
        0.2,
        robz,
        -0.1,
        0.1,
        1,
        0,
        simul_counter,
        return_scan=True,
        save=False,
    )
    # test for issues #1080
    # use tolerance to get axis precision
    assert pytest.approx(robz2.tolerance) == 1e-4
    assert s.scan_info["title"].startswith("a2scan robz2 -1.200000 -0.800000")
    #
    assert robz2.position == start_pos
    scan_data = s.get_data()
    assert numpy.allclose(
        scan_data["robz2"],
        numpy.linspace(start_pos - 0.2, start_pos + 0.2, 2),
        atol=5e-4,
    )
    assert numpy.array_equal(scan_data["sim_ct_gauss"], simul_counter.data)

    channels = s.scan_info["channels"]
    assert channels["axis:robz2"]["start"] == pytest.approx(-1.2)
    assert channels["axis:robz2"]["stop"] == pytest.approx(-0.8)
    assert channels["axis:robz"]["start"] == pytest.approx(0.9)
    assert channels["axis:robz"]["stop"] == pytest.approx(1.1)


def test_lineup(session):
    simul_counter = session.env_dict["sim_ct_gauss"]
    robz2 = session.env_dict["robz2"]
    start_pos = robz2.position
    s = scans.lineup(
        robz2, -0.2, 0.2, 2, 0, simul_counter, return_scan=True, save=False
    )
    scan_data = s.get_data()
    assert numpy.allclose(
        scan_data["robz2"],
        numpy.linspace(start_pos - 0.2, start_pos + 0.2, 3),
        atol=5e-4,
    )
    assert numpy.array_equal(scan_data["sim_ct_gauss"], simul_counter.data)
    # after lineup motor goes to where the counter has its max value
    assert robz2.position == 0


def test_dscan_move_done(session):
    simul_counter = session.env_dict["sim_ct_gauss"]
    robz2 = session.env_dict["robz2"]

    # Callback
    positions = []

    def target(done):
        if done:
            positions.append(robz2.dial)

    event.connect(robz2, "move_done", target)

    # contrary to ascan, dscan returns to start pos
    start_pos = robz2.position
    s = scans.dscan(robz2, -0.2, 0.2, 1, 0, simul_counter, return_scan=True, save=False)
    assert robz2.position == start_pos
    scan_data = s.get_data()
    assert numpy.allclose(
        scan_data["robz2"],
        numpy.linspace(start_pos - 0.2, start_pos + 0.2, 2),
        atol=5e-4,
    )
    assert numpy.array_equal(scan_data["sim_ct_gauss"], simul_counter.data)
    assert positions[0] == -0.2
    assert positions[-2] == 0.2
    assert positions[-1] == 0

    event.disconnect(robz2, "move_done", target)


def test_pointscan(session):
    robz2 = session.env_dict["robz2"]
    diode = session.env_dict["diode"]
    points = [0.0, 0.1, 0.3, 0.7]
    s = scans.pointscan(robz2, points, 0, diode, save=False, run=False)
    assert s.state == ScanState.IDLE
    s.run()
    assert robz2.position == 0.7
    scan_data = s.get_data()
    assert numpy.array_equal(scan_data["robz2"], points)
    assert diode.fullname in scan_data


def test_lookupscan(session):
    roby = session.env_dict["roby"]
    robz = session.env_dict["robz"]
    diode = session.env_dict["diode"]
    s = scans.lookupscan([(roby, (0, 0.1)), (robz, (0.1, 0.2))], 0.1, diode, save=False)
    scan_data = s.get_data()
    assert numpy.array_equal(scan_data["roby"], (0, 0.1))
    assert numpy.array_equal(scan_data["robz"], (0.1, 0.2))

    # A default plot is expected, displaying the time as x-axis
    plot = s.scan_info["plots"][0]
    assert plot["items"][0]["x"] == "timer:elapsed_time"


def test_anscan(session):
    roby = session.env_dict["roby"]
    robz = session.env_dict["robz"]
    diode = session.env_dict["diode"]
    s = scans.anscan([(roby, 0, 0.1), (robz, 0.1, 0.2)], 1, 0.1, diode, save=False)
    scan_data = s.get_data()
    assert numpy.array_equal(scan_data["roby"], (0, 0.1))
    assert numpy.array_equal(scan_data["robz"], (0.1, 0.2))


def test_all_anscan(session):
    roby = session.env_dict["roby"]
    robz = session.env_dict["robz"]
    robz2 = session.env_dict["robz2"]
    m0 = session.env_dict["m0"]
    m1 = session.env_dict["m1"]
    diode = session.env_dict["diode"]
    # just call them to check syntax
    # real test is done else where
    scans.a5scan(
        roby,
        0,
        0.1,
        robz,
        0,
        0.1,
        robz2,
        0,
        0.1,
        m0,
        0,
        0.1,
        m1,
        0,
        0.1,
        2,
        0.1,
        diode,
        save=False,
        run=False,
    )
    scans.a4scan(
        roby,
        0,
        0.1,
        robz,
        0,
        0.1,
        robz2,
        0,
        0.1,
        m0,
        0,
        0.1,
        2,
        0.1,
        diode,
        save=False,
        run=False,
    )
    scans.a3scan(
        roby, 0, 0.1, robz, 0, 0.1, robz2, 0, 0.1, 2, 0.1, diode, save=False, run=False
    )


def test_all_dnscan(session):
    roby = session.env_dict["roby"]
    robz = session.env_dict["robz"]
    robz2 = session.env_dict["robz2"]
    m0 = session.env_dict["m0"]
    m1 = session.env_dict["m1"]
    diode = session.env_dict["diode"]
    # just call them to check syntax
    # real test is done else where
    scans.d5scan(
        roby,
        0,
        0.1,
        robz,
        0,
        0.1,
        robz2,
        0,
        0.1,
        m0,
        0,
        0.1,
        m1,
        0,
        0.1,
        2,
        0.1,
        diode,
        save=False,
        run=False,
    )
    scans.d4scan(
        roby,
        0,
        0.1,
        robz,
        0,
        0.1,
        robz2,
        0,
        0.1,
        m0,
        0,
        0.1,
        2,
        0.1,
        diode,
        save=False,
        run=False,
    )
    scans.d3scan(
        roby, 0, 0.1, robz, 0, 0.1, robz2, 0, 0.1, 2, 0.1, diode, save=False, run=False
    )


def test_scan_watch_data_no_print(session, capsys):
    roby = session.config.get("roby")
    diode = session.config.get("diode")
    scans.ascan(roby, 0, 10, 10, 0.01, diode, save=False)
    captured = capsys.readouterr()

    assert captured.out == ""


def test_timescan_with_lima(default_session, lima_simulator):
    ls = default_session.config.get("lima_simulator")
    s = scans.timescan(0.01, ls, save=False, run=False)
    try:
        g = gevent.spawn(s.run)

        s.wait_state(ScanState.STARTING)

        gevent.sleep(0.3)  # let time to take some images...
    finally:
        g.kill()

    assert len(s.get_data()[ls.image]) > 1


def test_calc_counters_0d(session):
    robz2 = session.env_dict["robz2"]
    cnt = session.env_dict["sim_ct_gauss"]

    class MyCCC(CalcCounterController):
        def calc_function(self, input_dict):
            return {"pow": input_dict["sim_ct_gauss"] ** 2}

    config = {
        "inputs": [{"counter": cnt, "tags": "sim_ct_gauss"}],
        "outputs": [{"name": "pow"}],
    }

    ccc = MyCCC("pow2", config)

    scan = scans.ascan(robz2, 0, 1, 2, 0.1, ccc, return_scan=True, save=False)

    # Check Redis stream info's
    infos = {"type": "numeric", "dtype_str": "<f8", "shape": tuple()}
    assert scan.streams["pow2:pow"].encoding == infos

    # Check the calculation
    scan_data = scan.get_data()
    assert numpy.array_equal(scan_data["sim_ct_gauss"] ** 2, scan_data["pow"])


def test_calc_counters_1d(session):
    robz2 = session.env_dict["robz2"]
    cnt = session.config.get("simu1")

    class MyCCC(CalcCounterController):
        def calc_function(self, input_dict):
            return {"pow": input_dict["simu1:spectrum_det0"] ** 2}

    config = {
        "inputs": [
            {"counter": cnt.counters.spectrum_det0, "tags": "simu1:spectrum_det0"}
        ],
        "outputs": [
            {
                "name": "pow",
                "shape": cnt.counters.spectrum_det0.shape,
                "dtype": cnt.counters.spectrum_det0.data_dtype,
            }
        ],
    }

    ccc = MyCCC("pow2", config)

    scan = scans.ascan(robz2, 0, 1, 2, 0.1, ccc, return_scan=True, save=False)

    # Check Redis stream info's
    infos = {
        "type": "numeric",
        "dtype_str": "<u4",
        "shape": cnt.counters.spectrum_det0.shape,
    }
    assert scan.streams["pow2:pow"].encoding == infos

    # Check the calculation
    scan_data = scan.get_data()
    assert numpy.array_equal(scan_data["simu1:spectrum_det0"] ** 2, scan_data["pow"])


def test_calc_counter_callback(session):
    m1 = session.env_dict["m1"]
    cnt = session.env_dict["sim_ct_gauss"]

    class CCAS(CalcCounterAcquisitionSlave):
        def prepare(self):
            super().prepare()
            self.device.prepare_called += 1

        def start(self):
            super().start()
            self.device.start_called += 1

        def stop(self):
            self.device.stop_called += 1
            super().stop()

    class MyCCC(CalcCounterController):
        def __init__(self, name, config):
            super().__init__(name, config)
            self.prepare_called = 0
            self.start_called = 0
            self.stop_called = 0

        def calc_function(self, input_dict):
            return {"pow": input_dict["sim_ct_gauss"] ** 2}

        def get_acquisition_object(
            self, acq_params, ctrl_params, parent_acq_params, acq_devices
        ):
            return CCAS(self, acq_devices, acq_params, ctrl_params=ctrl_params)

    config = {
        "inputs": [{"counter": cnt, "tags": "sim_ct_gauss"}],
        "outputs": [{"name": "pow"}],
    }

    ccc = MyCCC("pow2", config)

    scans.ascan(m1, 0, 1, 9, 0.1, ccc, save=False)

    assert ccc.prepare_called == 10
    assert ccc.start_called == 10
    assert ccc.stop_called == 1


def test_save_images(session, beacon, lima_simulator):
    lima_sim = beacon.get("lima_simulator")
    robz2 = session.env_dict["robz2"]
    scan_saving = session.scan_saving
    scan_saving.images_path_template = ""

    s = scans.ascan(robz2, 0, 1, 2, 0.001, lima_sim, run=False)
    scan_path = s.writer.get_filename()
    images_path = os.path.dirname(scan_path)
    image_filename = "lima_simulator_000%d.edf"

    s.run()

    assert os.path.isfile(scan_path)
    for i in range(2):
        assert os.path.isfile(os.path.join(images_path, image_filename % i))

    os.unlink(scan_path)
    os.unlink(os.path.join(images_path, image_filename % 0))

    s = scans.ascan(robz2, 1, 0, 2, 0.001, lima_sim, save_images=False, run=False)

    s.run()

    scan_path = s.writer.get_filename()
    assert os.path.isfile(scan_path)
    assert not os.path.isfile(os.path.join(scan_saving.base_path, image_filename % 0))

    os.unlink(scan_path)

    s = scans.ascan(
        robz2, 0, 1, 2, 0.001, lima_sim, save=False, save_images=True, run=False
    )

    s.run()

    assert not os.path.isfile(scan_path)
    assert not os.path.isfile(os.path.join(images_path, image_filename % 0))


def test_calc_counters_std_scan(session):
    robz2 = session.env_dict["robz2"]
    cnt = session.env_dict["sim_ct_gauss"]
    variables = {"nb_points": 0}

    class MyCCC(CalcCounterController):
        def calc_function(self, input_dict):
            variables["nb_points"] += 1
            return {"out": input_dict["sim_ct_gauss"] ** 2}

    config = {
        "inputs": [{"counter": cnt, "tags": "sim_ct_gauss"}],
        "outputs": [{"name": "out"}],
    }

    calc_counter_controller = MyCCC("pow2", config)

    s = scans.ascan(robz2, 0, 0.1, 9, 0, calc_counter_controller, save=False)
    assert variables["nb_points"] == 10
    data = s.get_data()
    # use of the magic '==' operator of numpy arrays, make a one-by-one
    # comparison and returns the result in a list
    assert all(data["out"] == data["sim_ct_gauss"] ** 2)


def test_calc_counters_with_two(session):
    robz2 = session.env_dict["robz2"]
    diode = session.env_dict["diode"]
    diode2 = session.env_dict["diode2"]

    class MyCCC(CalcCounterController):
        def calc_function(self, input_dict):
            data_out = (input_dict["data1"] + input_dict["data2"]) / 2.0
            return {"out": data_out}

    config = {
        "inputs": [
            {"counter": diode, "tags": "data1"},
            {"counter": diode2, "tags": "data2"},
        ],
        "outputs": [{"name": "out"}],
    }

    mean_counter_controller = MyCCC("mean", config)

    s = scans.ascan(robz2, 0, 0.1, 10, 0, mean_counter_controller, save=False)
    data = s.get_data()
    assert all(data["out"] == (data["diode"] + data["diode2"]) / 2.0)


def test_update_ctrl_params(default_session, beacon, lima_simulator):
    lima_sim = beacon.get("lima_simulator")

    s = scans.loopscan(1, 0.1, lima_sim, run=False)
    with pytest.raises(RuntimeError):
        s.update_ctrl_params(lima_sim, {"unkown_key": "bla"})

    s.update_ctrl_params(lima_sim, {"saving_format": "EDFGZ"})
    s.run()

    json_stream = s.streams["lima_simulator:image"]._json_stream
    lima_ref_stream = LimaStream(json_stream, ref_mode=True)
    filepath = lima_ref_stream[0].file_path
    assert filepath.endswith(".edf.gz")
    assert os.path.isfile(filepath)


def test_dscan_return_to_target_pos(default_session, beacon):
    m0 = beacon.get("m0")
    diode = beacon.get("diode")
    m0.move(1.5)
    s = scans.dscan(m0, -1.1, 1.1, 2, 0, diode, save=False)
    assert pytest.approx(m0._set_position) == 1.5
    d = s.get_data()
    assert min(d[m0]) == pytest.approx(0.0)
    assert max(d[m0]) == pytest.approx(3.0)


def test_dscan_move_back_to_start_pos_ignores_nan(default_session):
    diode = default_session.config.get("diode")
    spectro = default_session.config.get("spectro")
    spectro.bragg_axis.move(75)
    spectro.analysers.ana_3.real_axes["zpos"].rmove(
        0.1
    )  # move one real to unvalidate ana3 bragg solution
    assert numpy.isnan(
        spectro.analysers.ana_3.bragg_axis.position
    )  # check ana3 bragg position is now Nan
    assert numpy.isnan(
        spectro.bragg_axis.position
    )  # check that spectro bragg position is now Nan
    spectro.freeze("ana_3")  # discard analyzer ana3
    assert (
        spectro.bragg_axis.position == 75.0
    )  # check that spectro bragg position is now valid again
    scans.dscan(spectro.bragg_axis, -5, 5, 3, 0.1, diode)  # perform dscan
    assert (
        spectro.bragg_axis.position == 75.0
    )  # check spectro moved back to starting position ignoring frozen ana3
    assert numpy.isnan(
        spectro.analysers.ana_3.bragg_axis.position
    )  # check ana3 bragg position is still Nan


def test_ct_sct(session, beacon):
    data_store = get_default_data_store()
    diode = beacon.get("diode")
    ct = scans.ct(0.1, diode)
    sct = scans.sct(0.1, diode)
    assert ct.scan_info["save"] is False
    since, key = data_store.get_next_scan(block=False, since=0)
    assert data_store.load_scan(key).info["save"] is False

    assert sct.scan_info["save"] is True
    _, key = data_store.get_next_scan(block=False, since=since)
    assert data_store.load_scan(key).info["save"] is True

    assert "positioners" in sct.scan_info
    assert "positioners" not in ct.scan_info

    assert len(session.scans) == 1  # only sct in scans


@pytest.mark.parametrize("scan_type", ["anscan", "lookupscan", "anmesh"])
def test_duplicated_motor_in_multiple_motor_scan(session, beacon, scan_type):
    # fix for issue 1564
    diode = beacon.get("diode")
    roby = beacon.get("roby")
    robz = beacon.get("robz")
    scan_func = getattr(scans, scan_type)
    # the ranges, diode and count time won't be used, normally - should be stopped before
    if scan_type == "anscan":
        args = (((roby, 0, 1), (robz, 0, 1), (roby, 0, 1)), 10, 0.1, diode)
    elif scan_type == "lookupscan":
        args = (((roby, (0, 1)), (robz, (0, 1)), (roby, (0, 1))), 0.1, diode)
    elif scan_type == "anmesh":
        args = (((roby, 0, 1, 10), (robz, 0, 1, 10), (roby, 0, 1, 10)), 0.1, diode)
    with pytest.raises(ValueError) as excinfo:
        scan_func(*args, run=False)
    assert "Duplicated axis" in str(excinfo)


def test_scan_title_accuracy(session):
    """
    Ensure precision of values printed in scan title correspond to axis tolerance.
    * test for issue #2783
    """
    robz2 = session.env_dict["robz2"]

    simul_counter = session.env_dict["sim_ct_gauss"]

    # title values should either round or add zeros up to four decimal digits
    # (default tolerance is 1e-4)
    s = scans.ascan(
        robz2, 1.010000, 1.010280, 3, 0.01, simul_counter, return_scan=True, save=False
    )
    assert s.scan_info["title"] == "ascan robz2 1.010000 1.010280 3 0.01"

    # force tolerance to 1e-2
    robz2._Axis__tolerance = 1e-2

    # same scan with two decimals display
    s = scans.ascan(
        robz2, 1.01, 1.01028, 3, 0.01, simul_counter, return_scan=True, save=False
    )
    assert s.scan_info["title"] == "ascan robz2 1.010000 1.010280 3 0.01"
