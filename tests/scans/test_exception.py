import gevent
import gevent.event
import pytest
from unittest import mock

from bliss.common import scans
from bliss.common.standard import _move
from bliss.common.counter import SamplingCounter
from bliss.controllers.counter import SamplingCounterController
from bliss.common.soft_axis import SoftAxis
from bliss.scanning.scan import Scan, ScanPreset, ScanAbort
from bliss.scanning.scan_state import ScanState
from bliss.scanning.chain import AcquisitionMaster, AcquisitionChain
from bliss.scanning.group import Sequence
from bliss.common.scans.ct import sct
from blissdata.redis_engine.scan import ScanState as BlissdataScanState


def test_exception_in_reading(session):
    """Check that exception during controller `read` is catched at scan run"""
    event = gevent.event.Event()

    class CntController(SamplingCounterController):
        def __init__(self):
            super().__init__("cnt_controller")

        def read(self, counter):
            try:
                if counter.nbpoints > 5:
                    return 1.0

                event.set()
                gevent.sleep(10e-3)
                raise RuntimeError("Bla bla bla")
            finally:
                counter.nbpoints -= 1

    class Cnt(SamplingCounter):
        def __init__(self, npoints):
            SamplingCounter.__init__(self, "bla", CntController())
            self.nbpoints = npoints

    c = Cnt(10)
    s = scans.timescan(0, c, npoints=10, save=False, run=False)

    with pytest.raises(RuntimeError):
        s.run()

    if not event.is_set():
        assert False, "RuntimeError was not raised from the right place"

    assert s.state == ScanState.KILLED
    assert s._scan_data.state == BlissdataScanState.CLOSED
    assert s._scan_data.info["end_reason"] == "FAILURE"


def test_exception_in_first_reading(session, bad_diode):
    s = scans.timescan(0, bad_diode, npoints=10, save=False, run=False)
    with pytest.raises(RuntimeError):
        with gevent.Timeout(1):
            s.run()

    assert s.state == ScanState.KILLED
    assert s._scan_data.state == BlissdataScanState.CLOSED
    assert s._scan_data.info["end_reason"] == "FAILURE"


def test_restarted_scan(session):
    diode = session.config.get("diode")
    s = scans.loopscan(1, 0, diode, save=False, run=False)
    s.run()
    with pytest.raises(RuntimeError):
        s.run()


def test_exception_in_move(default_session):
    class FailingAxis:
        def __init__(self):
            self._position = 0

        @property
        def position(self):
            return self._position

        @position.setter
        def position(self, value):
            if value > 1:
                raise RuntimeError
            self._position = value

    diode = default_session.config.get("diode")
    axis = SoftAxis("TestAxis", FailingAxis())

    s = scans.ascan(axis, 0, 2, 5, 0.1, diode, save=False, run=False)
    with pytest.raises(RuntimeError):
        s.run()

    assert s.state == ScanState.KILLED
    assert s._scan_data.state == BlissdataScanState.CLOSED
    assert s._scan_data.info["end_reason"] == "FAILURE"


def test_exception_on_kill(default_session):
    diode = default_session.config.get("diode")
    roby = default_session.config.get("roby")
    s = scans.ascan(roby, 0, 1, 100, 0.1, diode, save=False, run=False)

    g = gevent.spawn(s.run)
    gevent.sleep(0.5)
    g.kill()

    assert s.state == ScanState.KILLED
    assert s._scan_data.state == BlissdataScanState.CLOSED
    assert s._scan_data.info["end_reason"] == "FAILURE"


def test_exception_on_KeyboardInterrupt(default_session):
    diode = default_session.config.get("diode")
    roby = default_session.config.get("roby")
    s = scans.ascan(roby, 0, 1, 100, 0.1, diode, save=False, run=False)

    scan_task = gevent.spawn(s.run)
    gevent.sleep(0.5)
    with pytest.raises(ScanAbort):
        scan_task.kill(KeyboardInterrupt)
        scan_task.get()

    assert s.state == ScanState.USER_ABORTED
    assert s._scan_data.state == BlissdataScanState.CLOSED
    assert s._scan_data.info["end_reason"] == "USER_ABORT"


def test_exception_in_preset(default_session):
    diode = default_session.config.get("diode")
    roby = default_session.config.get("roby")

    class Preset1(ScanPreset):
        def stop(self, scan):
            raise BufferError()

    s = scans.ascan(roby, 0, 1, 3, 0.1, diode, save=False, run=False)
    p = Preset1()
    s.add_preset(p)
    scan_task = gevent.spawn(s.run)
    scan_task.join()
    assert s.state == ScanState.KILLED
    assert s._scan_data.state == BlissdataScanState.CLOSED
    assert s._scan_data.info["end_reason"] == "FAILURE"

    class Preset2(ScanPreset):
        def start(self, scan):
            raise BufferError()

    s = scans.ascan(roby, 0, 1, 3, 0.1, diode, save=False, run=False)
    p = Preset2()
    s.add_preset(p)
    scan_task = gevent.spawn(s.run)
    scan_task.join()
    assert s.state == ScanState.KILLED
    assert s._scan_data.state == BlissdataScanState.CLOSED
    assert s._scan_data.info["end_reason"] == "FAILURE"

    class Preset3(ScanPreset):
        def prepare(self, scan):
            raise BufferError()

    s = scans.ascan(roby, 0, 1, 3, 0.1, diode, save=False, run=False)
    p = Preset3()
    s.add_preset(p)
    scan_task = gevent.spawn(s.run)
    scan_task.join()
    assert s.state == ScanState.KILLED
    assert s._scan_data.state == BlissdataScanState.CLOSED
    assert s._scan_data.info["end_reason"] == "FAILURE"


def test_exception_in_dscan_move_back(default_session):
    diode = default_session.config.get("diode")
    bad = default_session.config.get("bad")
    bad.move(0)
    s = scans.dscan(bad, 1, 2, 1, 0.1, diode, save=False, run=False)

    def patched_move(*args):
        bad.controller.bad_start = True
        _move(*args)

    with mock.patch("bliss.common.standard._move", new=patched_move):
        with pytest.raises(RuntimeError):
            s.run()

    assert bad.position > 1
    assert bad.state.READY


def test_sequence_state(default_session):
    diode = default_session.config.get("diode")
    roby = default_session.config.get("roby")

    seq = Sequence()

    def task():
        class Preset1(ScanPreset):
            def stop(self, scan):
                raise BufferError()

        with seq.sequence_context() as scan_seq:
            s1 = scans.ascan(roby, 0, 1, 3, 0.1, diode, save=False, run=False)
            scan_seq.add_and_run(s1)
            s = scans.ascan(roby, 0, 1, 3, 0.1, diode, save=False, run=False)
            p = Preset1()
            s.add_preset(p)
            scan_seq.add_and_run(s)

    scan_task = gevent.spawn(task)
    scan_task.join()

    assert seq.scan.state == ScanState.KILLED
    assert seq.scan._scan_data.state == BlissdataScanState.CLOSED
    assert seq.scan._scan_data.info["end_reason"] == "FAILURE"


@pytest.mark.parametrize(
    "first_iteration,preset",
    [(True, False), (False, False), (True, True), (False, True)],
)
def test_exception_in_start_and_stop_and_preset(session, first_iteration, preset):
    class Master(AcquisitionMaster):
        name = "bla"

        def __iter__(self):
            self.it = 0
            for i in range(3):
                yield self
                self.it += 1

        def prepare(self):
            pass

        def start(self):
            if first_iteration or self.it > 1:
                1 / 0

        def stop(self):
            raise RuntimeError()

    class Preset(ScanPreset):
        def stop(self, scan):
            raise BufferError()

    m = Master()
    c = AcquisitionChain()
    c.add(m)
    s = Scan(c)
    if preset:
        p = Preset()
        s.add_preset(p)
    try:
        with gevent.Timeout(3):
            s.run()
    except Exception as e:
        assert isinstance(e, ZeroDivisionError)
    else:
        assert False


def test_issue_3383(session, nexus_writer_service, capsys):
    diode = session.config.get("diode")
    session.scan_saving.writer = "nexus"
    ct_scan = sct(0.001, diode, run=False)

    with mock.patch.object(
        ct_scan,
        "_execute_scan_runner",
        side_effect=RuntimeError("Failed to execute scan"),
    ):
        with pytest.raises(RuntimeError):
            ct_scan.run()
        _, stderr = capsys.readouterr()
        assert "No writer for scan ''" not in str(stderr)
