# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
import gevent.event
import pytest
import numpy as np
from unittest import mock

from bliss.common import scans
from bliss.common.data_store import get_default_data_store
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave
from bliss.scanning.channel import AcquisitionChannel
from bliss.scanning.group import Sequence, ScanSequenceError


def test_sequence_terminated_scans(session):
    diode = session.config.get("diode")
    seq = Sequence()
    with seq.sequence_context() as seq_context:
        seq_context.add(scans.loopscan(3, 0.1, diode, name="foo"))
        seq_context.add(scans.loopscan(3, 0.2, diode, name="bar"))

    # Query blissdata to find the sequence and ensure it is published
    data_store = get_default_data_store()
    _, key = data_store.get_next_scan(block=False, since=0)
    sequence_scan = data_store.load_scan(key)
    assert sequence_scan.info["title"] == "sequence_of_scans"
    assert "SUBSCANS" in sequence_scan.streams

    subscans_stream = sequence_scan.streams["SUBSCANS"]
    subscans = [data_store.load_scan(entry["key"]) for entry in subscans_stream]
    assert len(subscans) == 2
    assert subscans[0].name == "foo"
    assert subscans[1].name == "bar"


def test_sequence_future_scans(session):
    diode = session.config.get("diode")
    seq = Sequence()
    with seq.sequence_context() as seq_context:
        s1 = scans.loopscan(3, 0.1, diode, run=False, name="foo")
        seq_context.add(s1)
        s1.run()
        s2 = scans.loopscan(3, 0.2, diode, run=False, name="bar")
        seq_context.add_and_run(s2)

    data_store = get_default_data_store()
    _, key = data_store.get_next_scan(block=False, since=0)
    sequence_scan = data_store.load_scan(key)
    assert sequence_scan.info["title"] == "sequence_of_scans"
    assert "SUBSCANS" in sequence_scan.streams

    subscans_stream = sequence_scan.streams["SUBSCANS"]
    subscans = [data_store.load_scan(entry["key"]) for entry in subscans_stream]
    assert len(subscans) == 2
    assert subscans[0].name == "foo"
    assert subscans_stream[0]["scan_number"] == s1.scan_info["scan_nb"]
    assert subscans[0].info["scan_nb"] == s1.scan_info["scan_nb"]
    assert subscans[1].name == "bar"
    assert subscans_stream[1]["scan_number"] == s2.scan_info["scan_nb"]
    assert subscans[1].info["scan_nb"] == s2.scan_info["scan_nb"]


def test_sequence_async_scans(session):
    diode = session.config.get("diode")
    sim_ct_gauss = session.config.get("sim_ct_gauss")
    robz = session.config.get("robz")

    # test that wait_all_subscans works
    seq = Sequence()
    with seq.sequence_context() as seq_context:
        s1 = scans.loopscan(20, 0.1, diode, run=False)
        s2 = scans.ascan(robz, 0, 1, 20, 0.1, sim_ct_gauss, run=False)
        seq_context.add(s1)
        seq_context.add(s2)
        g1 = gevent.spawn(s1.run)
        g2 = gevent.spawn(s2.run)

        gevent.sleep(0)
        seq_context.wait_all_subscans()

    gevent.joinall([g1, g2], raise_error=True)

    # test that gevent.join is sufficent
    seq = Sequence()
    with seq.sequence_context() as seq_context:
        s1 = scans.loopscan(20, 0.1, diode, run=False)
        s2 = scans.ascan(robz, 0, 1, 20, 0.1, sim_ct_gauss, run=False)
        seq_context.add(s1)
        seq_context.add(s2)
        g1 = gevent.spawn(s1.run)
        g2 = gevent.spawn(s2.run)

        gevent.joinall([g1, g2], raise_error=True)


def test_sequence_non_started_scans_in_seq(session):
    diode = session.config.get("diode")

    with pytest.raises(ScanSequenceError):
        seq = Sequence()
        with seq.sequence_context() as seq_context:
            s0 = scans.loopscan(1, 0.1, diode)
            seq_context.add(s0)
            s1 = scans.loopscan(20, 0.1, diode, run=False)
            seq_context.add(s1)


def test_sequence_empty_in_seq(session):
    diode = session.config.get("diode")
    seq = Sequence()
    with seq.sequence_context() as seq_context:
        pass

    with pytest.raises(ScanSequenceError):
        seq = Sequence()
        with seq.sequence_context() as seq_context:
            s1 = scans.loopscan(20, 0.1, diode, run=False)
            seq_context.add(s1)


def test_sequence_custom_channel(session):
    diode = session.config.get("diode")
    seq = Sequence(scan_info={"something": "else"})
    seq.add_custom_channel(AcquisitionChannel("mychannel", float, ()))
    with seq.sequence_context() as seq_context:
        s1 = scans.loopscan(3, 0.1, diode, run=False)
        seq_context.add(s1)
        seq.custom_channels["mychannel"].emit(1.1)
        s1.run()
        seq.custom_channels["mychannel"].emit([2.2, 3.3])
        s2 = scans.loopscan(3, 0.05, diode)
        seq_context.add(s2)
        seq.custom_channels["mychannel"].emit([4.4])

    data_store = get_default_data_store()
    _, key = data_store.get_next_scan(block=False, since=0)
    sequence_scan = data_store.load_scan(key)
    assert sequence_scan.info["title"] == "sequence_of_scans"
    assert sequence_scan.info["something"] == "else"

    assert {"SUBSCANS", "mychannel"} == sequence_scan.streams.keys()
    assert np.array_equal(sequence_scan.streams["mychannel"][:], [1.1, 2.2, 3.3, 4.4])

    subscans_stream = sequence_scan.streams["SUBSCANS"]
    subscans = [data_store.load_scan(entry["key"]) for entry in subscans_stream]
    assert len(subscans) == 2
    assert subscans[0].info["scan_nb"] == s1.scan_info["scan_nb"]
    assert subscans[1].info["scan_nb"] == s2.scan_info["scan_nb"]


def test_sequence_add_and_run(session):
    diode = session.config.get("diode")

    seq = Sequence()
    with seq.sequence_context() as seq_context:
        s0 = scans.loopscan(1, 0.1, diode, run=False)
        seq_context.add_and_run(s0)

    with pytest.raises(ScanSequenceError):
        seq = Sequence()
        with seq.sequence_context() as seq_context:
            s0 = scans.loopscan(1, 0.1, diode, run=False)
            seq_context.add_and_run(s0)
            s1 = scans.loopscan(1, 0.1, diode)
            seq_context.add_and_run(s1)


def test_sequence_missing_events(session):
    # Test for issue #2423
    diode = session.config.get("diode")

    with gevent.Timeout(10):
        seq = Sequence()
        with pytest.raises(ScanSequenceError):
            with seq.sequence_context() as seq_context:
                # Stop publishing sequence events to check that
                # ScanSequenceError is raised upon exiting the context
                seq_context.sequence.group_acq_master.scan_queue.put(StopIteration)
                # Add a scan so that some sequence events are expected
                seq_context.add(scans.loopscan(3, 0.1, diode))

    with gevent.Timeout(10):
        seq = Sequence()
        with seq.sequence_context() as seq_context:
            # Stop publishing sequence events to check that
            # ScanSequenceError is raised upon exiting the context
            seq_context.sequence.group_acq_master.scan_queue.put(StopIteration)
            # Do not add scans so no sequence events are expected


def test_sequence_issue_2752(session, scan_meta):
    seq = Sequence()

    def my_metadata_generator(scan):
        gevent.sleep(4)  # more than 3 seconds

    scan_meta.clear()
    scan_meta.add_categories(["my_metadata_category"])
    scan_meta.my_metadata_category.set("my_metadata", my_metadata_generator)

    with gevent.Timeout(10):
        with seq.sequence_context():
            pass

    seq = Sequence()
    scan_meta.clear()
    with mock.patch.object(seq.scan, "_runctx_scan_data", return_value=AssertionError):
        with pytest.raises(RuntimeError, match="Failed to prepare scan sequence"):
            with seq.sequence_context():
                pass


def test_sequence_create_scan_before_running(session):

    diode = session.config.get("diode")

    seq = Sequence()
    scan = seq.scan
    calc = CalcChannelAcquisitionSlave("", [], None, [])
    scan.acq_chain.add(seq.group_acq_master, calc)

    nodes_before_running = scan.acq_chain.nodes_list

    with seq.sequence_context() as seq_context:
        s1 = scans.loopscan(20, 0.1, diode, run=False)
        seq_context.add(s1)
        g1 = gevent.spawn(s1.run)

        gevent.joinall([g1], raise_error=True)

    nodes_after_running = seq.scan.acq_chain.nodes_list

    # check that scan sequence does not have been recreated with sequence context
    assert nodes_before_running == nodes_after_running
