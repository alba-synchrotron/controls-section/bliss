# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


from bliss.common.scans import ascan
from bliss.controllers.simulation_diode import simulation_diode


def test_integrating_acquisition_slave_without_mastercc(default_session):
    # verify that IntegratingCounterAcquisitionSlave without master counter controller
    # still obtains its parent when put in the acquisition chain
    diode = simulation_diode("diode", {"integration": True})
    roby = default_session.config.get("roby")
    s = ascan(roby, 0, 1, 2, 0.01, diode)
    dao = s.acq_chain.nodes_list[2]
    assert dao.name == "simulation_diode_integrating_controller"
    assert dao.parent is s.acq_chain.nodes_list[1]
    # roby.tolerance == 0.0001
    assert s.scan_info["channels"]["axis:roby"]["decimals"] == 6
