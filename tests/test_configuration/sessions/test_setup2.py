from bliss.shell.standard import *  # noqa: F403
from bliss import current_session
from bliss import is_bliss_shell

# Do not remove this print (used in tests)
print("TEST_SESSION2 INITIALIZED")

load_script("script1")  # noqa: F405

if is_bliss_shell():
    protect("toto")  # noqa: F405

var1 = 1
var2 = 2

if is_bliss_shell():
    protect(["var1", "var2"])  # noqa: F405

user_script_homedir(current_session.config.db_path)  # noqa: F405
user_script_load("sessions/scripts/script6", export_global="mc")  # noqa: F405
user_script_load("sessions/scripts/script7", export_global="mc")  # noqa: F405
