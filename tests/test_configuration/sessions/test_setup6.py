from bliss import current_session
from bliss.session.test_session6.script4 import timescan

assert SCAN_SAVING.__class__.__name__ == "ESRFScanSaving"  # noqa F821

current_session.disable_esrf_data_policy()

assert SCAN_SAVING.__class__.__name__ != "ESRFScanSaving"  # noqa F821

assert timescan() is None

# check basic functions are accessible without import
load_script("script5")  # noqa F821

# check config objects are accessible without import
assert roby.name == "roby"  # noqa F821
