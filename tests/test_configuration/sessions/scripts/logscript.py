print("Executing logscript.py ...")

from bliss.common import logtools  # noqa E402


class LogScriptDummy:
    def __init__(self):
        logtools.log_error(self, "logscript.py: Beacon error")


logtools.elog_error("logscript.py: E-logbook error")
LogScriptDummy()

scriptfinished = True

print("End of logscript.py.")
