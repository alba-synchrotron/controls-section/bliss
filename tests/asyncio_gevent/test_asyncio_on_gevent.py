import asyncio  # noqa: E402
import gevent  # noqa: E402
import pytest

from bliss.common.greenlet_utils import asyncio_gevent

# test_asyncio_on_gevent_will_run_in_dummy_thread


def current_greenlet_is_main():
    # cannot use gevent.getcurrent() is gevent.get_hub() because
    # gevent.getcurrent() can return gevent.Greenlet or greenlet.greenlet objects
    return gevent.getcurrent().parent is None


def test_asyncio_on_gevent_will_run_in_dummy_thread_with_asyncio_run():
    assert current_greenlet_is_main()

    result = asyncio.run(asyncio_on_gevent_will_run_in_dummy_thread_async(None))
    assert result == 42


def test_asyncio_on_gevent_will_run_in_dummy_thread_with_future_to_greenlet():
    assert current_greenlet_is_main()
    greenlet = asyncio_gevent.future_to_greenlet(
        asyncio_on_gevent_will_run_in_dummy_thread_async(gevent.get_hub()),
    )
    result = greenlet.get()
    assert result == 42


async def asyncio_on_gevent_will_run_in_dummy_thread_async(parent):
    assert gevent.getcurrent().parent is parent
    return 42


# test_asyncio_on_gevent_supports_nested_async_calls


def test_asyncio_on_gevent_supports_nested_async_calls_with_asyncio_run():
    result = asyncio.run(asyncio_on_gevent_supports_nested_async_calls_1())
    assert result == 42


def test_asyncio_on_gevent_supports_nested_async_calls_with_future_to_greenlet():
    greenlet = asyncio_gevent.future_to_greenlet(
        asyncio_on_gevent_supports_nested_async_calls_1()
    )
    greenlet.start()
    greenlet.join()
    result = greenlet.get()
    assert result == 42


async def asyncio_on_gevent_supports_nested_async_calls_1():
    result = await asyncio_on_gevent_supports_nested_async_calls_2()
    assert result == 100
    return 42


async def asyncio_on_gevent_supports_nested_async_calls_2():
    result = await asyncio_on_gevent_supports_nested_async_calls_3()
    assert result == "it works"
    return 100


async def asyncio_on_gevent_supports_nested_async_calls_3():
    await asyncio.sleep(1)
    return "it works"


# test asyncio_on_gevent_supports_awaiting_greenlets


def test_wrap_greenlet_into_future():
    def task():
        gevent.sleep(0.1)
        return 42

    async def async_main():
        # wrap greenlet before it starts
        greenlet = gevent.Greenlet(task)
        future = asyncio_gevent.greenlet_to_future(greenlet)

        greenlet.start()
        result = await future
        assert not greenlet
        assert result == 42
        assert greenlet.get() == 42

    # need an async context to await future
    asyncio.run(async_main())


def test_wrap_started_greenlet_into_future():
    # Wrapping should not be possible on a started greenlet, even if it was not
    # scheduled yet. If authorized, an IO between spawn and the wrapping can
    # prevent it to apply correctly, leaving exceptions like KeyboardInterrupt
    # not handled expectingly.

    # greenlet should no be started
    greenlet = gevent.Greenlet(gevent.sleep, 10)
    greenlet.start()
    with pytest.raises(RuntimeError) as exc_info:
        _ = asyncio_gevent.greenlet_to_future(greenlet)
    assert "greenlet has already started" in str(exc_info.value)
    greenlet.kill()
    greenlet.join()

    # greenlet should no be spawned
    greenlet = gevent.spawn(gevent.sleep, 10)
    with pytest.raises(RuntimeError) as exc_info:
        _ = asyncio_gevent.greenlet_to_future(greenlet)
    assert "greenlet has already started" in str(exc_info.value)
    greenlet.kill()
    greenlet.join()

    # greenlet should no be dead
    greenlet = gevent.Greenlet(gevent.sleep, 10)
    greenlet.start()
    greenlet.kill()
    greenlet.join()
    with pytest.raises(RuntimeError) as exc_info:
        _ = asyncio_gevent.greenlet_to_future(greenlet)
    assert "greenlet has already started" in str(exc_info.value)
