# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.common import logtools

__all__ = ["LogInitController"]


class LogInitController:
    def __init__(self, name, config):
        logtools.elog_error("LogInitController: E-logbook error")
        logtools.log_error(self, "LogInitController: Beacon error")
