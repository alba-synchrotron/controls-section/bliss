from bliss.common.scans import ct, loopscan, ascan
from bliss.controllers.lima.limatools import limatake
from bliss.controllers.test.intermediate_controller import IntermediateController
from bliss.common.scans.step_by_step import DEFAULT_CHAIN


def dump_scan_info(config):
    """Dump scan info from various scans"""

    def save_scan_info(s, name):
        from ruamel.yaml import YAML

        filename = f"tests/qt/flint/scans_description/{name}.yaml"
        with open(filename, "wt") as f:
            yaml = YAML(pure=True)
            # yaml.default_flow_style = False
            scan_info = dict(s.scan_info)
            scan_info.pop("nexuswriter", None)
            scan_info.pop("positioners", None)
            scan_info.pop("images_path", None)
            scan_info.pop("filename", None)
            scan_info.pop("writer_options", None)
            scan_info.pop("dataset_metadata_snapshot", None)
            scan_info.pop("instrument", None)
            scan_info["start_time"] = "2023-12-01T19:00:00.000000+01:00"
            scan_info["end_time"] = "2023-12-01T19:00:01.000000+01:00"
            scan_info["user_name"] = "maurice"
            yaml.dump(scan_info, stream=f)

    beamviewer = config.get("beamviewer")
    sx = config.get("sx")
    diode1 = config.get("diode1")
    diode2 = config.get("diode2")
    legacy_mca = config.get("mca1")

    s = limatake(1)
    save_scan_info(s, "limatake")
    s = ct(1, beamviewer)
    save_scan_info(s, "ct_in_lima")
    s = loopscan(10, 0.1, beamviewer)
    save_scan_info(s, "loopscan_on_lima")
    s = loopscan(10, 0.1, beamviewer.image)
    save_scan_info(s, "loopscan_on_lima_image")
    s = ascan(sx, 0.1, 0.2, 10, 0.1, beamviewer)
    save_scan_info(s, "ascan_lima")

    s = loopscan(10, 0.1, diode1, run=True, name="foo")
    save_scan_info(s, "loopscan_on_diode1")
    s = loopscan(10, 0.1, diode2, run=True, name="bar")
    save_scan_info(s, "loopscan_on_diode2")

    mymaster = IntermediateController("mymaster", {})
    DEFAULT_CHAIN.set_settings([{"device": beamviewer, "master": mymaster}])
    s = loopscan(10, 0.1, beamviewer)
    DEFAULT_CHAIN.clear()
    save_scan_info(s, "loopscan_on_external_lima")

    s = loopscan(10, 0.1, legacy_mca)
    save_scan_info(s, "loopscan_on_legacy_mca")

    mymaster = IntermediateController("mymaster", {})
    DEFAULT_CHAIN.set_settings(
        [
            {
                "device": legacy_mca,
                "acquisition_settings": {
                    "start_once": False,
                    "prepare_once": False,
                    "npoints": 1,
                },
                "master": mymaster,
            }
        ]
    )
    s = loopscan(10, 0.1, legacy_mca)
    DEFAULT_CHAIN.clear()
    save_scan_info(s, "loopscan_on_external_legacy_mca")
