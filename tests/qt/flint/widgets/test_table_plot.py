"""Testing table plot widget."""

import pytest
import numpy
from bliss.flint.widgets.table_plot import TablePlotWidget
from bliss.flint.model import scan_model
from bliss.flint.model import flint_model


def create_flint_model():
    flint = flint_model.FlintState()
    return flint


@pytest.fixture
def tablePlotWidget(local_flint):
    widget = TablePlotWidget()
    flint = create_flint_model()
    widget.setFlintModel(flint)
    yield widget
    widget.close()


def create_scan():
    scan = scan_model.Scan()
    master = scan_model.Device(scan)
    master.setName("master")
    device = scan_model.Device(scan)
    device.setName("device")
    device.setMaster(master)
    channel = scan_model.Channel(device)
    channel.setName("epoch")
    channel.setType(scan_model.ChannelType.COUNTER)
    channel = scan_model.Channel(device)
    channel.setName("axis:chan2")
    channel.setType(scan_model.ChannelType.COUNTER)
    channel = scan_model.Channel(device)
    channel.setName("chan1")
    channel.setType(scan_model.ChannelType.COUNTER)
    scan.seal()
    return scan


def test_display(silxTestUtilsModule, tablePlotWidget):
    """Show a plot with already existing data"""
    scan = create_scan()
    tablePlotWidget.setScan(scan)
    silxTestUtilsModule.qWaitForWindowExposed(tablePlotWidget, timeout=200)


def test_update_scan(silxTestUtilsModule, tablePlotWidget):
    """Show a plot and then update the data"""
    silxTestUtilsModule.qWaitForWindowExposed(tablePlotWidget, timeout=200)
    scan = create_scan()
    tablePlotWidget.setScan(scan)
    silxTestUtilsModule.qWait(500)


def test_update_data(silxTestUtilsModule, tablePlotWidget):
    """Show a plot and then update the data"""
    silxTestUtilsModule.qWaitForWindowExposed(tablePlotWidget, timeout=200)
    scan = create_scan()
    tablePlotWidget.setScan(scan)
    scan.getChannelByName("epoch").setData(scan_model.Data(scan, numpy.array([10])))
    scan.getChannelByName("axis:chan2").setData(
        scan_model.Data(scan, numpy.array([20]))
    )
    scan.getChannelByName("chan1").setData(scan_model.Data(scan, numpy.array([30])))
    silxTestUtilsModule.qWait(500)
