"""Testing for profile action"""

import pytest
import numpy
from bliss.flint.model import scan_model
from bliss.flint.viewers.live_image.stages import flat_field_stage


def test_correction():
    correction = flat_field_stage.FlatFieldStage()
    flat = numpy.array([[2, 2, 2, 3]])
    dark = numpy.array([[1, 1, 2, 1]])
    data = numpy.array([[11, 21, 11, 11]])
    correction.setDark(dark, 1)
    correction.setFlat(flat, 1)
    result = correction.correction(data, exposureTime=1)
    expected = numpy.array([[10, 20, 0, 5]])
    numpy.testing.assert_almost_equal(result, expected)


def test_dark_correction():
    correction = flat_field_stage.FlatFieldStage()
    flat = numpy.array([[2, 2, 2, 3]])
    dark = numpy.array([[1, 1, 2, 1]])
    data = numpy.array([[11, 21, 11, 11]])
    correction.setDark(dark, 1)
    correction.setFlat(flat, 1)
    result = correction.correction(data, exposureTime=1, use_flat=False)
    expected = numpy.array([[10, 20, 9, 10]])
    numpy.testing.assert_almost_equal(result, expected)


def test_exposure():
    correction = flat_field_stage.FlatFieldStage()
    flat = numpy.array([[1, 1, 1, 1.5]])
    dark = numpy.array([[0.25, 0.25, 0.5, 0.25]])
    data = numpy.array([[22, 42, 22, 22]])
    correction.setDark(dark, 0.25)
    correction.setFlat(flat, 0.5)
    result = correction.correction(data, exposureTime=2)
    expected = numpy.array([[10, 20, 0, 5]])
    numpy.testing.assert_almost_equal(result, expected)


def test_wrong_dark_shape():
    correction = flat_field_stage.FlatFieldStage()
    flat = numpy.array([[2, 2, 0, 3]])
    dark = numpy.array([[1, 1, 2]])
    data = numpy.array([[20, 40, 11, 15]])
    correction.setDark(dark, 1)
    correction.setFlat(flat, 1)
    result = correction.correction(data, exposureTime=1)
    expected = numpy.array([[10, 20, 0, 5]])
    numpy.testing.assert_almost_equal(result, expected)


def test_wrong_flat_shape():
    correction = flat_field_stage.FlatFieldStage()
    flat = numpy.array([[2, 2, 0]])
    dark = numpy.array([[1, 1, 11, 1]])
    data = numpy.array([[11, 21, 11, 6]])
    correction.setDark(dark, 1)
    correction.setFlat(flat, 1)
    result = correction.correction(data, exposureTime=1)
    expected = numpy.array([[10, 20, 0, 5]])
    numpy.testing.assert_almost_equal(result, expected)


def test_read_tomo_flat_scan():
    scan = scan_model.Scan()
    scanInfo = {
        "technique": {"flat": {"exposure_time": 2.0, "exposure_time@units": "s"}}
    }
    scan.setScanInfo(scanInfo)
    correction = flat_field_stage.FlatFieldStage()
    data = numpy.array([[11, 21, 11, 6]])
    correction.captureRawDetector(scan, data)
    assert correction.flat().exposureTime == 2


def test_read_tomo_dark_scan():
    scan = scan_model.Scan()
    scanInfo = {
        "technique": {"dark": {"exposure_time": 2.0, "exposure_time@units": "s"}}
    }
    scan.setScanInfo(scanInfo)
    correction = flat_field_stage.FlatFieldStage()
    data = numpy.array([[11, 21, 11, 6]])
    correction.captureRawDetector(scan, data)
    assert correction.dark().exposureTime == 2


@pytest.mark.skip(reason="For profiling")
def test_timeit():
    """Make sure numexpr implementation is still faster"""
    size = 1024, 1024
    flat = numpy.random.rand(*size) + 2
    dark = numpy.random.rand(*size)
    data = numpy.random.rand(*size)

    def patch(index, flat_n, dark_n, data_n):
        flat[0, index] = flat_n
        dark[0, index] = dark_n
        data[0, index] = data_n

    patch(0, 2, 1, 3)
    patch(1, 0, 0, 3)

    correction = flat_field_stage.FlatFieldStage()
    correction.setDark(dark, 1)
    correction.setFlat(flat, 1)

    import timeit

    def f():
        correction.correction(data, 1)

    # cache the numexpr function
    f()

    correction.USE_NUMEXPR = False
    ts1 = timeit.repeat(f, number=10, repeat=7)
    correction.USE_NUMEXPR = True
    ts2 = timeit.repeat(f, number=10, repeat=7)

    # exclude outliers
    t1 = numpy.mean(sorted(ts1)[2:-2])
    t2 = numpy.mean(sorted(ts2)[2:-2])
    print("Numexpr is", t1 / t2, "time faster")
    assert t2 < t1
