"""Testing image plot."""

from silx.gui.plot import PlotWindow
from bliss.flint.model import flint_model
from bliss.flint.manager.manager import ManageMainBehaviours
from bliss.flint.widgets.viewer.actions import export_action


def create_flint_model():
    flint = flint_model.FlintState()
    return flint


def create_manager():
    manager = ManageMainBehaviours()
    flintModel = create_flint_model()
    manager.setFlintModel(flintModel)
    return manager


def test_logbook(local_flint, beacon, icat_mock_client):
    manager = create_manager()
    plot = PlotWindow()
    action = export_action.ExportToLogBookAction(plot, plot)
    action.setFlintModel(manager.flintModel())

    # The action is not available
    assert not action.isEnabled()

    # The action is now available
    kwargs = dict()
    kwargs["proposal"] = "id001234"
    kwargs["beamline"] = "id00"
    config = {"disable": False, "kwargs": kwargs}
    manager.setIcatClientConfig(config)
    assert action.isEnabled()

    # The action can use it
    action.trigger()

    action.deleteLater()
    plot.deleteLater()

    icat_mock_client.assert_called_once_with(beamline="id00", proposal="id001234")
    icat_mock_client.return_value.send_binary_data.assert_called_once()
    kwargs = icat_mock_client.return_value.send_binary_data.call_args[-1]
    assert {"data", "mimetype"} == set(kwargs)
    assert kwargs["mimetype"] == "image/png"
    assert b"PNG" in kwargs["data"]


def test_logbook_send_data(local_flint):
    class IcatClientMockup:
        def __init__(self):
            self.data = None
            self.mimetype = None

        def send_binary_data(self, data: bytes, mimetype: str):
            self.data = data
            self.mimetype = mimetype

    model = create_flint_model()
    client = IcatClientMockup()
    model.setIcatClient(client)
    plot = PlotWindow()
    action = export_action.ExportToLogBookAction(plot, plot)
    action.setFlintModel(model)

    assert action.isEnabled()
    action.trigger()

    assert b"PNG" in client.data
    assert client.mimetype == "image/png"

    action.deleteLater()
    plot.deleteLater()
