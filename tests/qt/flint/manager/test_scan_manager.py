"""Testing scan_manager module."""

import numpy
from bliss.flint.manager import scan_manager
from blissdata.lima.image_utils import ImageData
from tests.qt.flint.factory import ScanInfoFactory
from blissdata.lima.client import LimaClient


def _create_scan_info_1():
    factory = ScanInfoFactory()
    factory.add_device(root_id="main", device_id="master")
    factory.add_channel(channel_id="axis:roby", device_id="master", dim=0)
    factory.add_device(root_id="main", device_id="slave", triggered_by="master")
    factory.add_channel(
        channel_id="timer:elapsed_time", device_id="slave", dim=0, unit="s"
    )
    return factory.scan_info()


def _create_scan_info_2():
    factory = ScanInfoFactory()
    factory.add_device(root_id="main", device_id="master")
    factory.add_channel(channel_id="axis:robz", device_id="master", dim=0)
    factory.add_device(root_id="main", device_id="slave", triggered_by="master")
    factory.add_channel(
        channel_id="timer:elapsed_time", device_id="slave", dim=0, unit="s"
    )
    return factory.scan_info()


def _create_scan_info_3():
    factory = ScanInfoFactory()
    factory.add_device(root_id="main", device_id="master")
    factory.add_channel(channel_id="lima:image", device_id="master", dim=2)
    return factory.scan_info()


class MockedScanManager(scan_manager.ScanManager):
    def emit_scan_started(self, scan_key, scan_info):
        self.on_scan_started(scan_key, scan_info)

    def emit_scan_finished(self, scan_key, scan_info):
        self.on_scan_finished(scan_key, scan_info)

    def emit_scalar_updated(self, scan_key, scan_info, channel_name, index, data):
        self.on_scalar_data_received(scan_key, channel_name, index, data)

    def emit_lima_event_updated(
        self, scan_key, scan_info, channel_name, last_index, lima_client
    ):
        self.on_lima_event_received(scan_key, channel_name, last_index, lima_client)


def _create_scan_info(base_scan_info):
    scan_info = {}
    scan_info.update(base_scan_info)
    return scan_info


def test_interleaved_scans():
    key_1 = "esrf:scan:AAAA"
    scan_info_1 = _create_scan_info_1()
    key_2 = "esrf:scan:BBBB"
    scan_info_2 = _create_scan_info_2()

    manager = MockedScanManager(flintModel=None)
    # Disabled async consumption

    scans = manager.get_alive_scans()
    assert len(scans) == 0

    manager.emit_scan_started(key_1, scan_info_1)
    scans = manager.get_alive_scans()
    assert len(scans) == 1
    assert scans[0].scanInfo() == scan_info_1

    manager.emit_scan_started(key_2, scan_info_2)
    manager.emit_scalar_updated(key_1, scan_info_1, "axis:roby", 0, numpy.arange(2))
    manager.emit_scalar_updated(key_2, scan_info_2, "axis:robz", 0, numpy.arange(3))
    manager.wait_data_processed()
    scans = manager.get_alive_scans()
    assert len(scans) == 2

    manager.emit_scan_finished(key_1, scan_info_1)
    scans = manager.get_alive_scans()
    assert len(scans) == 1
    assert scans[0].scanInfo() == scan_info_2

    manager.emit_scan_finished(key_2, scan_info_2)
    scans = manager.get_alive_scans()
    assert len(scans) == 0


def test_sequencial_scans():
    key_1 = "esrf:scan:AAAA"
    scan_info_1 = _create_scan_info_1()
    key_2 = "esrf:scan:BBBB"
    scan_info_2 = _create_scan_info_2()

    manager = MockedScanManager(flintModel=None)

    manager.emit_scan_started(key_1, scan_info_1)
    manager.emit_scalar_updated(key_1, scan_info_1, "axis:roby", 0, numpy.arange(2))
    manager.wait_data_processed()
    scans = manager.get_alive_scans()
    assert len(scans) == 1
    manager.emit_scan_finished(key_1, scan_info_1)
    assert manager.get_alive_scans() == []
    assert scans[0].scanInfo() == scan_info_1

    manager.emit_scan_started(key_2, scan_info_2)
    manager.emit_scalar_updated(key_2, scan_info_2, "axis:robz", 0, numpy.arange(3))
    manager.wait_data_processed()
    scans = manager.get_alive_scans()
    assert len(scans) == 1
    manager.emit_scan_finished(key_2, scan_info_2)
    assert manager.get_alive_scans() == []
    assert scans[0].scanInfo() == scan_info_2


def test_bad_sequence__end_before_new():
    key_1 = "esrf:scan:AAAA"
    scan_info_1 = _create_scan_info_1()
    manager = MockedScanManager(flintModel=None)

    manager.emit_scan_finished(key_1, scan_info_1)
    manager.emit_scan_started(key_1, scan_info_1)
    # FIXME What to do anyway then? The manager is locked


class MockedLimaClient(LimaClient):
    def __init__(self, len=None, last_live_image=None):
        self.len = len
        self.last_live_image = last_live_image

    def __len__(self):
        return self.len

    def get_last_live_image(self):
        if isinstance(self.last_live_image, Exception):
            raise self.last_live_image
        return ImageData(self.last_live_image, self.len - 1)


def test_image__default():
    key_3 = "esrf:scan:CCCC"
    scan_info_3 = _create_scan_info_3()

    manager = MockedScanManager(flintModel=None)

    manager.emit_scan_started(key_3, scan_info_3)
    scan = manager.get_alive_scans()[0]

    image = numpy.arange(1).reshape(1, 1)
    lima_client = MockedLimaClient(len=3, last_live_image=image)
    manager.emit_lima_event_updated(
        key_3, scan_info_3, "lima:image", last_index=2, lima_client=lima_client
    )

    manager.emit_scan_finished(key_3, scan_info_3)

    result = scan.getChannelByName("lima:image").data()
    assert result.frameId() == 2
    assert result.array().shape == (1, 1)


def test_prefered_user_refresh():
    key_3 = "esrf:scan:CCCC"
    scan_info_3 = _create_scan_info_3()

    manager = MockedScanManager(flintModel=None)

    manager.emit_scan_started(key_3, scan_info_3)
    scan = manager.get_alive_scans()[0]
    channel = scan.getChannelByName("lima:image")
    channel.setPreferedRefreshRate("foo", 500)

    image = numpy.arange(1).reshape(1, 1)
    lima_client = MockedLimaClient(len=0, last_live_image=image)

    for i in range(10):
        lima_client.len = i + 1
        manager.emit_lima_event_updated(
            key_3, scan_info_3, "lima:image", i, lima_client
        )

    manager.emit_scan_finished(key_3, scan_info_3)

    # The first end the last
    assert channel.updatedCount() == 2
    # The last is there
    assert channel.data().frameId() == 9


def test_scalar_data_lost():
    key_1 = "esrf:scan:AAAA"
    scan_info_1 = _create_scan_info_1()

    manager = MockedScanManager(flintModel=None)
    # Disabled async consumption

    manager.emit_scan_started(key_1, scan_info_1)
    scans = manager.get_alive_scans()
    assert len(scans) == 1
    assert scans[0].scanInfo() == scan_info_1

    manager.on_scalar_data_received(key_1, "axis:roby", 0, numpy.array([1, 2, 3, 4]))

    manager.on_scalar_data_received(key_1, "axis:roby", 6, numpy.array([5, 6, 7, 8]))

    manager.wait_data_processed()

    manager.on_scan_finished(key_1, scan_info_1)

    scan = scans[0]
    channel = scan.getChannelByName("axis:roby")
    array = channel.data().array()
    numpy.testing.assert_array_equal(
        array, [1, 2, 3, 4, numpy.nan, numpy.nan, 5, 6, 7, 8]
    )
