"""Testing business model when a curve plot is updated."""

from __future__ import annotations

from bliss.flint.manager import curve_plot_update
from bliss.flint.model import plot_item_model
from bliss.flint.helper import model_helper
from bliss.flint.scan_info_parser.plots import create_plot_model
from bliss.flint.scan_info_parser.scans import create_scan_model
from tests.qt.flint.factory import ScanInfoFactory
from . import scan_examples


def get_scan_and_plot_from_scan_info(scan_info):
    scan = create_scan_model(scan_info)
    plots = create_plot_model(scan_info, scan)
    plot = [p for p in plots if isinstance(p, plot_item_model.CurvePlot)][0]
    return scan, plot


def test_ascan_axis_updated(local_flint):
    """
    Test plot state with consecutive scans

    - Create a ascan -> sx should be the axis
    - Then create a ascan -> sy should be the axis
    """
    prevScanInfo = scan_examples.create_ascan_scan_info("axis:sx", "axis:sy")
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)
    newScanInfo = scan_examples.create_ascan_scan_info("axis:sy", "axis:sx")
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    # sanity check
    item = previousPlot.items()[0]
    assert item.xChannel().name() == "axis:sx"

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    item = resultingPlot.items()[0]
    assert item.xChannel().name() == "axis:sy"


def test_enforced_channel_from_scan_info(local_flint):
    """
    Test a new plot with enforced channel (plotinit)

    We expect the channel from the scan_info to be used,
    anyway the user selection was done on the previous plot
    """
    prevScanInfo = scan_examples.create_ascan_scan_info("axis:sx", "axis:sy")
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)

    enforced_channel = "axis:sy"

    # Enforce a user selection
    previousPlot.tagUserEditTime()
    item = previousPlot.items()[0]
    # Make sure the following test have meaning
    assert item.yChannel().name() != enforced_channel

    newScanInfo = scan_examples.create_ascan_scan_info("axis:sy", "axis:sx")
    newScanInfo["display_extra"] = {"displayed_channels": [enforced_channel]}
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    item = resultingPlot.items()[0]
    assert item.yChannel().name() == enforced_channel


def test_plot_updated_with_same_content(local_flint):
    """
    Test a previous plot with a new plot.

    Make sure that the previous selection is still selected.
    """
    prevScanInfo = scan_examples.create_loopscan_scan_info(diode1=True, diode2=False)
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)
    newScanInfo = scan_examples.create_loopscan_scan_info(diode1=True, diode2=False)
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    item = resultingPlot.items()[0]
    assert item.yChannel().name() == "diode:diode1"


def test_plot_updated_with_different_content(local_flint):
    """
    Test a previous plot with a new plot containing different diodes.

    Make sure there is something selected.
    """
    prevScanInfo = scan_examples.create_loopscan_scan_info(diode1=True, diode2=False)
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)
    newScanInfo = scan_examples.create_loopscan_scan_info(diode1=False, diode2=True)
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    item = resultingPlot.items()[0]
    assert item.yChannel().name() == "diode:diode2"


def test_from_loopscan_to_ascan(local_flint):
    """
    Test plot state with consecutive scans

    - Create a loopscan -> elapsed_time should be the axis
    - Then create a ascan -> the motor should be the axis
    """
    prevScanInfo = scan_examples.create_loopscan_scan_info()
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)
    newScanInfo = scan_examples.create_ascan_scan_info("axis:sx")
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    item = resultingPlot.items()[0]
    assert item.xChannel().name() == "axis:sx"


def test_update_loopscan_to_ascan_with_locked_counters(local_flint):
    """
    Test plot state with consecutive scans and a user selection in between

    We expect the user selection to be restored
    """
    prevScanInfo = scan_examples.create_loopscan_scan_info()
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)
    assert len(previousPlot.items()) == 1

    # user selection
    model_helper.updateDisplayedChannelNames(
        previousPlot, previousScan, ["diode:diode1", "diode:diode2"]
    )
    previousPlot.tagUserEditTime()

    newScanInfo = scan_examples.create_ascan_scan_info("axis:sx")
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    assert len(resultingPlot.items()) == 2
    items = resultingPlot.items()
    assert [i.xChannel().name() for i in items] == [
        "axis:sx",
        "axis:sx",
    ]
    assert set([i.yChannel().name() for i in items]) == set(
        ["diode:diode1", "diode:diode2"]
    )
    assert resultingPlot.editSource() == "user"
    assert resultingPlot.xaxisEditSource() is None


def test_update_ascan_to_loopscan_with_locked_counters(local_flint):
    """
    Test plot state with consecutive scans and a user selection in between

    We expect the user selection to be restored
    """
    prevScanInfo = scan_examples.create_ascan_scan_info("axis:sx")
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)

    # user selection
    model_helper.updateDisplayedChannelNames(
        previousPlot, previousScan, ["diode:diode1", "diode:diode2"]
    )
    previousPlot.tagUserEditTime()

    newScanInfo = scan_examples.create_loopscan_scan_info()
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)
    assert len(newPlot.items()) == 1

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    assert len(resultingPlot.items()) == 2
    items = resultingPlot.items()
    assert [i.xChannel().name() for i in items] == [
        "timer:elapsed_time",
        "timer:elapsed_time",
    ]
    assert set([i.yChannel().name() for i in items]) == set(
        ["diode:diode1", "diode:diode2"]
    )
    assert resultingPlot.editSource() == "user"
    assert resultingPlot.xaxisEditSource() is None


def test_update_from_empty():
    """Update a plot from nothing.

    The new plot must be displayed.
    """
    previousPlot = None
    previousScan = None
    newScanInfo = scan_examples.create_ascan_scan_info("axis:sx")
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    item = resultingPlot.items()[0]
    assert item.xChannel().name() == "axis:sx"


def test_new_ascan_with_locked_counters():
    """Update a ascan using a locked counters.

    The new plot is another ascan using a different motor.

    The resulting plot have to contain the previous y selection
    """
    prevScanInfo = scan_examples.create_ascan_scan_info("axis:sx", "axis:sy")
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)

    # user selection
    model_helper.updateDisplayedChannelNames(
        previousPlot, previousScan, ["diode:diode1", "diode:diode2"]
    )
    previousPlot.tagUserEditTime()

    newScanInfo = scan_examples.create_ascan_scan_info("axis:sy", "axis:sx")
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    assert len(resultingPlot.items()) == 2
    items = resultingPlot.items()
    assert [i.xChannel().name() for i in items] == ["axis:sy", "axis:sy"]
    assert set([i.yChannel().name() for i in items]) == set(
        ["diode:diode1", "diode:diode2"]
    )
    assert resultingPlot.editSource() == "user"
    assert resultingPlot.xaxisEditSource() is None


def test_new_ascan_with_locked_axis():
    """Update a ascan using a locked axis. with another ascan.

    The new plot is another ascan using a different motor.

    The resulting plot have to have the previous locked axis.
    """
    prevScanInfo = scan_examples.create_ascan_scan_info("axis:sx", "axis:sy")
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)

    # user selection
    model_helper.updateDisplayedChannelNames(
        previousPlot, previousScan, ["diode:diode1", "diode:diode2"]
    )
    topMaster = list(previousScan.devices())[0]
    model_helper.updateXAxis(
        previousPlot, previousScan, topMaster, "timer:elapsed_time"
    )
    previousPlot.tagXaxisUserEditTime()

    newScanInfo = scan_examples.create_ascan_scan_info("axis:sy", "axis:sx")
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    assert len(resultingPlot.items()) == 2
    items = resultingPlot.items()
    assert [i.xChannel().name() for i in items] == [
        "timer:elapsed_time",
        "timer:elapsed_time",
    ]
    assert resultingPlot.editSource() in [None, "plotselect"]
    assert resultingPlot.xaxisEditSource() == "user"


def test_select_good_default_counter():
    """Update a plot.

    The new plot must be displayed.
    """
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0, unit="s")
    factory.add_device(root_id="timer", device_id="diode", triggered_by="timer")
    factory.add_channel(channel_id="axis:foo", device_id="timer", dim=0)
    factory.add_channel(channel_id="diode:diode1", dim=0)
    factory["plots"] = [
        {"kind": "curve-plot", "items": [{"kind": "curve", "x": "axis:foo"}]}
    ]
    newScanInfo = factory.scan_info()

    previousPlot = None
    previousScan = None
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )
    assert len(resultingPlot.items()) == 1
    item = resultingPlot.items()[0]
    assert item.xChannel().name() == "axis:foo"
    assert item.yChannel().name() == "diode:diode1"


def test_loopscan_from_diode1_to_diode2(local_flint):
    """
    Test plot state with consecutive scans

    - Create a loopscan with diode1
    - Create a loopscan with diode2

    We expect the diode2 to be selected
    """
    prevScanInfo = scan_examples.load_scan_info("loopscan_on_diode1.yaml")
    previousScan, previousPlot = get_scan_and_plot_from_scan_info(prevScanInfo)
    previousPlot = resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        None, None, previousScan, previousPlot
    )

    newScanInfo = scan_examples.load_scan_info("loopscan_on_diode2.yaml")
    newScan, newPlot = get_scan_and_plot_from_scan_info(newScanInfo)

    resultingPlot = curve_plot_update.resolveCurvePlotUpdate(
        previousScan, previousPlot, newScan, newPlot
    )

    item = resultingPlot.items()[0]
    assert item.xChannel().name() == "timer:elapsed_time"
    assert item.yChannel().name() == "simulation_diode_sampling_controller:diode2"
