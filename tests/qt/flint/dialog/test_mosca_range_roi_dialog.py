import pytest
from bliss.flint.dialog import mosca_range_roi_dialog
from silx.gui import qt


def test_single_channel(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    assert v.validate("1", 0) == (qt.QValidator.Acceptable, "1", 0)
    assert v.fixup("1") == "1"
    assert v.toValue("1") == 1


def test_all_channel(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    assert v.validate("-1", 0) == (qt.QValidator.Acceptable, "-1", 0)
    assert v.fixup("-1") == "-1"
    assert v.toValue("-1") == -1


def test_disabled_all_channel(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    v.setNegOneAccepted(False)
    assert v.validate("-1", 0) == (qt.QValidator.Intermediate, "-1", 0)
    assert v.fixup("-1") == "-1"
    assert v.toValue("-1") == -1


def test_range_channel(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    assert v.validate("5-10", 0) == (qt.QValidator.Acceptable, "5-10", 0)
    assert v.fixup("5-10") == "5-10"
    assert v.toValue("5-10") == (5, 10)


def test_ignore_last_unused_char(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    assert v.validate("000b111", 4) == (qt.QValidator.Acceptable, "000111", 3)


def test_fix_leading_zeros(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    assert v.fixup("00001") == "1"
    assert v.toValue("00001") == 1


def test_fix_same_range(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    assert v.fixup("1-1") == "1"
    assert v.toValue("1-1") == 1


def test_fix_reversed_range(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    assert v.fixup("3-1") == "1-3"
    assert v.toValue("3-1") == (1, 3)


def test_negative_range(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    assert v.validate("-1-3", 0)[0] == qt.QValidator.Intermediate
    with pytest.raises(ValueError):
        v.toValue("-1-3")


def test_wrong_negative(qapp):
    v = mosca_range_roi_dialog._ChannelValidator()
    assert v.validate("-2", 0)[0] == qt.QValidator.Intermediate
    with pytest.raises(ValueError):
        v.toValue("-1-3")
