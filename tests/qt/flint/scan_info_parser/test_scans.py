from bliss.flint.scan_info_parser.scans import create_scan_model
from bliss.flint.model import scan_model
from tests.qt.flint.factory import ScanInfoFactory


def test_create_scan_model():
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_device(root_id="timer2", device_id="timer2")
    meta = {"points": 10}
    factory.add_channel(
        device_id="timer", channel_id="timer:elapsed_time", dim=0, meta=meta
    )
    factory.add_channel(channel_id="timer:epoch", dim=0)
    factory.add_device(root_id="timer", device_id="diode", triggered_by="timer")
    factory.add_device(
        root_id="timer2", device_id="opium", triggered_by="timer2", type="mca"
    )
    factory.add_device(
        root_id="timer2", device_id="lima", triggered_by="timer2", type="lima"
    )
    factory.add_channel(channel_id="diode:diode", dim=0)
    factory.add_channel(channel_id="opium:mca1", dim=1)
    factory.add_channel(channel_id="lima:image1", dim=2)
    scan_info = factory.scan_info()

    scan = create_scan_model(scan_info)
    assert scan.isSealed()

    channelCount = 0
    deviceCount = len(list(scan.devices()))
    for device in scan.devices():
        channelCount += len(list(device.channels()))
    assert channelCount == 5
    assert deviceCount == 8

    expected = [
        ("diode:diode", scan_model.ChannelType.COUNTER, "diode", "timer"),
        ("timer:elapsed_time", scan_model.ChannelType.COUNTER, "timer", "timer"),
        ("timer:epoch", scan_model.ChannelType.COUNTER, "timer", "timer"),
        ("opium:mca1", scan_model.ChannelType.SPECTRUM, "mca1", "opium"),
        ("lima:image1", scan_model.ChannelType.IMAGE, "lima", "timer2"),
    ]

    for channel_info in expected:
        name, kind, device, master = channel_info
        channel = scan.getChannelByName(name)
        assert channel.name() == name
        assert channel.type() == kind
        assert channel.device().name() == device
        assert channel.device().master().name() == master

    assert scan.getChannelByName("timer:elapsed_time").metadata().points is not None
    assert scan.getChannelByName("timer:epoch").metadata().points is None


def test_create_scan_model_with_lima_rois():

    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0)

    factory.add_channel(channel_id="timer:epoch", dim=0)
    rois = {
        "roi1": {"kind": "rect", "x": 190, "y": 110, "width": 600, "height": 230},
        "roi4": {
            "kind": "arc",
            "cx": 487.0,
            "cy": 513.0,
            "r1": 137.0,
            "r2": 198.0,
            "a1": 100.0,
            "a2": 200.0,
        },
    }

    factory.add_lima_device(
        device_id="beamviewer",
        root_id="timer",
        triggered_by="timer",
        image=True,
        rois=rois,
    )
    factory.add_channel(channel_id="beamviewer:roi_counters:roi1_sum", dim=0)
    factory.add_channel(channel_id="beamviewer:roi_counters:roi1_avg", dim=0)
    factory.add_channel(channel_id="beamviewer:roi_counters:roi4_sum", dim=0)
    factory.add_channel(channel_id="beamviewer:roi_counters:roi4_avg", dim=0)
    factory.add_channel(channel_id="beamviewer:roi_counters:roi5_avg", dim=0)
    scan_info = factory.scan_info()
    scan = create_scan_model(scan_info)
    assert scan.isSealed()

    channelCount = 0
    deviceCount = len(list(scan.devices()))
    for device in scan.devices():
        channelCount += len(list(device.channels()))
    assert channelCount == 8
    assert deviceCount == 6

    channel = scan.getChannelByName("beamviewer:roi_counters:roi1_avg")
    device = channel.device()
    assert device.metadata().roi is not None
    assert device.metadata().roi.x == 190

    channel = scan.getChannelByName("beamviewer:roi_counters:roi4_avg")
    assert channel.name() == "beamviewer:roi_counters:roi4_avg"
    device = channel.device()
    assert device.name() == "roi4"
    assert device.type() == scan_model.DeviceType.VIRTUAL_ROI
    assert device.metadata().roi is not None
    assert device.metadata().roi.cx == 487.0

    channel = scan.getChannelByName("beamviewer:roi_counters:roi5_avg")
    device = channel.device()
    assert device.metadata().roi is None


def test_read_scatter_data__different_groups():
    scan_info = {
        "acquisition_chain": {"timer": {"devices": ["timer"]}},
        "devices": {"timer": {"name": "timer", "channels": ["foo", "foo2", "bar"]}},
        "channels": {
            "foo": {"group": "scatter1", "axis_id": 0},
            "foo2": {"group": "scatter1", "axis_id": 1},
            "bar": {"group": "scatter2", "axis_id": 0},
        },
    }
    scan = create_scan_model(scan_info)
    foo = scan.getChannelByName("foo")
    scatterData = scan.getScatterDataByChannel(foo)
    assert scatterData is not None
    foo2 = scan.getChannelByName("foo2")
    assert scatterData.contains(foo2)
    bar = scan.getChannelByName("bar")
    assert not scatterData.contains(bar)
    assert scatterData.maxDim() == 2


def test_read_scatter_data__twice_axis_at_same_place():
    scan_info = {
        "acquisition_chain": {"timer": {"devices": ["timer"]}},
        "devices": {"timer": {"name": "timer", "channels": ["foo", "foo2", "bar"]}},
        "channels": {
            "foo": {"group": "scatter1", "axis_id": 0},
            "foo2": {"group": "scatter1", "axis_id": 0},
            "bar": {"group": "scatter1", "axis_id": 1},
        },
    }
    scan = create_scan_model(scan_info)
    foo = scan.getChannelByName("foo")
    scatterData = scan.getScatterDataByChannel(foo)
    assert scatterData is not None
    foo2 = scan.getChannelByName("foo2")
    assert scatterData.contains(foo2)
    bar = scan.getChannelByName("bar")
    assert scatterData.contains(bar)
    assert scatterData.maxDim() == 2


def test_read_scatter_data__non_regular_3d():
    scan_info = {
        "acquisition_chain": {"timer": {"devices": ["timer"]}},
        "devices": {
            "timer": {
                "name": "timer",
                "channels": ["axis1", "axis2", "diode1", "frame"],
            }
        },
        "channels": {
            "axis1": {
                "axis_id": 0,
                "axis_points_hint": 10,
                "group": "foo",
                "max": 9,
                "min": 0,
                "points": 500,
            },
            "axis2": {
                "axis_id": 1,
                "axis_points_hint": 10,
                "group": "foo",
                "max": 9,
                "min": 0,
                "points": 500,
            },
            "diode1": {"axis_points_hint": None, "group": "foo"},
            "frame": {
                "axis_id": 2,
                "axis_kind": "step",
                "axis_points": 5,
                "axis_points_hint": None,
                "group": "foo",
                "points": 500,
                "start": 0,
                "stop": 4,
            },
        },
    }

    scan = create_scan_model(scan_info)
    axis1 = scan.getChannelByName("axis1")
    scatterData = scan.getScatterDataByChannel(axis1)
    assert scatterData is not None
    axis2 = scan.getChannelByName("axis2")
    assert scatterData.contains(axis2)
    frame = scan.getChannelByName("frame")
    assert scatterData.contains(frame)
    diode1 = scan.getChannelByName("diode1")
    assert not scatterData.contains(diode1)
    assert scatterData.maxDim() == 3


def test_create_scan_model_with_mca():
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0)
    factory.add_channel(channel_id="timer:epoch", dim=0)
    factory.add_device(
        root_id="timer", device_id="mca", triggered_by="timer", type="mca"
    )
    factory.add_channel(channel_id="mca:realtime_det0", dim=0)
    factory.add_channel(channel_id="mca:realtime_det1", dim=0)
    factory.add_channel(channel_id="mca:deadtime_det0", dim=0)
    factory.add_channel(channel_id="mca:deadtime_det1", dim=0)
    scan_info = factory.scan_info()

    scan = create_scan_model(scan_info)
    assert scan.isSealed()

    channelCount = 0
    deviceCount = len(list(scan.devices()))
    for device in scan.devices():
        channelCount += len(list(device.channels()))
    assert channelCount == 6
    assert deviceCount == 5

    channel = scan.getChannelByName("mca:realtime_det0")
    assert channel.name() == "mca:realtime_det0"
    device = channel.device()
    assert device.name() == "det0"
    assert device.type() == scan_model.DeviceType.VIRTUAL_MCA_DETECTOR
