"""Testing scan info helper module."""

import numpy
from bliss.flint.scan_info_parser import plots as plots_module
from bliss.flint.model import plot_item_model
from bliss.flint.model import plot_model
from tests.qt.flint.factory import ScanInfoFactory
from bliss.flint.scan_info_parser.scans import create_scan_model
from ..manager import scan_examples


def test_create_plot_model():
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_device(root_id="timer2", device_id="timer2")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0)
    factory.add_channel(channel_id="timer:epoch", dim=0)
    factory.add_device(root_id="timer", device_id="diode", triggered_by="timer")
    factory.add_device(root_id="timer2", device_id="opium", triggered_by="timer2")
    factory.add_device(root_id="timer2", device_id="lima", triggered_by="timer2")
    factory.add_channel(channel_id="diode:diode", dim=0)
    factory.add_channel(channel_id="opium:mca1", dim=1)
    factory.add_channel(channel_id="lima:image1", dim=2)
    scan_info = factory.scan_info()

    plots = plots_module.create_plot_model(scan_info)
    # default curve plot + image plot + onedim plot
    assert len(plots) == 3


def test_create_scatter_plot_model():
    factory = ScanInfoFactory()
    factory["data_dim"] = 2
    factory.add_device(root_id="axis", device_id="axis")
    factory.add_device(root_id="axis", device_id="timer", triggered_by="axis")
    factory.add_device(root_id="axis", device_id="diode", triggered_by="axis")
    factory.add_channel(channel_id="axis:roby", dim=0)
    factory.add_channel(channel_id="axis:robz", dim=0)
    factory.add_channel(channel_id="timer:elapsed_time", unit="s")
    factory.add_channel(channel_id="diode:diode", dim=0)
    factory.add_channel(channel_id="diode:diode2", dim=0)
    factory.add_channel(channel_id="diode:diode3", dim=0)
    scan_info = factory.scan_info()

    result_plots = plots_module.create_plot_model(scan_info)
    plots = [
        plot for plot in result_plots if isinstance(plot, plot_item_model.ScatterPlot)
    ]
    assert len(plots) == 1
    plot = plots[0]
    assert len(plot.items()) == 1
    item = plot.items()[0]
    assert type(item) is plot_item_model.ScatterItem
    assert item.xChannel().name() == "axis:roby"
    assert item.yChannel().name() == "axis:robz"
    assert item.valueChannel().name() == "diode:diode"

    plots = [
        plot for plot in result_plots if isinstance(plot, plot_item_model.CurvePlot)
    ]
    assert len(plots) == 1
    plot = plots[0]
    assert len(plot.items()) >= 1
    item = plot.items()[0]
    assert type(item) is plot_item_model.CurveItem
    # The first channel should be the diode/time
    assert item.xChannel().name() == "timer:elapsed_time"
    assert item.yChannel().name() == "diode:diode"


def test_create_curve_plot_from_motor_scan():
    factory = ScanInfoFactory()
    factory.add_device(root_id="axis", device_id="axis")
    factory.add_device(root_id="axis", device_id="timer", triggered_by="axis")
    factory.add_device(root_id="axis", device_id="diode", triggered_by="axis")
    factory.add_channel(channel_id="axis:roby", dim=0)
    factory.add_channel(channel_id="timer:elapsed_time", dim=0, unit="s")
    factory.add_channel(channel_id="diode:diode", dim=0)
    factory.add_channel(channel_id="diode:diode2", dim=0)
    scan_info = factory.scan_info()

    result_plots = plots_module.create_plot_model(scan_info)
    plots = [
        plot for plot in result_plots if isinstance(plot, plot_item_model.CurvePlot)
    ]
    assert len(plots) == 1
    plot = plots[0]
    curves = []
    for item in plot.items():
        curves.append((item.xChannel().name(), item.yChannel().name()))
    expected_curves = [("axis:roby", "diode:diode")]
    assert set(expected_curves) == set(curves)


def test_create_scan__no_plots():
    factory = ScanInfoFactory()
    factory.add_device(root_id="axis", device_id="axis")
    factory.add_device(root_id="axis", device_id="timer", triggered_by="axis")
    factory.add_device(root_id="axis", device_id="diode", triggered_by="axis")
    factory.add_channel(channel_id="axis:roby", dim=0)
    factory.add_channel(channel_id="timer:elapsed_time", dim=0, unit="s")
    factory.add_channel(channel_id="diode:diode", dim=0)
    factory.add_channel(channel_id="diode:diode2", dim=0)
    scan_info = factory.scan_info()
    scan_info["plots"] = {}

    plots = plots_module.create_plot_model(scan_info)
    assert len(plots) == 0


def test_amesh_scan_with_image_and_mca():
    factory = ScanInfoFactory()
    factory["data_dim"] = 2
    factory.add_device(root_id="axis", device_id="axis")
    factory.add_device(root_id="axis", device_id="timer", triggered_by="axis")
    factory.add_device(root_id="axis", device_id="diode", triggered_by="axis")
    factory.add_device(
        root_id="axis", device_id="mca1", triggered_by="axis", type="mca"
    )
    factory.add_lima_device(
        root_id="axis", device_id="tomocam", image=True, triggered_by="axis"
    )
    factory.add_channel(channel_id="axis:sy", dim=0)
    factory.add_channel(channel_id="axis:sz", dim=0)
    factory.add_channel(channel_id="timer:elapsed_time", unit="s")
    factory.add_channel(channel_id="timer:epoch", unit="s")
    factory.add_channel(channel_id="diode:diode", dim=0)
    factory.add_channel(channel_id="mca1:realtime_det0", dim=0)
    factory.add_channel(channel_id="mca1:spectrum_det0", dim=1)
    factory["plots"] = [
        {
            "kind": "scatter-plot",
            "items": [{"kind": "scatter", "x": "axis:sy", "y": "axis:sz"}],
        },
        {
            "kind": "scatter-plot",
            "name": "foo",
            "items": [{"kind": "scatter", "x": "axis:sy", "y": "axis:sz"}],
        },
    ]
    scan_info = factory.scan_info()
    scan = create_scan_model(scan_info)
    result_plots = plots_module.create_plot_model(scan_info, scan)
    result_kinds = [type(p) for p in result_plots]
    assert set(result_kinds) == set(
        [
            plot_item_model.ScatterPlot,
            plot_item_model.ImagePlot,
            plot_item_model.McaPlot,
        ]
    )
    # The first one is the scatter
    assert result_kinds[0] == plot_item_model.ScatterPlot
    assert result_kinds[1] == plot_item_model.ScatterPlot
    assert result_plots[1].name() == "foo"


def test_create_plot_model_with_rois():
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0)

    factory.add_channel(channel_id="timer:epoch", dim=0)
    rois = {
        "roi1": {"kind": "rect", "x": 190, "y": 110, "width": 600, "height": 230},
        "roi4": {
            "kind": "arc",
            "cx": 487.0,
            "cy": 513.0,
            "r1": 137.0,
            "r2": 198.0,
            "a1": 100.0,
            "a2": 200.0,
        },
    }

    factory.add_lima_device(
        device_id="beamviewer",
        root_id="timer",
        triggered_by="timer",
        image=True,
        rois=rois,
    )
    scan_info = factory.scan_info()

    scan = create_scan_model(scan_info)
    plots = plots_module.infer_plot_models(scan)
    image_plots = [p for p in plots if isinstance(p, plot_item_model.ImagePlot)]
    plot = image_plots[0]
    roi_items = [i for i in plot.items() if isinstance(i, plot_item_model.RoiItem)]
    assert len(roi_items) == 2
    for item in roi_items:
        roi = item.roi(scan)
        assert roi is not None


def test_read_plot_models__empty_scatter():
    scan_info = {"plots": [{"name": "plot", "kind": "scatter-plot"}]}
    plots = plots_module.read_plot_models(scan_info)
    assert len(plots) == 1


def test_read_plot_models__empty_scatters():
    scan_info = {
        "plots": [
            {"name": "plot", "kind": "scatter-plot"},
            {"name": "plot", "kind": "scatter-plot"},
        ]
    }
    plots = plots_module.read_plot_models(scan_info)
    assert len(plots) == 2


def test_read_plot_models__scatter():
    scan_info = {
        "plots": [
            {
                "name": "plot",
                "kind": "scatter-plot",
                "items": [{"kind": "scatter", "x": "a", "y": "b", "value": "c"}],
            }
        ]
    }
    plots = plots_module.read_plot_models(scan_info)
    assert len(plots) == 1
    assert len(plots[0].items()) == 1
    item = plots[0].items()[0]
    assert item.xChannel().name() == "a"
    assert item.yChannel().name() == "b"
    assert item.valueChannel().name() == "c"


def test_read_plot_models__scatter_axis():
    scan_info = {
        "plots": [
            {
                "name": "plot",
                "kind": "scatter-plot",
                "items": [{"kind": "scatter", "x": "a", "y": "b"}],
            }
        ]
    }
    plots = plots_module.read_plot_models(scan_info)
    assert len(plots) == 1
    assert len(plots[0].items()) == 1
    item = plots[0].items()[0]
    assert item.xChannel().name() == "a"
    assert item.yChannel().name() == "b"
    assert item.valueChannel() is None


def test_read_plot_models__curve_axis():
    scan_info = {
        "plots": [
            {
                "name": "plot",
                "kind": "scatter-plot",
                "items": [{"kind": "scatter", "x": "a"}],
            }
        ]
    }
    plots = plots_module.read_plot_models(scan_info)
    assert len(plots) == 1
    assert len(plots[0].items()) == 1
    item = plots[0].items()[0]
    assert item.xChannel().name() == "a"
    assert item.yChannel() is None


def test_read_plot_models__curve_item():
    scan_info = {
        "plots": [
            {
                "name": "plot",
                "kind": "curve-plot",
                "items": [{"kind": "curve", "x": "a", "y": "b"}],
            }
        ]
    }
    plots = plots_module.read_plot_models(scan_info)
    assert len(plots) == 1
    assert len(plots[0].items()) == 1
    item = plots[0].items()[0]
    assert item.xChannel().name() == "a"
    assert item.yChannel().name() == "b"
    assert item.yAxis() == "left"


def test_read_plot_models__curve_right_item():
    scan_info = {
        "plots": [
            {
                "name": "plot",
                "kind": "curve-plot",
                "items": [{"kind": "curve", "x": "a", "y": "b", "y_axis": "right"}],
            }
        ]
    }
    plots = plots_module.read_plot_models(scan_info)
    assert len(plots) == 1
    assert len(plots[0].items()) == 1
    item = plots[0].items()[0]
    assert item.xChannel().name() == "a"
    assert item.yChannel().name() == "b"
    assert item.yAxis() == "right"


def test_read_onedim_detector():
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0)
    factory.add_channel(channel_id="timer:epoch", dim=0)
    factory.add_device(root_id="timer", device_id="onedim", triggered_by="timer")
    factory.add_channel(channel_id="onedim:d1", dim=1)
    factory.add_channel(channel_id="onedim:d2", dim=1)
    scan_info = factory.scan_info()

    scan = create_scan_model(scan_info)
    plots = plots_module.create_plot_model(scan_info, scan)
    assert len(plots) == 1
    plot = plots[0]
    assert isinstance(plot, plot_item_model.OneDimDataPlot)
    assert len(plot.items()) == 2
    item = plot.items()[0]
    assert isinstance(item.xChannel(), plot_model.XIndexChannelRef)


def test_read_onedim_detector__xaxis_array():
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0)
    factory.add_channel(channel_id="timer:epoch", dim=0)
    onedim_meta = {"xaxis_array": numpy.array([0, 1, 4])}
    factory.add_device(
        root_id="timer", device_id="onedim", triggered_by="timer", meta=onedim_meta
    )
    factory.add_channel(channel_id="onedim:d1", dim=1)
    factory.add_channel(channel_id="onedim:d2", dim=1)
    scan_info = factory.scan_info()

    scan = create_scan_model(scan_info)
    plots = plots_module.create_plot_model(scan_info, scan)
    assert len(plots) == 1
    plot = plots[0]
    assert isinstance(plot, plot_item_model.OneDimDataPlot)
    assert len(plot.items()) == 2
    item = plot.items()[0]
    assert isinstance(item, plot_item_model.CurveItem)
    numpy.testing.assert_array_equal(item.xData(scan).array(), [0, 1, 4])


def test_read_onedim_detector__xaxis_channel():
    factory = ScanInfoFactory()
    factory.add_device(root_id="timer", device_id="timer")
    factory.add_channel(channel_id="timer:elapsed_time", dim=0)
    factory.add_channel(channel_id="timer:epoch", dim=0)
    onedim_meta = {"xaxis_channel": "onedim:d1"}
    factory.add_device(
        root_id="timer", device_id="onedim", triggered_by="timer", meta=onedim_meta
    )
    factory.add_channel(channel_id="onedim:d1", dim=1)
    factory.add_channel(channel_id="onedim:d2", dim=1)
    scan_info = factory.scan_info()

    scan = create_scan_model(scan_info)
    plots = plots_module.create_plot_model(scan_info, scan)
    assert len(plots) == 1
    plot = plots[0]
    assert isinstance(plot, plot_item_model.OneDimDataPlot)
    assert len(plot.items()) == 1
    item = plot.items()[0]
    assert isinstance(item, plot_item_model.CurveItem)
    assert item.xChannel().name() == "onedim:d1"
    assert item.yChannel().name() == "onedim:d2"


def test_mca_plot_on_legacy_mca(local_flint):
    """The image is selected because we only scan the image"""
    scan_info = scan_examples.load_scan_info("loopscan_on_legacy_mca.yaml")
    scan = create_scan_model(scan_info)
    plots = plots_module.create_plot_model(scan_info, scan)
    plots = [p for p in plots if isinstance(p, plot_item_model.McaPlot)]
    assert len(plots) == 1
    assert plots[0].deviceName() == "mca1"


def test_mca_plot_on_external_legacy_mca(local_flint):
    """The image is selected because we only scan the image"""
    scan_info = scan_examples.load_scan_info("loopscan_on_external_legacy_mca.yaml")
    scan = create_scan_model(scan_info)
    plots = plots_module.create_plot_model(scan_info, scan)
    plots = [p for p in plots if isinstance(p, plot_item_model.McaPlot)]
    assert len(plots) == 1
    assert plots[0].deviceName() == "mca1"
