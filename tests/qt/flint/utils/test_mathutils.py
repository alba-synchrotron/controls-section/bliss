"""Testing mathutils module."""


import numpy
import pytest
from bliss.flint.utils import mathutils


def test_gaussian_fit():
    xx = numpy.arange(5)
    yy = numpy.array([10, 10, 110, 10, 10])
    r = mathutils.fit_gaussian(xx, yy)

    assert r is not None
    numpy.testing.assert_almost_equal(r.height, 100)
    numpy.testing.assert_almost_equal(r.pos_x, 2)
    numpy.testing.assert_almost_equal(r.background, 10)

    yy2 = r.transform(xx)
    numpy.testing.assert_almost_equal(yy2, yy)


def test_angles():
    assert mathutils.angular_dist(0, 1) == pytest.approx(1)
    assert mathutils.angular_dist(0, -1) == pytest.approx(-1)
    assert mathutils.angular_dist(1, 0) == pytest.approx(-1)
    assert mathutils.angular_dist(-1, 0) == pytest.approx(1)
    assert mathutils.angular_dist(0 + numpy.pi * 2, 1) == pytest.approx(1)
    assert mathutils.angular_dist(0 + numpy.pi * 2, -1) == pytest.approx(-1)
    assert mathutils.angular_dist(1 + numpy.pi * 2, 0) == pytest.approx(-1)
    assert mathutils.angular_dist(-1 + numpy.pi * 2, 0) == pytest.approx(1)
    assert mathutils.angular_dist(0 - numpy.pi * 2, 1) == pytest.approx(1)
    assert mathutils.angular_dist(0 - numpy.pi * 2, -1) == pytest.approx(-1)
    assert mathutils.angular_dist(1 - numpy.pi * 2, 0) == pytest.approx(-1)
    assert mathutils.angular_dist(-1 - numpy.pi * 2, 0) == pytest.approx(1)
    assert -0.3 < mathutils.angular_dist(numpy.pi + 0.1, -numpy.pi - 0.1) < 0
    assert 0 < mathutils.angular_dist(-numpy.pi - 0.1, numpy.pi + 0.1) < 0.3
    numpy.testing.assert_array_almost_equal(
        mathutils.angular_dist(numpy.array([0, 1, 2]), numpy.array([0, 0, 0])),
        numpy.array([0, -1, -2]),
    )
