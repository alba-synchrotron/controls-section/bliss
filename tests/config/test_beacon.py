# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.config import static
from redis.exceptions import ResponseError
import pytest
import socket
import os


def test_redis_client_name(redis_conn):
    conn_name = f"{socket.gethostname()}:{os.getpid()}"
    client_id = str(redis_conn.client_id())
    client_list = redis_conn.execute_command("CLIENT LIST")
    for client_info in client_list:
        if client_info.get("id") == client_id:
            assert conn_name == client_info.get("name")
            break
    else:
        assert False


def test_config_base_path(beacon):
    saved_cfg = static.Config.instance
    static.Config.instance = None
    try:
        cfg = static.get_config(base_path="./sessions")
        assert "test_session" in cfg.names_list
    finally:
        cfg.close()
        static.Config.instance = saved_cfg


def test_redis_modules_are_loaded(redis_conn, redis_data_conn):
    for conn in (redis_conn, redis_data_conn):
        info = conn.execute_command("CLIENT INFO")
        if info["db"] == 0:
            conn.execute_command(
                "FT.CREATE myIdx ON HASH PREFIX 1 doc: SCHEMA title TEXT WEIGHT 5.0 body TEXT url TEXT"
            )
            assert (
                conn.execute_command(
                    "HSET",
                    "doc:1",
                    "title",
                    "hello world",
                    "body",
                    "lorem ipsum",
                    "url",
                    "http://redis.io",
                )
                == 3
            )
            assert conn.execute_command(
                "FT.SEARCH", "myIdx", "hello world", "LIMIT", "0", "10"
            )
        else:
            # test with another DB (not 0)
            with pytest.raises(ResponseError):
                # it is not possible to set an index in DB != 0
                conn.execute_command(
                    "FT.CREATE myIdx ON HASH PREFIX 1 doc: SCHEMA title TEXT WEIGHT 5.0 body TEXT url TEXT"
                )

        conn.execute_command("JSON.SET", "animal", "$", '"dog"')
        assert conn.execute_command("JSON.TYPE animal $")[0].decode() == "string"
