# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


import datetime
import os
import gevent

import pytest

from bliss.config.wardrobe import ParametersWardrobe
from bliss.common.axis import Axis
from bliss.config.conductor.client import get_redis_proxy


@pytest.mark.parametrize("caching", [False, True])
def test_parameter_wardrobe_1(session, caching):
    spw = ParametersWardrobe("myPWkey", connection=get_redis_proxy(caching=caching))

    # checking default is created
    assert "default" in spw.instances
    assert len(spw.instances) == 1

    numbers = (("first", 1), ("second", 2), ("third", 3))
    romans_numbers = (("first", "I"), ("second", "II"), ("third", "III"))

    for k, v in numbers:
        spw.add(k, v)  # adding parameters
        assert hasattr(spw, k)  # check existance
        assert getattr(spw, k) == v  # check values
        assert isinstance(getattr(spw, k), int)  # check types

    # creating a new set of parameters
    spw.switch("roman")
    for k, v in numbers:
        assert getattr(spw, k) == v  # check values
        assert isinstance(getattr(spw, k), int)  # check types
    # assigning new values to romans
    for k, v in romans_numbers:
        setattr(spw, k, v)
    for k, v in romans_numbers:
        assert getattr(spw, k) == v  # check values
        assert isinstance(getattr(spw, k), str)  # check types
    # switch back to default
    spw.switch("default")
    for k, v in numbers:
        assert getattr(spw, k) == v  # check values
        assert isinstance(getattr(spw, k), int)  # check types
    # deleting
    spw.remove("roman")
    assert "roman" not in spw.instances
    assert "default" in spw.instances


@pytest.mark.parametrize("caching", [False, True])
def test_parameters_wardrobe_switch(session, caching):
    dress = ParametersWardrobe("dress", connection=get_redis_proxy(caching=caching))
    slots = ("head", "body", "legs")
    default = ("nothing", "t-shirt", "jeans")

    # creating default
    for k, v in zip(slots, default):
        dress.add(k, v)

    check = dress.to_dict()
    for k, v in zip(slots, default):
        assert check[k] == v
    len(check) == 7

    dress.switch("casual")  # on casual
    dress.head = "football hat"
    dress.switch("default")  # on default
    assert dress.head == "nothing"
    dress.switch("casual")  # on casual
    assert dress.head == "football hat"
    dress.switch("night")  # on night
    dress.body = "shirt"
    dress.remove(".legs")
    with pytest.raises(KeyError):
        dress.legs

    check = dress.to_dict(export_properties=True)
    assert check.get("head") == "nothing"
    assert check.get("body") == "shirt"
    assert check.get("_creation_date") is not None
    assert len(check) == 5
    check = dress.to_dict()
    assert len(check) == 2

    # testing instances method
    for suite in ("a", "b", "c"):
        with pytest.raises(AssertionError):
            assert suite in dress.instances
    for suite in ("casual", "default", "night"):
        assert suite in dress.instances


@pytest.mark.parametrize("caching", [False, True])
def test_parameter_wardrobe_init_with_default(session, caching):
    def_val = {"pasta": 80, "pizza": 150, "cheese": "1 piece", "meat": "1 steak"}
    food = ParametersWardrobe(
        "food_per_person",
        default_values=def_val,
        connection=get_redis_proxy(caching=caching),
    )
    assert food.pasta == 80
    assert food.pizza == 150
    assert food.cheese == "1 piece"
    assert food.meat == "1 steak"
    with pytest.raises(AttributeError):
        food.other == "boh"
    food.switch("double")
    assert food.pasta == 80
    assert food.pizza == 150
    assert food.cheese == "1 piece"
    assert food.meat == "1 steak"
    with pytest.raises(AttributeError):
        food.other == "boh"
    food.pasta = 150
    assert food.pasta == 150
    food.switch("default")
    assert food.pasta == 80


@pytest.mark.parametrize("caching", [False, True])
def test_parameter_wardrobe_from_dict(session, caching):
    def_val = {"pasta": 80, "pizza": 150, "cheese": "1 piece", "meat": "1 steak"}
    food = ParametersWardrobe("otherfood", connection=get_redis_proxy(caching=caching))
    for k in def_val.keys():
        # creating default None values
        food.add(k)
    food.switch("junk")
    food.from_dict(def_val)
    assert food.pasta == 80
    assert food.pizza == 150
    assert food.cheese == "1 piece"
    assert food.meat == "1 steak"
    with pytest.raises(AttributeError):
        food.other == "boh"
    food.switch("double")
    for k in def_val.keys():
        # all default are None
        assert getattr(food, k) is None
    with pytest.raises(AttributeError):
        food.from_dict({"wrong": 1, "parameters": 2})


@pytest.mark.parametrize("caching", [False, True])
def test_parameter_wardrobe_none(session, caching):
    sport = ParametersWardrobe("sport", connection=get_redis_proxy(caching=caching))
    sport.add("Soccer")
    # check if is creating Soccer and if is assigning None
    assert sport.Soccer is None


@pytest.mark.parametrize("caching", [False, True])
def test_parameter_wardrobe_global_object(session, caching):
    # checks if we are able to store references to
    # global objects
    motors = ParametersWardrobe(
        "motors_chk", connection=get_redis_proxy(caching=caching)
    )
    m0 = session.config.get("m0")
    motors.add("m0", m0)  # creating reference to motor m0
    del motors
    check_motors = ParametersWardrobe(
        "motors_chk", connection=get_redis_proxy(caching=caching)
    )
    # checking if reference is ok
    assert isinstance(check_motors.m0, Axis)


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_remove(session, caching):
    dont_change_me = ParametersWardrobe(
        "notme", not_removable=["myself"], connection=get_redis_proxy(caching=caching)
    )
    dont_change_me.add("myself", "best")
    dont_change_me.add("yourself", "how?")

    with pytest.raises(AttributeError):
        dont_change_me.remove(".myself")
    with pytest.raises(NameError):
        dont_change_me.remove("default")
    with pytest.raises(NameError):
        dont_change_me.remove("not existent name")
    assert dont_change_me.yourself == "how?"
    dont_change_me.remove(".yourself")
    with pytest.raises(KeyError):
        assert dont_change_me.yourself


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_show_table(session, capsys, caching):
    dress = ParametersWardrobe("mydress", connection=get_redis_proxy(caching=caching))
    dress.add("_hideme")
    dress.add("showme")
    dress.show_table()
    captured = capsys.readouterr()
    # check hiding parameters with _
    assert "_hideme" not in captured.out
    assert "showme" in captured.out
    dress.add("hat", "football")
    dress.add("feet", "shoes")
    # check if asterisk is present
    dress.switch("sport")
    dress.feet = "tennis"
    dress.show_table()
    captured = capsys.readouterr()
    assert "* football" in captured.out
    assert "* tennis" not in captured.out
    assert "tennis" in captured.out


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_get_current_instance(session, caching):
    games = ParametersWardrobe("games", connection=get_redis_proxy(caching=caching))
    for name in "soccer tennis football squash".split():
        # create and switch to different sets
        games.switch(name)
        assert games.current_instance == name
    for name in "soccer tennis football squash".split():
        # just switch to different created sets
        games.switch(name)
        assert games.current_instance == name


@pytest.mark.parametrize("caching", [False, True])
def test_creation_time(session, caching):
    drinks = ParametersWardrobe("drinks", connection=get_redis_proxy(caching=caching))

    # an empty Wardrobe has only creation/access info
    assert len(drinks.to_dict(export_properties=True)) == 3

    assert "wine" not in drinks.instances
    drinks.switch("wine")

    def str2datetime(string):
        return datetime.datetime.strptime(string, "%Y-%m-%d %H:%M:%S")

    # epsilon time bellow which time comparison is considered to be the same
    # This value is pretty big to make sure the CI machine is fast enough
    epsilon = datetime.timedelta(seconds=2).total_seconds()

    # creation and last access was done now
    now = datetime.datetime.now()
    creation_date = str2datetime(drinks.creation_date)
    assert creation_date.timestamp() == pytest.approx(now.timestamp(), abs=epsilon)
    last_accessed = str2datetime(drinks.last_accessed)
    assert last_accessed.timestamp() == pytest.approx(now.timestamp(), abs=epsilon)

    # a later access update the last access time
    gevent.sleep(1)
    drinks.creation_date  # access it => will change 'last_accessed' if not using caching
    now = datetime.datetime.now()
    last_accessed2 = str2datetime(drinks.last_accessed)
    if caching:
        assert last_accessed == last_accessed2, "last_accessed was not cached"
    else:
        assert last_accessed != last_accessed2, "last_accessed was not updated"

    assert last_accessed2.timestamp() == pytest.approx(now.timestamp(), abs=epsilon)


@pytest.mark.parametrize("caching", [False, True])
def test_from_dict_ok(session, caching):
    colors = ParametersWardrobe("colors", connection=get_redis_proxy(caching=caching))
    colors.add("background", "black")
    colors.add("foreground", "white")
    colors.switch("portrait")
    new_colors = {"background": "yellow", "foreground": "blue"}

    colors.from_dict(new_colors)
    assert colors.background == "yellow"
    assert colors.foreground == "blue"

    colors.switch("default")

    assert colors.background == "black"
    assert colors.foreground == "white"

    with pytest.raises(AttributeError):
        # attribute does not exist in Wardrobe
        colors.from_dict({**new_colors, **{"border": "pink"}})


@pytest.mark.parametrize("caching", [False, True])
def test_from_dict_not_ok(session, caching):
    cats = ParametersWardrobe("cats", connection=get_redis_proxy(caching=caching))
    with pytest.raises(AttributeError):
        cats.from_dict({"breed": "snowcat"})
    with pytest.raises(TypeError):
        cats.from_dict({})
    with pytest.raises(TypeError):
        cats.from_dict(None)
    with pytest.raises(TypeError):
        cats.from_dict()


class MyPar(ParametersWardrobe):
    """
    Test class to check property attributes
    """

    SLOTS = []

    def __init__(self, name, connection=None):
        super().__init__(
            name, property_attributes=["myproperty"], connection=connection
        )

    @property
    def myproperty(self):
        return "OK"


@pytest.mark.parametrize("caching", [False, True])
def test_from_and_to_dict_with_inheritance(session, caching):
    mypar = MyPar("mypar", connection=get_redis_proxy(caching=caching))
    mypar.add("first", "I")
    mypar.add("second", "I(")
    assert mypar.myproperty == "OK"
    dict_ = mypar.to_dict()
    assert "myproperty" not in dict_  # check presence of property
    dict_ = mypar.to_dict(export_properties=True)
    assert "myproperty" in dict_  # check presence of property

    assert len(dict_) == 6
    mypar.from_dict(dict_)
    with pytest.raises(AttributeError):
        mypar.from_dict({**dict_, **{"fakeattr": 123}})
    with pytest.raises(AttributeError):
        # can't set attribute
        mypar.myproperty = 23
    with pytest.raises(NameError):
        mypar.add("myproperty", 123)


@pytest.mark.parametrize("caching", [False, True])
def test_creation_and_update_appear_on_shell(session, capsys, caching):
    fake = ParametersWardrobe("fake", connection=get_redis_proxy(caching=caching))
    print(fake.__info__())
    captured = capsys.readouterr()
    assert "last_accessed" in captured.out
    assert "creation_date" in captured.out
    fake.show_table()
    captured = capsys.readouterr()
    assert "last_accessed" in captured.out
    assert "creation_date" in captured.out


@pytest.mark.parametrize("caching", [False, True])
def test_dir_shows_attrs_on_shell(session, capsys, caching):
    myfake = MyPar("myfake", connection=get_redis_proxy(caching=caching))
    myfake.add("band", "rolling stones")
    myfake.add("music", ["rock", "pop"])
    print(dir(myfake))
    captured = capsys.readouterr()
    for (
        name
    ) in "add remove switch instance current_instance to_dict from_dict from_file freeze show_table creation_date last_accessed band music myproperty".split():
        assert name in captured.out


"""
@pytest.mark.parametrize("caching", [False, True])
def test_delete_wardrobe(session, caching):
    deleting = ParametersWardrobe('deleting', connection=get_redis_proxy(caching=caching))
    deleting.add('erasing',1000)
    assert deleting.erasing == 1000
    del deleting
    deleting = ParametersWardrobe('deleting', connection=get_redis_proxy(caching=caching))
    with pytest.raises(AttributeError):
        deleting.erasing
"""


@pytest.mark.parametrize("caching", [False, True])
def test_non_removable(session, caching):
    fake = ParametersWardrobe(
        "fake", not_removable=("immortal",), connection=get_redis_proxy(caching=caching)
    )
    with pytest.raises(AttributeError):
        fake.immortal  # not yet created
    fake.add("immortal", "me")
    with pytest.raises(AttributeError):
        fake.remove(".immortal")


@pytest.mark.parametrize("caching", [False, True])
def test_bad_name_for_attribute(session, caching):
    bad = ParametersWardrobe("bad", connection=get_redis_proxy(caching=caching))
    for name in r"!@#$%^&*()123804/`-+=,./".split():
        with pytest.raises(TypeError):
            bad.add(name)


def get_materials(session, caching):
    ma = ParametersWardrobe("materials", connection=get_redis_proxy(caching=caching))
    ma.add("color")
    ma.add("specific_weight")
    ma.add("dimensions")
    ma.add("pieces")
    ma.add("precious", False)
    ma.add("motor", session.config.get("roby"))

    ma.switch("water")
    ma.color = "transparent"
    ma.specific_weight = 1

    ma.switch("gold")
    ma.color = "gold"
    ma.specific_weight = 19.32
    ma.dimensions = (1, 2, 3)
    ma.pieces = {"first": 10.3, "second": 20.2, "count": [5, 2, 5]}
    ma.precious = True

    ma.switch("copper")
    ma.color = "yellow-brown"
    ma.specific_weight = 8.96
    ma.dimensions = (5, 10, 15)
    ma.pieces = {"first": 40.3, "second": 27.2, "count": [1, 2, 3]}
    ma.motor = session.config.get("robz")

    return ma


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_to_yml_file(session, caching):
    materials = get_materials(session, caching)

    path = "/tmp/materials_copper.yml"
    path_1 = "/tmp/materials_copper_1.yml"

    # delete files if they exists
    if os.path.isfile(path):
        os.remove(path)
    if os.path.isfile(path_1):
        os.remove(path_1)

    materials.switch("copper")

    # export only copper (current instance)
    materials.to_file(path)

    materials.switch("gold")
    materials.to_file(path_1, "copper")
    with open(path) as f, open(path_1) as f1:
        # those approach are equivalent
        assert f.read() == f1.read()

    # export all materials
    materials.to_file("/tmp/materials_all.yml", *materials.instances)


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_from_yml_file(session, caching):
    copper_reload = ParametersWardrobe(
        "materials_reload", connection=get_redis_proxy(caching=caching)
    )

    # setting default values
    copper_reload.add("color", "nocolor")
    copper_reload.add("specific_weight", 0)
    copper_reload.add("dimensions")
    copper_reload.add("pieces")
    copper_reload.add("precious")
    copper_reload.add("motor")

    copper_reload.from_file("/tmp/materials_copper.yml", instance_name="copper")
    assert copper_reload.color == "yellow-brown"
    assert copper_reload.specific_weight == 8.96
    assert copper_reload.dimensions == [5, 10, 15]
    assert copper_reload.pieces == {"first": 40.3, "second": 27.2, "count": [1, 2, 3]}
    assert copper_reload.precious is False
    assert copper_reload.motor == session.config.get("robz")

    materials_reload = ParametersWardrobe(
        "materials_reload", connection=get_redis_proxy(caching=caching)
    )
    materials_reload.add("color", "nocolor")
    materials_reload.add("specific_weight", 0)
    materials_reload.add("dimensions")
    materials_reload.add("pieces")
    materials_reload.add("precious")
    materials_reload.add("motor")

    materials_reload.switch("copper")
    materials_reload.from_file("/tmp/materials_all.yml", instance_name="copper")
    materials_reload.switch("gold")
    materials_reload.from_file("/tmp/materials_all.yml", instance_name="gold")
    materials_reload.switch("default")
    materials_reload.from_file("/tmp/materials_all.yml", instance_name="default")

    materials_reload.switch("gold")
    assert materials_reload.color == "gold"
    assert materials_reload.specific_weight == 19.32
    assert materials_reload.dimensions == [1, 2, 3]
    assert materials_reload.pieces == {
        "first": 10.3,
        "second": 20.2,
        "count": [5, 2, 5],
    }
    assert materials_reload.precious is True
    assert materials_reload.motor.name == session.config.get("roby").name

    materials_reload.switch("copper")
    assert materials_reload.color == "yellow-brown"
    assert materials_reload.specific_weight == 8.96
    assert materials_reload.dimensions == [5, 10, 15]
    assert materials_reload.pieces == {
        "first": 40.3,
        "second": 27.2,
        "count": [1, 2, 3],
    }
    assert materials_reload.precious is False
    assert materials_reload.motor.name == session.config.get("robz").name

    materials_reload.switch("default")
    # default should be loaded from file and be different
    # from previous values
    assert materials_reload.color is None
    assert materials_reload.specific_weight is None
    assert materials_reload.dimensions is None
    assert materials_reload.specific_weight is None
    assert materials_reload.precious is False
    assert materials_reload.motor.name == session.config.get("roby").name


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_empty_from_yml_file(session, caching):
    empty_material = ParametersWardrobe(
        "empty_material", connection=get_redis_proxy(caching=caching)
    )
    with pytest.raises(KeyError):
        # current set is empty and to be strict we should not be able
        # to load values
        empty_material.from_file("/tmp/materials_all.yml")


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_from_yml_file_partial(session, caching):
    material = ParametersWardrobe(
        "material", connection=get_redis_proxy(caching=caching)
    )
    material.add("color")
    material.add("specific_weight")
    material.add("other")  # this is not in the yml file but importing should work
    material.add("dimensions")
    material.add("pieces")
    material.add("precious")
    material.add("motor")
    # this should succeed
    material.from_file("/tmp/materials_all.yml", instance_name="copper")
    material.from_file("/tmp/materials_all.yml", instance_name="gold")
    material.from_file("/tmp/materials_all.yml", instance_name="default")


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_check_atomic_operation(session, caching):
    atomic = ParametersWardrobe("atomic", connection=get_redis_proxy(caching=caching))
    atomic.add("first", 1)
    atomic.add("second", 2)
    d = atomic.to_dict()
    d["first"] = "I"
    d["second"] = "II"
    d["third"] = 3
    with pytest.raises(AttributeError):
        atomic.from_dict(d)
    assert atomic.first == 1
    assert atomic.second == 2
    with pytest.raises(AttributeError):
        atomic.third


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_freeze(session, caching):
    temperature = ParametersWardrobe(
        "temperature", connection=get_redis_proxy(caching=caching)
    )
    temperature.add("water", "liquid")
    temperature.switch("t20")

    assert "water" not in temperature._get_redis_single_instance("t20")  # not in redis
    temperature.freeze()
    temperature.switch("warm", copy="default")
    assert "water" in temperature._get_redis_single_instance(
        "t20"
    )  # should be in Redis
    assert "water" in temperature._get_redis_single_instance(
        "warm"
    )  # should be in Redis


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_low_level_methods(session, caching):
    materials = get_materials(session, caching)
    assert materials._get_redis_single_instance("not existant") == {}
    with pytest.raises(NameError):
        assert materials._get_instance("not existant") == {}


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_atomic_reset_default(beacon, caching):
    """Check that reseting an attribute do not reset others"""

    # get a toy, paint it, and mark it
    toy = ParametersWardrobe(
        "toy",
        default_values={"marks": [], "color": "white"},
        connection=get_redis_proxy(caching=caching),
    )
    toy.color = "pink"
    toy.marks = ["blue", "yellow"]

    # clean up the marks
    same_toy = ParametersWardrobe(
        "toy",
        default_values={"marks": [], "color": "white"},
        connection=get_redis_proxy(caching=caching),
    )
    same_toy.marks = None

    # there is no more marks but the color stay the same
    again_same_toy = ParametersWardrobe(
        "toy",
        default_values={"marks": [], "color": "white"},
        connection=get_redis_proxy(caching=caching),
    )
    assert again_same_toy.marks == []
    assert again_same_toy.color == "pink"


@pytest.mark.parametrize("caching", [False, True])
def test_wardrobe_from_to_beacon(session, caching):
    materials = get_materials(session, caching)
    materials.to_beacon("mat_eri-als23", *materials.instances)

    beacon_material = ParametersWardrobe(
        "beacon_material", connection=get_redis_proxy(caching=caching)
    )
    beacon_material.add("color")
    beacon_material.add("specific_weight")
    beacon_material.add("dimensions")
    beacon_material.add("pieces")
    beacon_material.add("precious")
    beacon_material.add("motor")

    beacon_material.from_beacon("mat_eri-als23", "default")

    beacon_material.switch("copper")
    beacon_material.from_beacon("mat_eri-als23", "copper")

    beacon_material.switch("gold")
    beacon_material.from_beacon("mat_eri-als23", "gold")

    assert beacon_material.color == "gold"
    assert beacon_material.specific_weight == 19.32
    assert beacon_material.dimensions == [1, 2, 3]
    assert beacon_material.pieces == {"first": 10.3, "second": 20.2, "count": [5, 2, 5]}
    assert beacon_material.precious is True
    assert beacon_material.motor.name == session.config.get("roby").name

    beacon_material.switch("copper")
    assert beacon_material.color == "yellow-brown"
    assert beacon_material.specific_weight == 8.96
    assert beacon_material.dimensions == [5, 10, 15]
    assert beacon_material.pieces == {"first": 40.3, "second": 27.2, "count": [1, 2, 3]}
    assert beacon_material.precious is False
    assert beacon_material.motor.name == session.config.get("robz").name

    beacon_material.switch("default")
    # default should be loaded from file and be different
    # from previous values
    assert beacon_material.color is None
    assert beacon_material.specific_weight is None
    assert beacon_material.dimensions is None
    assert beacon_material.specific_weight is None
    assert beacon_material.precious is False
    assert beacon_material.motor.name == session.config.get("roby").name


@pytest.mark.parametrize("caching", [False, True])
def test_bad_name_for_beacon(session, caching):
    bad = ParametersWardrobe("bad", connection=get_redis_proxy(caching=caching))
    for name in r"!@#$%^&*()123804/`-+=,./".split():
        with pytest.raises(NameError):
            bad.to_beacon(name, "default")
        with pytest.raises(NameError):
            bad.from_beacon(name, "default")


@pytest.mark.parametrize("caching", [False, True])
def test_purge(beacon, caching):
    purge_me = ParametersWardrobe(
        "purge_me", connection=get_redis_proxy(caching=caching)
    )
    purge_me.switch("new_instance")
    connection = beacon._connection.get_redis_proxy(db=0)
    assert connection.exists("parameters:purge_me")
    assert connection.exists("parameters:purge_me:default")
    assert connection.exists("parameters:purge_me:new_instance")
    purge_me.purge()
    assert not connection.exists("parameters:purge_me")
    assert not connection.exists("parameters:purge_me:default")
    assert not connection.exists("parameters:purge_me:new_instance")

    with pytest.raises(IOError):
        # try to access Wardrobe after purge will raise an exception
        purge_me.current_instance


@pytest.mark.parametrize("caching", [False, True])
@pytest.mark.parametrize("new_value", [4, None])
def test_wardrobe_parameter_to_slot(beacon, caching, new_value):
    default_values = {"a": 1, "b": 2, "to_slot": 3}
    wardrobe = ParametersWardrobe(
        "wardrobe",
        default_values=default_values,
        connection=get_redis_proxy(caching=caching),
    )
    assert wardrobe.to_dict() == default_values

    class ModifiedParametersWardrobe(ParametersWardrobe):
        SLOTS = ParametersWardrobe.SLOTS + ["to_slot"]

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.to_slot = new_value
            assert getattr(self, "to_slot") == new_value, "issue #4050"

    default_values.pop("to_slot")
    wardrobe = ModifiedParametersWardrobe(
        "wardrobe", connection=get_redis_proxy(caching=caching)
    )
    assert wardrobe.to_dict() == default_values
    assert wardrobe.to_slot == new_value
