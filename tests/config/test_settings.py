# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pickle
import gevent
from collections import OrderedDict
import pytest

from bliss.config import settings
from bliss.config.conductor.client import get_redis_proxy


class DummyObject(object):
    pass


@pytest.mark.parametrize("caching", [False, True])
def test_simple_setting(beacon, caching):
    myValue = "valval"
    sss = settings.SimpleSetting(
        "mySkey", default_value=myValue, connection=get_redis_proxy(caching=caching)
    )
    assert sss.get() == "valval"
    sss.set("V2")  # 'V2' key exists now in redis
    assert sss.get() == "V2"


@pytest.mark.parametrize("caching", [False, True])
def test_simple_setting_types(beacon, caching):
    # INT
    anIntValue = 666
    iii = settings.SimpleSetting(
        "myIntkey",
        default_value=anIntValue,
        connection=get_redis_proxy(caching=caching),
    )
    assert iii.get() == anIntValue
    iii.set(63825363)
    assert iii.get() == 63825363
    assert type(iii.get()) is int

    # FLOAT
    aFloatValue = 3.14159
    fff = settings.SimpleSetting(
        "myFloatkey",
        default_value=aFloatValue,
        connection=get_redis_proxy(caching=caching),
    )
    assert fff.get() == aFloatValue
    fff.set(2.71)
    assert fff.get() == pytest.approx(2.71, 0.001)
    assert type(fff.get()) is float

    # STRING
    aStringValue = "Hello World !"
    sss = settings.SimpleSetting(
        "myStringKey",
        default_value=aStringValue,
        connection=get_redis_proxy(caching=caching),
    )
    assert sss.get() == aStringValue
    sss.set("Good bye")
    assert sss.get() == "Good bye"
    assert type(sss.get()) is str

    # BOOLEAN
    aBoolValue = False
    bbb = settings.SimpleSetting(
        "myBoolKey",
        default_value=aBoolValue,
        connection=get_redis_proxy(caching=caching),
    )
    assert bbb.get() == aBoolValue
    bbb.set(True)
    assert bbb.get() is True
    assert type(bbb.get()) is bool

    # TUPLE
    aTupleValue = (1, 2, 3)
    ttt = settings.SimpleSetting(
        "myTupleKey",
        default_value=aTupleValue,
        connection=get_redis_proxy(caching=caching),
    )
    assert ttt.get() == aTupleValue
    ttt.set(("a", "b", "c"))
    # Oh oH !!! this is now a string.
    assert ttt.get() == "('a', 'b', 'c')"
    assert type(ttt.get()) is str


def _assert_dict_api(pyDict, redisDict):
    def mysorted(items):
        if isinstance(pyDict, OrderedDict):
            return list(items)
        else:
            return sorted(items)

    def validate_getters():
        assert redisDict.get_all() == pyDict
        assert len(redisDict) == len(pyDict)
        assert mysorted(redisDict.items()) == mysorted(pyDict.items())
        assert mysorted(redisDict.values()) == mysorted(pyDict.values())
        assert mysorted(redisDict.keys()) == mysorted(pyDict.keys())
        for key in pyDict:
            assert redisDict[key] == pyDict[key]

    validate_getters()

    pyDict["key3"] = "value3"
    redisDict["key3"] = "value3"
    validate_getters()

    pyDict["key2"] += "suffix"
    redisDict["key2"] += "suffix"
    validate_getters()

    del pyDict["key2"]
    del redisDict["key2"]
    validate_getters()

    add = [("key4", "value4"), ("key5", "value5")]
    pyDict.update(add)
    redisDict.update(OrderedDict(add))
    validate_getters()

    assert pyDict.pop("key4") == redisDict.pop("key4")
    assert pyDict.pop("key6", None) == redisDict.pop("key6", None)
    validate_getters()

    assert pyDict.get("key5") == redisDict.get("key5")
    assert pyDict.get("key6") == redisDict.get("key6")
    assert pyDict.get("key6", "value6") == redisDict.get("key6", "value6")
    validate_getters()


@pytest.mark.parametrize("caching", [False, True])
def test_basehash_setting_dictapi(beacon, caching):
    pyDict = {"key1": "value1", "key2": "value2"}
    redisDict = settings.BaseHashSetting(
        "pyDict", connection=get_redis_proxy(caching=caching)
    )
    redisDict.update(pyDict)
    _assert_dict_api(pyDict, redisDict)


@pytest.mark.parametrize("caching", [False, True])
def test_hash_setting_dictapi(beacon, caching):
    pyDict = {"key1": "value1", "key2": "value2"}
    redisDict = settings.HashSetting(
        "pyDict", connection=get_redis_proxy(caching=caching)
    )
    redisDict.update(pyDict)
    _assert_dict_api(pyDict, redisDict)


@pytest.mark.parametrize("caching", [False, True])
def test_orderedhash_setting_dictapi(beacon, caching):
    pyDict = OrderedDict([("key1", "value1"), ("key2", "value2")])
    redisDict = settings.OrderedHashSetting(
        "pyDict", connection=get_redis_proxy(caching=caching)
    )
    redisDict.update(pyDict)
    _assert_dict_api(pyDict, redisDict)


@pytest.mark.parametrize("caching", [False, True])
def test_orderedhash_setting_from_hash_setting(beacon, caching):
    test_dict = {k: i for i, k in enumerate(["r3", "r03", "r11", "r2", "r1", "r14"])}

    # first create a classic hash setting in Redis
    redisDict = settings.HashSetting(
        "test_dict", connection=get_redis_proxy(caching=caching)
    )
    redisDict.update(test_dict)

    # then, convert it to "ordered"
    redisDict2 = settings.OrderedHashSetting(
        "test_dict", connection=get_redis_proxy(caching=caching)
    )
    # check that ordered keys have been created and type preserved
    assert redisDict2._cnx().zrange(redisDict2._name_order, 0, -1) == [
        b"r1",
        b"r2",
        b"r3",
        b"r03",
        b"r11",
        b"r14",
    ]
    # check that OrderedHashSetting returns ordered and converted keys
    assert list(redisDict2.keys()) == ["r1", "r2", "r3", "r03", "r11", "r14"]


@pytest.mark.parametrize("caching", [False, True])
def test_basehash_setting(beacon, caching):
    my_dict = {"C1": "riri", "C2": "fifi"}
    shs = settings.BaseHashSetting(
        "newkey", connection=get_redis_proxy(caching=caching)
    )
    for k, v in my_dict.items():
        shs[k] = v

    assert list(my_dict.items()) == list(shs.items())
    assert list(my_dict.values()) == list(shs.values())
    assert list(my_dict.keys()) == list(shs.keys())


@pytest.mark.parametrize("caching", [False, True])
def test_basehash_settings_ttl(beacon, caching):
    ihavetodie = settings.BaseHashSetting(
        "ihavetodie", connection=get_redis_proxy(caching=caching)
    )
    ihavetodie["I"] = "alive"
    assert ihavetodie.ttl(1)
    assert ihavetodie["I"] == "alive"
    gevent.sleep(1.5)
    with pytest.raises(KeyError):
        ihavetodie["I"]


@pytest.mark.parametrize("caching", [False, True])
def test_hash_setting(beacon, caching):
    myDict = {"C1": "riri", "C2": "fifi"}
    shs = settings.HashSetting(
        "myHkey", default_values=myDict, connection=get_redis_proxy(caching=caching)
    )
    assert list(myDict.items()) == list(shs.items())
    assert list(myDict.values()) == list(shs.values())
    assert list(shs.keys()) == list(myDict.keys())


@pytest.mark.parametrize("caching", [False, True])
def test_hash_setting_default_value(beacon, caching):
    shs = settings.HashSetting("myNewHkey", connection=get_redis_proxy(caching=caching))
    test_object = DummyObject()
    assert shs.get("a") is None
    setting_object = shs.get("a", default=test_object)
    assert test_object is setting_object

    default_values = {"key1": "value1", "key2": "value2"}
    shs = settings.HashSetting(
        "myNewHkey",
        default_values=default_values,
        connection=get_redis_proxy(caching=caching),
    )
    assert default_values == shs.get_all()
    shs.remove("key1")
    assert default_values == shs.get_all()


@pytest.mark.parametrize("caching", [False, True])
def test_hash_setting_default_value_readwrite_conv(beacon, caching):
    shs = settings.HashSetting(
        "myNewHkey",
        read_type_conversion=settings.pickle_loads,
        write_type_conversion=pickle.dumps,
        connection=get_redis_proxy(caching=caching),
    )

    test_object = DummyObject()

    assert shs.get("a") is None
    setting_object = shs.get("a", default=test_object)
    assert test_object is setting_object


def test_struct(beacon):
    pyDict = {"key1": "value1", "key2": "value2"}
    defaults = {"key1": "default1"}
    redisStruct = settings.Struct("pyDict", default_values=defaults)

    redisStruct.key1 = pyDict["key1"]
    redisStruct.key2 = pyDict["key2"]
    assert redisStruct.key1 == pyDict["key1"]
    assert redisStruct.key2 == pyDict["key2"]
    assert sorted(dir(redisStruct)) == ["key1", "key2"]

    del redisStruct.key1
    del redisStruct.key2
    assert redisStruct.key1 == defaults["key1"]
    assert redisStruct.key2 is None
    assert sorted(dir(redisStruct)) == ["key1"]


"""
@pytest.mark.parametrize("caching", [False, True])
def test_hash_settings_from_keys(beacon, caching):
    fromk = settings.OrderedHashSetting("fromk", connection=get_redis_proxy(caching=caching))
    fromk.set({"first":"I","second":"II"})
    assert list(fromk.fromkeys('first','second','third')) == ['I',"II",None]
    assert list(fromk.fromkeys('third','second','first')) == [None, 'II',"I"]

    generator = fromk.fromkeys('first','second','third')
    assert next(generator) =="I"
    assert next(generator) =="II"
    fromk['third'] = "III"
    assert next(generator) is None
"""


@pytest.mark.parametrize("caching", [False, True])
def test_orderedhash_settings(beacon, caching):
    data = tuple(
        (str(n), v) for n, v in enumerate((ch for ch in "abcdefghilmnopqrstuvz"))
    )
    ohs = settings.OrderedHashSetting(
        "ordhashset", connection=get_redis_proxy(caching=caching)
    )
    for k, v in data:
        ohs[k] = v

    get_all = ohs.get_all()

    assert tuple(ohs.items()) == data == tuple(get_all.items())
    assert tuple(ohs.keys()) == tuple(k for k, v in data)


@pytest.mark.parametrize("caching", [False, True])
def test_orderedhas_settings_resize(beacon, caching):
    data = tuple(
        (str(n), v) for n, v in enumerate((ch for ch in "abcdefghilmnopqrstuvz"))
    )
    ohs = settings.OrderedHashSetting(
        "resizeordhashset", connection=get_redis_proxy(caching=caching)
    )
    for k, v in data:
        ohs[k] = v
    cnx = ohs._cnx()
    # popping will remove from the zset the element with lower score
    # in this case is attr='1' score=1
    attr, order = cnx.zpopmin(ohs._name_order)[0]
    assert attr, order == ("1", 1)
    ohs[100] = "bla"  # assigning a value will call lua script and trigger the resize
    attr, order = cnx.zpopmin(ohs._name_order)[0]
    assert attr, order == ("2", 1)  # the order is still 1 because is resized
    attr, order = cnx.zpopmin(ohs._name_order)[0]
    assert attr, order == ("3", 2)
    ohs[100] = "bla"  # doing it again
    attr, order = cnx.zpopmin(ohs._name_order)[0]
    assert attr, order == ("4", 1)


@pytest.mark.parametrize("caching", [False, True])
def test_orderedhash_settings_remove(beacon, caching):
    removeme = settings.OrderedHashSetting(
        "removeme", connection=get_redis_proxy(caching=caching)
    )
    removeme["a"] = "a"
    removeme["b"] = "b"
    removeme["c"] = (1, 2, 3)
    assert tuple(removeme.items()) == (("a", "a"), ("b", "b"), ("c", "(1, 2, 3)"))
    assert tuple(removeme.values()) == ("a", "b", "(1, 2, 3)")
    removeme.remove("b")
    assert "b" not in removeme
    assert tuple(removeme.items()) == (("a", "a"), ("c", "(1, 2, 3)"))


@pytest.mark.parametrize("caching", [False, True])
def test_orderedhash_settings_update(beacon, caching):
    updateme = settings.OrderedHashSetting(
        "updateme", connection=get_redis_proxy(caching=caching)
    )
    updateme.update({1: 1, 2: 2})
    assert tuple(updateme.items()) == (("1", 1), ("2", 2))
    updateme.update({4: 4, 3: 3})
    assert tuple(updateme.items()) == (("1", 1), ("2", 2), ("4", 4), ("3", 3))

    updateme = settings.OrderedHashSetting(
        "updateme2", connection=get_redis_proxy(caching=caching)
    )
    updateme["a"] = 1
    updateme["b"] = 2
    updateme.update({"c": 3})

    assert tuple(updateme.keys()) == tuple(("a", "b", "c"))
    assert len(updateme) == 3
    assert tuple(updateme.values()) == tuple((1, 2, 3))


@pytest.mark.parametrize("caching", [False, True])
def test_orderedhash_settings_set(beacon, caching):
    setme = settings.OrderedHashSetting(
        "setme", connection=get_redis_proxy(caching=caching)
    )

    setme.set({"first": "I", "second": "II"})
    assert tuple((k, v) for k, v in setme.get_all().items()) == tuple(
        (("first", "I"), ("second", "II"))
    )

    setme.set({"firstagain": 1, "secondagain": 2})
    assert tuple((k, v) for k, v in setme.get_all().items()) == tuple(
        (("firstagain", 1), ("secondagain", 2))
    )

    setme.set({"1": 11, "2": 22})
    assert tuple((k, v) for k, v in setme.get_all().items()) == tuple(
        (("1", 11), ("2", 22))
    )
    assert len(setme) == 2
    del setme["1"]
    assert len(setme) == 1

    setme.set({"a": 11, "b": 22})
    assert setme["a"] == 11
    setme["a"] = None
    assert "a" not in setme.keys()
    assert "a" not in setme


@pytest.mark.parametrize("caching", [False, True])
def test_orderedhash_settings_contains(beacon, caching):
    haskeys = settings.OrderedHashSetting(
        "haskeys", connection=get_redis_proxy(caching=caching)
    )
    haskeys.set({"first": "I", "second": "II"})
    for item in "first", "second":
        assert item in haskeys


"""
def test_orderedhash_settings_from_keys(beacon):
    fromkeys = settings.OrderedHashSetting("fromkeys")
    fromkeys.set({"first":"I","second":"II"})
    assert list(fromkeys.fromkeys('first','second','third')) == ['I',"II",None]
    assert list(fromkeys.fromkeys('third','second','first')) == [None, 'II',"I"]
"""


@pytest.mark.parametrize("caching", [False, True])
def test_orderedhash_settings_ttl(beacon, caching):
    ihavetodie = settings.OrderedHashSetting(
        "ihavetodie", connection=get_redis_proxy(caching=caching)
    )
    ihavetodie["I"] = "alive"
    assert ihavetodie.ttl(1)
    assert ihavetodie["I"] == "alive"
    gevent.sleep(1.5)
    with pytest.raises(KeyError):
        ihavetodie["I"]
    assert len(ihavetodie._cnx().zrange(ihavetodie._name_order, 0, -1)) == 0


@pytest.mark.parametrize("caching", [False, True])
def test_queue_setting(beacon, caching):
    myList = ["a", "b", "c", "d"]

    sqs = settings.QueueSetting("myQkey", connection=get_redis_proxy(caching=caching))
    sqs.set(myList)
    assert sqs.pop_back() == "d"
    # myList = ["a", "b", "c"]
    assert sqs.pop_front() == "a"
    # myList = ["b", "c"]
    sqs.remove("c")
    # myList = ["b"]
    assert sqs.pop_back() == "b"

    with pytest.raises(ValueError):
        print(settings.InvalidValue())


@pytest.mark.parametrize("caching", [False, True])
def test_pipeline_settings(beacon, caching):
    t = settings.HashSetting("super_fancy", connection=get_redis_proxy(caching=caching))
    values = [("val1", 1), ("val2", 2)]
    with settings.pipeline(t):
        for val_name, value in values:
            t[val_name] = value
    try:
        with settings.pipeline(t) as p:
            for val_name, value in values:
                t[val_name]  # get
            assert p.execute() == [b"1", b"2"]
    finally:
        t.clear()


def test_pipeline_bad_setting_object(beacon):
    class BadSetting:
        pass

    bad_setting = BadSetting()
    with pytest.raises(TypeError):
        with settings.pipeline(bad_setting):
            pass
