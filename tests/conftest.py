# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from __future__ import annotations
import asyncio
import atexit
import gc
import io
import logging
import os
import pytest
import shutil
import socket
import subprocess
import sys
import traceback
import weakref
import typing
from collections import namedtuple
from contextlib import contextmanager
from pathlib import Path
from unittest.mock import MagicMock

import gevent
from gevent import Greenlet
from prompt_toolkit.application import create_app_session
from prompt_toolkit.input.defaults import create_pipe_input
from prompt_toolkit.output.plain_text import PlainTextOutput
from prompt_toolkit.output.vt100 import Vt100_Output
from prompt_toolkit.output import Output
import redis
import redis.connection

import bliss
from bliss import global_log, global_map
from bliss.common import logtools, plot
from bliss.common import session as session_module
from bliss.common.data_store import set_default_data_store
from bliss.common.tango import ApiUtil, DeviceProxy, DevState
from bliss.common.utils import copy_tree, get_open_ports, rm_tree
from bliss.config import static
from bliss.config.conductor import client, connection
from bliss.config.conductor.client import get_default_connection
from bliss.controllers import simulation_diode, tango_attr_as_counter
from bliss.controllers.lima.roi import Roi
from bliss.controllers.wago.emulator import WagoEmulator
from bliss.controllers.wago.wago import ModulesConfig
from bliss.flint.client import proxy as flint_proxy
from bliss.icat.client import DatasetId
from bliss.scanning import scan_meta
from bliss.shell.cli.repl import cli
from bliss.shell.cli.bliss_repl import BlissRepl
from bliss.shell.cli.bliss_app_session import bliss_app_session
from bliss.shell.log_utils import logging_startup
from bliss.tango.clients.utils import wait_tango_db
from bliss.testutils.comm_utils import wait_tcp_online
from bliss.testutils.process_utils import start_tango_server, wait_terminate

from blissdata.redis_engine.store import DataStore

BLISS = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
BEACON = [sys.executable, "-m", "bliss.config.conductor.server"]
BEACON_DB_PATH = os.path.join(BLISS, "tests", "test_configuration")
IMAGES_PATH = os.path.join(BLISS, "tests", "images")


@pytest.fixture
def clear_pt_context():
    """Clear the context used by prompt-toolkit in order to isolate tests"""
    from prompt_toolkit.application import current

    app_session = current._current_app_session.get()
    is_default = app_session._input is None and app_session._output is None
    yield
    if is_default:
        app_session._input = None
        app_session._output = None


@pytest.fixture
def capsys(clear_pt_context, capsys):
    """Monkey patch capsys to make it compatible with prompt-toolkit

    capsys replace sys.stdout, then prompt toolkit creates a context on it.
    This mocked stdout is finally closed, but the prompt toolkit context
    still point to it. `clear_pt_context` force to drop the pt context.
    """
    yield capsys


def eprint(*args):
    print(*args, file=sys.stderr, flush=True)


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"
    setattr(item, "rep_" + rep.when, rep)


def raise_when_test_passed(request, err_msg: str, end_check: bool = True) -> None:
    node = request.node
    if end_check and hasattr(node, "rep_call") and node.rep_call.passed:
        raise RuntimeError(err_msg)
    else:
        eprint(err_msg)


class ResourcesContext:
    """
    This context ensure that every resource created during its execution
    are properly released.

    If a resource is not released at the exit, a warning is displayed,
    and it tries to release it.

    It is not concurrency safe.
    """

    def __init__(self, release, is_released, *resource_classes):
        self.resource_classes = resource_classes
        self.is_released = is_released
        self.release = release
        self.resources_before = weakref.WeakSet()
        self.all_resources_released = None

    def _iter_referenced_resources(self):
        for ob in gc.get_objects():
            try:
                if not isinstance(ob, self.resource_classes):
                    continue
            except ReferenceError:
                continue
            yield ob

    def __enter__(self):
        self.resources_before.clear()
        self.all_resources_released = None
        for ob in self._iter_referenced_resources():
            self.resources_before.add(ob)
        return self

    def resource_repr(self, ob):
        return repr(ob)

    def __exit__(self, exc_type, exc_val, exc_tb):
        resources = []
        for ob in self._iter_referenced_resources():
            if ob in self.resources_before:
                continue
            if not self.is_released(ob):
                eprint(f"Resource not released: {self.resource_repr(ob)}")
            resources.append(ob)

        self.resources_before.clear()
        self.all_resources_released = all(self.is_released(r) for r in resources)
        if not resources:
            return
        err_msg = f"Resources {self.resource_classes} cannot be released"
        with gevent.Timeout(10, RuntimeError(err_msg)):
            for r in resources:
                self.release(r)


class GreenletsContext(ResourcesContext):
    def __init__(self):
        super().__init__(lambda glt: glt.kill(), lambda glt: glt.ready(), Greenlet)


class SocketsContext(ResourcesContext):
    def __init__(self):
        super().__init__(
            lambda sock: sock.close(), lambda sock: sock.fileno() == -1, socket.socket
        )

    def resource_repr(self, sock):
        try:
            return f"{repr(sock)} connected to {sock.getpeername()}"
        except Exception:
            return f"{repr(sock)} not connected"


class RedisConnectionContext(ResourcesContext):
    def __init__(self):
        super().__init__(
            lambda conn: conn.disconnect(),
            lambda conn: conn._sock.fileno() == -1,
            redis.connection.Connection,
        )

    def resource_repr(self, conn):
        return f"{repr(conn)} connected to {conn._sock.getpeername()}"


@pytest.fixture(autouse=True)
def clean_louie(request):
    import louie.dispatcher as disp

    disp.connections = {}
    disp.senders = {}
    disp.senders_back = {}
    disp.plugins = []

    d = {"end-check": True}
    yield d
    end_check = d.get("end-check")

    try:
        if disp.connections:
            raise_when_test_passed(
                request, "Louie connections not released", end_check=end_check
            )
        if disp.senders:
            raise_when_test_passed(
                request, "Louie senders not released", end_check=end_check
            )
        if disp.senders_back:
            raise_when_test_passed(
                request, "Louie senders_back not released", end_check=end_check
            )
        if disp.plugins:
            raise_when_test_passed(
                request, "Louie plugins not released", end_check=end_check
            )
    finally:
        disp.reset()


@pytest.fixture(autouse=True)
def clean_gevent(request):
    """
    Context manager to check that greenlets are properly released during a test.

    It is not concurrency safe. The global context is used to
    check available greenlets.

    If the fixture is used as the last argument if will only test the greenlets
    creating during the test.

    .. code-block:: python

        def test_a(fixture_a, fixture_b, clean_gevent):
            ...

    If the fixture is used as the first argument if will also test greenlets
    created by sub fixtures.

    .. code-block:: python

        def test_b(clean_gevent, fixture_a, fixture_b):
            ...
    """
    d = {"end-check": True}
    with GreenletsContext() as context:
        yield d
    end_check = d.get("end-check")
    if not context.all_resources_released:
        raise_when_test_passed(
            request, "Not all greenlets were released", end_check=end_check
        )


@pytest.fixture
def clean_socket(request):
    """
    Context manager to check that sockets are properly closed during a test.

    It is not concurrency safe. The global context is used to
    check available sockets.

    If the fixture is used as the last argument if will only test the sockets
    creating during the test.

    .. code-block:: python

        def test_a(fixture_a, fixture_b, clean_gevent):
            ...

    If the fixture is used as the first argument if will also test sockets
    created by sub fixtures.

    .. code-block:: python

        def test_b(clean_socket, fixture_a, fixture_b):
            ...
    """
    d = {"end-check": True}
    with SocketsContext() as context:
        yield d
    end_check = d.get("end-check")
    if not context.all_resources_released:
        raise_when_test_passed(
            request, "Not all sockets were released", end_check=end_check
        )


@pytest.fixture(autouse=True)
def clean_globals():
    orig_excepthook = sys.excepthook
    yield
    sys.excepthook = orig_excepthook
    global_log.clear()
    global_map.clear()
    # reset module-level globals
    simulation_diode.DEFAULT_CONTROLLER = None
    simulation_diode.DEFAULT_CONTROLLER = None
    simulation_diode.DEFAULT_INTEGRATING_CONTROLLER = None
    bliss._BLISS_SHELL_MODE = False
    session_module.sessions.clear()
    scan_meta.USER_SCAN_META = None

    # clean modif from shell.cli.repl
    logtools.elogbook.disable()

    tango_attr_as_counter._TangoCounterControllerDict = weakref.WeakValueDictionary()


@pytest.fixture(autouse=True)
def clean_tango():
    # close file descriptors left open by Tango (see tango-controls/pytango/issues/324)
    try:
        ApiUtil.cleanup()
    except RuntimeError:
        # no Tango ?
        pass


@pytest.fixture(scope="session")
def homepage_port(ports):
    yield ports.homepage_port


@pytest.fixture(scope="session")
def beacon_tmpdir(tmpdir_factory):
    tmpdir = str(tmpdir_factory.mktemp("beacon"))
    yield tmpdir


@pytest.fixture(scope="session")
def beacon_directory(beacon_tmpdir):
    beacon_dir = os.path.join(beacon_tmpdir, "test_configuration")
    shutil.copytree(BEACON_DB_PATH, beacon_dir)
    yield beacon_dir


@pytest.fixture(scope="session")
def log_directory(beacon_tmpdir):
    log_dir = os.path.join(beacon_tmpdir, "log")
    os.mkdir(log_dir)
    yield log_dir


@pytest.fixture(scope="session")
def images_directory(tmpdir_factory):
    images_dir = os.path.join(str(tmpdir_factory.getbasetemp()), "images")
    shutil.copytree(IMAGES_PATH, images_dir)
    yield images_dir


@pytest.fixture(scope="session")
def ports(beacon_directory, log_directory):
    redis_uds = os.path.join(beacon_directory, ".redis.sock")
    redis_data_uds = os.path.join(beacon_directory, ".redis_data.sock")

    port_names = [
        "redis_port",
        "redis_data_port",
        "tango_port",
        "beacon_port",
        "logserver_port",
        "homepage_port",
    ]

    ports = namedtuple("Ports", " ".join(port_names))(*get_open_ports(6))
    os.environ["TANGO_HOST"] = f"localhost:{ports.tango_port}"
    os.environ["BEACON_HOST"] = f"localhost:{ports.beacon_port}"
    args = [
        f"--port={ports.beacon_port}",
        f"--redis-port={ports.redis_port}",
        f"--redis-socket={redis_uds}",
        f"--redis-data-port={ports.redis_data_port}",
        f"--redis-data-socket={redis_data_uds}",
        f"--db-path={beacon_directory}",
        f"--tango-port={ports.tango_port}",
        f"--homepage-port={ports.homepage_port}",
        f"--log-server-port={ports.logserver_port}",
        f"--log-output-folder={log_directory}",
        "--log-level=WARN",
        "--tango-debug-level=0",
    ]
    proc = subprocess.Popen(BEACON + args)
    wait_tcp_online("localhost", ports.beacon_port, timeout=10)

    try:
        wait_tango_db(port=ports.tango_port, db=2, timeout=10)
    except BaseException:
        # don't wait for all tests to fail if the tango db doesn't work
        pytest.exit(traceback.format_exc(), returncode=-1)

    # disable .rdb files saving (redis persistence)
    r = redis.Redis(host="localhost", port=ports.redis_port)
    r.config_set("SAVE", "")
    del r

    yield ports

    atexit._run_exitfuncs()
    wait_terminate(proc)


@pytest.fixture
def blissdata(ports):
    redis_url = f"redis://localhost:{ports.redis_data_port}"

    red = redis.Redis.from_url(redis_url)
    red.flushall()
    try:
        data_store = DataStore(redis_url, init_db=True)
    except RuntimeError:
        # https://gitlab.esrf.fr/bliss/bliss/-/issues/4098
        # Memory tracker is part of beacon fixture which is session scoped.
        # Thus, it keeps running while this fixture restart, which may cause
        # the db to not be empty after flushall.
        red.delete("_MEMORY_TRACKING_")
        data_store = DataStore(redis_url, init_db=True)

    set_default_data_store(redis_url)
    try:
        yield data_store
    finally:
        data_store._redis.connection_pool.disconnect()
        data_store._redis = None


@pytest.fixture
def beacon(ports, beacon_directory, blissdata):
    redis_db = redis.Redis(port=ports.redis_port)
    redis_db.flushall()
    static.Config.instance = None
    client._default_connection = connection.Connection("localhost", ports.beacon_port)
    config = static.get_config()
    yield config
    config.close()
    client._default_connection.close()
    # Ensure no connections are created due to garbage collection:
    client._default_connection = None

    # Always restore beacon directory
    rm_tree(Path(beacon_directory))  # still keeps Unix socket "files"
    # Restore files from BEACON_DB_PATH
    copy_tree(Path(BEACON_DB_PATH), Path(beacon_directory))


@pytest.fixture
def beacon_host_port(ports):
    return "localhost", ports.beacon_port


@pytest.fixture
def redis_conn(beacon):
    cnx = get_default_connection()
    redis_conn = cnx.get_redis_proxy()
    yield redis_conn


@pytest.fixture
def redis_data_conn(beacon):
    cnx = get_default_connection()
    redis_conn = cnx.get_redis_proxy(db=1)
    yield redis_conn


@pytest.fixture
def scan_tmpdir(tmpdir):
    yield tmpdir


@contextmanager
def lima_simulator_context(personal_name, device_name):
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/LimaCCDs/{personal_name}"

    conda_env = os.environ.get("LIMA_SIMULATOR_CONDA_ENV")
    if not conda_env:
        conda_env = None
    conda = os.environ.get("CONDA_EXE", None)
    if conda_env and conda:
        if os.sep in conda_env:
            option = "-p"
        else:
            option = "-n"
        runner = [conda, "run", option, conda_env, "--no-capture-output", "LimaCCDs"]
    else:
        runner = ["LimaCCDs"]

    with start_tango_server(
        *runner,
        personal_name,
        # "-v4",               # to enable debug
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=None,
        check_children=conda_env is not None,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def lima_simulator(ports):
    with lima_simulator_context("simulator", "id00/limaccds/simulator1") as fqdn_proxy:
        yield fqdn_proxy


@pytest.fixture
def lima_simulator2(ports):
    with lima_simulator_context("simulator2", "id00/limaccds/simulator2") as fqdn_proxy:
        yield fqdn_proxy


@contextmanager
def mosca_simulator_context(personal_name, device_name):
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/SimulSpectro/{personal_name}"

    conda_env = os.environ.get("MOSCA_SIMULATOR_CONDA_ENV")
    if not conda_env:
        conda_env = None
    conda = os.environ.get("CONDA_EXE", None)
    if conda_env and conda:
        if os.sep in conda_env:
            option = "-p"
        else:
            option = "-n"
        runner = [
            conda,
            "run",
            option,
            conda_env,
            "--no-capture-output",
            "SimulSpectro",
        ]
    else:
        runner = ["SimulSpectro"]

    with start_tango_server(
        *runner,
        personal_name,
        # "-v4",               # to enable debug
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=None,
        check_children=conda_env is not None,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def mosca_simulator(ports):
    with mosca_simulator_context(
        "mosca_simulator", "id00/mosca/simulator"
    ) as fqdn_proxy:
        yield fqdn_proxy


@pytest.fixture
def bliss_tango_server(ports, beacon):
    device_name = "id00/bliss/test"
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/bliss/test"

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.tango.servers.bliss_ds",
        "test",
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=DevState.STANDBY,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def dummy_tango_server(ports, beacon):

    device_name = "id00/tango/dummy"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.dummy_tg_server",
        "dummy",
        device_fqdn=device_fqdn,
        state=DevState.CLOSE,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def dummy_tango_server2(ports, beacon):

    device_name = "id00/tango/dummy2"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.dummy_tg_server",
        "dummy2",
        device_fqdn=device_fqdn,
        state=DevState.CLOSE,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def wago_tango_server(ports, default_session, wago_emulator):
    device_name = "1/1/wagodummy"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    # patching the property Iphost of wago tango device to connect to the mockup
    wago_ds = DeviceProxy(device_fqdn)
    wago_ds.put_property({"Iphost": f"{wago_emulator.host}:{wago_emulator.port}"})

    with start_tango_server(
        "Wago", "wago_tg_server", device_fqdn=device_fqdn
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def machinfo_tango_server(ports, beacon):
    device_name = "id00/tango/machinfo"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        sys.executable,
        "-u",
        "-m",
        "bliss.testutils.servers.machinfo_tg_server",
        "machinfo",
        device_fqdn=device_fqdn,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def regulation_tango_server(ports, beacon):
    device_name = "id00/regulation/loop"
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/regulation/test"

    with start_tango_server(
        "Regulation",
        "test",
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=DevState.ON,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def motor_tango_server(ports, beacon):
    device_name = "id00/tango/remo_ctrl"
    fqdn_prefix = f"tango://{os.environ['TANGO_HOST']}"
    device_fqdn = f"{fqdn_prefix}/{device_name}"
    admin_device_fqdn = f"{fqdn_prefix}/dserver/blissmotorcontroller/test"

    with start_tango_server(
        "BlissMotorController",
        "test",
        device_fqdn=device_fqdn,
        admin_device_fqdn=admin_device_fqdn,
        state=DevState.ON,
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def session(beacon, scan_tmpdir):
    session = beacon.get("test_session")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    yield session
    session.close()


@pytest.fixture
def default_session(beacon, scan_tmpdir):
    default_session = session_module.DefaultSession()
    default_session.setup()
    default_session.scan_saving.base_path = str(scan_tmpdir)
    yield default_session
    default_session.close()


def pytest_addoption(parser):
    """
    Add pytest options
    """
    parser.addoption("--pepu", help="pepu host name")
    parser.addoption("--ct2", help="ct2 address")
    parser.addoption("--axis-name", help="axis name")
    parser.addoption("--axis-name2", help="axis name2")
    parser.addoption("--axis-name3", help="axis name3")
    parser.addoption("--mythen", action="store", help="mythen host name")
    parser.addoption(
        "--wago",
        help="connection information: tango_cpp_host:port,domani,wago_dns\nExample: --wago bibhelm:20000,ID31,wcid31c",
    )


@pytest.fixture
def alias_session(beacon, lima_simulator, scan_tmpdir):
    session = beacon.get("test_alias")
    env_dict = dict()
    session.setup(env_dict)
    session.scan_saving.base_path = str(scan_tmpdir)

    ls = env_dict["lima_simulator"]
    rois = ls.roi_counters
    r1 = Roi(0, 0, 100, 200)
    rois["r1"] = r1
    r2 = Roi(100, 100, 100, 200)
    rois["r2"] = r2
    r3 = Roi(200, 200, 200, 200)
    rois["r3"] = r3

    env_dict["ALIASES"].add("myroi", ls.counters.r1_sum)
    env_dict["ALIASES"].add("myroi3", ls.counters.r3_sum)

    yield session

    session.close()


@pytest.fixture
def wago_emulator(beacon):
    config_tree = beacon.get_config("wago_simulator")
    modules_config = ModulesConfig.from_config_tree(config_tree)
    wago = WagoEmulator(modules_config)

    yield wago

    wago.close()


@contextmanager
def flint_context(with_flint=True, stucked=False):
    """Helper to capture and clean up all new Flint processes created during the
    context.

    It also provides arguments to request a specific Flint state.
    """
    flint_singleton = flint_proxy._get_singleton()
    try:
        pids = set()

        def register_new_flint_pid(pid):
            nonlocal pids
            pids.add(pid)

        assert flint_singleton._on_new_pid is None
        flint_singleton._on_new_pid = register_new_flint_pid

        try:
            try:
                if with_flint:
                    flint = plot.get_flint()

                if stucked:
                    assert with_flint is True
                    try:
                        with gevent.Timeout(seconds=0.1):
                            # This command does not return that is why it is
                            # aborted with a timeout
                            flint.test_infinit_loop()
                    except gevent.Timeout:
                        pass

                yield
            finally:
                for pid in pids:
                    try:
                        wait_terminate(pid, timeout=10)
                    except gevent.Timeout:
                        # This could happen, if the kill fails, after 10s
                        pass
        finally:
            flint_singleton._on_new_pid = None
    finally:
        flint_singleton._proxy_cleanup()


@pytest.fixture
def flint_session(xvfb, beacon, scan_tmpdir):
    session = beacon.get("flint")
    session.setup()
    session.scan_saving.base_path = str(scan_tmpdir)
    with flint_context():
        yield session
    session.close()


@pytest.fixture
def test_session_with_flint(xvfb, session):
    with flint_context():
        yield session


@pytest.fixture
def test_session_with_stucked_flint(xvfb, session):
    with flint_context(stucked=True):
        yield session


@pytest.fixture
def test_session_without_flint(xvfb, session):
    """This session have to start without flint, but can finish with"""
    with flint_context(False):
        yield session


@pytest.fixture
def log_context():
    """
    Initialize BLISS logging and restore previous logging state on exit
    """
    # Save the logging context
    old_handlers = list(logging.getLogger().handlers)
    old_logger_dict = dict(logging.getLogger().manager.loggerDict)

    # Bliss __init__ module loads the global_log which creates
    # "global" and "global.controllers" BlissLoggers
    # so they exist in old_logger_dict but with only a NullHandler handler

    logging_startup()
    # this modifies the 'global' logger:
    #   - adding a beacon handler
    #   - changing level to logging.INFO
    #   - turning propagation to False

    # revert logging_startup modifications of the 'global' logger for the test purposes
    logging.getLogger("global").propagate = True
    logging.getLogger("global").setLevel(logging.NOTSET)

    yield

    global_log.restore_initial_state()
    # this removes the beacon handler from the 'global' logger

    # logging.getLogger("bliss").setLevel(logging.NOTSET)
    # logging.getLogger("flint").setLevel(logging.NOTSET)

    # logging.getLogger("parso.python.diff").disabled = False
    # logging.getLogger("parso.cache").disabled = False

    # Restore the logging context
    logging.shutdown()
    logging.setLoggerClass(logging.Logger)
    logging.getLogger().handlers.clear()  # deletes all handlers
    logging.getLogger().handlers.extend(old_handlers)
    logging.getLogger().manager.loggerDict.clear()  # deletes all loggers
    logging.getLogger().manager.loggerDict.update(old_logger_dict)


@pytest.fixture
def elogbook_enabled():
    """Enables the Elogbook for any Bliss session"""
    logtools.elogbook.enable()
    try:
        yield
    finally:
        logtools.elogbook.disable()


@pytest.fixture
def log_shell_mode(elogbook_enabled):
    pass


@pytest.fixture
def icat_mock_client(mocker, elogbook_enabled) -> MagicMock:
    """Enables the ICAT client and Elogbook for any Bliss session.
    The retured object can be used to check that all desired calls are being made."""
    config = static.get_config()
    config.root["icat_servers"] = {"disable": False}
    mockedclass = mocker.patch("bliss.icat.client.IcatClient")

    datasetids = None

    def store_dataset(path=None, **_):
        nonlocal datasetids
        if datasetids is None:
            datasetids = list()
        datasetids.append(DatasetId(name=os.path.basename(path), path=path))

    def registered_dataset_ids(**_):
        return datasetids

    mockedclass.return_value.store_dataset.side_effect = store_dataset

    mockedclass.return_value.registered_dataset_ids.side_effect = registered_dataset_ids

    return mockedclass


@pytest.fixture
def nexus_writer_service(ports):
    device_name = "id00/bliss_nxwriter/test_session"
    device_fqdn = "tango://localhost:{}/{}".format(ports.tango_port, device_name)

    with start_tango_server(
        "NexusWriterService", "testwriters", "--log", "warning", device_fqdn=device_fqdn
    ) as dev_proxy:
        yield device_fqdn, dev_proxy


@pytest.fixture
def simulation_monochromator(beacon):
    # adjust files directories
    tracker_cfg = beacon.get_config("tracker")
    for tracker in tracker_cfg["trackers"]:
        for params in tracker["parameters"]:
            table_file = params.get("table")
            if table_file:
                params["table"] = os.path.join(BEACON_DB_PATH, table_file)
    mono_xtals = beacon.get_config("simul_mlmono_xtals")
    for xtal in mono_xtals["xtals"]:
        ml_lab_file = xtal["ml_lab_file"]
        xtal["ml_lab_file"] = os.path.join(BEACON_DB_PATH, ml_lab_file)
    #
    bragg = beacon.get("sim_mono")
    bragg.move(10)
    mono = beacon.get("simul_mono")
    mono.xtal.change("Si220")
    yield mono
    mono._close()


@contextmanager
def asyncio_loop():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        yield loop
    finally:
        loop.close()


@pytest.fixture
def pt_test_context():
    class SimulatedOutput(PlainTextOutput):
        def __init__(self):
            self._cursor_up: bool = False
            self._output_memory = io.StringIO()
            PlainTextOutput.__init__(self, self._output_memory)

        def displayed_text(self) -> str:
            return self._output_memory.getvalue()

        def write(self, data: str):
            self._cursor_up = False
            super().write(data)

        def write_raw(self, data: str):
            self._cursor_up = False
            super().write_raw(data)

        def cursor_up(self, amount: int) -> None:
            # Just a guess for now
            self._cursor_up = True
            super().cursor_up(amount)

        def erase_down(self):
            if self._cursor_up:
                # Just a guess for now
                self._clear()
            super().erase_down()

        def _clear(self):
            """Clear the actual content of the buffer"""
            self._output_memory.seek(0, os.SEEK_SET)
            self._output_memory.truncate(0)

        def _flush_app(self):
            """Called internally by BLISS when an application is done"""
            print(self.displayed_text())
            self._clear()

    actions = []

    class Context:
        def __init__(self, pipe_input):
            self.input = pipe_input
            self.output = SimulatedOutput()

        def send_input(self, chars: str):
            self.input.send_text(chars)

        def send_input_later(self, timeout: float, chars: str):
            def do_later():
                gevent.sleep(timeout)
                self.input.send_text(chars)

            g = gevent.spawn(do_later)
            actions.append(g)

    with asyncio_loop():
        with create_pipe_input() as pipe_input:
            context = Context(pipe_input)
            with create_app_session(input=context.input, output=context.output):
                yield context

        for g in actions:
            gevent.kill(g)


class IntrospectBlissRepl(BlissRepl):
    """BlissRepl which can be introspected for unittest"""

    def send_input(self, text: str):
        """Send and run an input in this repl"""
        if not text.endswith("\r"):
            # Make sure the text will be executed
            text += "\r"
        # Execute single loop in the repl
        self.app.input.send_text(text)
        expression = self.app.run()
        self.run_and_show_expression(expression)


@contextmanager
def bliss_repl(
    locals_dict=None,
    confirm_exit=False,
    vt100: bool = False,
    no_cli: bool = False,
    stdout: typing.TextIO | None = None,
    **kwargs,
) -> typing.Generator[IntrospectBlissRepl, None, None]:
    """
    Arguments:
        vt100: If true, Use a vt100 output
        no_cli: If true, make sure the `cli` function is not used
    """
    if locals_dict is None:
        locals_dict = {}
    from prompt_toolkit.data_structures import Size
    from prompt_toolkit.output.color_depth import ColorDepth

    if stdout is None:
        stdout = sys.stdout

    with asyncio_loop():
        with create_pipe_input() as input:
            output: Output
            if vt100:

                def get_size() -> Size:
                    # Make sure the terminal size does not change between tests
                    return Size(rows=200, columns=80)

                output = Vt100_Output(
                    stdout,
                    get_size,
                    term="dump",
                    default_color_depth=ColorDepth.DEPTH_8_BIT,
                )
            else:
                output = PlainTextOutput(stdout)
            with bliss_app_session(input=input, output=output) as app_session:
                if "session_name" in kwargs and not no_cli:
                    # full initialization of a session
                    br = cli(
                        repl_class=IntrospectBlissRepl,
                        locals=locals_dict,
                        app_session=app_session,
                        **kwargs,
                    )
                else:
                    br = IntrospectBlissRepl(
                        style="default",
                        theme_mode="dark",
                        get_globals=lambda: locals_dict,
                        **kwargs,
                    )
                br.confirm_exit = confirm_exit
                try:
                    yield br
                finally:
                    br.exit()
                    if br.bliss_session is not None:
                        br.bliss_session.close()
