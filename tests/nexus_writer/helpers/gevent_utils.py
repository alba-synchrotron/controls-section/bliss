from types import ModuleType
from typing import Generator
from contextlib import contextmanager
import gevent.monkey


def unpatch_module(module: ModuleType, name: str) -> None:
    """Undo gevent monkey patching of this module

    :param module:
    :param name: the name given by gevent to this module
    """
    original_module_items = gevent.monkey.saved.pop(name, None)
    if not original_module_items:
        return
    for attr, value in original_module_items.items():
        setattr(module, attr, value)


def repatch_module(module: ModuleType, name: str) -> None:
    """Redo gevent monkey patching of this module,
    whether it is already patched or not.

    :param module:
    :param name: the name given by gevent to this module
    """
    unpatch_module(module, name)
    gevent.monkey._patch_module(name)


@contextmanager
def original_module(module, name) -> Generator[None, None, None]:
    """Use the original module within this context

    :param module:
    :param name: the name given by gevent to this module
    """
    unpatch_module(module, name)
    try:
        yield
    finally:
        repatch_module(module, name)
