from copy import deepcopy
from typing import List

import h5py

from bliss.common import scans
from bliss.common.data_store import get_default_data_store
from blissdata.redis_engine.scan import Scan as RediScan

from blisswriter.nexus.devices import device_info
from blisswriter.nexus.mapping import get_primary_dataset_path

from tests.nexus_writer.helpers import nxw_test_utils


def test_nexus_mapping(nexus_writer_config):
    blissscan = _run_scan(**nexus_writer_config)

    data_store = get_default_data_store()
    _, key = data_store.get_last_scan()
    rediscan = data_store.load_scan(key, scan_cls=RediScan)

    bliss_devices = blissscan.scan_info["nexuswriter"]["devices"]
    writer_devices = device_info(deepcopy(bliss_devices), blissscan.scan_info)
    subscan_devices = writer_devices["axis"]

    # Device info on the Bliss side is a subset of device info on the writer side
    subset = bliss_devices["simu1:deadtime_det3"]
    superset = subscan_devices["simu1:deadtime_det3"]
    assert len(subset) < len(superset)
    assert all(item in superset.items() for item in subset.items())

    # Device info on the writer side defines the NeXus structure.
    # Check the primary dataset paths.
    data_paths = [
        get_primary_dataset_path(stream.name, subscan_devices)
        for stream in rediscan.streams.values()
    ]
    filename = blissscan.scan_info["filename"]
    scan_nb = blissscan.scan_info["scan_nb"]
    _assert_primary_dataset_paths(filename, scan_nb, data_paths)


def _assert_primary_dataset_paths(
    filename: str, scan_nb: str, data_paths: List[str]
) -> None:
    with h5py.File(filename, "r") as nxroot:
        # Check that all data paths exist in the scan
        scan = nxroot[f"/{scan_nb}.1"]
        for data_path in data_paths:
            try:
                assert len(scan[data_path]) == 10, data_path
            except KeyError:
                raise KeyError(f"Dataset {data_path} not found") from None
        # Check that we have all datasets
        assert len(scan["measurement"]) == len(data_paths)


@nxw_test_utils.writer_stdout_on_exception
def _run_scan(session=None, writer=None, **_):
    scan_shape = (10,)
    scan = scans.ascan(
        session.env_dict["robx"], 0, 1, scan_shape[0] - 1, 0.1, run=False
    )
    nxw_test_utils.run_scan(scan)
    nxw_test_utils.wait_scan_data_finished([scan], writer=writer)
    return scan
