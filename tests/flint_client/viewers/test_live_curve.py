"""Testing custom plots provided by Flint."""

from bliss.common import plot


def test_live_curve_axes_scale(flint_session):
    """Get the default curve plot

    Check the axes scale API
    """
    flint = plot.get_flint()
    p = flint.get_live_plot("default-curve")

    # Check the scales
    assert p.xscale == "linear"
    assert p.yscale == "linear"
    p.xscale = "log"
    p.yscale = "log"
    assert p.xscale == "log"
    assert p.yscale == "log"
    assert p.xaxis_channel_name is None
