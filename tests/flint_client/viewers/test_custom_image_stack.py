"""Testing custom plots provided by Flint."""

import numpy
from bliss.common import plot


def test_image_stack(flint_session):
    f = plot.get_flint()
    p = f.get_plot(plot_class="imagestack", name="imagestack")

    cube = numpy.arange(10 * 10 * 10)
    cube.shape = 10, 10, 10

    # Check the default data setter
    p.set_data(cube)
    vrange = p.get_data_range()
    assert vrange[0:2] == [[0, 10], [0, 10]]

    # Allow to setup the colormap
    p.set_colormap(lut="viridis", vmin=0, vmax=10)

    # Set none can be use to clear the data
    p.set_data(None)
    vrange = p.get_data_range()
    assert vrange[0:2] == [None, None]

    # Check deprecated API
    p.add_data(cube, field="cube")
    p.select_data("cube")
    vrange = p.get_data_range()
    assert vrange[0:2] == [[0, 10], [0, 10]]

    # Check the default way to clear data
    p.clear_data()
    vrange = p.get_data_range()
    assert vrange[0:2] == [None, None]
