"""Testing custom plots provided by Flint."""

import numpy
from bliss.common import plot


def test_plot__create_2_plot(flint_session):
    """Create 2 plots

    Make sure it exists 2 plots
    """
    p = plot.plot(name="Foo")
    pid = plot.get_flint()._pid
    assert "flint_pid={}".format(pid) in repr(p)
    assert p.name == "Foo"

    p = plot.plot(name="Some name")
    assert "flint_pid={}".format(pid) in repr(p)
    assert p.name == "Some name"


def test_plot__reuse_plot__api_1_0(flint_session):
    """Test reuse of custom plot from an ID"""
    widget = plot.plot_curve(name="foo")
    cos_data = numpy.cos(numpy.linspace(0, 2 * numpy.pi, 10))
    widget.add_data({"cos": cos_data, "foo": cos_data})
    widget2 = plot.plot_curve(name="foo", existing_id=widget.plot_id)
    cos = widget2.get_data()["cos"]
    numpy.testing.assert_allclose(cos, cos_data)


def test_plot__reuse_plot__api_1_6(flint_session):
    """Test reuse of custom plot from a name"""
    widget = plot.plot_curve(name="foo", existing_id="myplot")
    cos_data = numpy.cos(numpy.linspace(0, 2 * numpy.pi, 10))
    widget.add_data({"cos": cos_data, "foo": cos_data})
    widget2 = plot.plot_curve(name="foo", existing_id="myplot")
    cos = widget2.get_data()["cos"]
    numpy.testing.assert_allclose(cos, cos_data)


def test_plot__remove_plot(flint_session):
    flint = plot.get_flint()
    p = flint.get_plot(plot_class="curve", name="foo-rm", unique_name="foo-rm")
    flint.remove_plot(p.plot_id)
    assert not flint.plot_exists("foo-rm")
