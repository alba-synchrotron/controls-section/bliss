# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import contextlib
import gevent

from bliss.shell import getval as getval_module
from bliss.shell.getval import getval_yes_no, getval_name, getval_int_range
from bliss.shell.getval import getval_idx_list, getval_char_list
from bliss.common.utils import Undefined


@contextlib.contextmanager
def bliss_prompt_session():
    bliss_prompt_orig = getval_module._prompt_factory
    bp = bliss_prompt_orig()
    getval_module._prompt_factory = lambda: bp
    try:
        with gevent.Timeout(3, TimeoutError):
            yield bp
    except TimeoutError:
        raise
    finally:
        getval_module._prompt_factory = bliss_prompt_orig


@pytest.mark.parametrize(
    "data",
    [
        ("y\n", True, Undefined),
        ("n\n", False, Undefined),
        ("yes\n", True, Undefined),
        ("YES\n", True, Undefined),
        ("NO\n", False, Undefined),
        ("yeS\n", True, Undefined),
        ("\x03", True, "yeah"),
        ("\x03", False, "yeah"),
    ],
)
def test_getval_yes_no(pt_test_context, data):
    keyboard, expected, ki_default = data
    pt_test_context.send_input_later(1, keyboard)
    with bliss_prompt_session() as bps:
        result = getval_yes_no("do you want", default=False, ki_default=ki_default)
    if ki_default != Undefined:
        assert result == ki_default
    else:
        assert result is expected
    assert bps.default_buffer.validation_error is None


def test_getval_yes_no_invalid(pt_test_context):
    pt_test_context.send_input_later(1, "yp\n")
    with pytest.raises(TimeoutError):
        with bliss_prompt_session() as bps:
            getval_yes_no("do you want", default=False)
    assert "The input have to" in bps.default_buffer.validation_error.message


def test_getval_yes_no_keyboardinterrupt(pt_test_context):
    pt_test_context.send_input_later(1, "\x03")
    with pytest.raises(KeyboardInterrupt):
        with bliss_prompt_session():
            getval_yes_no("do you want", default=False)


@pytest.mark.parametrize("data", [("titi\n", "titi"), ("tutu3\n", "tutu3")])
def test_getval_name(pt_test_context, data):
    keyboard, expected = data
    pt_test_context.send_input_later(1, keyboard + "\n")
    with bliss_prompt_session() as bps:
        result = getval_name("Enter name", default=False)
    assert result == expected
    assert bps.default_buffer.validation_error is None


def test_getval_name_invalid(pt_test_context):
    pt_test_context.send_input_later(1, "1toto\n")
    with pytest.raises(TimeoutError):
        with bliss_prompt_session() as bps:
            getval_name("Enter name", default=False)
    assert bps.default_buffer.validation_error is not None


@pytest.mark.parametrize(
    "data",
    [
        (1, 10, "1\n", 1),
        (1, 10, "50\b\n", 5),
        (-1, 10, "-1\n", -1),
        (1, 10, "\n", 9),
    ],
)
def test_getval_int_range(pt_test_context, data):
    minimum, maximum, keyboard, expected = data
    pt_test_context.send_input_later(1, keyboard)
    with bliss_prompt_session() as bps:
        result = getval_int_range("Give me", minimum, maximum, default=9)
    assert result == expected
    assert bps.default_buffer.validation_error is None


def test_getval_int_too_small(pt_test_context):
    pt_test_context.send_input_later(1, "1\n")
    with pytest.raises(TimeoutError):
        with bliss_prompt_session() as bps:
            getval_int_range("Give me", 2, 400, default=False)
    assert bps.default_buffer.validation_error is not None


def test_getval_int_too_big(pt_test_context):
    pt_test_context.send_input_later(1, "401\n")
    with pytest.raises(TimeoutError):
        with bliss_prompt_session() as bps:
            getval_int_range("Give me", 2, 400, default=False)
    assert bps.default_buffer.validation_error is not None


def test_getval_idx_list(pt_test_context):
    dspacing_list = ["111", "311", "642"]
    pt_test_context.send_input_later(1, "2\n")
    with bliss_prompt_session() as bps:
        result = getval_idx_list(dspacing_list, "enter value")
    assert result == (2, "311")
    assert bps.default_buffer.validation_error is None


def test_getval_char_list(pt_test_context):
    actions_list = [("a", "add a roi"), ("r", "remove a roi"), ("m", "modify a roi")]
    pt_test_context.send_input_later(1, "\na\n")
    with bliss_prompt_session() as bps:
        result = getval_char_list(actions_list, "enter value")
    assert result == ("a", "add a roi")
    assert bps.default_buffer.validation_error is None


def test_getval_char_list__default(pt_test_context):
    actions_list = [("a", "add a roi"), ("r", "remove a roi"), ("m", "modify a roi")]
    pt_test_context.send_input_later(1, "a\n")
    with bliss_prompt_session() as bps:
        result = getval_char_list(actions_list, "enter value", default="a")
    assert result == ("a", "add a roi")
    assert bps.default_buffer.validation_error is None
