from bliss.shell.interlocks import interlock_show
from bliss.shell.interlocks import interlock_state
from .conftest import bliss_repl


def test_interlock_show(default_session):
    session_dict = default_session.env_dict
    # we do not test the default 'interlock_show', we test the 'interlocks' module
    # version imported above...
    # TODO: clean interlocks !
    session_dict["interlock_show"] = interlock_show
    with bliss_repl(session_dict) as br:
        br.send_input("interlock_show()\r")
        assert "No interlock instance found" in br.app.output[-1]
        _ = default_session.config.get("wago_simulator")
        br.send_input("interlock_show()\r")
        assert "Interlock Firmware is not present in the PLC" in br.app.output[-1]
        assert "2 interlock instance" in br.app.output[-1]


def test_interlock_state(default_session):
    interlock_state()
    wago_simulator = default_session.config.get("wago_simulator")
    assert interlock_state(wago_simulator) == {}
    assert interlock_state() == {}
