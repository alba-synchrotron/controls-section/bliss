import gevent
from ..conftest import bliss_repl


def pytest_addoption(parser):
    # To allow to display the pt menus
    parser.addoption("--ptmenu", action="store_true", default=False)


def _feed_cli_with_input(
    text, check_line_ending=True, locals_dict=None, timeout=10, confirm_exit=False
):
    """
    Create a Prompt, feed it with the given user input and return the CLI
    object.

    Inspired by python-prompt-toolkit/tests/test_cli.py
    """
    with gevent.timeout.Timeout(timeout):
        # If the given text doesn't end with a newline, the interface won't finish.
        if check_line_ending:
            assert text.endswith("\r")

        with bliss_repl(locals_dict, confirm_exit) as br:
            br.app.input.send_text(text)

            try:
                result = br.app.run()
            except EOFError:
                return None, None, None

            return result, br.app, br
