# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import psutil
import gevent
from unittest import mock

from bliss.shell import standard
from bliss.shell.standard import flint
from bliss.shell.standard import plotselect


def test_open_close_flint(test_session_without_flint):
    f = flint()
    assert f is not None
    pid = f.pid
    assert psutil.pid_exists(pid)
    f.close()
    assert not psutil.pid_exists(pid)


def test_open_kill_flint(test_session_without_flint):
    f = flint()
    assert f is not None
    pid = f.pid
    assert psutil.pid_exists(pid)
    f.kill()
    try:
        process = psutil.Process(pid)
    except psutil.NoSuchProcess:
        pass
    else:
        try:
            with gevent.Timeout(1):
                # gevent timeout have to be used here
                # See https://github.com/gevent/gevent/issues/622
                process.wait(timeout=None)
        except gevent.Timeout:
            pass
    assert not psutil.pid_exists(pid)


def test_edit_roi_counters(
    beacon, default_session, lima_simulator, test_session_with_flint
):
    cam = beacon.get("lima_simulator")
    # edit_rois is tested in another test, here we can just ensure the method
    # is called on the Lima object
    with mock.patch.object(cam, "edit_rois") as mocked_edit_rois:
        standard.edit_roi_counters(cam)
        assert mocked_edit_rois.call_count == 1


def test_plotselect(session, beacon, capsys):
    # Ensure a wrong counter name triggers a warning.
    plotselect("unexisting_counter_name")
    captured = capsys.readouterr()

    assert "is not a valid counter" in captured.out

    # Ensure the counter name is printed.
    assert "unexisting_counter_name" in captured.out
