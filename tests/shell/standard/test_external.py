# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import time


def test_open_silx(xvfb):
    # checking if the process opens without stdout errors
    from bliss.shell.standard._external import _launch_silx

    process = _launch_silx()
    time.sleep(1)
    assert process.returncode is None
    process.terminate()
