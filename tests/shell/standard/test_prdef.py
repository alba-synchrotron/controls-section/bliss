# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import re
import pytest
import os
from bliss.shell.standard._utils import _get_source_code
from bliss.shell import standard


@pytest.fixture
def prdef_session(beacon):
    session = beacon.get("test_prdef_session")
    session.setup()
    yield session
    session.close()


@pytest.fixture
def prdef_user_script(tmpdir):
    filename = tmpdir / "user_test_script.py"
    with open(filename, "w") as f:
        f.write("# Still comment should not be visible\n")
        f.write("\n\n")
        f.write("def dummy_user():\n")
        f.write("    pass\n")
        f.write("\n\n")
        f.write("def prdef_example_user():\n")
        f.write("    return 100\n")
    yield filename


def modify_script(filename, replacemap):
    with open(filename, "r") as f:
        content = list(f)
    content = [replacemap.get(line, line) for line in content]
    with open(filename, "w") as f:
        f.writelines(content)


def modify_return_value(filename, old, new):
    replacemap = {f"    return {old}\n": f"    return {new}\n"}
    modify_script(filename, replacemap)


def assert_source_code(func, expected_return_value, script_path, line_number):
    expected_header = f"'{func.__name__}' is defined in:\n{script_path}:{line_number}\n"
    expected_content = [
        f"def {func.__name__}():\n",
        f"    return {expected_return_value}\n",
    ]
    header, content = _get_source_code(func)
    assert header == expected_header
    assert content == expected_content


def test_prdef_session_setup_script(prdef_session, beacon_directory):
    script_path = os.path.join(beacon_directory, "sessions", "test_prdef_session.py")

    func = prdef_session.env_dict["prdef_example_setup"]
    assert func() == 1
    assert_source_code(func, 1, "beacon://" + script_path, 10)

    modify_return_value(script_path, 1, 2)
    func = prdef_session.env_dict["prdef_example_setup"]
    assert func() == 1
    assert_source_code(func, 1, "beacon://" + script_path, 10)

    prdef_session.resetup()
    func = prdef_session.env_dict["prdef_example_setup"]
    assert func() == 2
    assert_source_code(func, 2, "beacon://" + script_path, 10)


def test_prdef_session_script(prdef_session, beacon_directory):
    script_path = os.path.join(
        beacon_directory, "sessions", "scripts", "prdef_script.py"
    )
    load_script = prdef_session.env_dict["load_script"]

    func = prdef_session.env_dict["prdef_example_script"]
    assert func() == 10
    assert_source_code(func, 10, "beacon://" + script_path, 8)

    modify_return_value(script_path, 10, 20)
    func = prdef_session.env_dict["prdef_example_script"]
    assert func() == 10
    assert_source_code(func, 10, "beacon://" + script_path, 8)

    load_script("prdef_script")
    func = prdef_session.env_dict["prdef_example_script"]
    assert func() == 20
    assert_source_code(func, 20, "beacon://" + script_path, 8)


def test_prdef_user_script(prdef_session, prdef_user_script):
    script_path = str(prdef_user_script)
    user_script_homedir = prdef_session.env_dict["user_script_homedir"]
    user_script_load = prdef_session.env_dict["user_script_load"]
    user_script_homedir(prdef_user_script.dirname)
    user_script_load(prdef_user_script.basename)

    func = prdef_session.env_dict["user"].prdef_example_user
    assert func() == 100
    assert_source_code(func, 100, script_path, 8)

    modify_return_value(script_path, 100, 200)
    func = prdef_session.env_dict["user"].prdef_example_user
    assert func() == 100
    assert_source_code(func, 100, script_path, 8)

    user_script_load(prdef_user_script.basename)
    func = prdef_session.env_dict["user"].prdef_example_user
    assert func() == 200
    assert_source_code(func, 200, script_path, 8)


def test_prdef_issue2785(session):
    func_code = "def special_com(x, y):\n    return numpy.average(x, weights=y)\n"
    from bliss.shell.standard._utils import _get_source_code

    header, lines = _get_source_code(session.env_dict["special_com"])
    assert "".join(lines).endswith(func_code)


def test_prdef(session, resource_helper, check_in_shell):
    """Make sure `prdef` looks what we expect"""

    def lazy_pi():
        return 3.15

    def remove_var_content(string):
        """Remove content which can change when we restart the test"""
        pattern = "(/)(.+)(/tests/shell/standard/test_prdef.py)"
        string = re.sub(pattern, r"\1[...]\3", string)
        string = re.sub(r"(\.py):(\d+)", r"\1:159", string)
        return string

    locals = {"lazy_pi": lazy_pi, "prdef": standard.prdef}
    check_in_shell.remove_var_content = remove_var_content
    check_in_shell.run_bliss_repl(locals=locals, cmd="prdef(lazy_pi)")
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)
