# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.shell.standard import wa, wm, wu, sta, stm
from bliss.shell.standard import lsmot, lsobj, lscnt


def test_sta_normal(default_session, resource_helper, check_in_shell):
    """Make sure `sta` looks what we expect"""
    locals = {"sta": sta}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('bad'); bad.controller.bad_position = False",
        cmd="sta()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_sta_slits(default_session, resource_helper, check_in_shell):
    """Make sure `sta` looks what we expect"""
    locals = {"sta": sta}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('s1hg')",
        cmd="sta()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)
    assert "s1f" not in check_in_shell.stdout
    assert "s1b" not in check_in_shell.stdout


def test_sta_exception(default_session, resource_helper, check_in_shell):
    """Make sure `sta` looks what we expect"""
    locals = {"sta": sta}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('bad'); bad.controller.bad_position = True",
        cmd="sta()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_stm_normal(default_session, resource_helper, check_in_shell):
    """Make sure `sta` looks what we expect"""
    locals = {"stm": stm}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('bad'); bad.controller.bad_position = False",
        cmd="stm('bad')",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_stm_exception(default_session, resource_helper, check_in_shell):
    """Make sure `sta` looks what we expect"""
    locals = {"stm": stm}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('bad'); bad.controller.bad_position = True",
        cmd="stm('bad')",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_wa_normal(default_session, resource_helper, check_in_shell):
    """Make sure `wa` looks what we expect"""
    locals = {"wa": wa}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('bad'); bad.controller.bad_position = False",
        cmd="wa()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_wm_with_alias(default_session, resource_helper, check_in_shell):
    """Make sure `wm` looks what we expect"""
    locals = {"wm": wm}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('roby')",
        cmd="wm(roby)",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_wu_normal(default_session, resource_helper, check_in_shell):
    """Make sure `wu` looks what we expect"""
    locals = {"wu": wu}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('bad'); bad.controller.bad_position = False",
        cmd="wu()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_wa_exception(default_session, resource_helper, check_in_shell):
    """Make sure `wa` looks what we expect"""
    locals = {"wa": wa}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd=[
            "config.get('bad_mot')",
            "config.get('dis_mot')",
            "config.get('nan_mot')",
        ],
        cmd="wa()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_wa_slits(default_session, resource_helper, check_in_shell):
    """Make sure `wa` looks what we expect"""
    locals = {"wa": wa}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('s1hg')",
        cmd="wa()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)
    assert "s1f" not in check_in_shell.stdout
    assert "s1b" not in check_in_shell.stdout


def test_wm_normal(default_session, resource_helper, check_in_shell):
    """Make sure `wm` looks what we expect"""
    locals = {"wm": wm}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('bad'); bad.controller.bad_position = False",
        cmd="wm('bad')",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_wm_rounding(default_session, resource_helper, check_in_shell):
    """Make sure `wm` looks what we expect"""
    locals = {"wm": wm}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('robz')",
        cmd="wm('robz')",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_wm_exception(default_session, resource_helper, check_in_shell):
    """Make sure `wm` looks what we expect"""
    locals = {"wm": wm}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd="config.get('bad'); bad.controller.bad_position = True",
        cmd="wm('bad')",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_lsmot(session, resource_helper, check_in_shell):
    """Make sure `lsmot` looks what we expect"""
    locals = {"lsmot": lsmot}
    check_in_shell.run_bliss_repl(
        locals=locals,
        cmd="lsmot()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)
    captured_out = check_in_shell.stdout
    assert "att1z" in captured_out
    assert "bad" in captured_out
    assert "custom_axis" in captured_out
    assert "hooked_error_m0" in captured_out
    assert "omega" in captured_out
    assert "roby" in captured_out
    assert "s1d" in captured_out
    assert "hooked_m1" in captured_out


def test_lscnt(default_session, resource_helper, check_in_shell):
    """Make sure `lscnt` looks what we expect"""
    locals = {"lscnt": lscnt}
    check_in_shell.run_bliss_repl(
        locals=locals,
        pre_cmd=[
            "config.get('simu1')",
            "config.get('diode')",
            "config.get('diode2')",
            "ALIASES.add('alias', 'simulation_diode_sampling_controller:diode2')",
        ],
        cmd="lscnt();lscnt(diode);lscnt(alias)",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_lsobj(session, resource_helper, check_in_shell):
    check_in_shell.run_bliss_repl(
        locals={"lsobj": lsobj},
        cmd="lsobj()",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_lsobj_jocker(session, resource_helper, check_in_shell):
    check_in_shell.run_bliss_repl(
        locals={"lsobj": lsobj},
        cmd="lsobj('dio*')",
    )
    resource_helper.assert_str("{test_name}_cell.txt", check_in_shell.output)
    resource_helper.assert_ansi("{test_name}_shell.ansi", check_in_shell.stdout)


def test_alias_lsobj(alias_session):
    """
    Ensure that lsobj() find all objects in the session.
    """
    obj_in_session = [
        "simu1",
        "robu",
        "lima_simulator",
        "m1",
        "m2",
        "mot0",
        "robyy",
        "robzz",
        "dtime",
        "rtime",
        "ltime",
        "dtime1",
        "dtime2",
        "robenc",
    ]
    obj_in_session.append("myroi")  # myroi and myroi3 are added in test_alias
    obj_in_session.append("myroi3")  #   session only during tests.
    obj_in_session.sort()

    from bliss.shell import iter_common

    obj_in_lsobj = iter_common.list_obj()
    obj_in_lsobj = list(set(obj_in_lsobj))  # for uniqueness of names.
    obj_in_lsobj.sort()

    assert obj_in_session == obj_in_lsobj
