import sys

from bliss import is_bliss_shell
from bliss.config import static

config = static.get_config()
session = config.get("flint")
session.setup()

success = not is_bliss_shell() and "bliss.shell.main" not in sys.modules.keys()

sys.exit(0 if success else 1)
