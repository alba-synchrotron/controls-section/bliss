# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import numpy
import gevent
import logging

from bliss.physics.backend import MaterialBackend
from bliss.controllers.lima.roi import Roi
from bliss.common.standard import ct


@pytest.fixture
def autof_session(beacon, scan_tmpdir, lima_simulator):
    mono = beacon.get("mono")
    mono.move(7)

    autof_session = beacon.get("test_autof_session")
    autof_session.setup()
    autof_session.scan_saving.base_path = str(scan_tmpdir)
    autof_session.scan_saving.writer = "null"

    logger = logging.getLogger("global.controllers.autofwheel_cu")
    logger.setLevel(logging.DEBUG)
    logger = logging.getLogger("bliss.common.auto_filter.chain_patching_inplace")
    logger.setLevel(logging.DEBUG)

    roi = Roi(0, 0, 1023, 1023)
    lima_simulator = autof_session.env_dict["lima_simulator"]
    lima_simulator.roi_counters["roi1"] = roi

    yield autof_session
    autof_session.close()


def test_autofilter_api(autof_session):
    # just call once all functions that are relevant to the user in autofilter1
    # and autofwheel_cu that are not related to a scan

    # get ready
    energy = autof_session.env_dict["energy"]
    energy.move(16)

    autofilter1 = autof_session.env_dict["autofilter1"]
    filterw = autof_session.env_dict["autofwheel_cu"]
    filterwago = autof_session.env_dict["autofwago_al"]

    # no filter
    filterw.filter = 0
    assert autofilter1.transmission == 1

    # 20 micron copper
    # http://henke.lbl.gov/optical_constants/filter2.html
    # Cu Density=8.96 Thickness=20. microns
    #  Photon Energy (eV), Transmission
    #     16000.      0.32744
    filterw.filter = 2
    if MaterialBackend.BACKEND_NAME == "xraylib":
        rtol = 0.5  # % relative difference
    else:
        # Large difference due to photoelectric cross section
        rtol = 7  # % relative difference
    assert autofilter1.transmission == pytest.approx(0.32744, rel=rtol / 100)

    # change energy and see if transmission is updated
    filterw.filter = 2
    trans_16keV = autofilter1.transmission

    energy.move(12)
    trans_12keV = autofilter1.transmission

    assert trans_16keV != trans_12keV

    # set monitor counter
    diode = autof_session.config.get("diode")
    autofilter1.monitor_counter = diode

    # enable all filters
    filterwago.enabled_filters = []
    mask = [f["position"] for f in filterwago.get_filters()]
    expected_mask = list(range(16))
    assert mask == expected_mask

    for i in range(16):
        filterwago.filter = i

    with pytest.raises(ValueError):
        filterwago.filter = 17

    # enable some filters
    filterwago.filter = 2
    filterwago.enabled_filters = [1, 2]
    mask = [f["position"] for f in filterwago.get_filters()]
    expected_mask = [0, 2, 4, 6]  # bit 1 and bit 3 not set
    assert mask == expected_mask

    for i in range(16):
        if i in mask:
            filterwago.filter = i
        else:
            with pytest.raises(ValueError):
                filterwago.filter = i

    # disable filter which is currently selected
    filterwago.enabled_filters = []
    filterwago.filter = 12
    with pytest.raises(ValueError):
        filterwago.enabled_filters = [1, 2]

    # check transmission
    filterwago.filter = 2
    assert filterwago.transmission == pytest.approx(0.4355, rel=0.0001)


def perform_autofilter_scan(
    autof_session,
    *detectors,
    save=True,
    save_images=True,
    simobj="sim_autofilter1_ctrs",
):
    energy = autof_session.env_dict["energy"]
    energy.move(16)

    filterw = autof_session.env_dict["autofwheel_cu"]
    filterw.filter = 0
    sim = autof_session.env_dict[simobj]

    with gevent.Timeout(20):  # normal time around 7 seconds
        scan = sim.auto_filter.ascan(
            sim.axis,
            0,
            1,
            sim.npoints,
            0.1,
            *sim.detectors,
            *detectors,
            save=save,
            save_images=save_images,
        )
    return sim, scan


def assert_sim_data(sim, scan_data, save=True, save_images=True):
    positioner_key = None
    # check that all channels have the desired number of points
    nexpected = sim.npoints + 1
    for name, stream in scan_data.streams.items():
        if name.endswith(":image"):
            last_lima_status = stream._json_stream[-1]
            assert last_lima_status["last_index"] == 50
            if save_images:
                assert last_lima_status["last_index_saved"] == 50
            else:
                assert last_lima_status["last_index_saved"] == -1
        else:
            assert len(stream) == nexpected, name
            if name.endswith("_axis"):
                positioner_key = name

    # check to positioner values
    measured = scan_data.streams[positioner_key][:]
    expected = numpy.linspace(0, 1, nexpected)
    numpy.testing.assert_allclose(measured, expected)

    # check that the corrected values are the expected ones
    measured = scan_data.streams["autofilter1:sim_autofilter1_det_corr"][:]
    expected = sim._data * sim.mon_value()
    numpy.testing.assert_allclose(measured, expected)

    # check that corrected detector counter
    measured = scan_data.streams["autofilter1:sim_autofilter1_det_corr"][:]
    expected = (
        scan_data.streams["AutoFilterDetMon:sim_autofilter1_det"][:]
        / scan_data.streams["autofilter1:transm"][:]
    )
    numpy.testing.assert_allclose(measured, expected)

    # check that all filters are out at beginning and end of scan
    assert scan_data.streams["autofilter1:transm"][0] == 1.0
    assert scan_data.streams["autofilter1:transm"][-1] == 1.0


def test_autofilter_ascan(autof_session):
    sim, scan = perform_autofilter_scan(autof_session, save=False)
    scan_data = scan._scan_data

    expected = {
        "axis:sim_autofilter1_ctrs_axis",
        "timer:elapsed_time",
        "timer:epoch",
        "AutoFilterDetMon:sim_autofilter1_mon",
        "AutoFilterDetMon:sim_autofilter1_det",
        "autofilter1:sim_autofilter1_det_corr",
        "autofilter1:curratt",
        "autofilter1:transm",
        "autofilter1:ratio",
    }
    assert set(scan_data.streams.keys()) == expected
    assert_sim_data(sim, scan_data, save=False)


def test_autofilter_ascan_with_corrected_counter(autof_session):
    diode = autof_session.config.get("diode")
    autofilter = autof_session.config.get("autofilter2")

    # ask for diode corrected counter
    autofilter.extra_correction_counters.add(diode)

    sim, scan = perform_autofilter_scan(
        autof_session, diode, save=False, simobj="sim_autofilter2_ctrs"
    )
    scan_data = scan.get_data()

    expected = {
        "axis:sim_autofilter2_ctrs_axis",
        "timer:elapsed_time",
        "timer:epoch",
        "AutoFilterDetMon:sim_autofilter2_mon",
        "AutoFilterDetMon:sim_autofilter2_det",
        "autofilter2:sim_autofilter2_det_corr",
        "autofilter2:diode_corr",
        "autofilter2:diode2_corr",
        "autofilter2:curratt2",
        "autofilter2:transm2",
        "autofilter2:ratio2",
        "simulation_diode_sampling_controller:diode",
        "simulation_diode_sampling_controller:diode2",
    }
    assert set(scan_data.keys()) == expected
    # assert_sim_data(sim, scan_data, save=False)


@pytest.mark.parametrize("save", (True, False))
@pytest.mark.parametrize("save_images", (True, False))
def test_autofilter_publishing(save, save_images, autof_session):
    lima_simulator = autof_session.env_dict["lima_simulator"]
    if save_images:
        frames_per_file = 10
        lima_simulator.saving.initialize()
        lima_simulator.saving.file_format = "HDF5"
        lima_simulator.saving.mode = "ONE_FILE_PER_N_FRAMES"
        lima_simulator.saving.frames_per_file = frames_per_file

    mca = autof_session.env_dict["simu1"]
    mca.rois.set("roi1", 100, 110)

    diode = autof_session.env_dict["diode"]

    sim, scan = perform_autofilter_scan(
        autof_session, lima_simulator, mca, diode, save=save, save_images=save_images
    )
    scan_data = scan._scan_data

    expected = {
        "axis:sim_autofilter1_ctrs_axis",
        "timer:elapsed_time",
        "timer:epoch",
        "AutoFilterDetMon:sim_autofilter1_mon",
        "AutoFilterDetMon:sim_autofilter1_det",
        "autofilter1:sim_autofilter1_det_corr",
        "autofilter1:curratt",
        "autofilter1:transm",
        "autofilter1:ratio",
        "lima_simulator:image",
        "lima_simulator:roi_counters:roi1_sum",
        "lima_simulator:roi_counters:roi1_avg",
        "lima_simulator:roi_counters:roi1_std",
        "lima_simulator:roi_counters:roi1_min",
        "lima_simulator:roi_counters:roi1_max",
        "simu1:roi1",
        "simulation_diode_sampling_controller:diode",
    }
    for i in range(4):
        expected |= {
            f"simu1:deadtime_det{i}",
            f"simu1:trigger_livetime_det{i}",
            f"simu1:triggers_det{i}",
            f"simu1:events_det{i}",
            f"simu1:realtime_det{i}",
            f"simu1:energy_livetime_det{i}",
            f"simu1:icr_det{i}",
            f"simu1:ocr_det{i}",
            f"simu1:spectrum_det{i}",
            f"simu1:roi1_det{i}",
        }
    assert set(scan_data.streams.keys()) == expected

    measured = scan_data.streams["lima_simulator:roi_counters:roi1_max"][:]
    expected = numpy.arange(61) * 100 + 100
    invalid_points = [18, 20, 22, 24, 27, 34, 36, 38, 40, 43]
    expected[invalid_points] = -1
    expected = expected[expected != -1]
    numpy.testing.assert_array_equal(measured, expected)

    assert_sim_data(sim, scan_data, save=save, save_images=save_images)

    npoints = sim.npoints + 1
    assert scan_data.info["npoints"] == npoints
    lima_stream = scan_data.streams["lima_simulator:image"]
    assert lima_stream.info["shape"] == (1024, 1024)
    assert lima_stream.info["dtype"] == "uint32"
    if save_images:
        assert lima_stream.info["lima_info"]["frame_per_file"] == frames_per_file


def test_ct_issue_2894(autof_session):
    autof = autof_session.env_dict["autofilter1"]
    ct_scan = ct(0.1, autof, run=False)
    ct_task = gevent.spawn(ct_scan.run)

    ct_task.join()

    assert ct_task.successful()
    for cnt in autof.counters:
        assert isinstance(ct_scan.get_data(cnt.name), numpy.ndarray)


def test_filterset_counters(autof_session):
    autofwheel_cu = autof_session.env_dict["autofwheel_cu"]
    autofwheel_pos_counter = autofwheel_cu.counters.autofwheel_cu_position
    autofwheel_transm_counter = autofwheel_cu.counters.autofwheel_cu_transm
    assert autofwheel_pos_counter
    assert autofwheel_transm_counter

    autofwheel_cu.filter = 3

    ct_scan = ct(0, autofwheel_cu, run=False)
    ct_scan.run()

    assert ct_scan.get_data()[autofwheel_pos_counter.name] == 3
    # assert ct_scan.get_data()[autofwheel_transm_counter.name]


def test_energy_change_issue_3576(autof_session):
    # get ready
    energy = autof_session.env_dict["energy"]
    energy.move(12)

    filterw = autof_session.env_dict["autofwheel_cu"]

    # check that the absorption table has been reduced
    nb_filtset_12keV = filterw._nb_filtset
    energy.high_limit = 30
    energy.move(30)
    filterw._update_all()
    nb_filtset_30keV = filterw._nb_filtset

    # from 12 to 30 keV, the abs. table has been reduced
    assert nb_filtset_30keV != nb_filtset_12keV
