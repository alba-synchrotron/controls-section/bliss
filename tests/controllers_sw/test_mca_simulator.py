# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy
from bliss import global_map
from bliss.common.scans import loopscan


def test_mca_simulator(default_session, mosca_simulator):
    # WARNING: fixture load order does matter ! (session before simulator)
    mca = default_session.config.get("mca_sim")

    # === check counters management

    # clear all rois and calc formula
    mca.rois.clear()
    mca.calc_formula = None

    cntnames = ["spectrum", "elapsed", "realtime", "trigger_livetime"]

    # check counters in main CounterController
    assert list(mca._masterCC._counters) == cntnames
    assert list(x.name for x in mca.counters) == cntnames
    assert list(x.name for x in mca.counter_groups["spectrum"]) == cntnames[0:1]
    assert list(x.name for x in mca.counter_groups["stat"]) == cntnames[1:]
    assert list(x.name for x in mca.counter_groups["default"]) == cntnames
    assert not mca.counter_groups["roi"]
    assert not mca.counter_groups["roi_corr"]
    assert not mca.counter_groups["roi_sum"]

    # check counters in CalcControllers
    assert mca._calcroiCC._counters == {}
    assert [x.name for x in mca._calcroiCC.inputs] == cntnames[1:]
    assert not mca._calcroiCC.outputs
    assert mca._sumroiCC._counters == {}
    assert not mca._sumroiCC.inputs
    assert not mca._sumroiCC.outputs

    # check counters global map registration
    gm_cntnames = [x.name for x in global_map.get_counters_iter()]
    for cname in cntnames:
        assert cname in gm_cntnames

    # set number of detectors
    mca._set_number_of_channels(6)

    # define some rois
    mca.rois["r1"] = 0, 2048  # applied on all detectors
    mca.rois["r2"] = 500, 1500  # applied on all detectors
    mca.rois["r3"] = 0, 2048, -1
    mca.rois["r4"] = 0, 2048, "0-5"

    # to simulate applying different hw config file
    # also recreate all CounterControllers and RoiManager
    mca.initialize()
    assert not mca.calc_formula
    assert mca._number_channels == 6

    # check roi counters (raw, calc and sum)
    fcall = list(x.name for x in mca.counters)
    fcgr = list(x.name for x in mca.counter_groups["roi"])
    gm_cntnames = [x.name for x in global_map.get_counters_iter()]
    for rname in ["r1", "r2"]:
        sumname = f"{rname}_sum"
        assert sumname in [x.name for x in mca._sumroiCC.outputs]
        assert sumname in mca._sumroiCC._counters
        assert sumname not in gm_cntnames
        assert sumname not in fcall
        assert sumname not in fcgr

        for detnum in mca.active_channels.values():
            rawname = f"{rname}_det{detnum:02d}"
            corrname = f"{rname}_corr_det{detnum:02d}"
            assert rawname in mca._masterCC._counters
            assert rawname in [x.name for x in mca._calcroiCC.inputs]
            assert rawname in gm_cntnames
            assert rawname in fcall
            assert rawname in fcgr

            assert corrname in [x.name for x in mca._calcroiCC.outputs]
            assert corrname in mca._calcroiCC._counters
            assert corrname not in gm_cntnames
            assert corrname not in fcall
            assert corrname not in fcgr

    for rawname in ["r3_sum_all", "r4_sum_00_05"]:
        assert rawname in mca._masterCC._counters
        assert rawname in gm_cntnames
        assert rawname in fcall
        assert rawname in fcgr
        assert rawname not in [x.name for x in mca._calcroiCC.inputs]

    fcgrc = list(x.name for x in mca.counter_groups["roi_corr"])
    fcgrs = list(x.name for x in mca.counter_groups["roi_sum"])
    assert not fcgrc
    assert not fcgrs

    # check counters values
    s = loopscan(3, 0.01, mca)
    for rname in ["r1", "r2"]:
        for data in s.get_data().values():
            assert data.shape[0] == 3
        for detnum in mca.active_channels.values():
            raw_roi_cnt_name = f"{rname}_det{detnum:02d}"
            rval = s.get_data(raw_roi_cnt_name)

    # Set a calc formula
    mca.calc_formula = "roi"  # identity
    assert mca._calcroiCC.inputs[-1].name != "diode1"
    # check calc and sum counters are now available
    fcall = list(x.name for x in mca.counters)
    fcgrc = list(x.name for x in mca.counter_groups["roi_corr"])
    fcgrs = list(x.name for x in mca.counter_groups["roi_sum"])
    for rname in ["r1", "r2"]:
        sumname = f"{rname}_sum"
        assert sumname in fcall
        assert sumname in fcgrs
        for detnum in mca.active_channels.values():
            corrname = f"{rname}_corr_det{detnum:02d}"
            assert corrname in fcall
            assert corrname in fcgrc

    for rawname in ["r3_sum_all", "r4_sum_00_05"]:
        assert rawname not in fcgrc
        assert rawname not in fcgrs

    s = loopscan(3, 0.01, mca)
    for rname in ["r1", "r2"]:
        rsum = 0
        for detnum in mca.active_channels.values():
            x = s.get_data(f"{rname}_corr_det{detnum:02d}")
            assert numpy.all(x == s.get_data(f"{rname}_det{detnum:02d}"))
            rsum += x
        assert numpy.all(rsum != 0)
        assert numpy.all(rsum == s.get_data(f"{rname}_sum"))
    assert numpy.all(numpy.isclose(s.get_data("r3_sum_all"), s.get_data("r1_sum")))
    assert numpy.all(numpy.isclose(s.get_data("r4_sum_00_05"), s.get_data("r1_sum")))

    mca.calc_formula = "roi / (1-trigger_livetime/realtime) / iodet"
    assert mca._calcroiCC.inputs[-1].name == "diode1"
    s = loopscan(3, 0.01, mca)
    for rname in ["r1", "r2"]:
        rsum = 0
        iodet = s.get_data("diode1")
        for detnum in mca.active_channels.values():
            rval = s.get_data(f"{rname}_det{detnum:02d}")
            trigger_livetime = s.get_data(f"trigger_livetime_det{detnum:02d}")
            realtime = s.get_data(f"realtime_det{detnum:02d}")
            calc = rval / (1 - trigger_livetime / realtime) / iodet
            assert numpy.all(calc == s.get_data(f"{rname}_corr_det{detnum:02d}"))
            rsum += calc
        assert numpy.all(rsum != 0)
        assert numpy.all(rsum == s.get_data(f"{rname}_sum"))
    assert numpy.all(
        numpy.isclose(s.get_data("r3_sum_all"), s.get_data("r4_sum_00_05"))
    )

    # remove roi r1
    mca.rois.remove("r1")

    # check roi r1 is properly removed from counters (raw, calc and sum)
    fcgr = list(x.name for x in mca.counter_groups["roi"])
    fcgrc = list(x.name for x in mca.counter_groups["roi_corr"])
    fcgrs = list(x.name for x in mca.counter_groups["roi_sum"])
    gm_cntnames = [x.name for x in global_map.get_counters_iter()]
    rname = "r1"
    sumname = f"{rname}_sum"
    for detnum in mca.active_channels.values():
        rawname = f"{rname}_det{detnum:02d}"
        corrname = f"{rname}_corr_det{detnum:02d}"
        assert rawname not in mca._masterCC._counters
        assert rawname not in fcgr
        assert rawname not in [x.name for x in mca._calcroiCC.inputs]
        assert rawname not in gm_cntnames

        assert corrname not in fcgrc
        assert corrname not in [x.name for x in mca._calcroiCC.outputs]
        assert corrname not in mca._calcroiCC._counters
        assert corrname not in gm_cntnames

    assert sumname not in fcgrs
    assert sumname not in [x.name for x in mca._sumroiCC.outputs]
    assert sumname not in mca._sumroiCC._counters
    assert sumname not in gm_cntnames
