# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.common import measurementgroup


def test_moco(dummy_tango_server, session):
    mymoco = session.config.get("mymoco")
    mg = measurementgroup.MeasurementGroup("local", {"counters": ["mymoco"]})

    assert set(mg.available) == {
        mymoco.counters.outm.fullname,
        mymoco.counters.inm.fullname,
    }


def test_moco_info(dummy_tango_server, session):
    mymoco = session.config.get("mymoco")
    assert "mymoco" in mymoco.__info__()
