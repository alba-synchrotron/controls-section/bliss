# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy
import pytest


def get_all_real_axes(spectro):
    axes = []
    for ana in spectro.analysers:
        axes.extend(ana.real_axes.values())
    axes.extend(spectro.detector.real_axes.values())
    return axes


def speedup_axes(axes, velocity=100000, acceleration=100000):
    for x in axes:
        x.velocity = velocity
        x.acceleration = acceleration


def test_spectrometer_api(default_session):
    spectro = default_session.config.get("spectro")
    speedup_axes(get_all_real_axes(spectro))

    # activate flint plot
    # spectro.plot() # needs flint_session fixture

    # set spectro origin in lab ref
    spectro.referential_origin = 0, 0, 0

    # check 5 analysers in config
    assert len(spectro.analysers) == 5
    assert [ana.name for ana in spectro.analysers] == [
        "ana_0",
        "ana_1",
        "ana_2",
        "ana_3",
        "ana_4",
    ]
    assert spectro.detector.name == "det_0"

    # check axes naming
    assert list(spectro.bragg_calc_controller.axes.keys()) == [
        "bragg_spectro",
        "bragg_det_0",
        "bragg_ana_0",
        "bragg_ana_1",
        "bragg_ana_2",
        "bragg_ana_3",
        "bragg_ana_4",
    ]
    assert list(spectro.bragg_calc_controller._tagged.keys()) == [
        "bragg",
        "real",
        "bragg_det_0",
        "bragg_ana_0",
        "bragg_ana_1",
        "bragg_ana_2",
        "bragg_ana_3",
        "bragg_ana_4",
    ]
    assert list(spectro.energy_calc_controller.axes.keys()) == [
        "ene_spectro",
        "ene_det_0",
        "ene_ana_0",
        "ene_ana_1",
        "ene_ana_2",
        "ene_ana_3",
        "ene_ana_4",
    ]
    assert list(spectro.energy_calc_controller._tagged.keys()) == [
        "energy",
        "real",
        "energy_det_0",
        "energy_ana_0",
        "energy_ana_1",
        "energy_ana_2",
        "energy_ana_3",
        "energy_ana_4",
    ]

    assert list(spectro.analysers.ana_0.bragg_calc_controller.axes.keys()) == [
        "bragg_ana_0",
        "xa0",
        "za0",
        "pita0",
        "yawa0",
    ]
    assert list(spectro.analysers.ana_0.bragg_calc_controller._tagged.keys()) == [
        "bragg",
        "real",
        "rpos",
        "zpos",
        "pitch",
        "yaw",
    ]

    assert list(spectro.analysers.ana_0.energy_calc_controller.axes.keys()) == [
        "ene_ana_0",
        "bragg_ana_0",
    ]
    assert list(spectro.analysers.ana_0.energy_calc_controller._tagged.keys()) == [
        "energy",
        "real",
        "bragg",
    ]

    assert list(spectro.detector.bragg_calc_controller.axes.keys()) == [
        "bragg_det_0",
        "xdet",
        "zdet",
        "pitdet",
    ]
    assert list(spectro.detector.bragg_calc_controller._tagged.keys()) == [
        "bragg",
        "real",
        "xpos",
        "zpos",
        "pitch",
    ]

    assert list(spectro.detector.energy_calc_controller.axes.keys()) == [
        "ene_det_0",
        "bragg_det_0",
    ]
    assert list(spectro.detector.energy_calc_controller._tagged.keys()) == [
        "energy",
        "real",
        "bragg",
    ]

    # activate all analysers
    spectro.unfreeze(*spectro.frozen)
    assert len(spectro._active_analysers) == 5
    assert [ana.name for ana in spectro._active_analysers] == [
        "ana_0",
        "ana_1",
        "ana_2",
        "ana_3",
        "ana_4",
    ]

    # check detector target
    assert spectro.detector.target.name == "ana_0"

    # check current ana crystal
    for ana in spectro.analysers:
        assert ana.xtal_sel == "Si844"

    # move to bragg = 70
    spectro.bragg_axis.move(70)
    assert spectro.is_aligned == (True, True)
    assert spectro.bragg_axis.position == 70
    assert numpy.isclose(spectro.analysers.ana_0.geo_bragg, 70, rtol=1e-6)

    # check detector enecalc xtals is target.xtals
    assert (
        spectro.detector.energy_calc_controller.xtals is spectro.detector.target.xtals
    )

    spectro.detector.target = spectro.analysers["ana_1"]
    assert (
        spectro.detector.energy_calc_controller.xtals is spectro.detector.target.xtals
    )

    spectro.analysers["ana_1"].xtal_sel = "Si220"
    assert spectro.detector.energy_calc_controller.xtals.xtal_sel == "Si220"

    spectro.detector.target = spectro.analysers["ana_0"]
    assert spectro.detector.energy_calc_controller.xtals.xtal_sel == "Si844"

    # try to freeze detector target
    with pytest.raises(ValueError):
        spectro.freeze(spectro.analysers.ana_0)

    # freeze a2 and a3
    pb2 = spectro.analysers["ana_2"].bragg_axis.position
    pb3 = spectro.analysers["ana_3"].bragg_axis.position
    pe2 = spectro.analysers["ana_2"].energy_axis.position
    pe3 = spectro.analysers["ana_3"].energy_axis.position

    spectro.freeze("ana_2", "ana_3")
    assert len(spectro._active_analysers) == 3
    assert [ana.name for ana in spectro._active_analysers] == [
        "ana_0",
        "ana_1",
        "ana_4",
    ]

    # set crystal Si844
    spectro.set_crystal("Si844")
    assert spectro.analysers["ana_1"].xtal_sel == "Si844"

    # check energy <=> bragg conversion
    #   11.218 keV <=> 85.5266649413 degree
    for ana in spectro._active_analysers:
        assert ana.energy_calc_controller.dspacing == 5.543013043019761e-11
    spectro.energy_axis.move(11.218)
    assert numpy.isclose(spectro.bragg_axis.position, 85.5266649413, rtol=1e-6)

    # move to bragg = 80
    spectro.bragg_axis.move(80)
    assert spectro.is_aligned == (True, True)
    assert spectro.bragg_axis.position == 80
    assert numpy.isclose(spectro.analysers.ana_0.geo_bragg, 80, rtol=1e-6)

    # check frozen analysers did not move
    assert spectro.analysers["ana_2"].bragg_axis.position == pb2
    assert spectro.analysers["ana_3"].bragg_axis.position == pb3
    assert spectro.analysers["ana_2"].energy_axis.position == pe2
    assert spectro.analysers["ana_3"].energy_axis.position == pe3

    # check unfreeze without args is like unfreeze all
    spectro.unfreeze()
    assert len(spectro._active_analysers) == 5
    assert [ana.name for ana in spectro._active_analysers] == [
        "ana_0",
        "ana_1",
        "ana_2",
        "ana_3",
        "ana_4",
    ]

    # re-align all analysers
    spectro.energy_axis.move(11.218)
    assert spectro.is_aligned == (True, True)

    # move pitch of ana0, ana1, ana2, ana4
    for name in ["ana_1", "ana_2", "ana_3", "ana_4"]:
        spectro.analysers[name].real_axes["pitch"].rmove(2)
    assert spectro.is_aligned == (False, False)

    # check freeze without args is like freeze all except target
    spectro.freeze()
    assert len(spectro._active_analysers) == 1
    assert [ana.name for ana in spectro._active_analysers] == [
        "ana_0",
    ]

    # ana3 pitch was not moved so after freezing the
    # other analysers it should be seen aligned
    assert spectro.is_aligned == (True, True)

    assert spectro.energy_axis.position == spectro.energy_axis._set_position
    spectro.energy_axis.rmove(0.001)

    spectro.detector.target = spectro.analysers["ana_3"]
    spectro.freeze()
    assert len(spectro._active_analysers) == 1
    assert [ana.name for ana in spectro._active_analysers] == [
        "ana_3",
    ]

    # test parking
    with pytest.raises(RuntimeError):
        spectro.park()

    spectro.unfreeze()

    with pytest.raises(ValueError):
        spectro.park()

    with pytest.raises(ValueError):
        spectro.park("park1")

    daxes = {tag: ax.position for tag, ax in spectro.detector.real_axes.items()}
    # check moving to parking
    spectro.park("park0", interactive=False)
    # check detector axes have not moved (because tagged 'fixed')
    for tag, ax in spectro.detector.real_axes.items():
        assert ax.position == daxes[tag]

    # clean Louie connections
    spectro.__close__()


def test_spectrometer_cartesian(default_session):
    spectro = default_session.config.get("spectro_cr")
    speedup_axes(get_all_real_axes(spectro))

    # activate flint plot
    # spectro.plot() # needs flint_session fixture

    # change sample ref pos
    spectro.referential_origin = 500, 0, 200

    assert spectro.detector.target == spectro.analysers.ana_0_cr

    # move to bragg = 80
    spectro.bragg_axis.move(80)
    assert spectro.is_aligned == (True, True)
    assert spectro.bragg_axis.position == 80
    assert numpy.isclose(spectro.analysers.ana_0_cr.geo_bragg, 80, rtol=1e-6)

    # set_energy
    ene = spectro.energy_axis.position
    theo_reals = spectro.analysers.ana_0_cr._current_bragg_solution[2]
    spectro.analysers.ana_0_cr.set_energy(ene, interactive=False)
    for k, v in theo_reals.items():
        assert numpy.isclose(
            spectro.analysers.ana_0_cr.real_axes[k].position, v, rtol=1e-4
        )

    # check various bragg positions for different miscut values
    rtol = 1e-6
    spectro.referential_origin = 0, 0, 0
    for ana in spectro._active_analysers:
        ana.radius = 500
        ana.offset_on_detector = 0

    for bragg_angle in range(60, 90, 5):
        for asym in range(0, 6, 1):
            for ana in spectro._active_analysers:
                ana.miscut = asym

            spectro.bragg_axis.move(bragg_angle)
            # print(spectro.__info__())
            assert spectro.is_aligned == (True, True)
            for ana in spectro._active_analysers:
                bragg, solution, _ = ana._current_bragg_solution
                assert numpy.isclose(bragg_angle, ana.bragg_axis.position, rtol=rtol)
                assert numpy.isclose(bragg_angle, bragg, rtol=rtol)
