"""Test related module to share controllers

This module is intended to be used by unittest and other projects.

Any API changes have to be done precociously, including changes in the master
branch.
"""
