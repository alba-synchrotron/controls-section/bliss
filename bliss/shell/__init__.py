# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""Shell (:term:`CLI` and Web based)

.. autosummary::
    :toctree:

    cli
    data
    dialog
    formatters
    qtapp
    interlocks
    standard

"""

from . import prompt_toolkit_patch  # noqa: F401
