# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
"""
Standard functions provided to the BLISS shell.
"""

from bliss.config import static  # noqa: E402,F401

from bliss.common.soft_axis import SoftAxis  # noqa: E402,F401
from bliss.common.counter import SoftCounter  # noqa: E402,F401

from bliss.common.cleanup import (  # noqa: F401
    cleanup,
    error_cleanup,
)

from bliss.scanning.scan_tools import (  # noqa: E402,F401
    cen,
    com,
    peak,
    trough,
    where,
    find_position,
    fwhm,
)

from numpy import (  # noqa: E402,F401
    sin,
    cos,
    tan,
    arcsin,
    arccos,
    arctan,
    arctan2,
    log,
    log10,
    sqrt,
    exp,
    power,
    deg2rad,
    rad2deg,
)

from numpy.random import rand  # noqa: E402,F401
from time import asctime as date  # noqa: E402,F401
from pprint import pprint  # noqa: F401
from gevent import sleep  # noqa: F401

from ._motion import (  # noqa: E402,F401
    mv,
    umv,
    mvr,
    umvr,
    mvd,
    umvd,
    mvdr,
    umvdr,
    rockit,
    move,
    goto_cen,
    goto_peak,
    goto_click,
    goto_min,
    goto_com,
    goto_custom,
)

from ._datapolicy import (  # noqa: E402,F401
    newproposal,
    endproposal,
    newsample,
    newcollection,
    newdataset,
    enddataset,
)

from ._logging import (  # noqa: E402,F401
    lslog,
    lsdebug,
    debugon,
    debugoff,
    log_stdout,
    elog_print,
    elog_add,
    elog_plot,
    elogbook,
)

from ._utils import (  # noqa: E402,F401
    lsconfig,
    info,
    bench,
    clear,
    countdown,
    prdef,
    print_html,
    print_ansi,
)

from ._plot import (  # noqa: E402,F401
    plotinit,
    plotselect,
    replot,
    plot,
    flint,
    edit_roi_counters,
)

from ._menu import (  # noqa: E402,F401
    menu,
    show_dialog,
)

from ._devices import (  # noqa: E402,F401
    wa,
    wm,
    wu,
    sta,
    stm,
    lsmot,
    sync,
    interlock_show,
    interlock_state,
    lscnt,
    lsmg,
    lsobj,
    wid,
    reset_equipment,
)

from ._debug import (  # noqa: E402,F401
    metadata_profiling,
    time_profile,
)

from bliss.controllers.lima.limatools import (  # noqa: E402,F401
    limastat,
    limatake,
)

from bliss.common.scans import *  # noqa: E402,F401,F403

from ._external import (  # noqa: E402,F401
    silx_view,
    pymca,
    tw,
)
