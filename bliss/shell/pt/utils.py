# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
"""
Some helpers to deal with prompt toolkit in bliss shell.
"""

from __future__ import annotations

import gevent
import typing
from bliss import is_bliss_shell
from prompt_toolkit.application import get_app_or_none

if typing.TYPE_CHECKING:
    from prompt_toolkit.input import Input


def isatty(input: Input) -> bool:
    """Return true if the input is a TTY"""
    stdin = getattr(input, "stdin", None)
    if stdin is None:
        return False
    if hasattr(stdin, "isatty"):
        return stdin.isatty()
    return False


def try_to_grab_shell():
    """Nothing if the shell can be used to display a prompt toolkit application.

    For now we assume that only a greenlet spawned by the BlissRepl can have this
    right, and only if no prompt toolkit application is already in use.

    Raises:
        RuntimeError: If it is not possible to grab the shell, with a reason
    """
    # Assume that outside the BLISS shell context, other application have to
    # deal with the ownership on there own
    if not is_bliss_shell():
        raise RuntimeError("It is not a BLISS shell")

    # If the current greenlet is not a Greenlet, guess it's a unittest
    g = gevent.getcurrent()
    if isinstance(g, gevent.Greenlet):
        is_eval_greenlet = g.spawn_tree_locals.get("eval_greenlet", False)
        if not is_eval_greenlet:
            raise RuntimeError("Not an eval greenlet")

    # Make sure it is not reentrant: It is not called from an app
    if get_app_or_none() is not None:
        raise RuntimeError("An application is already in use")
