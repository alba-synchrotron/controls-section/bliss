# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Package to handle the bootstrapping of the bliss-shell.

Everything about the setup of the bliss-shell application
should be done there.
"""
