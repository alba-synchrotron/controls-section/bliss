"""ICAT is a database associated to the ESRF data policy for storing metadata and e-logbook messages.

This sub-package provides:

* an API to ICAT in the `client` subpackage
* an API for ICAT metadata definitions in the `definitions` module
* an API to generate ICAT metadata from a Bliss session in the `metadata` module
* an API to ICAT metadata in Redis in the `policy` module
"""
