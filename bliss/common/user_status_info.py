# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
This module is a central point for function to provide human readable
information to the user.

It can be used the following way:

.. code-block:: python

    from bliss.common.user_status_info import status_message
    import gevent
    with status_message() as update:
        update("Pif")
        gevent.sleep(1)
        update("Paf")
        gevent.sleep(1)
        update("Pouf")
        gevent.sleep(1)
        update("Toc")
        gevent.sleep(1)
        update("He fall down")
        gevent.sleep(1)
"""

import weakref
import contextlib

_USER_MESSAGE_STATUS = weakref.WeakKeyDictionary()
_DISPLAY_CALLBACK = None

_CLEAR_LINE = "\x1b[2K"
"""Ansi escape character to clear the entire line"""


@contextlib.contextmanager
def status_message():
    """
    Helper to inform end user about a status message
    """

    class K:
        pass

    key = K()

    def set(message):
        set_user_status_message(key, message)

    try:
        yield set
    finally:
        remove_user_status_message(key)


def set_user_status_message(key, message):
    """
    Set a message to the end user about a status of something.
    example: when a scan is in pause during a refill.
    """
    _USER_MESSAGE_STATUS[key] = message
    trigger_callback()


def remove_user_status_message(key):
    if _USER_MESSAGE_STATUS.pop(key, None) is not None:
        trigger_callback()


def trigger_callback():
    values = _USER_MESSAGE_STATUS.values()
    if _DISPLAY_CALLBACK is not None:
        _DISPLAY_CALLBACK(*values)
    else:
        print(_CLEAR_LINE, end="")
        print(*values, sep=",", end="\r", flush=True)


def set_display_callback(func):
    """
    Change the global display of status information
    Default display callback is print with sep=',' and end='\r'.
    Can be set to None to disable display of status messages
    func -- musst have a signature like func(*messages)
    """
    global _DISPLAY_CALLBACK
    _DISPLAY_CALLBACK = func


@contextlib.contextmanager
def callback():
    prev_display = _DISPLAY_CALLBACK
    try:
        yield set_display_callback
    finally:
        set_display_callback(prev_display)
