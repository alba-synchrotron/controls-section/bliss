# bliss_lima_simulators

Provides tango devices to simulate lima cameras.

Latest documentation: **https://bliss.gitlab-pages.esrf.fr/bliss/master/bliss_demo.html**
