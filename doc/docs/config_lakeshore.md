# Lakeshore with **regulation plugin**:

Models (used at ESRF and for which a BLISS controller exists)
have the following possible interfaces:

* model 331 can use RS232.
* model 332 can use GPIB or RS232.
* model 335 can use GPIB or USB.
* model 336 can use GPIB or Ethernet.
* model 340 can use GPIB (or RS232 with RJ11 connector).

## Configuration

See [Regulation framework configuration](regul_regulation_fw.md) for Regulation
parameters configuration.

Lakeshore specific parameters:

* `comm_max_frequency`: value defined to tame communication flow.
  it must be set according to user needs and hardware capabilities.
  default value is `20`.

## yml configuration example

If necessary, a Brainbox ethernet to serial adapter can be used with the appropriate serial setting in the Brainbox and :
```yaml
  tcp:
    url: id14serial1.esrf.fr:9001
```

Lakeshore 336, acessible via GPIB, USB or Ethernet


```yaml

- class: LakeShore336
  module: temperature.lakeshore.lakeshore336
  plugin: regulation
  name: lakeshore336
  comm_max_frequency: 50 # 20 by default if not defined
  gpib:
     url: enet://gpibid42f.esrf.fr
     pad: 9
     eol: "\r\n" #  ! the usual EOL but could have been modified
                        through the hardware interface
  usb:
     url: ser2net://lid1421:28000/dev/ttyUSB0
     baudrate: 57600    # = the only possible value
  tcp:
     url: idXXls336.esrf.fr:7777

  inputs:
    - name: ls336_A
      channel: A
      # possible set-point units: Kelvin, Celsius, Sensor_unit
      unit: Kelvin
      #tango_server: ls_336
    - name: ls336_A_c    # input temperature in Celsius
      channel: A
      unit: Celsius
    - name: ls336_A_su  # in sensor units (Ohm or Volt)
      channel: A
      unit: Sensor_unit

    - name: ls336_B
      channel: B
      # possible set-point units: Kelvin, Celsius, Sensor_unit
      unit: Kelvin
      #tango_server: ls_336
    - name: ls336_B_c    # input temperature in Celsius
      channel: B
      unit: Celsius
    - name: ls336_B_su  # in sensor units (Ohm or Volt)
      channel: B
      unit: Sensor_unit

    # can add also input channels C and D

  outputs:
    - name: ls336o_1
      channel: 1
      unit: Kelvin
    - name: ls336o_2
      channel: 2

  ctrl_loops:
    - name: ls336l_1
      input: $ls336_A
      output: $ls336o_1
      channel: 1
    - name: ls336l_2
      input: $ls336_B
      output: $ls336o_2
      channel: 2

    # can add also output channels 3 and 4

```

For models 331, 332 and 340, a serial line can be use to communicate:

```yaml
  serial:
     url: ser2net://lid102:28000/dev/ttyR1
     baudrate: 9600
     eol: "\r\n"       # the usual EOL but could have been modified through the hardware interface
                       # if not specified, serial default is \n.
```

Valid Serial Line baudrate values depend on model:
* for 331: 300, 1200, 9600
* for 332: 300, 1200, 9600
* for 340: 300, 1200, 2400, 4800, 9600, 19200 (! RJ11 connector)

!!! Note

    The standard EOL used by the Lakeshore controllers is `\r\n`. However
    this could have been modified throught the controller's hardware interface.

    ![Screenshot](img/LakeshoreEOLconfig.png)


![Screenshot](img/lakeshore_classes.svg)

