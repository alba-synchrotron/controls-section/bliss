# Types of settings in BLISS

The in-memory data structure store [*REDIS*](https://redis.io) is used for data in *BLISS* that requires a persistent representation. *REDIS* is a key-value pair storage where the keys are strings and the values can have [types](https://redis.io/topics/data-types). The most common types used in *BLISS* are:

* *STRING*: Binary data (`bytes` in python). This is mostly used for strings and scalar numbers.
* *LIST*: Ordered, position-indexed, mutable list of *STRING* values (`list` of `bytes` in python)
* *SET*: Unordered, mutable set (no duplicates) of *STRING* values (`set` of `bytes` in python)
* *ZSET*: Ordered, mutable set (no duplicates) of *STRING* values (no native type in python)
* *HASH*: Mutable mapping of *STRING* to *STRING* (`dict` of `bytes` to `bytes` in python)
* *STREAM*: Append-only, indexed list of *STRING* values called *events*. The stream index or event ID of an *event* consists of two integers. By default the first integer is the time of publication (milliseconds since epoch) and the second integer is a sequence number (to handle events with the same rounded time since epoch). A *STREAM* can have a maximal length which makes it a ring buffer.

## Single-key REDIS representation

These data types in *BLISS* have a persistent representation in *REDIS* with one key-value pair:

* `SimpleSetting`: *STRING* representation of simple types `str`, `bytes`, `int` and `float`
* `SimpleObjSetting`: *STRING* representation of any pickleable type
* `QueueSetting`: *LIST* representation of a `list` of simple values
* `QueueObjSetting`: *LIST* representation of a `list` of pickleable values
* `HashSetting`: *HASH* representation of a `dict` with simple keys and simple values
* `Struct`: same as `HashSetting` (exposes keys as attributes)
* `HashObjSetting`: *HASH* representation of a `dict` with simple keys and pickleable values
* `OrderedHashSetting`: ordered version of `HashSetting`
* `OrderedHashObjSetting`: ordered version of `HashObjSetting`

## Multi-key REDIS representation

These data types in *BLISS* have a persistent representation in *REDIS* with more than one key-value pair.

* `ParameterWardrobe`:
    * *db_name* (`QueueSetting`): list of instance names
    * *db_name*:creation_order: type *ZSET* in *REDIS* but no *BLISS* type
    * *db_name*:default (`OrderedHashSetting`): parameters of the default instance
    * *db_name*:instance2 (`OrderedHashSetting`): parameters of the default instance2
    * *db_name*:instance3 (`OrderedHashSetting`): parameters of the default instance3
    * ...
