# Spectrometer framework

A framework for the implementation of the various spectrometers assemblies found on beamlines.
This framework is designed for spectrometers that are made of a **set of crystal analysers** and a **2D detector**.

The `Spectrometer` class manages one detector and a set of analysers and provides 2 pseudo-axes to drive the system Energy or Bragg position.
Each analyser has its own crystal and can be driven independently in Energy or Bragg position.
A detector has a target analyser and its Energy or Bragg position is bound to its target position.

For the standard `Cartesian` and `Cylindrical` geometries, users can declare their own setup through the YAML configuration file.
The geometry kind is defined by the real axes dependency of the analysers and detector mounting.
For example, the `Cartesian` geometry assume that an analyser depends on 4 real axes ['xpos', 'zpos', 'pitch', 'yaw'] 
and the detector on 3 real axes['xpos', 'zpos', 'pitch'].

### YML Configuration

``` yml
analysers:
            
  - name: ana_0
    plugin: generic
    module: spectrometers.spectro_base
    class: CylindricalAnalyser
    miscut: -5.04931
    radius: 500
    angular_offset: 0
    offset_on_detector: 0
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    xtals:
      - xtal: Si844
        dspacing: 0.5543013043019761  # dspacing * 1e-10
    real_axes:
      - rpos: $xa0
      - zpos: $za0
      - pitch: $pita0
      - yaw: $yawa0

    energy_axis:           # optional
      - name: ene_ana_0    # optional
    bragg_axis:            # optional
      - name: bragg_ana_0  # optional
        
  - name: ana_1
    plugin: generic
    module: spectrometers.spectro_base
    class: CylindricalAnalyser
    miscut: -5.04931
    radius: 500
    angular_offset:  -3.5
    offset_on_detector: 14
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    xtals:
      - xtal: Si844
        dspacing: 0.5543013043019761  # dspacing * 1e-10
    real_axes:
      - rpos: $xa1
      - zpos: $za1
      - pitch: $pita1
      - yaw: $yawa1
    energy_axis:           # optional
      - name: ene_ana_1    # optional
    bragg_axis:            # optional
      - name: bragg_ana_1  # optional

  - name: ana_2
    plugin: generic
    module: spectrometers.spectro_base
    class: CylindricalAnalyser
    miscut: -5.04931
    radius: 500
    angular_offset: 3.5
    offset_on_detector: -14
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    xtals:
      - xtal: Si844
        dspacing: 0.5543013043019761  # dspacing * 1e-10
    real_axes:
      - rpos: $xa2
      - zpos: $za2
      - pitch: $pita2
      - yaw: $yawa2
    energy_axis:           # optional
      - name: ene_ana_2    # optional
    bragg_axis:            # optional
      - name: bragg_ana_2  # optional


detectors:

  - name: det_0
    plugin: generic
    module: spectrometers.spectro_base
    class: Detector
    target: $ana_0
    width: 400
    height: 200
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    real_axes:
      - xpos: $xdet
      - zpos: $zdet
      - pitch: $pitdet
    energy_axis:           # optional
      - name: ene_det_0    # optional
    bragg_axis:            # optional
      - name: bragg_det_0  # optional


spectrometers:

  - name: spectro
    plugin: generic
    module: spectrometers.spectro_base
    class: Spectrometer
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    detector: $det_0
    analysers:
      - name: $ana_0
      - name: $ana_1
      - name: $ana_2
      - name: $ana_3
      - name: $ana_4
    energy_axis:
      - name: ene_spectro    # optional
    bragg_axis:
      - name: bragg_spectro  # optional
    

```


For none-standard geometries, users can inherit from the base classes `Detector`, `Analyser`, `Spectrometer` and overload the `compute_bragg_solution` and `_get_pos`
methods of the `Detector` and `Analyser` class (as an example see the `mmspectro.py` file). The main difference with the standard geometries is the real axes on which
depend the analysers and detector. 

``` yml
analysers:
  - name: ana_0_mm
    plugin: generic
    module: spectrometers.mmspectro
    class: MMAnalyser
    miscut: 0
    radius: 500
    ypos: 0
    offset_on_detector: 0
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    xtals:
      - xtal: Si444
        dspacing: 0.783882894235
      - xtal: Si844
        dspacing: 0.5543013043019761  # dspacing * 1e-10
    real_axes:
      - xpos: $xa0mm
      - zpos: $za0mm
      - theta: $pita0mm
      - chi: $yawa0mm
    energy_axis:           # optional
      - name: ene_ana_0_mm   # optional
    bragg_axis:            # optional
      - name: bragg_ana_0_mm  # optional
        
  - name: ana_1_mm
    plugin: generic
    module: spectrometers.mmspectro
    class: MMAnalyser
    miscut: 0
    radius: 500
    ypos: -120
    offset_on_detector: 0 
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    xtals:
      - xtal: Si444
        dspacing: 0.783882894235
      - xtal: Si844
        dspacing: 0.5543013043019761  # dspacing * 1e-10
      - xtal: Si555
        dspacing: 0.627106315
    real_axes:
      - xpos: $xa1mm
      - zpos: $za1mm
      - theta: $pita1mm
      - chi: $yawa1mm
    energy_axis:           # optional
      - name: ene_ana_1_mm    # optional
    bragg_axis:            # optional
      - name: bragg_ana_1_mm  # optional

  - name: ana_2_mm
    plugin: generic
    module: spectrometers.mmspectro
    class: MMAnalyser
    miscut: 0
    radius: 500
    ypos: 120
    offset_on_detector: 0 
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    xtals:
      - xtal: Si444
        dspacing: 0.783882894235
      - xtal: Si844
        dspacing: 0.5543013043019761  # dspacing * 1e-10
    real_axes:
      - xpos: $xa2mm
      - zpos: $za2mm
      - theta: $pita2mm
      - chi: $yawa2mm
    energy_axis:           # optional
      - name: ene_ana_2_mm    # optional
    bragg_axis:            # optional
      - name: bragg_ana_2_mm  # optional


detectors:
  - name: det_0_mm
    plugin: generic
    module: spectrometers.mmspectro
    class: MMDetector
    target: $ana_0_mm
    width: 400
    height: 200
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    real_axes:
      - dx: $xdetmm
      - dz: $zdetmm
      - dth: $pitdetmm
      - dlong: $dlong
      - azlong: $azlong
    energy_axis:           # optional
      - name: ene_det_0_mm    # optional
    bragg_axis:            # optional
      - name: bragg_det_0_mm  # optional


spectrometers:
  - name: spectro_mm
    plugin: generic
    module: spectrometers.mmspectro
    class: MMSpectrometer
    bragg_low_limit: 25   # optional (default=15)
    bragg_high_limit: 88  # optional (default=89)
    detector: $det_0_mm
    analysers:
      - name: $ana_0_mm
      - name: $ana_1_mm
      - name: $ana_2_mm
    energy_axis:
      - name: ene_spectro_mm    # optional
    bragg_axis:
      - name: bragg_spectro_mm  # optional
    

```

