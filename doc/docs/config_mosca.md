# MOSCA MCA Controller


!!!example "YAML configuration for a MOSCA MCA controller"

    ```yaml
    - name: mca_sim
      class: McaSimulator
      module: mosca.simulator
      plugin: generic

      tango_name: id00/mosca/simulator

      roi_correction_formula: roi / (1-trigger_livetime/realtime) / iodet  #(optional)
    
      external_counters:   #(optional)
        iodet: $diode1     #(optional)
 
      # Valid variables for the 'roi_correction_formula' are:
      #  - 'roi'
      #  - a stat label (ex: 'icr', 'ocr', 'deadtime', ...)
      #  - an external_counter tag as declared below 'external_counters' (ex: 'iodet')   

    ```