# Image

A plot is provided to display a specific image.

It provides a dedicated view to display a single image, with tools
to provides vertical and horizontal histograms.

![Screenshot](../img/custom-plot-image-view.png)

## Example

```python
import numpy

# Create the plot
f = flint()
p = f.get_plot("image", name="My plot")

# Create the data and setup the plot
y, x = numpy.ogrid[:10, :10]
image = numpy.sin(numpy.sqrt((x-5)**2 + (y-5)**2))
p.set_data(image)

# The colormap can be updated
p.set_colormap(lut="red")

# The direction of the y-axis can be specified
p.yaxis_direction = "down"
```

## APIs

### Side histogram

In case the side histograms are not needed and take space for nothing, this can
be hidden the following way.

```
p.side_histogram_displayed = False
```
