# Other live display

## Count widget

For `ct` scans, the count widget will be automatically displayed.

![Flint screenshot](../img/flint-count-widget.png)

## Positioners widget

Usually a scan contains information on the positioners, which is the location
of all the motors before and after the scan.

The positioners widget **can** be displayed. The 📜 `Windows` menu provides an
entry to show/hide this widget.

![Flint screenshot](../img/flint-positioners-widget.png)
