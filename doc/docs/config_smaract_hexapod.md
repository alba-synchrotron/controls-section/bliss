# Smaract hexapod motor controller

### Supported features

Encoder | Shutter | Trajectories
------- | ------- | ------------
NO	    |    NO   |    NO

## Configuration example

```yaml
plugin: emotion
class: SmaractHexapod
module: smaract_hexapod
tcp:
  url: smhexid31:2000
pivot_relative: False
pivot_point: [0, 0, 0]
axes:
    - name: smx
      channel: 0
      unit: um
      steps_per_unit: 1
      tolerance: 0.05
    - name: smy
      unit: um
      channel: 1
      steps_per_unit: 1
      tolerance: 0.05
    - name: smz
      unit: um
      channel: 2
      steps_per_unit: 1
      tolerance: 0.05
    - name: smrx
      unit: um
      channel: 3
      steps_per_unit: 1
      tolerance: 1e-4
    - name: smry
      unit: um
      channel: 4
      steps_per_unit: 1
      tolerance: 1e-4
    - name: smrz
      unit: um
      channel: 5
      steps_per_unit: 1
      tolerance: 1e-4
```

