# I5 Device Server #

## Launching the device server ##

Let's make it easy and see how to launch the device server:

```
(base) user@beamline:~/bliss$ conda activate bliss_dev
(bliss) user@beamline:~/bliss$ export TANGO_HOST=localhost:20000
(bliss) user@beamline:~/bliss$ python tango/servers/i5_ds.py -?
usage :  i5_ds instance_name [-v[trace level]] [-nodb [-dlist <device name list>]]
Instance name defined in database for server i5_ds :
        id00
(bliss) user@beamline:~/bliss$ python tango/servers/i5_ds.py id00
Ready to accept request
```

NB: not yet distributed as a separate conda package.


## Configuration ##

### Jive ###

create server
```
Server: i5_ds/id00
Class: PreciamolenI5
Device: id00/i5/0
properties:
- beacon_name: i5
```

### Not yet With Beacon ###




