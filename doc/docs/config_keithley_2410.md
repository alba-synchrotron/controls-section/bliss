# Keithley 2700 configuration

Keithley 2410 from the 2400 series is roughtly like the 2000 multimeter with a sourcemeter (V/I) source and sink (4-quadrant).


##Compliance limits
Compliance considerations are specific to the 2400 series.

When sourcing voltage, you can set the SourceMeter to limit current from 1nA to 1.05A.
Conversely, when sourcing current, you can set the SourceMeter to limit voltage from 200 μ V to
1.1kV. The SourceMeter output will not exceed the programmed compliance limit.
Types of compliance
There are two types of compliance that can occur: “real” and “range.” Depending upon which
value is lower, the output will clamp at either the displayed compliance setting (“real”) or at the
maximum measurement range reading (“range”).
The “real” compliance condition can occur when the compliance setting is less than the high-
est possible reading of the measurement range. When in compliance, the source output clamps
at the displayed compliance value. For example, if the compliance voltage is set to 1V and the
measurement range is 2V, the output voltage will clamp (limit) at 1V.
“Range” compliance can occur when the compliance setting is higher than the possible read-
ing of the selected measurement range. When in compliance, the source output clamps at the
maximum measurement range reading (not the compliance value). For example, if the compli-
ance voltage is set to 1V and the measurement range is 200mV, the output voltage will clamp
(limit) at 210mV.

##Determining compliance limit
The relationships to determine which compliance is in effect are summarized as follows.
They assume the measurement function is the same as the compliance function.

- Compliance Setting < Measurement Range = Real Compliance
- Measurement Range < Compliance Setting = Range Compliance
  
You can determine the compliance that is in effect by comparing the displayed compliance
setting to the present measurement range. If the compliance setting is lower than the maximum possible reading on the present measurement range, the compliance setting is the compliance limit. If the compliance setting is higher than the measurement range, the maximum reading on that measurement range is the compliance limit.

**Please use a range measurement lower than the source compliant value (i.e. *yaml* following example).**
If not, the set of the range measurement will be ignored and a discrepancy of reading of the range value will occur.


## Source voltage usage
The source voltage can be defined as a soft axis in the setup of the bliss session as following:
```
k2410_motor = SoftAxis('k2410_motor',keith2410 ,move='source_value', position='source_value')
```

## Yaml sample configuration
```YAML
  - model: 2410
    gpib:
      url: enet://gpibbcu2.esrf.fr      
      pad: 24
    sensors:
      - name: keith2410      
        address: 1
        meas_func: CURR # or VOLT
        auto_range: False
        range: 1e-3
        source_func: VOLT # VOLT or CURR
        source_cmpl: 2e-3 # compliance (current or volt) value  
        source_range: 2  
        source_value: 0
        source_max_value: 2
```
