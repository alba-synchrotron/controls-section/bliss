# 2D detectors (with Lima) #

2D detectors (CCD, pixel, ...) supported by Lima can be controlled in BLISS via
the BLISS `Lima` class.

This class uses the usual Lima Tango device server to access the detector.


## Configuration ##

```
beamline_configuration/
├── eh2
│   ├── cameras
│   │   ├── andor1.yml
│   │   ├── andor2.yml
│   │   ├── cdte22.yml
│   │   ├── eiger1.yml
│   │   └── mpx22.yml
```

Example of YAML configuration file:

```yaml
name: mpx22
class: Lima
tango_url: id42/limaccd/mpx_22
prepare_timeout: 120
```
### Additional parameters

Each individual Lima sub-device can be configured directly from the YML file.
The section names are the sub-devices names, item keys are the attribute names
just like specified in the Tango device:

#### Saving example

```yaml
saving:
    mode: ONE_FILE_PER_FRAME
    file_format: EDF
    _max_writing_tasks: 4
```
#### Processing example

```yaml
processing:
    runlevel_roicounter: 9
```

#### Image example

```yaml
image:
    rotation: 90
```

#### Accumulation example

```yaml
accumulation:
    max_expo_time: 2
```

### Optional parameters for experts

There are 3 extra parameters reserved for the experts !
```yaml
prepare_timeout: 120
start_waittime: 0.05
disable_bpm: false
```

With **prepare_timeout** (default is 3 seconds) you can add an addition preparation time for cameras which can take longer to prepare the acquisition for instance to allocate the requested huge memory for a long scan.

With **start_waittime** (default is 5ms) you will add an extra waiting time before Bliss fire (trig) the camera. Than can be useful if a detector is not fully started and ready to listen for the trigger just after the call to the startAcq() .

With **disable_bpm** set to false (default is true) bliss will never activate the Bpm tango device. The Bpm calculation on an image takes about 100ms and this can become a critical when you want to run a camera at fast speed.

### Directories mapping

Directories mapping allows to replace filesystem paths used by BLISS, in favour
of different mappings when Lima is running under Windows for example or on a
computer with different mounting points.

Paths specified in `SCAN_SAVING` will be transformed, in order to pass the good
path arguments to Lima:

```yaml
directories_mapping:
    default:
        - path: /data/visitor
          replace-with: Z:/visitor
        - path: /data/id15
          replace-with: Z:/id15
```

### Configuring scan default chain with Lima devices

Default chain configuration can have an `acquisition_settings` section, which
allows to set attributes on the main Lima Tango device at scan preparation
time. The item key names correspond to the Tango device attributes.

#### Setting 2D detectors in external trigger mode only

```yaml
- name: default_chain_eh2
  plugin: default
  chain_config:
    - device: $mpx22
      acquisition_settings:
        acq_trigger_mode: EXTERNAL_TRIGGER_MULTI
      master: $p201_20
```
## Miscellaneous

### Taking dark (background) images

The `.take_dark()` method of Lima controller objects can be used to take background images,
that will be substracted to subsequent frames during acquisition, in order to reduce
noise level.

`.take_dark` accepts the following arguments:

- exposure time (in s.)
- optional arguments:
    - shutter, `None` by default
    - save [bool], `True` by default

When `save` is True, current SCAN_SAVING parameters are used to save the background image,
and to install it as Lima reference for background substraction. Currently background
images are always saved in EDF format. A `background_` prefix is added to the image prefix.
If `save` is False, an image is taken but it only stays in memory ; Lima will use this
internal image to do background substraction.

The shutter argument allows to pass a BLISS `Shutter` object, that will be **closed**
while taking the background image. If the shutter was open when passed to the function,
it will be re-opened at the end of the `take_dark` procedure.

### Example to add a pseudo axis to drive the Maxipix's threshold

```python
if hasattr(setup_globals,'mpx22'):
    mpxthl = SoftAxis('mpxthl', mpx22.camera,
                       position='energy_threshold',
                       move='energy_threshold')
```
