
# Bipod Calculational Motor

Two-legs table calculational motor transforming two translations into a rotation
and a translation.

One or more rotation center can be defined.

## Conventions for bipod calculations

A direct coordiante system (x,y,z) is used:

                    ^
                    | Z axis
                    |
        X axis      |
          <----------
                   /
                  /
                 / Y axis
                /
                v

The rotations are named **rotx**, **roty** and **rotz** around respectively the
**X**, **Y** and **Z** axes with counterclockwise positive value.

Parameters relative to legs are following the same convention:

* coordinates of "lega" are named: "ax", "ay"
* coordinates of "legb" are named: "bx", "by"
* ...

The calculation takes into account the "unit" defined for each real or
calculated motors. If the "unit" field is missing, the universal units are used:
meters (m) for distance, radians (rad) for angles.


* the 2 legs are tagged **lega** and **legb**
* the height of the table is tagged *trans*
* X-axis is the axis passing through lega and legb, positive direction: from lega to legb
* The rotation is around Y-axis, perpendicular to the [lega-legb] axis (X-axis)
* the rotation is tagged **ry**
* *C* is the center of rotation and its height is represented by the position of the **rz** axis
* *C* is the origin of the X/Y/Z coordinates system
* **ax** is the coordinate of **lega** in the X/Y/Z coordinates system
* **bx** is the coordinate of **legb** in the X/Y/Z coordinates system

## Example of yml file with 2 centers.


!!!note
    At least one center is mandatory.

* center_id = **inside**:
   - C is inside of the legs
   - `ax` is a negative value in meter
   - `bx` is a positive value in meter

```
                   Z-axis
                        ^
                        |
                  ______|______________
                 /      |             /
                /       |...ax..>|   /
     X axis    /        |        |  /     X axis
     <--------/-.-------C--------.-/----<------
             /  |<.bx../         |/
            /   |     /          |
           /____|____/__________/|
                |   /            |
                |  v Y-axis      |
                |                |
              legb             lega
```

* center_id = **outside**
 - C is outside of the legs
 - `ax` is a positive value in meter
 - `bx` is a positive value in meter

```

                                        Z-axis
                                          ^
                                          |
                  _____________________   |
                 /                    /   |
                /<.......bx...............|
     X axis    /|                   /     |    X axis
     <--------/-.----------------.-/------C----------
             /  |                |/      /
            /   |                |<.ax../
           /____|_______________/|     /
                |                |    /
                |                |   v Y axis
                |                |
              legb             lega
```


```yaml
- plugin: emotion
  module: bipod
  class: bipod
  name: btable
  centers:
    - center_id: inside
      ax: -0.3
      bx: 0.3
    - center_id: outside
      ax: 0.3
      bx: 0.9
  axes:
    - name: $sim_btz1
      tags: real lega
    - name: $sim_btz2
      tags: real legb
    - name: sim_btz
      tags: tz
      unit: mm
    - name: sim_btr
      tags: ry
      unit: deg
```

!!!warning
    Take care to specify `unit` otherwise meters are used.
