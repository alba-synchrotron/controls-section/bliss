
# Trans4

4-motors Q-Sys support table for transfocators


Real motor roles:

* `dh`: downstream hall
* `dr`: downstream ring
* `ur`: upstream ring
* `uh`: upstream hall

Calc. motor roles:

* `ty`: alias for calculated y translation
* `tz`: alias for calculated z translation
* `thetay`: alias for calculated y rotation
* `thetaz`: alias for calculated z rotation

Configuration parameters:

* `d1`: half distance in x direction (in mm) between the two moving blocks.
* `d3`: distance in z direction (in mm) between pivot plane and the top plate.

POI : point of interest (rotation center)

!!!note
    We are using orientation conventions used on Q-Sys document.

```
    <---X---Z
            |
            |
            Y
            |
            |
            V

       M2=DR                M3=UR
                POI
                   X           <------------------------- BEAM

       M1=DH       |        M4=UH
        |          |
        |<---D1--->|
```


## Config example
```yaml
controller:
  class: trans4
  name: transf_mot_oh2
  d1: 230
  d2: 120
  axes:
  - name: $tf2m1
    tags: real dh
  - name: $tf2m2
    tags: real dr
  - name: $tf2m3
    tags: real ur
  - name: $tf2m4
    tags: real uh
  - name: t2ry
    tags: thetay
    unit: deg
  - name: t2rz
    tags: thetaz
    unit: deg
  - name: t2y
    tags: ty
    unit: mm
  - name: t2z
    tags: tz
    unit: mm
```
