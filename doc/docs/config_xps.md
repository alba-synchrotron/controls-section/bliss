# Newport XPS motor controller

Bliss controller for Newport XPS-Q motor controller.

## Supported features

Encoder | Shutter | Trajectories
------- | ------- | ------------
No      | No      | No


## Configuration

!!!note
    All axes of each group must be declared in config.

!!!note
    The address is the index of the axis in its group (starting at 1).


Example:
```YAML
controller:
  class: NewportXPS
  module: newport
  name: xps-q
  description: Newport-Q
  tcp: 160.103.146.95:5001
  axes:
    -
      name: omega
      group: M1
      address: 1             # first address should be 1
      velocity: 70.0
      acceleration: 320.0
      minJerkTime: 0.005
      maxJerkTime: 0.05
      steps_per_unit: 1
      backlash: 0.0
      low_limit: 0
      high_limit: 360
      offset: 0.0
      unit: deg
      autoHome: True
      user_tag: Omega
      gpio_conn: GPIO3        # GPIO connector for constant velocity pulse
      motion_hooks:
        - $newport_hook       # execute post motion
```
