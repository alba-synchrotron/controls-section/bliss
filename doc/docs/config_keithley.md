# Keithley configuration

!!! warning
    Changing the `range` changes the *analog out range* ->
    take care to not run a scan in autorange.

!!! note
    Changing the `rate` aka `nplc` does not affect the analog out.

## Configuration

Model 6485 can be configured to terminate each message that it
transmits with any of the following combinations of `<CR>` and `<LF>`:

* `LF` line feed `\n`
* `CR` carriage return `\r`
* `LFCR` line feed, carriage return `\n\r` (to avoid...)
* `CRLF` carriage return, line feed `\r\n`

To check and change the hardware configuration of the Keithley, follow the sequence:

* press the `config local` button,
* press the `COMM` button. You should see `RS232` or `GPIB`. ( OR
  SHIFT + ZCHK/RS-232 button for the 6514 model )
* with the help of the `Δ` / `∇` buttons (right most on the front
  panel) you could go through all the parameters and change them if
  needed.
* to end the procedure press `ENTER` and then `EXIT`.

The instrument keeps the setup when switched off.

### Minimum


```yaml
plugin: keithley
keithleys:
  - model: 2000
    gpib:
      url: enet://gpibid11c.esrf.fr
      pad: 22
    sensors:
      - name: pico6
        meas_func: VOLT
        address: 1

  - model: 2000
    gpib:
      url: enet://gpibid11c.esrf.fr
      pad: 12
    sensors:
      - name: sample_temp #Temperature Sensor
        meas_func: TEMP
        address: 1
	nplc: 1 #(optional) 1 is the default for temperature
	measurement_resolution: 6 #number of digits (optional) (default: 6)
	thermocouple_type: J # Thermocouple type (J, K, or T) (optional) (default: J)

  - model: 6485
    gpib:
      url: enet://gpibid11c.esrf.fr
      pad: 23
    sensors:
      - name: pico7
        address: 1
```

### Full

```yaml
plugin: keithley
keithleys:
  - model: 6485
    auto_zero: False
    display: False
    gpib:
      url: enet://gpibid11c.esrf.fr
      pad: 22
    sensors:
      - name: pico6
        address: 1
        nplc: 0.1
        auto_range: False
	range: 2e-8
        zero_check: False
        zero_correct: False
```


* plugin name (mandatory: `keithley`)
* controller name (mandatory). Some controller settings are needed. To hook the
   settings to the controller we use the controller name. That is why it is
   mandatory
* controller **model** (optional. default: discover by asking instrument `*IDN`)
  For "old" models talking DDC protocol, **485/486/487**, one must set the model.
* **auto_zero** enabled (optional, default: `False`)
* **display** enabled (optional, default: `True`)
* **zero_check** enabled (optional, default: False). Only for 6485 and 6487!
* **zero_correct** enabled (optional, default: False). Only for 6485 and 6487!
* controller URL (mandatory, valid: gpib, tcp, serial)
    - gpib (mandatory: *url* and *pad*). See `Gpib` for
      list of options
    - serial (mandatory: *port*). See `Serial` for list
      of options
    - tcp (mandatory: *url*). See `Tcp` for list of options
* list of sensors (mandatory)
* sensor name (mandatory)
* sensor address (mandatory). Valid values:
    - model 6482: 1, 2
    - model 6485: 1
    - model 2000: 1
    - model 2700: 1
    - model 485: 1
    - model 486: 1
    - model 487: 1
* sensor DC current **nplc** (optional, default: 0.1), not for 485/486/487 modeles.
* sensor DC current **auto_range** (optional, default: False)
* sensor DC current **range**, only mandatory if the auto_range is False, for supported values call the *possible_ranges* property.


## parameters' persistance

Some parameters (described below) are stored as settings. This means that the
static configuration described above serves as a *default configuration*.
The first time ever the system is brought to life it will read this
configuration and apply it to the settings. From now on, the keithley object
will rely on its settings. This is the same principle as it is applied on the
bliss axis velocity for example.

The following controller parameters are stored as settings:

* `auto_zero`
* `display`
* `zero_check` only for 6485 and 6487
* `zero_correct` only for 6485 and 6487

The following sensor parameters are stored as settings:

* `nplc`
* `auto_range`
* `range`



## Usage

### range

Keithley controllers have a set of measurement ranges. The range has to be
adapted to the value to measure in order to improve measurement accuracy.

Valid ranges depend on controller model.

Example for a Keithley 6485:
```python
DEMO [13]: nkiodet.range_available
 Out [13]: [2e-09, 2e-08, 2e-07, 2e-06, 2e-05, 0.0002, 0.002, 0.02]
```
To change the range of a Keithley, the `range` property has to be set **to the
value to measure**. The BLISS controller will then choose the most adapted range
(i.e.: the upper valid range)

Example:
```python
DEMO [14]: nkiodet.range=0.003
nkiodet range set to 0.02
```

!!!warning

    This behavior can produce strange situations:
    ```python
    DEMO [16]: nkiodet.range=0.0021
    DEMO [17]: print(nkiodet.range)
    0.021         # one zero seems lost but no, this range is requiered to measure 0.0021 value.

    DEMO [19]: nkiodet.range=0.000000000000000000000021
    nkiodet range set to 2e-09        # lower range is selected

    DEMO [20]: nkiodet.range=0.1
    nkiodet range set to 0.02         # higher range is selected
```
