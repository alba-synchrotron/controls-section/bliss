blissterm is the web shell for BLISS

![blissterm](img/blissterm.png)

blissterm provides a web based terminal for BLISS, a monitor panel for essential status information (i.e. frontend, shutters, etc), and regions that can be adorned with UI widgets for monitoring and interacting with hardware components via a simple UI. It also supports secondary layouts that can be used as separate interfaces to monitor many hardware objects. blissterm is built using shared UI components from [daiquiri-ui](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/) to provide a consistent user experience beteween applications. It supports multiple sessions via a single process.

## Configuration

blissterm is configured via beacon using the `blissterm` folder i.e. `beacon://blissterm/config.yml`. The root folder should contain an `__init__.yml` with `bliss_ignored: true`:

```yaml title="blissterm/__init__.yml"
bliss_ignored: true
```

The configuration file supports the following attributes:

```yaml title="blissterm/config.yml"
cors: true
iosecret: 12345asdasdas
server_args:
  debug: false

monitor:
  - comparator: in
    comparison:
      - OPEN
      - RUNNING
    name: Front End
    overlay: fe.status
    value: fe.state

  - name: Omega
    value: omega.position

  - comparator: ==
    comparison: OPEN
    name: Absorber
    overlay: abs1.status
    value: abs1.state

topItems:
  - type: hardwaregroup
    ids:
      - id: omega
      - id: beamstop

sideItems:
  - type: hardwaregroup
    even: true
    ids:
      - id: robz
```

## Layouts

Custom layouts can be defined in `beacon://blissterm/layouts.yml`. These use the [daiquiri-ui](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/) &raquo; [Layout Manager](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/layout-manager/) to render a user interface from a standardised YAML layout language. See the [Layout Manager](https://ui.gitlab-pages.esrf.fr/daiquiri-ui/layout-manager/) documentation for more information about the available wrapper components and how to use them.

!!! info
    blissterm currently supports only the `hardwaregroup` component type.

For example:

```yaml title="blissterm/layouts.yml"
layouts:
  - name: Monitor
    acronym: monitor
    icon: fa-cog
    children:
      - type: row
        children:
          - type: col
            children:
              - type: panel
                title: Rotation
                children:
                - type: hardwaregroup
                  ids:
                    - id: omega
                    - id: robz
                      steps: [0.1, 0.5, 1]
                      step: 0.5

          - type: col
            children:
              - type: panel
                title: Detector
                children:
                  - type: hardwaregroup
                    ids:
                      - id: beamstop

      - type: row
        children:
          - type: col
            children:
              - type: hardwaregroup
                ids:
                  - id: samx
                  - id: samy
                  - id: samz
```

## REST API

blissterm exposes a REST API that is used to power the web shell and interface, as well as providing remote access to BLISS. The API documentation is available here: [Rest API documentation](blissterm_api/index.html)
