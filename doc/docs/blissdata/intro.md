# Blissdata

Blissdata is a library to store and access scan data at any moment of their life cycle.
It acts as a streaming engine by relying on a Redis database, but it may also redirect queries to HDF5 files depending on the timing.

Moreover, a HDF5 wrapper is currently in development to mimic [h5py](https://docs.h5py.org/en/stable/index.html) API to run scans and to make already written scripts relying on files reusable.
