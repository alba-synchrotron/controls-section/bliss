# Installation

Blissdata source code is available on [Gitlab](https://gitlab.esrf.fr/bliss/bliss/-/tree/master/blissdata?ref_type=heads)

Installation from the repository:
```shell
git clone https://gitlab.esrf.fr/bliss/bliss.git
cd bliss/blissdata
pip install -e .
```

Packages are available on [PyPI](https://pypi.org/project/blissdata/) and [Conda](https://anaconda.org/esrf-bcu/blissdata)

!!! Important
    Blissdata 1.0 package is planned for December 2023, use Gitlab repository for cutting edge version.

## Redis setup

Blissdata needs a running Redis database with:

- Redis server 7
- ReJson extension
- RediSearch extension

The easiest way is to use Redis Stack distribution which include Redis Server and the most common extensions.
```
docker run -p 6379:6379 redis/redis-stack:7.2.0-v5
```

### Redis server configuration

The following options must be set on Redis before running blissdata:

```shell
maxmemory 4gb # or the amount you want
maxmemory-policy noeviction # memory management is done by memory tracker
save "" # disable file backup
```

This can be done either by editing `redis.conf` file, or from the command line.

## Blissdata index initialization

Blissdata needs to configure the database once at redis server start. This is mostly for RediSearch to know what to index.
```python
from blissdata.redis_engine.store import DataStore

_ = DataStore("redis://localhost:6379", init_db=True)
```

## Memory tracker

Blissdata relies on a memory tracker to clear space when necessary.
This is a daemon process that monitor the database.

For default configuration, run:
```
memory_tracker --redis-url redis://localhost:6379
2023-10-31 15:23:16,158 INFO: INIT | Redis max. memory: 1GB
2023-10-31 15:23:16,158 INFO: INIT | Mem. usage to trigger cleanup: 80% (819.2MB)
2023-10-31 15:23:16,158 INFO: INIT | Protected history length: 180 seconds
2023-10-31 15:23:16,159 INFO: Memory usage: 1.857MB

```
