# Andeen-Hagerling 2550A

## Description

This module allows to configure AH2550A capacitance bridge as a sampling counter.
3 counters can be configured corresponding to 3 measurement possibilities:
- capacitance
- loss
- voltage


## Configuration

### YAML configuration file example


```YAML
- class: AH2550A
  module: andeen_hagerling_2550a
  name: ah
  gpib:
    url: enet://gpibbcu2
    pad: 28
  counters:
    - counter_name: C
      measure: capacitance
    - counter_name: L
      measure: loss
    - counter_name: V
      measure: voltage
```

## References

* Manufacturer documentation: http://www.andeen-hagerling.com/ah2550a.htm