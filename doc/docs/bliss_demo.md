## Starting BLISS demo processes

BLISS demonstration processes can be started with the commands that are available
in the demo environments (see [installation instructions](installation.md)). We will
assume here that these environments are all conda environments with the names
`bliss_env`, `lima_env` and `oda_env` (the later is optional).

Start Beacon and the simulated device servers

```bash
conda activate bliss_env
bliss-demo-servers --lima-environment=lima_env [--oda-environment=oda_env]
```

When `--oda-environment` is specified, visit http://localhost:5555 to
monitor the data processing.

Start the BLISS shell with the demo session

```bash
conda activate bliss_env
bliss-demo-session
```
