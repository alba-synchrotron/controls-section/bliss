# White Beam Attenuator Object

The WhiteBeamAttenuator class handles the ESRF White Beam Attenuator. The ESRF White Beam Attenuators are Icepap driven coper poles with several holes/filters.

Each attenuator pole has positive/negative limit switch and a switch active for each filter. The configuration procedure tries to find the switches and set the position of each filter at the middle of the switch position.

Two models currently exist at ESRF:

* **WhiteBeamAttenuator** : position switches cabled to icepap home signal
* **WhiteBeamAttenuatorWago** : position switches cabled to a wago box

The configuration is defined in .yml file, but can also be changed/removed
interactively. All the changes are saved in the corresponding .yml file.

A hook to the Frontend object is optional: if given no movement of the attenuator
will be possible when the frontend (a TangoShutter) is not on a state considered safe.


## WhiteBeamAttenuator YAML configuration example 

This is for attenuators which have position switches cabled to icepap home signal.

```yaml
class: WhiteBeamAttenuator
plugin: bliss
name: wba
attenuators:
    - attenuator: $wba_Al
    - attenuator: $wba_Mo
    - attenuator: $wba_Cu
frontend: $frontend
```

!!! warning "attenuator"
Each *attenuator* is configured as a [MultiplePosition object](config_mp.md).


## WhiteBeamAttenuatorWago YAML configuration example 

This is for attenuators which have position switches cabled to a wago box. Each position switch is cabled to an individual input on a wago module.

```yaml
class: WhiteBeamAttenuatorWago
name: att
wago: $wcid03b
attenuators:
- attenuator: $att1
  wagokeys:
    FILTER3: att1_pos4
    FILTER2: att1_pos3
    FILTER1: att1_pos2
    EMPTY: att1_pos1
- attenuator: $att2
  wagokeys:
    FILTER3: att2_pos4
    FILTER2: att2_pos3
    FILTER1: att2_pos2
    EMPTY: att2_pos1
- attenuator: $att3
  wagokeys:
    FILTER3: att3_pos4
    FILTER2: att3_pos3
    FILTER1: att3_pos2
    EMPTY: att3_pos1
```

Each *attenuator* is configured as a [MultiplePosition object](config_mp.md).

The wagokeys section map the name of the multiple position label with an input name of a wago module.

## Configuration procedure

For each individual attenuator, a configuration procedure will help finding positions of all switches.

This procedure will first go to the motor negative limit. Then it will move to positive limit, finding position and length of switch along the way. Output values can then be used to configure the attenuator [MultiplePosition object](config_mp.md).

Example with a WhiteBeamAttenuatorWago:

```
att.find_configuration("att1")
 - Search negative limit switch
 - Reset motor position to 0
 - Check no switch are active
 - Search positive limit switch
 ...
 - Motor stopped

Filter switch positions found:
 - FILTER3 : from 4.168 to 11.827 ==> 7.998 +/- 3.830
 - FILTER2 : from 38.960 to 46.869 ==> 42.914 +/- 3.955
 - FILTER1 : from 73.901 to 81.911 ==> 77.906 +/- 4.005
 - EMPTY : from 97.779 to 106.340 ==> 102.059 +/- 4.280

```

