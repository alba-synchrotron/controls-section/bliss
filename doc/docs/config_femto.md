# FEMTO Variable Gain Low Noise Current Amplifier

see also: https://www.femto.de/images/pdf-dokumente/de-dlpca-200.pdf

The equipment is a Variable Gain Low Noise Current Amplifier. The above document
is the datasheet, which gives some hints on its use. The only type tested with
the bliss - femto module was the type in the datasheet, a dlpca-200.

The controller is connected to a WAGO controller, which in turn changes the
levels on the controllers analog inputs.


## Control
The controller implements a number of properties.

* The control is implemented through the *WAGO Tango device server*.


## Wagobox modules:

"standard" for a Femto:

* 750-554: 2 Channel 4/20mA  Output for two offsets
* 750-519: 4 Channel Digital Output for 3 gains and one gain
* 750-414: 4 Channel Digital Input  for overload

No other configurations were present in the developement system.

## Configuration

Yaml file(s) in `~blissadm/local/beamline_configuration` allow to set the signal
names used in the wago controller config:

```yaml
plugin: bliss
class: femto
list:
  - name:     femto1
    wago:     idxx/wcidxxc/tg
    type:     dlpca
    offset:   f1oset  # (1)  750-556
    gain:     f1gain  # (3)  750-519
    range:    f1hilo  # (1)  750-519
    overload: f1ovl   # (1)  750-414
    coupling: dc
    # possible other values could be:
    # coupling: f1acdc
    # coupling: dc
    # coupling: ac
    # bandwidth: f1bw10 f1bw1

  - name: femto2
    wago: $wcid99d
    type: dlpca
    gain:     f2gain
    range:    f2hilo
    overload: f2ovl
```




## Usage
The controller implements a short list of properties:

### Properties
#### 'offset'
#### 'coupling'
&ensp; Can be set to 'ac' or 'dc'. It will, however, have no incidence on the usage.
#### 'gain'
#### 'range'
#### 'overload'
#### 'bandwidth'

Should a controller not implement all signal, the property will return *None*.

In practice, with the controller defined as example in the above mentioned
configuration, one would read the *gain* as follows:

```python
BLISS [6]: print("%g\n" % femto1.gain)
1e+10
```
and set it accordingly:
```python
BLISS [7]: femto1.gain = 1e4
BLISS [8]: print("%g\n" % femto1.gain)
10000
```
The *offset* would be set to values between __0__ and __10__.

The *coupling* would be set with the words "__ac__" or "__dc__".

The *gain* is a value allowed by the controller, for the *dlpca* it should be a
value like 10e3 (1e4) to 10e11 (1e12):

<!-- Attention: the table didn't render the last column, so I added an empty one ! -->

| Gain setting for dlpca-200 |          |          |         |         |
|----------------------------|  ---     |  ---     |  ---    |  ---    |
| Low noise    |  High speed |          |          |         |         |
| Pin 14=HIGH  |  Pin 14=LOW |  Pin 12  |  Pin 11  |  Pin 10 |         |
| Gain (V/A)   |  Gain (V/A) |  MSB     |          |  LSB    |         |
|   10e3     |    10e5       |  LOW     |   LOW    |  LOW    |         |
|   10e4     |    10e6       |  LOW     |   LOW    |  HIGH   |         |
|   10e5     |    10e7       |  LOW     |   HIGH   |  LOW    |         |
|   10e6     |    10e8       |  LOW     |   HIGH   |  HIGH   |         |
|   10e7     |    10e9       |  HIGH    |   LOW    |  LOW    |         |
|   10e8     |    10e10      |  HIGH    |   LOW    |  HIGH   |         |
|   10e9     |    10e11      |  HIGH    |   HIGH   |  LOW    |         |


The *range* would be set with the words "__low__" or "__high__".

*Overload* is read only.

*Bandwidth* must be set with one of "__Full__", "__1MHz__" or "__10MHz__".



