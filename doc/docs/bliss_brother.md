
![Screenshot](img/bliss_brother.svg)


BlissBrother is a PyTango device server designed to ease long time archiving of
BLISS settings to be able to retreive a *snapshot* of the Beamline state over
time.

For now, it reads only the position of all axes (including calculated axes)
declared in a BLISS configuration.

According to users needs, archiving frequencies must be tuned to be useful and
to not saturate HDB++.

Using Tango attributes has the advantage to be archived using HDB++ system and tools:

* reliability: centralized mariaDB database
* data retrieving: jhdbviewer + API


In short, BlissBrother server:

* reads settings of user interest (for now: all axes positions) in Redis
  database;
* exposes these values as tango attributes;
* Stores them in HDB++ database according to defined policy (~ when they
  change, but not too often);


# Configuration

## Tango device server configuration

* **`blacklist`**: *`string`* : list of axes separated by comas to not monitor OR path of a file containing blacklisted axes.
* **`delta_time_still_motor`**: *`int`*: time interval (in seconds) to store motion-less motors.
* **`still_time_before_saving`**: *`int`*: time (in seconds) to wait to store motor position if motor has moved.


To store attributes values in HDB++, BlissBrother DS must have access to a
configurator server and the subscriber server.

!!!note
    On a standard ESRF Beamline, the addresses of these servers can be automatically
    retreived from Tango Database HDB++ Properties:

    * `configurator_url`: url of the configurator server
    * `archiver_url`: url of the archiver server
    ![Screenshot](img/tango_db_hdb_properties.jpg)


Example of Tango configuration:
```
delta_time_still_motor: 1200
still_time_before_saving: 15
```
and optionnaly:

* **`configurator_url`**: *`string`*: "//acs:10000/sys/hdb++config/id42"
* **`archiver_url`**: *`string`*: "tango://acs.esrf.fr:10000/sys/hdb-es/id42"
* **`blacklist`**: *`string`*: "~/local/beamline_configuration/bliss_brother_blacklist.txt"
* **`blacklist`**: *`string`*: "sampy, zoom, ffsamy, ffsamz, samrot"


## axes blacklist

Axes names added in blacklist will not be monitored. The blacklist can be a list
of axes in DS configuration or a plain text file defined in DS configuration.

Blacklist file example:
```shell
cat ~/local/beamline_configuration/bliss_brother_blacklist.txt
```
```shell
sampy, zoom,
ffsamy, ffsamz,
samrot
```


# Database reading

Archived data can be retrieved using `jhdbviewer` or via HDB++ API.

![Screenshot](img/bliss_brother_id21.jpg)
