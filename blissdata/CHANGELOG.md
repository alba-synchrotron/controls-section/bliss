# CHANGELOG.md

## 1.0.2
New features:
- An exception is raised when trying to push more than 1MB in scan's JSON (scan's large static artifacts needs a proper mechanism)

Bug fixes:
- Python 3.7 is not supported

## 1.0.1
New features:
- Memory tracker can initialize content of a fresh Redis DB with --init-db option

Bug fixes:
- LimaClient: Fixed invalid calculation of the index for the last file readable image.
- LimaClient: Open EDF files in read-only mode
- LimaClient: Do not use 'readImage(-1)' on lima-tango-server<1.10 (ask for 'last_image_ready' instead)
- Tests: Running blissdata tests no longer dumps .rdb file

## 1.0.0
New features:
- New internal data structure based on RedisJson and RediSearch (see documentation)
- New API with datastore, scans and streams (see documentation)

## 0.3.3

New features:
- Allow creation of ordered hash from a hash setting content in Redis

Bug fixes:
- Fix missing gevent submodules import

## 0.3.2

Bug fixes:
- Python 3.10 and higher are not supported

## 0.3.1

Bug fixes:
- Allow h5py 3.6 and higher

## 0.3.0

New features:
- `h5py`-like API for
    - static HDF5 files (file content does not change while reading)
    - dynamic HDF5 files (file content changes while reading)

## 0.2.0

New features:
- `RemoteNodeWalker` for non-gevent readers (gevent is still used in a subprocess)

## 0.1.2

Bug fixes:
- pytango is optional

## 0.1.1

Bug fixes:
- yaml config URL parsing fails for filename on Windows

## 0.1.0

New features:
- Access to all data produced by Bliss acquisitions (Redis database 1)
- Access to all public device settings (Redis database 0)
- Access to beamline configuration (Beacon server)

The Redis related code has been extracted from the *Bliss* project without
changing the API.
