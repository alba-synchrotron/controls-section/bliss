#%TITLE% nanodac.mac
#
#%DESCRIPTION%
# Macros for the Nanodac temperature Eurotherm controller.
#
#%END%
#
#%SETUP%
#  In order to configure a macro motor:
#  %DL%
#    %DT% 1)
#         You must define in MOTORS configuration
#         %BR%
#         %DL%
#           %DT% DEVICE
#           set to "nanodac"
#           %DT% TYPE
#           set to "Macro Motor"
#           %DT% ADDR
#           set the Tango %B%loop%B% 1 or 2 device name with %B%loop%B% in the name. Example: id09/regulation/loop1
#           %DT% NUM
#           must be set to the number of motors used + 1
#         %XDL%
#    %DT% 2)
#         Per nanodac, you must define a motor with:
#         %BR%
#         %DL%
#           %DT% Controller
#           set to "MAC_MOT"
#           %DT% Unit
#           field must be set to the "MOTOR" entry.
#           %DT% Chan
#           field must be set to 1
#           %DT% Hardware read mode
#           = PR + AL + NQ
#           %DT% Add following custom parameters ('p' to access to menu in spec config)
#           %DT%"tango_wattr" is the writing Tango attribute (usually: "setpoint");
#           %DT%"tango_rattr" is the reading Tango attribute (usually: "target_read");
#           %DT%"dead_band" (0.5 by default) is the dead band for finishing the motion %B%in spec%B%;
#           %DT%"wait_end_movement" = 1 or 0 for having the prompt before the end of the motion.
#
#         %XDL%
#  %XDL%
#
#  In order to configure a macro counter:
#  %DL%
#    %DT% 1)
#         You must have a "SCALERS" defined with:
#         %BR%
#         %DL%
#           %DT% DEVICE
#           set to "nanodac_c"
#           %DT% ADDR
#           set the Tango %B%loop%B% 1 or 2 device name. Example: id09/regulation/loop1 %BR%
#           or set the Tango %B%input%B% 3 or 4 device name. Example: id09/regulation/input3
#           %DT% NUM
#           must be set also to the number of couters used.
#           %DT% TYPE
#           set to "Macro Counter"
#         %XDL%
#    %DT% 2)
#         Per nanodac, you must define a counter with:
#         %BR%
#         %DL%
#           %DT% Device
#           set to "MAC_CNT"
#           %DT% Mnemonic
#           must be the motor mnemonique plus "_c"
#           %DT% Unit
#           must be set to the "SCALER" entry
#           %DT% Chan field must be set to the number of counters
#           %DT% Add a parameter "tango_rattr" with tango attribute name as value:
#           %DT%  "target_read" for process value (c1_pv);
#           %DT%  "working_setpoint" for the working setpoint (ramp1_workingsp):   ;
#           %DT% setpoint corresponding to the reading attribute corresponding to the target setpoint ;      ???
#           %DT% ramprate corresponding to the reading attribute corresponding to the output percent value.  ???
#           %DT% outvalue corresponding to the reading attribute corresponding to the output percent value.  ???
#         %XDL%
#
#  %XDL%
# %BR%
#  read/write  |       nanodac        |         tango           | spec suggested counter names%BR%
#  ----------------------------------------------------------------------------%BR%
#      write       |     ramp1_targetsp   |     setpoint (loop1)    |    setpoint (sp1)  %BR%
#      read        |    ramp1_workingsp   | working_setpoint(loop1) |   wsetpoint (nwsp1)  %BR%
#      read        |       c1_pv          |   target_read (input1)  |   processvalue (npv1)%BR%
#      read        |    ramp1_out         |   set_value (output1)   |   outvalue (nout1)   %BR%
#      write       |     ramp2_targetsp   |     setpoint (loop2)    |    setpoint2 (nsp2)  %BR%
#      read        |    ramp2_workingsp   | working_setpoint(loop2) |   wsetpoint2 (nwsp2)  %BR%
#      read        |       c2_pv          |   target_read (input2)  |   processvalue2 (npv2)%BR%
#      read        |    ramp2_out         |   set_value (output2)   |   outvalue2 (nout2)   %BR%
#
#      read        |       c3_pv          |   target_read (input3)  |   processvalue (npv3)%BR%
#      read        |       c4_pv          |   target_read (input4)  |   processvalue (npv4)%BR%
#
#%END%


#%UU%
#%MDESC%
# MACRO MOTOR:
# Display loop information.
# nanodac_menu  <motor_mnemonique>
def nanodac_menu '{
   if ($# != 1) {
       printf("Usage $0 or\n")
       printf(" $0 <motor_mnemonique>\n")
       exit
    }

    mot_mne = "$1"
    mot_num = motor_num(mot_mne)
    nanodac_device_loop = motor_par(mot_num,"address")

    local option
    option = 1
    while((option != "quit") && (option != "q")){
        tty_cntl("cl")
        print("Nanodac Status:")
        print _nanodac_tango_attr(nanodac_device_loop, "r", "target_info")
        print("\n")
        print("=== Settings ===")
        print("s) Change target set point\n")
        print("r) Change ramp rate (d/mn)\n")
        print("p) Change the proportional band\n")
        print("i) Change the integral time\n")
        print("d) Change the derivated time\n")

        option= getval("\n\n\tSwitch outputs  ---> ", "[q]uit or [Enter]refresh")

        if((option != "quit") && (option != "q")){
            if(option == "r") {
                option = getval("\nEnter the ramp rate (deg/min):", _nanodac_tango_attr(nanodac_device_loop, "r", "ramprate"))
                _nanodac_tango_attr(nanodac_device_loop, "w", "ramprate", option)
            }
            else if(option == "s"){
                option = getval("\nEnter the target set point:",_nanodac_tango_attr(nanodac_device_loop, "r", "setpoint"))
                _nanodac_tango_attr(nanodac_device_loop, "w", "setpoint", option)
            }
            else if(option == "p"){
                option = getval("\nEnter the proportional band:",_nanodac_tango_attr(nanodac_device_loop, "r", "kp"))
                _nanodac_tango_attr(nanodac_device_loop, "w", "kp", option)
            }
            else if(option == "i"){
                option = getval("\nEnter the integraltime:",_nanodac_tango_attr(nanodac_device_loop, "r", "ki"))
                _nanodac_tango_attr(nanodac_device_loop, "w", "ki", option)
            }
            else if(option == "d"){
                option = getval("\nEnter the derivativetime:",_nanodac_tango_attr(nanodac_device_loop, "r", "kd"))
                _nanodac_tango_attr(nanodac_device_loop, "w", "kd", option)
            }
        }
    }
}'


#%UU%
#%MDESC%
# MACRO MOTOR:
# Stop the ramp and hold the value.
# nanodac_ramp_stop  <motor_mnemonique>
def nanodac_ramp_stop '{

    if ($# != 1) {
       printf("Usage $0 or\n")
       printf(" $0 <motor_mnemonique>\n")
       exit
    }

    mot_mne = "$1"
    mot_num = motor_num(mot_mne)
    nanodac_device_loop = motor_par(mot_num,"address")
    TANGO_ERR = -1
    if(tango_io(nanodac_device_loop,"stop") != "0"){
        tty_cntl("md")
        print "\ntango_io ERROR " nanodac_device_loop  " stop\n"
	printf ("TANGO_ERR_MSG = \"%s\" \n\n", TANGO_ERR)
	print TANGO_ERR_STACK
	tty_cntl("me")
	return ".error."
    }
}'


#%UU%
#%MDESC%
# MACRO MOTOR:
# to change the ramp rate
#  nanodac_change_P <motor_mnemonique> [<ramp_rate_value>]
def nanodac_change_Ramp ' {
    local  nanodac_device_loop mot_mne mot_num value new_value

    if ($# != 1 && $# != 2) {
       printf("Usage $0 or\n")
       printf(" $0 <motor_mnemonique> [<ramp_rate_value>] \n")
       exit
    }

    mot_mne = "$1"
    mot_num = motor_num(mot_mne)

    nanodac_device_loop = motor_par(mot_num,"address")
    if ($# == 1) {
        nanodac_device_loop = motor_par(mot_num,"address")
        value = _nanodac_tango_attr(nanodac_device_loop, "r", "ramprate")
        printf("Ramp rate IS %d\n", value)
     }else{
        val = $2
        value = _nanodac_tango_attr(nanodac_device_loop, "r", "ramprate")
        _nanodac_tango_attr(nanodac_device_loop, "w", "ramprate", val)
        new_value = _nanodac_tango_attr(nanodac_device_loop, "r", "ramprate")
        printf("Ramp rate WAS %d\n", value)
        printf("Ramp rate NOW is %d\n", new_value)
    }
}'


#%UU%
#%MDESC%
# MACRO MOTOR:
# to change proportional value in PID
#  nanodac_change_P <motor_mnemonique> [<proportional_value>]
def nanodac_change_P ' {
    local  nanodac_device_loop mot_mne mot_num value new_value

    if ($# != 1 && $# != 2) {
       printf("Usage $0 or\n")
       printf(" $0 <motor_mnemonique> [<proportional_value>] \n")
       exit
    }

    mot_mne = "$1"
    mot_num = motor_num(mot_mne)

    nanodac_device_loop = motor_par(mot_num,"address")
    if ($# == 1) {
        nanodac_device_loop = motor_par(mot_num,"address")
        value = _nanodac_tango_attr(nanodac_device_loop, "r", "kp")
        printf("Kp IS %d\n", value)
     }else{
        val = $2
        value = _nanodac_tango_attr(nanodac_device_loop, "r", "kp")
        _nanodac_tango_attr(nanodac_device_loop, "w", "kp", val)
        new_value = _nanodac_tango_attr(nanodac_device_loop, "r", "kp")
        printf("Kp WAS %d\n", value)
        printf("Kp NOW is %d\n", new_value)
    }
}'


#%UU%
#%MDESC%
# MACRO MOTOR:
# to change integral value in PID
#  nanodac_change_I <motor_mnemonique> [<integral_value>]
def nanodac_change_I ' {
    local  nanodac_device_loop mot_mne mot_num value new_value

    if ($# != 1 && $# != 2) {
       printf("Usage $0 or\n")
       printf(" $0 <motor_mnemonique> [<proportional_value>] \n")
       exit
    }

    mot_mne = "$1"
    mot_num = motor_num(mot_mne)

    nanodac_device_loop = motor_par(mot_num,"address")
    if ($# == 1) {
        nanodac_device_loop = motor_par(mot_num,"address")
        value = _nanodac_tango_attr(nanodac_device_loop, "r", "ki")
        printf("Ki IS %d\n", value)
     }else{
        val = $2
        value = _nanodac_tango_attr(nanodac_device_loop, "r", "ki")
        _nanodac_tango_attr(nanodac_device_loop, "w", "ki", val)
        new_value = _nanodac_tango_attr(nanodac_device_loop, "r", "ki")
        printf("Ki WAS %d\n", value)
        printf("Ki NOW is %d\n", new_value)
    }
}'


#%UU%
#%MDESC%
# MACRO MOTOR:
# to change derivated value in PID
#  nanodac_change_D <motor_mnemonique> [<derivated_value>]
def nanodac_change_D ' {
    local  nanodac_device_loop mot_mne mot_num value new_value

    if ($# != 1 && $# != 2) {
       printf("Usage $0 or\n")
       printf(" $0 <motor_mnemonique> [<derivated_value>] \n")
       exit
    }

    mot_mne = "$1"
    mot_num = motor_num(mot_mne)
    nanodac_device_loop = motor_par(mot_num,"address")

    if ($# == 1) {
        nanodac_device_loop = motor_par(mot_num,"address")
        value = _nanodac_tango_attr(nanodac_device_loop, "r", "kd")
        printf("Kd IS %d\n", value)
     }else{
        val = $2
        value = _nanodac_tango_attr(nanodac_device_loop, "r", "kd")
        _nanodac_tango_attr(nanodac_device_loop, "w", "kd", val)
        new_value = _nanodac_tango_attr(nanodac_device_loop, "r", "kd")
        printf("Kd WAS %d\n", value)
        printf("Kd NOW is %d\n", new_value)
    }
}'


#%UU%
#%MDESC%
# Check the regulation bliss server version for the concerned motor enslaved
# This spec macro is compatible with the regulation server 1.0.x
# nanodac_check_version  <motor_mnemonique>
def nanodac_check_version(mot_mne) '{
    local  nanodac_device_loop mot_num vers1[] vers2[]

    mot_num = motor_num(mot_mne)
    nanodac_device_loop = motor_par(mot_num,"address")

    TANGO_ERR = -1
    pvers = tango_get(nanodac_device_loop, "server_version")
    if (TANGO_ERR == 0) {
        nb = split(pvers, vers1, "-")
        if (!nb) {
            print "ERROR: You are probably not using the right server version for the Nanodac."
            print "       Check the \'regulation_ds.py\' file."
            return 0
        } else{
            split(vers1[1], vers2, ".")
            if (vers2[0] == "1" && vers2[1] == "0"){
                return 1
            } else{
            return 0
            }
        }
    }
}'


######################################################################
############################               ###########################
############################  MACRO MOTOR  ###########################
############################               ###########################
######################################################################

#%IU%()
#%MDESC%
#    Called by spec after reading the config file
#
def nanodac_config(mot_num, type, p1, p2) '{
    # p1==controller number (0..N)
    # p2==number of motors supported (1..M)
    # p1==unit p2==module p3==channel
    if(type == "ctrl") {
        if (p2 < 1){
            print "Wrong number of motors in DEVICE config (NUM must be >1)"
        }
    }
    else if(type == "mot"){
        local nanodac_device_loop mot_mne
        local nanodac_device_loop_arr[]

        mot_mne = motor_mne(mot_num)
        is_good_version = nanodac_check_version(mot_mne)
        if(!is_good_version){
            print "ERROR: You are probably not using the right server version for the Nanodac."
            print "       Check the \'regulation_ds.py\' file."
            exit
        }


        nanodac_device_loop = motor_par(mot_num,"address")
        nb = split(nanodac_device_loop , nanodac_device_loop_arr, "/")
        TANGO_ERR = -1
        tango_io(nanodac_device_loop,"status")
        if(nb != 3 || TANGO_ERR != "0" || !(index(nanodac_device_loop_arr[2], "loop")) ) {
            printf("nanodac_config error: Wrong ADDR field with tango loop device name or device server is OFF.\n")
            return  ".error."
        }
    }
}'


#%IU%()
#%MDESC%
# MACRO MOTOR:
# Called by spec on motor operation.
#
def nanodac_cmd(mot_num, key, p1, p2) '{
    local pos real_pos dead_band
    local tango_wattr tango_rattr
    local _mot_mne wait_end_movement nanodac_device_loop  # _attr

    ## Needed - otherwise motor_par gives an error below
    if(mot_num == "..") {
        return
    }
    nanodac_device_loop = motor_par(mot_num,"address")
    tango_wattr = motor_par(mot_num,"tango_wattr")
    tango_rattr = motor_par(mot_num,"tango_rattr")
    dead_band = motor_par(mot_num,"dead_band")

    # Returns the current "setpoint" motor position in mm or deg
    if (key == "position") {
        pos =  _nanodac_tango_attr(nanodac_device_loop, "r",  tango_wattr)
	# #printf ("pos = %g", pos)
        return(pos)
        p"fin position"
    }

    # Starts a motion (p1==abs pos, p2==rel pos, with pos in mm or deg)
    if (key == "start_one") {
        _nanodac_tango_attr(nanodac_device_loop, "w", tango_wattr, p1)

	}

    # Status
    if (key == "get_status") {
        wait_end_movement = motor_par(mot_num,"wait_end_movement")
        if (wait_end_movement != 0){
            TANGO_ERR = -1
            pos = _nanodac_tango_attr(nanodac_device_loop, "r",  tango_wattr)
			real_pos = _nanodac_tango_attr(nanodac_device_loop, "r",  tango_rattr)
            if (dead_band == 0){
               dead_band = 0.5
	    }
            if (fabs(real_pos-pos) <=  dead_band){
               ## position achieved
               return(0)
            }
	    else{
		## Still moving
		printf("%s still moving. Real position = %g\r",\
		motor_mne(mot_num), real_pos)
		return(0x02)
            }
         }else{
	    ##do not wait the end of the motion
	    return(0)
        }
    }

    # Stops a single motor.
    if (key == "abort_one") {
        real_pos = _nanodac_tango_attr(nanodac_device_loop, "r",  tango_rattr)
        _nanodac_tango_attr(nanodac_device_loop, "w",  tango_wattr, real_pos)
        printf("\nAsked the motion to stop at the position %g\n\n", real_pos)
        return 1
    }
}'


######################################################################
###########################                 ##########################
###########################  MACRO COUNTER  ##########################
###########################                 ##########################
######################################################################

#%IU%()
#%MDESC%
# Called by spec after reading the config file
#
def nanodac_c_config(num, type, p1, p2, p3) '{
    local tango_rattr _attr value _dev

    if(type == "ctrl") {
        if (p2 < 1){
            print "Wrong number of counters in DEVICE config (NUM must be >=1)"
        }
     }

    if(type == "cnt") {
        local nanodac_device
        local nanodac_device_arr[]

        nanodac_device = counter_par(num,"address")
        nb = split(nanodac_device, nanodac_device_arr, "/")
        TANGO_ERR = -1
        tango_io(nanodac_device,"status")
        if(nb != 3 || TANGO_ERR != "0" || \
           !( !(index(nanodac_device_arr[2], "loop")) && (index(nanodac_device_arr[2], "input")) || \
               (index(nanodac_device_arr[2], "loop")) && !(index(nanodac_device_arr[2], "input")) ) \
          ){
            printf("nanodac_c_config error: Wrong ADDR field with tango loop device name or device server is OFF.\n")
            return  ".error."
        }
    }
}'

#%IU%()
#%MDESC%
# Called by spec on counter operation.
#
def nanodac_c_cmd(num,key,p1,p2) '{
    local tango_rattr nanodac_device

    if (key == "counts") {
        nanodac_device = counter_par(num,"address")
        tango_rattr = counter_par(num,"tango_rattr")
        value =  _nanodac_tango_attr(nanodac_device, "r",  tango_rattr)
        return(value)
    }
}'


#%IU%()
#%MDESC%
# Called by spec for writing or reading tango attributes.
#
def _nanodac_tango_attr(nanodac_device, rw, tango_attr, value) '{
    local nanodac_device_input
    local nanodac_device_output
    local tango_device

#    print("-----------------------------------------------------")
#    printf("_nanodac_tango_attr(nanodac_device: %s, rw:%s, tango_attr:%s, value:%s\n", \
#           nanodac_device, rw, tango_attr, value)

    if (index(nanodac_device, "loop")) {
#        printf("nanodac_device is a loop\n")
	nanodac_device_loop = nanodac_device
	nanodac_device_output = tango_get(nanodac_device_loop, "target_output_device_name")
	nanodac_device_input = tango_get(nanodac_device_loop, "target_input_device_name")
    } else {
#        printf("nanodac_device is not a loop\n")
        nanodac_device_input = nanodac_device
    }

#    printf("  loop device: %s\n", nanodac_device_loop)
#    printf(" input device: %s\n", nanodac_device_input)
#    printf("output device: %s\n", nanodac_device_output)

    if (rw == "w" && (tango_attr == "setpoint" || tango_attr == "ramprate" || \
                      tango_attr == "kp" || tango_attr == "ki" ||tango_attr == "kd") \
        ) {

        ###### WRITE

        TANGO_ERR = -1
        tango_put(nanodac_device_loop, tango_attr, value)
	if (TANGO_ERR != "0"){
	    tty_cntl("md")
	    print "\ntango_put ERROR " nanodac_device_loop  " "  tango_attr "\n\n"
	    printf ("TANGO_ERR_MSG = \"%s\" \n\n", TANGO_ERR)
	    print TANGO_ERR_STACK
	    tty_cntl("me")
	    return ".error."
	}
    }
    else {
        ####### READ

	if (tango_attr == "setpoint" || tango_attr == "working_setpoint" || \
            tango_attr == "ramprate" || tango_attr == "target_info" || \
            tango_attr == "spec_version" || \
            tango_attr == "kp" || tango_attr == "ki" || tango_attr == "kd") {
            tango_device = nanodac_device_loop
        }
        else if (tango_attr == "target_read"){
            tango_device = nanodac_device_input
        }
        else if (tango_attr == "set_value"){
            tango_device = nanodac_device_output
        }
        else{
            printf("\n\nnanodac ERROR: unknown tango attribute:%s\n\n\n", tango_attr)
        }

        TANGO_ERR = -1

#        printf( "tango_device=%d, tango_attr=%d\n", tango_device, tango_attr)

        value = tango_get(tango_device, tango_attr)
        if (TANGO_ERR != "0"){
	    _cnt_mne = cnt_mne(num)
	    tty_cntl("md")
	    print "\ntango_get ERROR " nanodac_device_loop  " "  tango_attr "\n\n"
	    printf ("TANGO_ERR_MSG = \"%s\" \n\n", TANGO_ERR)
	    print TANGO_ERR_STACK
	    tty_cntl("me")
	    return ".error."
	}
        return(value)
    }

}'
