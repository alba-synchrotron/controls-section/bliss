<!--
A new scriv changelog fragment.

Uncomment the section that is right (remove the HTML comment wrapper).
-->

### Removed

- shell
   - Removed `STYLE` from `bliss.shell.getval` module

-->
### Added

- shell heplers
   - Added `bliss.shell.formatters.tabulate` for tables with style
- shell standard
   - Added `print_html` and `print_ansi`
   - Added arguement `display_mode` for `move` function in order to display or not the live motion in the shell

### Changed

- shell standard
   - Changed `mv`/`wa`/`wu`/`wm` does not support anymore unknown arguments
   - Changed `umv`-like functions now only display the motion live if it is possible, else the motion is only logged
   - Changed `umv`-like functions only log the motion in the display if `wait=False`

### Deprecated

- Deprecated colors from `bliss.common.utils`, prefer to use `print_html` or `bliss.shell.formatters.ansi`

### Fixed

- shell:
   - Fixed `mvdr` when `axis.sign` is negative

<!--
### Security

- A bullet item for the Security category.

-->
