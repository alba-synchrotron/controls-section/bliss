<!--
A new scriv changelog fragment.

Uncomment the section that is right (remove the HTML comment wrapper).
-->

<!--
### Removed

- A bullet item for the Removed category.

-->

### Added

- blissterm
   - Added web based blissterm, based on a REST API (backend) and Daiquiri components (frontend) 

<!--
### Changed

- first official release of blissterm, based on a REST API (backend) and Daiquiri components (frontend) 

-->
<!--
### Deprecated

- A bullet item for the Deprecated category.

-->
<!--
### Fixed

- A bullet item for the Fixed category.

-->
<!--
### Security

- A bullet item for the Security category.

-->
