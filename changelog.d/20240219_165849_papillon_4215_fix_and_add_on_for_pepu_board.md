<!--
A new scriv changelog fragment.

Uncomment the section that is right (remove the HTML comment wrapper).
-->

<!--
### Removed

- A bullet item for the Removed category.

-->
### Added

- pepu registered in global map
- PepuCalcController to convert pepu channels

<!--
### Changed

- A bullet item for the Changed category.

-->
<!--
### Deprecated

- A bullet item for the Deprecated category.

-->
### Fixed

- pepu config parsing
- pepu acquisition fixes (soft mode, excption raised on stop)

<!--
### Security

- A bullet item for the Security category.

-->
