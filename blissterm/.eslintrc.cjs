const { createConfig } = require('eslint-config-galex/dist/createConfig');
const {
  createJestOverride,
} = require('eslint-config-galex/dist/overrides/jest');
const {
  createReactOverride,
} = require('eslint-config-galex/dist/overrides/react');
const {
  createTypeScriptOverride,
} = require('eslint-config-galex/dist/overrides/typescript');

const { getDependencies } = require('eslint-config-galex/dist/getDependencies');

const dependencies = getDependencies();

module.exports = createConfig({
  __dirname,
  enableJavaScriptSpecificRulesInTypeScriptProject: true, // to lint `.eslintrc.js` files and the like
  rules: {
    'func-names': 'off', // would rather convert to arrow functions than come up with names, but can't due to `this` references
    'no-negated-condition': 'off', // ternaries are sometimes more readable when `true` branch is most significant branch
    'sort-keys-fix/sort-keys-fix': 'off', // keys should be sorted based on significance
    'import/no-default-export': 'off', // default exports are common in React
    'sonarjs/elseif-without-else': 'off', // `if`/`else if` serves a different purpose than `switch`
    'unicorn/prefer-prototype-methods': 'off', // not really more readable and makes Jest crash
    'unicorn/consistent-destructuring': 'off', // properties available after typeguard may be tedious to destructure (e.g. in JSX)

    // Prefer explicit, consistent return - e.g. `return undefined;`
    'unicorn/no-useless-undefined': 'off',
    'consistent-return': 'error',

    // Enforce curly braces, but only for blocks that span multiple lines
    curly: ['error', 'multi-line'],

    // With this rules if-blocks dont have anymore semantic
    'unicorn/no-lonely-if': 'off',
    'sonarjs/no-collapsible-if': 'off',

    // ===== For ease of migration to `eslint-config-galex` =====
    'new-cap': 'off',
    'no-console': 'off',
    'prefer-destructuring': 'off',
    'import/no-namespace': 'off',
    'import/order': 'off',
    'promise/prefer-await-to-callbacks': 'off',
    'promise/prefer-await-to-then': 'off',
    'require-unicode-regexp': 'off',
    'sonarjs/cognitive-complexity': 'off',
    'sonarjs/no-duplicate-string': 'off',
    'sonarjs/no-identical-expressions': 'off',
    'sonarjs/no-identical-functions': 'off',
    'unicorn/consistent-function-scoping': 'off',
    'unicorn/custom-error-definition': 'off',
    'unicorn/error-message': 'off',
    'unicorn/prefer-add-event-listener': 'off',
    'unicorn/prefer-spread': 'off',

    'unicorn/switch-case-braces': 'off',
  },
  overrides: [
    createReactOverride({
      ...dependencies,
      rules: {
        'react/jsx-no-constructed-context-values': 'off', // too strict
        'react/no-unknown-property': 'off', // false positives with R3F
        'react/jsx-no-useless-fragment': [2, { allowExpressions: true }],
      },
    }),
    createTypeScriptOverride({
      ...dependencies,
      rules: {
        '@typescript-eslint/ban-ts-comment': 'off', // too strict
        '@typescript-eslint/lines-between-class-members': 'off', // allow grouping single-line members
        '@typescript-eslint/prefer-nullish-coalescing': 'off', // `||` is often conveninent and safe to use with TS
        '@typescript-eslint/explicit-module-boundary-types': 'off', // worsens readability sometimes (e.g. for React components)
        '@typescript-eslint/no-unnecessary-type-arguments': 'off', // lots of false positives

        // Allow removing properties with destructuring
        '@typescript-eslint/no-unused-vars': [
          'warn',
          { ignoreRestSiblings: true },
        ],

        // Allow writing void-returning arrow functions in shorthand to save space
        '@typescript-eslint/no-confusing-void-expression': [
          'error',
          { ignoreArrowShorthand: true },
        ],

        // Prefer `interface` over `type`
        '@typescript-eslint/consistent-type-definitions': [
          'error',
          'interface',
        ],

        // Disallows calling function with value of type `any` (disabled due to false positives)
        // Re-enabling because has helped fix a good number of true positives
        '@typescript-eslint/no-unsafe-argument': 'warn',

        '@typescript-eslint/consistent-type-assertions': [
          'error',
          {
            assertionStyle: 'as',
            objectLiteralTypeAssertions: 'allow', // `never` is too strict
          },
        ],

        // Disallow shadowing variables for an outer scope, as this can cause bugs
        // when the inner-scope variable is removed, for instance
        '@typescript-eslint/no-shadow': 'error',

        // daiquiri hangovers
        '@typescript-eslint/explicit-member-accessibility': 'off',
        '@typescript-eslint/no-explicit-any': 'off',

        // TODO: Migration path...
        '@typescript-eslint/restrict-template-expressions': 'off',
      },
    }),
    createJestOverride({
      ...dependencies,
      rules: {
        'jest/no-focused-tests': 'warn', // warning instead of error
        'jest/prefer-strict-equal': 'off', // `toEqual` is shorter and sufficient in most cases
        'jest-formatting/padding-around-all': 'off', // allow writing concise two-line tests
        'jest/require-top-level-describe': 'off', // filename should already be meaningful, extra nesting is unnecessary
        'jest/no-conditional-in-test': 'off', // false positives in E2E tests (snapshots), and too strict (disallows using `||` for convenience)
        'testing-library/no-unnecessary-act': 'off', // `act` is sometimes required when advancing timers manually
      },
    }),
  ],
});
