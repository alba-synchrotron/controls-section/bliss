import json
from blissterm.blissterm import init_server
from blissterm.models.config import ConfigSchema

config = ConfigSchema(monitor=[], topItems=[], sideItems=[], iosecret="test")
app, socketio = init_server(config=config)

with open("openapi.json", "w", encoding="utf-8") as file:
    json.dump(app.api_doc, file, indent=4)
