const customApiUrl = import.meta.env.VITE_API_URL || undefined;
const isProd = import.meta.env.PROD;
const requireHttps = import.meta.env.VITE_HTTPS === 'true';

const baseUrl =
  customApiUrl ||
  (isProd ? '/api' : `${requireHttps ? 'https' : 'http'}://localhost:5000/api`);

const config = {
  baseUrl,
};

export default config;
