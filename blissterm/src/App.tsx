import { BrowserRouter, Route, Routes } from 'react-router-dom';

import AppErrorBoundary from './components/layout/AppErrorBoundary';
import Header from './components/layout/Header';
import Layout from './components/layout/Layout';
import Sidebar from './components/layout/Sidebar';
import Main from './components/layout/Main';

function App() {
  return (
    <div className="app">
      <BrowserRouter>
        <AppErrorBoundary>
          <Header />
          <Sidebar />
          <Routes>
            <Route path="/" element={<Main />} />
            <Route path="layout/:acronym" element={<Layout />} />
          </Routes>
        </AppErrorBoundary>
      </BrowserRouter>
    </div>
  );
}

export default App;
