import React from 'react';
import ReactDOM from 'react-dom/client';

import { CacheProvider } from '@rest-hooks/react';
import { SocketIO } from './api/SocketIO';
import StreamManager from './api/resources/base/StreamManager';
import App from './App';
import config from './config';
import './scss/main.scss';
import { HardwareResource } from './api/resources/Hardware';

SocketIO.baseUrl = config.baseUrl;

const managers = [
  ...CacheProvider.defaultProps.managers,
  // @ts-expect-error
  new StreamManager(HardwareResource),
];

ReactDOM.createRoot(document.querySelector('#root') as HTMLElement).render(
  <React.StrictMode>
    <CacheProvider managers={managers}>
      <App />
    </CacheProvider>
  </React.StrictMode>
);
