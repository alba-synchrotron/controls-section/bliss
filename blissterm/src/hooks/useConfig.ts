import { useSuspense } from '@rest-hooks/react';
import { ConfigEndpoint } from '../api/resources/Config';

export default function useConfig() {
  return useSuspense(ConfigEndpoint);
}
