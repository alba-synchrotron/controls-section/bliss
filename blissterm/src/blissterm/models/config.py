from typing import List, Optional, Union

from pydantic import BaseModel, Field, ConfigDict


class MonitorConfigSchema(BaseModel):
    name: str = Field(description="Title of this monitor item")
    value: str = Field(description="The value to display: object.property")
    overlay: Optional[str] = Field(
        None, description="A property to show on mouse hover: object.property"
    )
    comparator: Optional[str] = Field(
        None,
        description="The comparitor to use: ==, <, >, >=, <=, != to show as 'good' or 'bad'",
    )
    comparison: Optional[Union[str, List[str]]] = Field(
        None, description="The value to compare against"
    )


class LayoutItem(BaseModel):
    type: str = Field(
        alias="type", description="The layout item type: row, col, component"
    )
    type: Optional[str] = Field(None, description="The component to use")
    children: Optional[List["LayoutItem"]] = None

    # Component options are now flat so allow these through
    model_config = ConfigDict(extra="allow")


class LayoutRoot(BaseModel):
    name: str = Field(description="Name of the layout")
    acronym: str = Field(description="Short name of the layout, used for the url slug")
    icon: Optional[str] = Field(
        None, description="An optional icon to use in the sidebar"
    )
    children: List[LayoutItem]


class ServerArgs(BaseModel):
    debug: bool = Field(False, description="Enable flask reloader / debugging")


class PublicConfigItemsSchema(BaseModel):
    monitor: List[MonitorConfigSchema]
    topItems: List[LayoutItem]
    sideItems: List[LayoutItem]


class ConfigSchema(PublicConfigItemsSchema):
    cors: Optional[bool] = Field(False, description="Whether to enable CORS")
    iosecret: str = Field(description="SocketIO secret, should be unique")
    server_args: ServerArgs = Field(
        {}, description="Arguments to pass to the flask instance"
    )
    allowed_sessions: Optional[List[str]] = Field(
        [], description="Optional list of allowed sessions"
    )


class PublicConfigSchema(PublicConfigItemsSchema):
    beamline: Optional[str] = Field(
        None, description="The beamline this instance is running on"
    )
    sessions: List[str] = Field(description="Available bliss sessions")
    activeSessions: List[str] = Field(description="Currently active bliss sessions")
    layouts: List[LayoutRoot]
