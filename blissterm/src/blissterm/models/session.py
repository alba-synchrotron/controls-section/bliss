from typing import Any, Dict, List, Literal, Optional, Union

from pydantic import BaseModel, Field


class SessionNamePath(BaseModel):
    session_name: str


class FunctionCallStatePath(BaseModel):
    session_name: str
    call_id: str


class ActiveMG(BaseModel):
    session_name: str = Field(description="The session name")
    enabled: List[str] = Field(description="A list of enabled counters")
    available: List[str] = Field(
        description="A list of available counters (including those enabled)"
    )
    disabled: List[str] = Field(description="A list of disabled counters")


class UpdateActiveMGBody(BaseModel):
    enabled: Optional[List[str]] = Field(
        None, description="A list of counters to enabled"
    )
    disabled: Optional[List[str]] = Field(
        None, description="A list of counters to disable"
    )


class CallFunction(BaseModel):
    call_async: Optional[bool] = Field(
        False, description="Whether to make the call asynchronously"
    )
    function: str = Field(description="The function to call")
    object: Optional[str] = Field(
        None, description="An object on which to call a function"
    )
    args: Optional[List[Any]] = Field(
        [],
        description="A list of arguments, $ will be interpolated to the corresponding object",
    )
    kwargs: Optional[Dict[str, Any]] = Field(
        {},
        description="A dictionary of kwargs, $ will be interpolated to the corresponding object",
    )


class CallFunctionResponse(BaseModel):
    return_value: Any
    call_id: Optional[str]


class CallFunctionAsyncState(CallFunctionResponse):
    state: Union[Literal["running"], Literal["terminated"]]


class ScanSaving(BaseModel):
    base_path: str
    beamline: Optional[str]
    data_path: str
    root_path: str
    filename: str

    template: Optional[str] = Field(
        None, description="For bliss_basic, the saving `template`"
    )
    data_filename: Optional[str] = Field(
        None, description="For bliss_basic, the saving `data_filename`"
    )

    proposal_name: Optional[str] = Field(
        None, description="For bliss_esrf, the proposal name"
    )
    collection_name: Optional[str] = Field(
        None, description="For bliss_esrf, the collection name"
    )
    dataset_name: Optional[str] = Field(
        None, description="For bliss_esrf, the dataset name"
    )


class UpdateScanSavingBody(BaseModel):
    # bliss basic
    template: Optional[str] = None
    data_filename: Optional[str] = None

    # esrf data policy
    proposal_name: Optional[str] = None
    collection_name: Optional[str] = None
    dataset_name: Optional[str] = None


class CallScanSavingBody(BaseModel):
    function: Literal["create_root_path"]
