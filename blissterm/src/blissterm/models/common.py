from typing import Any, Dict
from pydantic import BaseModel, Field


class MessageResponse(BaseModel):
    message: str = Field(description="The response message")


class ErrorResponse(BaseModel):
    error: str = Field(description="The error message")


class ExceptionResponse(BaseModel):
    exception: str = Field(description="Raised exception")
    traceback: str = Field(description="Exception traceback")


def custom_description(model: BaseModel, description: str) -> Dict[str, Any]:
    """Customise the description of a response"""
    return {
        "description": description,
        "content": {
            "application/json": {
                "schema": model.model_json_schema(),
            }
        },
    }
