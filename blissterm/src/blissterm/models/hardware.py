from typing import Any, Dict, List, Optional

from pydantic import BaseModel, Field
from pydantic._internal._model_construction import ModelMetaclass


class RegisterHardwareSchema(BaseModel):
    ids: List[str]


class HardwaresResourceQuery(BaseModel):
    type: Optional[str] = Field(None, description="Filter by a specific type")


class HardwareIDPath(BaseModel):
    id: str = Field(description="The bliss object id")


class HardwareObjectSchema(BaseModel):
    id: str = Field(description="The bliss object id")
    type: str = Field(description="The object type: motor, multiposition, ...")
    online: bool = Field(description="Whether the object is available")
    errors: List[Dict[str, Any]] = Field(
        description="Any errors accessing specific properties"
    )
    name: str = Field(description="The object name, if different to its `id`")
    alias: Optional[str] = Field(None, description="The objects alias (if any)")
    callables: List[str] = Field(
        description="A list of functions that can be called on the object"
    )
    properties: Dict[str, Any] = Field(
        description="A list of available properties on the object and their values"
    )


class SetObjectProperty(BaseModel):
    property: str = Field(description="The property to set")
    value: Any = Field(description="Its value")


class CallObjectFunction(BaseModel):
    function: str = Field(description="The function to call")
    value: Any = Field(description="Its value (if any)")


class CallObjectFunctionResponse(BaseModel):
    function: str = Field(description="The function to call")
    value: Any = Field(description="Its value (if any)")
    response: Any = Field(description="The response value")


class HOConfigAttribute(BaseModel):
    """The Hardware Object Config Attribute Schema"""

    id: str = Field(description="Attribute id")
    name: Optional[str] = Field(None, description="Attribute name (can be customised)")
    ui_schema: Optional[dict] = Field(
        None, description="Define how the UI renders this attribute"
    )
    # "enum": ["float", "int", "bool", "str"],
    type: Optional[str] = Field(None, description="Attribute type")
    step: Optional[float] = Field(None, description="Step size for attribute")
    min: Optional[float] = Field(None, description="Minimum value for attribute")
    max: Optional[float] = Field(None, description="Maximum value for attribute")


class HOConfigSchema(BaseModel):
    """HardwareObject base configuration schema"""

    name: Optional[str] = Field(None, description="Object name")
    id: str = Field(description="Object id")
    attributes: Optional[List[HOConfigAttribute]] = Field(
        None,
        description="Attribute configuration for run time schemas",
    )
    type: Optional[str] = Field(None, description="Object type for objects without id")


class IterableModelMetaClass(ModelMetaclass):
    def __contains__(self, val):
        return val in self.model_fields

    def __iter__(self):
        self.__iter = iter(self.model_fields.keys())
        return self.__iter

    def __next__(self):
        return next(self.__iter)


class HardwareSchema(BaseModel, metaclass=IterableModelMetaClass):
    @classmethod
    def read_only(self, prop):
        extra = self.model_fields[prop].json_schema_extra
        if extra:
            return extra.get("readOnly", False)

    @classmethod
    def validate(self, prop, value):
        data = {}
        data[prop] = value
        valid = self(**data)

        return getattr(valid, prop)
