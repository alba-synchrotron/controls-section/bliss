# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""Bliss REPL (Read Eval Print Loop) as a Flask+xtermjs web application"""

# put bliss imports first, in order to ensure proper monkey-patching is done
# before importing flask and other dependencies
from bliss.shell import log_utils
from blissdata.beacon.files import read_config as bliss_read_config

import os
import logging
import argparse
import socket
from flask_openapi3 import OpenAPI, Info
from flask_socketio import SocketIO
from pydantic import ValidationError

from .config import Config
from .console import Console
from .hardware.endpoint import Hardware
from .models.config import ConfigSchema
from .session import Session
from .utils import nocache


def init_server(config: ConfigSchema, static_folder: str = None):
    info = Info(
        title="BlissAPI", version="0.0.1", description="A REST/WebSocket API for BLISS"
    )
    app = OpenAPI(
        __name__,
        info=info,
        static_url_path="",
        static_folder=static_folder,
    )

    sio_kws = {}
    if config.cors:
        print("CORS Enabled")
        from flask_cors import CORS

        sio_kws["cors_allowed_origins"] = "*"
        CORS(app)

    app.config["SECRET_KEY"] = config.iosecret
    socketio = SocketIO(app, **sio_kws)

    Config(app=app, socketio=socketio, config=config)
    Console(app=app, socketio=socketio, config=config)
    Session(app=app, socketio=socketio, config=config)
    Hardware(app=app, socketio=socketio, config=config)

    @app.route("/manifest.json")
    def manifest():
        return app.send_static_file("manifest.json")

    @app.route("/favicon.ico")
    def favicon():
        return app.send_static_file("favicon.ico")

    @app.route("/", defaults={"path": ""})
    @app.route("/<string:path>")
    @app.route("/<path:path>")
    @nocache
    def index(path):
        return app.send_static_file("index.html")

    return app, socketio


def run_server(port: int = 5000, static_folder: str = None):
    config = read_config()
    app, socketio = init_server(config=config, static_folder=static_folder)
    log_utils.logging_startup(logging.WARN)
    print(
        f"** Running Bliss Terminal Server on http://{socket.gethostname()}:{port} **"
    )
    with log_utils.filter_warnings():
        socketio.run(
            app, host="0.0.0.0", port=port, **config.server_args.model_dump()
        )  # nosec


def read_config():
    try:
        config_yaml = bliss_read_config("beacon:///blissterm/config.yml")
    except Exception as e:
        print("Could not load beacon config file: `blissterm/config.yml`")
        print(e)
        exit()
    else:
        try:
            return ConfigSchema(**config_yaml)
        except ValidationError as e:
            print("Configuration from beacon is not valid:")
            print(e)
            exit()


def parse_args():
    """Parse command line args"""
    parser = argparse.ArgumentParser(description="Bliss Web Terminal")
    parser.add_argument(
        "--static-folder",
        dest="static_folder",
        default=os.path.join(os.path.dirname(__file__), "static"),
        help="Web server static folder",
    )
    parser.add_argument(
        "-p", "--port", dest="port", default=5000, help="Web server port", type=int
    )

    return parser.parse_args()


def main():
    args = parse_args()
    run_server(port=args.port, static_folder=args.static_folder)
