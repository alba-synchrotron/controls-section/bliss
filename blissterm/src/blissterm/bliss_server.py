import io
import types
import sys
import gevent
import gevent.event
import gevent.lock
import pydoc
import traceback
import threading
from collections import namedtuple, defaultdict
from contextlib import contextmanager
from pdb import Pdb
from typing import cast, TextIO

from bliss.common.greenlet_utils.asyncio_gevent import yield_future
from prompt_toolkit.output.vt100 import Vt100_Output
from bliss.shell.cli.repl import cli
from bliss.shell.cli.bliss_repl import BlissRepl
from bliss.shell.cli.bliss_app_session import bliss_app_session
from bliss.shell import getval
import bliss as bliss_module
from prompt_toolkit.data_structures import Size
from prompt_toolkit.shortcuts.prompt import PromptSession
from prompt_toolkit.input import create_pipe_input


class Debugger(Pdb):
    def __init__(self, server_repl, stdin, stdout):
        super().__init__(stdout=stdout)
        self._repl = server_repl
        self._current_line_execution = None

        self._input = stdin

        def custom_send_text(input, txt, orig_send_text=self._input.send_text):
            if txt == "\x03":
                if self._current_line_execution:
                    self._current_line_execution.kill(KeyboardInterrupt)
                    return
            return orig_send_text(txt)

        self._input.send_text = types.MethodType(custom_send_text, self._input)
        self.app = PromptSession(
            history=None,
            multiline=False,
            key_bindings=None,
            message="(Pdb) ",
        )

    def user_line(self, frame):
        self._repl._current_app = self.app
        return super().user_line(frame)

    def set_trace(self, frame=None):
        if frame is None:
            frame = sys._getframe().f_back
        return super().set_trace(frame)

    def set_continue(self):
        self._repl._current_app = self._repl.app
        return super().set_continue()

    def do_clear(self, arg):
        if not arg:
            # the original implementation asks confirmation with 'input',
            # let's make it simple for now
            self.message("Please specify breakpoints to clear")
        else:
            return super().do_clear(arg)

    def default(self, line):
        self._current_line_execution = gevent.spawn(super().default, line)
        return self._current_line_execution.get()

    def cmdloop(self):
        self.preloop()
        stop = None
        while not stop:
            if self.cmdqueue:
                line = self.cmdqueue.pop(0)
            else:
                # Run the prompt in a different thread.

                line = ""
                keyboard_interrupt = False
                watcher = gevent.get_hub().loop.async_()
                line_read_event = gevent.event.Event()
                watcher.start(line_read_event.set)

                def in_thread():
                    nonlocal line, keyboard_interrupt
                    try:
                        line = self.app.prompt()
                    except EOFError:
                        line = "EOF"
                    except KeyboardInterrupt:
                        keyboard_interrupt = True
                    finally:
                        watcher.send()

                th = threading.Thread(target=in_thread)
                th.start()

                try:
                    line_read_event.wait()
                finally:
                    watcher.stop()
                    watcher.close()
                    th.join()

                if line == "EOF":
                    self.do_quit("")
                    sys.settrace(None)
                    self.quitting = False
                    return
                elif keyboard_interrupt:
                    raise KeyboardInterrupt

            line = self.precmd(line)
            self.stdout.flush()
            stop = self.onecmd(line)
            self.stdout.flush()
            stop = self.postcmd(stop, line)
            self.stdout.flush()

        self.postloop()


class BlissServerReplStdout(io.BytesIO):
    def __init__(self, session_name, emit, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._emit = emit
        self._session_name = session_name

    @property
    def encoding(self):
        return "utf-8"

    @property
    def session_name(self):
        return self._session_name

    def write(self, data):
        if isinstance(data, str):
            data = bytes(data, "utf-8")
        # sys.__stdout__.write(f"IN SERVER STDouT {data}\n")
        return super().write(data)

    def flush(self):
        self._emit("terminal_output", self.getvalue(), to=self.session_name)
        self.seek(0)
        self.truncate()


class BlissServerRepl(BlissRepl):
    default_term_size = Size(rows=40, columns=80)

    @staticmethod
    @contextmanager
    def ptpython_session(session_name, emit):
        # PipeInput object, for sending input to the CLI
        # (This is something that we can use in the prompt_toolkit event loop,
        # but still write data in manually.)
        with create_pipe_input() as pipe_input:
            stdout = BlissServerReplStdout(session_name, emit)
            output = Vt100_Output(
                stdout=cast(TextIO, stdout),
                get_size=lambda: BlissServerRepl.default_term_size,
            )
            with bliss_app_session(pipe_input, output) as ptpython_session:
                yield ptpython_session

    # code here is inspired from ptpython/contrib/asyncssh_repl.py
    def __init__(self, *args, **kwargs):
        self._term_rows, self._term_cols = BlissServerRepl.default_term_size
        self.emit = kwargs.pop("emit")
        # sys.breakpointhook = self.breakpoint_hook

        super().__init__(*args, **kwargs)

        self._debugger = None
        self._current_app = self.app

        # Disable open-in-editor and system prompt. Because it would run and
        # display these commands on the server side, rather than in the browser
        self.enable_open_in_editor = False
        self.enable_system_bindings = False

    def eval_greenlet(self, text):
        eval_g = super().eval_greenlet(text)
        eval_g.spawn_tree_locals["session_name"] = self.session_name
        eval_g.spawn_tree_locals["emit"] = self.emit
        return eval_g

    def initialize_session(self, early_log_info=None):
        init_g = gevent.spawn(super().initialize_session, early_log_info)
        init_g.spawn_tree_locals["session_name"] = self.session_name
        init_g.spawn_tree_locals["emit"] = self.emit
        return init_g.get()

    def breakpoint_hook(self):
        self._current_app.output.flush()
        with BlissServerRepl.ptpython_session(
            self.session_name, self.emit
        ) as ptpython_session:
            self._debugger = Debugger(
                self, ptpython_session.input, ptpython_session.output
            )
            self._debugger.app.output._get_size = self._get_size
            self._current_app = self._debugger.app
            # print = functools.partial(_orig_print, file=ptpython_session.output)
            try:
                self._debugger.set_trace(sys._getframe().f_back)
            finally:
                self._current_app = self.app

    def send_text(self, text):
        self._current_app.input.send_text(text)

    def _get_size(self) -> Size:
        """
        Callable that returns the current `Size`, required by Vt100_Output.
        """
        return Size(rows=self._term_rows, columns=self._term_cols)

    def terminal_size_changed(self, width, height):
        """
        When the terminal size changes, report back to CLI.
        """
        self._term_rows = height
        self._term_cols = width

        def screen_shape(fp):
            return width - 1, height

        self.app._on_resize()


Repl = namedtuple("Repl", ["cmd_line_i", "cli_greenlet"])
repls = {}
init_locks = defaultdict(gevent.lock.Semaphore)


def get_session_name_from_current_greenlet():
    current_greenlet = gevent.getcurrent()
    # sys.__stdout__.write(f"CURRENT GREENLET={repr(current_greenlet)}\n")
    if isinstance(current_greenlet, gevent.Greenlet):
        # sys.__stdout__.write(f"  session name = {repr(session_name)}\n")
        return current_greenlet.spawn_tree_locals.get("session_name")


def get_emit_from_current_greenlet():
    current_greenlet = gevent.getcurrent()
    if isinstance(current_greenlet, gevent.Greenlet):
        return current_greenlet.spawn_tree_locals.get("emit")


def get_current_session():
    session_name = get_session_name_from_current_greenlet()
    # sys.__stdout__.write(f"SESS={session_name}\n")
    if session_name:
        return bliss_module._sessions[session_name]


bliss_module._get_current_session = get_current_session


def get_cmd_line(session_name, emit):
    # sys.__stdout__.write(f"GET CMDLINE {session_name} , {repr(repls)}\n")
    with init_locks[session_name]:
        try:
            return repls[session_name].cmd_line_i
        except KeyError:
            ready_event = gevent.event.Event()
            cmd_line_i = None

            def show_help(thing):
                # format web page with pydoc, the code below is similar to the genuine 'help' builtin
                obj, name = pydoc.resolve(thing, 0)  # forceload=0
                page = pydoc.html.page(
                    pydoc.describe(obj), pydoc.html.document(obj, name)
                )
                emit("show_help", page, to=session_name)

            def run_cmd_line():
                nonlocal cmd_line_i
                with BlissServerRepl.ptpython_session(
                    session_name, emit
                ) as app_session:
                    try:
                        cmd_line_i = cli(
                            repl_class=BlissServerRepl,
                            exit_msg="blissterm sessions cannot be exited",
                            session_name=session_name,
                            app_session=app_session,
                            emit=emit,
                            style="xcode",  # solarized-dark",
                        )
                    except Exception:
                        traceback.print_exc(file=sys.__stdout__)
                        raise
                    cmd_line_i.bliss_session.env_dict["help"] = show_help
                    cmd_line_i.bliss_session.env_dict[
                        "breakpoint"
                    ] = cmd_line_i.breakpoint_hook

                    ready_event.set()

                    yield_future(cmd_line_i.run_async())

            cli_greenlet = gevent.spawn(run_cmd_line)

            ready_event.wait()

            repls[session_name] = Repl(cmd_line_i, cli_greenlet)

            return cmd_line_i


def getval_prompt(message, validator=None):
    session_name = get_session_name_from_current_greenlet()
    emit = get_emit_from_current_greenlet()
    cmd_line_i = get_cmd_line(session_name, emit)

    with BlissServerRepl.ptpython_session(session_name, emit):
        session = getval.BlissPromptSession()
        repl = cmd_line_i.app
        cmd_line_i._current_app = session

        try:
            return yield_future(
                session.prompt_async(
                    message,
                    validator=validator,
                    handle_sigint=False,
                )
            )
        except getval._GetvalKeyboardInterrupt:
            raise KeyboardInterrupt
        finally:
            cmd_line_i._current_app = repl


getval.bliss_prompt = getval_prompt
