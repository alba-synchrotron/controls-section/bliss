import json
import traceback
import uuid

import gevent

from .core import CoreBase, CoreResource, doc
from .models.session import (
    SessionNamePath,
    FunctionCallStatePath,
    ActiveMG,
    UpdateActiveMGBody,
    CallFunction,
    CallFunctionResponse,
    CallFunctionAsyncState,
    ScanSaving,
    UpdateScanSavingBody,
    CallScanSavingBody,
)
from .models.common import (
    ErrorResponse,
    ExceptionResponse,
    MessageResponse,
    custom_description,
)
from .utils import serialiser
from .bliss_server import repls

session_not_active = custom_description(ErrorResponse, "Session is not active")
session_no_mg = custom_description(
    ErrorResponse, "Session does not have an active measurement group"
)


class ActiveMGResource(CoreResource):
    @doc(
        summary="Get active MG",
        responses={"200": ActiveMG, "400": session_not_active, "404": session_no_mg},
    )
    def get(self, path: SessionNamePath):
        """Get the current `ACTIVE_MG` from `session_name` with its list of enabled and available counters"""
        try:
            cmd_line_i = repls[path.session_name].cmd_line_i
        except KeyError:
            return {"error": "Session is not active"}, 400
        else:
            mg = cmd_line_i.bliss_session.active_mg
            if not mg:
                return {
                    "error": "Session does not have an active measurement group"
                }, 404

        return {
            "session_name": path.session_name,
            "enabled": list(mg.enabled),
            "available": list(mg.available),
        }

    @doc(
        summary="Update active MG",
        responses={
            "200": ActiveMG,
            "400": session_not_active.copy(),
            "404": session_no_mg.copy(),
        },
    )
    def patch(self, path: SessionNamePath, body: UpdateActiveMGBody):
        """Update the enabled and disabled counters for the current `ACTIVE_MG` in `session_name`"""
        try:
            cmd_line_i = repls[path.session_name].cmd_line_i
        except KeyError:
            return {"error": "Session is not active"}, 400
        else:
            mg = cmd_line_i.bliss_session.active_mg
            if not mg:
                return {
                    "error": "Session does not have an active measurement group"
                }, 404

            if body.enabled:
                for item in body.enabled:
                    mg.enable(item)

            if body.disabled:
                for item in body.disabled:
                    mg.disable(item)

        return {
            "session_name": path.session_name,
            "enabled": list(mg.enabled),
            "available": list(mg.available),
        }


class ScanSavingResource(CoreResource):
    @doc(
        summary="Get SCAN_SAVING",
        responses={"200": ScanSaving, "400": session_not_active.copy()},
    )
    def get(self, path: SessionNamePath):
        """Get the current `SCAN_SAVING` settings for `session_name`

        The returned values will depend on the type of scan saving currently enabled.
        Both bliss_basic and bliss_esrf are supported.
        """
        try:
            cmd_line_i = repls[path.session_name].cmd_line_i
        except KeyError:
            return {"error": "Session is not active"}, 400
        else:
            scan_saving = cmd_line_i.bliss_session.scan_saving

            scan_saving_response = {}
            for key in [
                "base_path",
                "beamline",
                "root_path",
                "data_path",
                "filename",
                "template",
                "data_filename",
                "proposal_name",
                "collection_name",
                "dataset_name",
            ]:
                try:
                    scan_saving_response[key] = getattr(scan_saving, key)
                except AttributeError:
                    pass

            return scan_saving_response

    @doc(
        summary="Update SCAN_SAVING",
        responses={"200": UpdateScanSavingBody, "400": session_not_active.copy()},
    )
    def patch(self, path: SessionNamePath, body: UpdateScanSavingBody):
        """Update the `SCAN_SAVING` settings for `session_name`"""
        try:
            cmd_line_i = repls[path.session_name].cmd_line_i
        except KeyError:
            return {"error": "Session is not active"}, 400
        else:
            scan_saving = cmd_line_i.bliss_session.scan_saving
            for key, value in body.model_dump(exclude_unset=True).items():
                setattr(scan_saving, key, value)

            return body.model_dump(exclude_unset=True)

    @doc(
        summary="Call a SCAN_SAVING function",
        responses={
            "200": custom_description(MessageResponse, "Function successfully called"),
            "400": session_not_active.copy(),
        },
    )
    def post(self, path: SessionNamePath, body: CallScanSavingBody):
        """Call a function from `SCAN_SAVING` in `session_name`

        For example `create_root_path`"""
        try:
            cmd_line_i = repls[path.session_name].cmd_line_i
        except KeyError:
            return {"error": "Session is not active"}, 400
        else:
            scan_saving = cmd_line_i.bliss_session.scan_saving

        if body.function == "create_root_path":
            scan_saving.create_root_path()
            return {"message": "success"}


class CallFunctionResource(CoreResource):
    @doc(
        summary="Call a function in the session",
        responses={
            "200": CallFunctionResponse,
            "404": custom_description(
                ErrorResponse, "Function not available in session"
            ),
            "400": session_not_active.copy(),
            "500": custom_description(ExceptionResponse, "Could not call function"),
            "503": custom_description(
                ExceptionResponse, "Could not serialise response"
            ),
        },
    )
    def post(self, path: SessionNamePath, body: CallFunction):
        """Call a function either directly in `session_name` or on an object in the session

        This allows for example, to execute scans, and interact with bliss objects in the context
        of the running session.

        The endpoint blocks until the function returns and will try to json serialise the response,
        if this is not possible an exception will be raised.

        Exceptions will be caught and returned along with the traceback.
        """
        try:
            cmd_line_i = repls[path.session_name].cmd_line_i
        except KeyError:
            return {"error": "Session is not active"}, 400
        else:
            try:
                if body.object:
                    obj = getattr(cmd_line_i.bliss_session.setup_globals, body.object)
                    func = getattr(obj, body.function)
                else:
                    func = getattr(
                        cmd_line_i.bliss_session.setup_globals, body.function
                    )
            except AttributeError:
                return {"error": "Function not available in session"}, 404
            else:
                try:
                    args = []
                    for arg in body.args:
                        if isinstance(arg, str) and arg.startswith("$"):
                            args.append(
                                getattr(
                                    cmd_line_i.bliss_session.setup_globals,
                                    arg.replace("$", ""),
                                )
                            )
                        else:
                            args.append(arg)

                    if body.call_async:
                        call_id = str(uuid.uuid4())
                        greenlet = gevent.spawn(func, *args, **body.kwargs)
                        self.parent._function_calls[call_id] = greenlet
                        response = None
                    else:
                        call_id = None
                        response = func(*args, **body.kwargs)
                # BaseException to catch gevent timeout, etc
                except BaseException as e:
                    return {
                        "exception": f"{e.__class__.__name__}: {e}",
                        "traceback": "".join(traceback.format_tb(e.__traceback__)),
                    }, 500
                try:
                    # Check if data can be serialised
                    json.dumps(serialiser(response))
                    return {
                        "call_id": call_id,
                        "return_value": serialiser(response),
                    }

                except TypeError:
                    return {
                        "error": f"Couldnt serialise function: `{body.function}` response: {str(response)}"
                    }, 503


class CallFunctionStateResource(CoreResource):
    @doc(
        summary="Get the state and response for an asynchronous call to a function in the session",
        responses={
            "200": CallFunctionAsyncState,
            "404": custom_description(
                ErrorResponse, "Could not find the requested function call"
            ),
            "500": custom_description(ExceptionResponse, "Could not call function"),
            "503": custom_description(
                ExceptionResponse, "Could not serialise response"
            ),
        },
    )
    def get(self, path: FunctionCallStatePath):
        """Get the state and response of an asynchronous function call in the session

        Exceptions will be caught and returned along with the traceback.
        """
        try:
            greenlet = self.parent._function_calls[path.call_id]
        except KeyError:
            return {"error": "Could not find specified function call"}, 404

        if greenlet:
            return {"state": "running"}
        else:
            try:
                response = greenlet.get()
                # The greenlet might return an Exception without raising it
                # i.e. `GreenletExit` when killed
                if isinstance(response, BaseException):
                    raise response
            except BaseException as e:
                return {
                    "exception": f"{e.__class__.__name__}: {e}",
                    "traceback": "".join(traceback.format_tb(e.__traceback__)),
                }, 500
            try:
                # Check if data can be serialised
                json.dumps(serialiser(response))
                return {
                    "state": "terminated",
                    "return_value": serialiser(response),
                }
            except TypeError:
                return {
                    "error": f"Couldnt serialise function: `{path.call_id}` response: {str(response)}"
                }, 503

    @doc(
        summary="Kill an asynchronous call to a function in the session",
        responses={
            "204": None,
            "404": custom_description(
                ErrorResponse, "Could not find the requested function call"
            ),
            "400": custom_description(ErrorResponse, "Could not kill function"),
        },
    )
    def delete(self, path: FunctionCallStatePath):
        """Kill an asynchronous function call in the session

        Exceptions will be caught and returned along with the traceback.
        """
        try:
            greenlet = self.parent._function_calls[path.call_id]
        except KeyError:
            return {"error": "Could not find specified function call"}, 404

        try:
            greenlet.kill()
            return b"", 204
        except BaseException as e:
            return {
                "exception": f"{e.__class__.__name__}: {e}",
                "traceback": "".join(traceback.format_tb(e.__traceback__)),
            }, 400


class Session(CoreBase):
    def setup(self):
        # TODO: This will eat memory, when to purge?
        self._function_calls = {}

        self.register_route(ActiveMGResource, "/<session_name>/active_mg")
        self.register_route(ScanSavingResource, "/<session_name>/scan_saving")
        self.register_route(CallFunctionResource, "/<session_name>/call")
        self.register_route(CallFunctionStateResource, "/<session_name>/call/<call_id>")
