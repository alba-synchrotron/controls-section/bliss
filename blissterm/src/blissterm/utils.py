from datetime import datetime
from functools import update_wrapper, wraps
import importlib
import logging

from flask import make_response
from bliss.scanning.scan import Scan

logger = logging.getLogger(__name__)


def loader(base, postfix, module, *args, **kwargs):
    """Try loading class "{module.title()}{postfix}" from
    "{base}{module}" and instantiate it.

    For example "ExampleActor" from "daiquiri.implementors.examplecomponent".

    :param str base: base module
    :param str postfix: add to module name to get class name
    :param str module: submodule
    :param args: for class instantiation
    :param kwargs: for class instantiation
    :returns Any:
    """
    # Making sure that everything up to the last module
    # is treated as base module
    if "." in module:
        mod_parts = module.split(".")
        base = base + "." + ".".join(mod_parts[0:-1])
        module = mod_parts[-1]
    # Load class from module
    mod_file = base + "." + module
    try:
        mod = importlib.import_module(mod_file)
        mod = importlib.reload(mod)
    except ModuleNotFoundError:
        err_msg = "Couldn't find module {}".format(mod_file)
        logger.error(err_msg)
        raise
    # Import class
    if hasattr(mod, "Default"):
        class_name = "Default"
    else:
        class_name = module.title() + postfix
    try:
        cls = getattr(mod, class_name)
    except AttributeError:
        err_msg = "Couldn't import '{}' from {}".format(class_name, mod_file)
        logger.error(err_msg)
        raise
    # Instantiate class
    instance = cls(*args, **kwargs)
    logger.debug(f"Instantiated '{class_name}' from {mod_file}")
    return instance


def get_nested_attr(obj, attr, **kw):
    attributes = attr.split(".")
    for i in attributes:
        try:
            obj = getattr(obj, i)
            if callable(obj):
                obj = obj()
        except AttributeError:
            if "default" in kw:
                return kw["default"]
            else:
                raise
    return obj


def nocache(view):
    """A response with no-cache

    Stolen from https://arusahni.net/blog/2014/03/flask-nocache.html
    """

    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers["Last-Modified"] = datetime.now()
        response.headers[
            "Cache-Control"
        ] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0"
        response.headers["Pragma"] = "no-cache"
        response.headers["Expires"] = "-1"
        return response

    return update_wrapper(no_cache, view)


def serialiser(obj):
    if isinstance(obj, Scan):
        return {
            "type": "bliss.scanning.scan.Scan",
            "key": obj._scan_data.key,
            "scan_number": obj.scan_number,
            "name": obj.name,
            "scan_info": {
                "name": obj.scan_info.get("name"),
                "title": obj.scan_info.get("title"),
                "filename": obj.scan_info.get("filename"),
                "save": obj.scan_info.get("save"),
                "npoints": obj.scan_info.get("npoints"),
                "start_time": obj.scan_info.get("start_time"),
                "end_time": obj.scan_info.get("end_time"),
                "end_reason": obj.scan_info.get("end_reason"),
            },
        }
    return obj
