#!/usr/bin/env python
# -*- coding: utf-8 -*-
import importlib
import logging

from bliss.config.static import get_config

from .object import BlissDummyObject
from .abstract.object import HardwareObject
from ..models.hardware import HOConfigSchema
from ..utils import loader


logger = logging.getLogger(__name__)

bl = logging.getLogger("bliss")
bl.disabled = True

bl = logging.getLogger("bliss.common.mapping")
bl.disabled = True


class BlissHandler:
    """The bliss protocol handler

    Returns an instance of an abstracted bliss object

    The bliss protocol handler first checks the kwargs conform to the BlissHOConfigSchema
    defined above. This address is used to retrieve the  bliss object. Its class is then mapped
    to an abstract class and a bliss specific instance is created (see hardware/bliss/motor.py)
    """

    _class_map = {
        "bliss.common.axis.Axis": "motor",
        "bliss.controllers.actuator.Actuator": "actuator",
        "bliss.controllers.multiplepositions.MultiplePositions": "multiposition",
        "bliss.common.shutter.BaseShutter": "shutter",
        "bliss.controllers.intraled.Intraled": "intraled",
    }

    _class_name_map = {
        "EBV": "beamviewer",
    }

    def get(self, config: HOConfigSchema, should_raise: bool = False) -> HardwareObject:
        obj_type = config.type
        if obj_type == "activetomoconfig":
            # It's a global proxy without real instance
            class GlobalBlissObject:
                name = None

            obj = GlobalBlissObject()
        else:
            cfg = get_config()
            try:
                obj = cfg.get(config.id)
            except Exception:
                if should_raise:
                    raise
                logger.exception(f"Couldn't get bliss object {config.id}")
                return BlissDummyObject(**config.model_dump())

        obj_type = config.type
        if obj_type is not None:
            return loader(
                "blissterm.hardware.mapping",
                "",
                obj_type,
                obj=obj,
                **config.model_dump(),
            )

        for bliss_mapping, mapped_class in self._class_map.items():
            bliss_file, bliss_class_name = bliss_mapping.rsplit(".", 1)
            # Some classes may not be available depending on the bliss version
            try:
                bliss_module = importlib.import_module(bliss_file)
                bliss_class = getattr(bliss_module, bliss_class_name)
            except ModuleNotFoundError:
                logger.warning(f"Could not find bliss module {bliss_mapping}")
                continue

            if isinstance(obj, bliss_class):
                return loader(
                    "blissterm.hardware.mapping",
                    "",
                    mapped_class,
                    obj=obj,
                    **config.model_dump(),
                )

        cls = obj.__class__.__name__
        if cls in self._class_name_map:
            return loader(
                "blissterm.hardware.mapping",
                "",
                self._class_name_map[cls],
                obj=obj,
                **config.model_dump(),
            )

        logger.error("No class found for {cls}".format(cls=cls))
        return BlissDummyObject(**config)
