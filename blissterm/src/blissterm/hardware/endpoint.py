import logging
import time
from typing import Any, List, Optional

import gevent
import numpy
from pydantic import ValidationError

from ..core import CoreBase, CoreResource, doc
from ..models.common import ErrorResponse, custom_description
from ..models.hardware import (
    RegisterHardwareSchema,
    HardwareIDPath,
    HardwareObjectSchema,
    HardwaresResourceQuery,
    SetObjectProperty,
    CallObjectFunction,
    CallObjectFunctionResponse,
)
from ..models.utils import paginated
from .component import Hardware as HardwareComponent
from .abstract.object import HardwareObject

logger = logging.getLogger(__name__)
could_not_register = custom_description(ErrorResponse, "Could not register objects")


class RegisterHardwareResource(CoreResource):
    @doc(
        summary="Register a series of hardware objects to be made available via the API",
        responses={"200": RegisterHardwareSchema, "400": could_not_register},
    )
    def post(self, body: RegisterHardwareSchema):
        """Register a series of hardware objects"""
        try:
            self.parent.register_objects(body.ids)
            return body.model_dump()

        except Exception as e:
            return {"error": f"Could not register objects: {e}"}, 400


class HardwaresResource(CoreResource):
    @doc(
        summary="Get a list of hardware objects",
        responses={"200": paginated(HardwareObjectSchema)},
    )
    def get(self, query: HardwaresResourceQuery):
        """Get a list of all hardware statuses"""
        return (
            self.parent.get_objects(type=query.type),
            200,
        )


class HardwareResource(CoreResource):
    @doc(
        summary="Get a single hardware object",
        responses={"200": HardwareObjectSchema, "404": ErrorResponse},
    )
    def get(self, path: HardwareIDPath):
        """Get the status of a particular hardware object"""
        obj = self.parent.get_object(path.id)
        if obj:
            return obj.state.model_dump(), 200
        else:
            return {"error": "No such object"}, 404

    @doc(summary="Update a hardware propery", responses={"200": SetObjectProperty})
    def put(self, path: HardwareIDPath, body: SetObjectProperty):
        """Update a property on a hardware object"""
        obj = self.parent.get_object(path.id)
        if obj:
            try:
                obj.set(body.property, body.value)
                return {"property": body.property, "value": body.value}, 200
            except ValidationError as e:
                return {"error": str(e)}, 422
            # To catch gevent.Timeout as well
            except BaseException as e:
                logger.exception(
                    f"Could not change property {body.property}: {str(e)} for {obj.id}",
                )
                return {"error": str(e)}, 400
        else:
            return {"error": "No such object"}, 404

    @doc(
        summary="Call a function on a hardware object",
        responses={"200": CallObjectFunctionResponse},
    )
    def post(self, path: HardwareIDPath, body: CallObjectFunction):
        """Call a function on a hardware object"""
        obj = self.parent.get_object(path.id)
        if obj:
            try:
                resp = obj.call(body.function, body.value)
                return {"function": body.function, "response": resp}, 200
            except ValidationError as e:
                return {"error": str(e)}, 422
            # To catch gevent.Timeout as well
            except BaseException as e:
                logger.exception(
                    f"Could not call function {body.function}: {e} for {obj.id}",
                )
                return {"error": "Could not call function: {err}".format(err=e)}, 400
        else:
            return {"error": "No such object"}, 404


class Hardware(CoreBase):
    """The Hardware Feature

    This makes all loaded hardware available via flask resources, properties can be changed
    via put, and functions called via post requests.

    Hardware changes are notified via socketio events
    """

    _base_url = "hardware"
    _namespace = "hardware"
    _last_value = {}

    def subscribe(self):
        for o in self._hardware.get_objects():
            o.subscribe("all", self._obj_param_change)
            o.subscribe_online(self._obj_online_change)

    def setup(self):
        self._hardware = HardwareComponent(self.config)

        self._emit_timeout = {}
        self._last_emit_time = {}
        self.subscribe()

        self.register_route(HardwaresResource, "")
        self.register_route(RegisterHardwareResource, "/register")
        self.register_route(HardwareResource, "/<string:id>")

    def _ensure_last_val(self, obj: str, prop: str):
        """Ensure a last value

        Args:
            obj (str): The object id
            prop (str): The property

        Ensure a last value for obj/prop
        """
        if obj not in self._last_value:
            self._last_value[obj] = {}
        if obj not in self._emit_timeout:
            self._emit_timeout[obj] = {}
        if obj not in self._last_emit_time:
            self._last_emit_time[obj] = {}

        if prop not in self._last_value[obj]:
            self._last_value[obj][prop] = None
        if prop not in self._emit_timeout[obj]:
            self._emit_timeout[obj][prop] = None
        if prop not in self._last_emit_time[obj]:
            self._last_emit_time[obj][prop] = 0

    def _obj_online_change(self, obj: HardwareObject, value: Any):
        """Hardware online callback

        Emits socketio event on online change
        """
        self._ensure_last_val(obj.id, "online")

        if self._last_value[obj.id]["online"] != value:
            self.emit("online", {"id": obj.id, "state": value})
        else:
            logger.debug(f"Duplicate update for {obj.id}, online, {value}")

    def _obj_param_change(self, obj: HardwareObject, prop: str, value: Any):
        """Hardware parameter change callback

        Emits a socketio event on parameter change

        Args:
            obj (obj): The hardware object whos parameter changed
            prop (str): The property that changed
            value (mixed): The new value
        """
        self._ensure_last_val(obj.id, prop)

        if isinstance(value, numpy.ndarray):
            # the following compare with `_last_value` and the socketio event do not support numpy array
            if value.size > 10:
                # Warn in case you are about to transmit huge data by mistakes
                logging.warning(
                    "Prop '%s' are about to transmit a numpy array (size %s) using a socketio event, with a slow conversion",
                    prop,
                    value.size,
                )
            value = value.tolist()

        if self._last_value[obj.id][prop] != value:
            self._queue_emit_value(obj, prop, value)
            self._last_value[obj.id][prop] = value

        else:
            logger.debug(f"Duplicate update for {obj.id}, {prop}, {value}")

    def _queue_emit_value(self, obj: HardwareObject, prop: str, value: Any):
        if self._emit_timeout[obj.id][prop] is not None:
            self._emit_timeout[obj.id][prop].kill()
            self._emit_timeout[obj.id][prop] = None

        now = time.time()
        if now - self._last_emit_time[obj.id][prop] > 0.2:
            self._emit_value(obj, prop, value)
        else:
            self._emit_timeout[obj.id][prop] = gevent.spawn_later(
                0.2, self._emit_value, obj, prop, value
            )

    def _emit_value(self, obj: HardwareObject, prop: str, value: Any):
        data = {}
        data[prop] = value
        self.emit("change", {"id": obj.id, "data": data})
        self._last_emit_time[obj.id][prop] = time.time()

    def get_objects(self, type: str = None) -> List[HardwareObject]:
        """Get a list of all hardware object states"""
        objects = [o.state.model_dump() for o in self._hardware.get_objects(type=type)]
        return {"total": len(objects), "results": objects}

    def get_object(self, objectid: str) -> Optional[HardwareObject]:
        """Get a specific object

        Args:
            objectid (str): The objects id

        Returns:
            object (obj): The hardware object
        """
        return self._hardware.get_object(objectid)

    def register_objects(self, objectids: List[str]):
        for object_string in objectids:
            object = self._hardware._prepare_register_object(
                object_string, should_raise=True
            )
            if not object:
                continue

            object.subscribe("all", self._obj_param_change)
            object.subscribe_online(self._obj_online_change)

        return True
