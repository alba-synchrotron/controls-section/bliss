import logging

from ..abstract.object import HardwareProperty
from ..abstract.light import Light as AbstractLight, LightStates
from ..object import BlissObject

logger = logging.getLogger(__name__)


class StateProperty(HardwareProperty):
    def translate_from(self, value):
        for s in LightStates:
            if s == value.upper():
                return s

        return ["UNKNOWN"]


class Intraled(BlissObject, AbstractLight):
    property_map = {
        "intensity": HardwareProperty("intensity"),
        "temperature": HardwareProperty("temperature"),
        "state": StateProperty("modus"),
    }
