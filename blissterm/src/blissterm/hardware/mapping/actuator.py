import logging

from ..abstract.object import HardwareProperty
from ..abstract.actuator import Actuator as AbstractActuator
from ..object import BlissObject

logger = logging.getLogger(__name__)


class Actuator(BlissObject, AbstractActuator):
    property_map = {"state": HardwareProperty("state")}

    callable_map = {"move_in": "open", "move_out": "close", "toggle": "toggle"}
