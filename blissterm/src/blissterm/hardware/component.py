#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from typing import List, Optional

from pydantic import ValidationError

from ..config import get_layouts
from ..models.hardware import HOConfigSchema
from ..models.config import ConfigSchema, LayoutItem, LayoutRoot
from .abstract.object import HardwareObject
from .handler import BlissHandler

logger = logging.getLogger(__name__)


def find_objects_from_config(config: ConfigSchema) -> List[str]:
    objects = []
    for item in config.monitor:
        for monitor_object_path in [item.value, item.overlay]:
            if monitor_object_path:
                monitor_object = monitor_object_path.split(".")[0]
                if monitor_object not in objects:
                    objects.append(monitor_object)

    for area in [config.topItems, config.sideItems]:
        for component in area:
            if component.type == "hardwaregroup":
                if hasattr(component, "ids"):
                    for id_obj in component.ids:
                        if not id_obj["id"] in objects:
                            objects.append(id_obj["id"])

    return objects


def find_layout_objects_recursively(
    items: List[LayoutItem], objects: List[str]
) -> None:
    for item in items:
        if item.type == "hardwaregroup":
            if hasattr(item, "ids"):
                for id_obj in item.ids:
                    if not id_obj["id"] in objects:
                        objects.append(id_obj["id"])

        elif item.children:
            find_layout_objects_recursively(item.children, objects)


def find_objects_from_layouts(objects: List[str]) -> List[str]:
    layout_top = get_layouts()
    layout_list = layout_top["layouts"]
    for layout in layout_list:
        layout_object = LayoutRoot(**layout)
        if layout_object.children:
            find_layout_objects_recursively(layout_object.children, objects)

    return objects


class Hardware:
    """Core Hardware Control

    This initialises all hardware defined in hardware.yml, first loading the protocol
    handlers, then initialising each object with the relevant handler. A list of groups
    is built from the yaml, as well as a list of object types along with their schema

    """

    def __init__(self, config: ConfigSchema):
        self._handler = BlissHandler()
        self._objects = []

        subconfigs = []
        objects = find_objects_from_config(config)
        objects = find_objects_from_layouts(objects)
        print("Registering objects:", objects)

        for object_string in objects:
            o = self._prepare_register_object(object_string)
            if not o:
                continue

            # Store the sub object configs to load them later
            subconfigs.extend(o.get_subobject_configs())

        # Load the sub object configs
        for config in subconfigs:
            self.register_object_from_config(config)

    def _prepare_register_object(self, object_string: str, should_raise: bool = False):
        try:
            obj = HOConfigSchema(id=object_string)
        except ValidationError as err:
            print(err)
            exit()

        if self.get_object(obj.id):
            return

        return self.register_object_from_config(
            obj, register_sub_objects=False, should_raise=should_raise
        )

    def register_object(self, obj: HardwareObject, register_sub_objects: bool = True):
        """Register a new object to this hardware repository

        Attributes:
            obj: Object to register
            register_sub_objects: If true (default), sub components are loaded
                                  and registered
        """
        logger.info(f"Register {obj._protocol}://{obj.id}")
        self._objects.append(obj)
        # self._schema.register(obj.schema())

        if register_sub_objects:
            subconfigs = obj.get_subobject_configs()
            for config in subconfigs:
                self.register_object_from_config(config)

    def register_object_from_config(
        self,
        config: HOConfigSchema,
        register_sub_objects: bool = True,
        should_raise: bool = False,
    ):
        """
        Register an object from its config.

        If the object id was already registered, the object is skipped.

        It have to contains a `protocol` and an `id` key.

        Attributes:
            config: Object to register
            register_sub_objects: If true (default), sub components are loaded
                                  and registered
        """
        objectid = config.id
        obj = self.get_object(objectid)
        if obj:
            logger.debug(
                "Object id %s already registered. Configuration skipped", objectid
            )
            return

        obj = self._handler.get(config, should_raise=should_raise)
        self.register_object(obj, register_sub_objects=register_sub_objects)
        return obj

    def disconnect(self):
        for h in self._handlers.values():
            h.disconnect()

    def get_objects(self, type: str = None) -> List[HardwareObject]:
        """Get a list of hardware objects

        Kwargs:
            type (str): Filter the list of objects by a type (i.e. motor)

        Returns:
            A list of objects
        """
        objs = self._objects

        if type:
            objs = filter(lambda obj: obj.type == type, objs)

        return objs

    def get_object(self, objectid: str) -> Optional[HardwareObject]:
        """Get a specific object

        Args:
            objectid (str): The object id

        Returns
            The object
        """
        for o in self._objects:
            if o.id == objectid:
                return o
        return None
