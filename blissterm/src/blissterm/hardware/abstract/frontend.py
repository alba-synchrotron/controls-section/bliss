import logging
from typing import Any, Literal, Optional

from pydantic import Field
from .object import HardwareObject
from ...models.hardware import HardwareSchema

logger = logging.getLogger(__name__)

FrontendStates = ["OPEN", "RUNNING", "STANDBY", "CLOSED", "UNKNOWN", "FAULT"]
FrontendItlkStates = ["ON", "FAULT"]


class FrontendPropertiesSchema(HardwareSchema):
    state: Optional[Literal[tuple(FrontendStates)]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    status: Optional[str] = Field(None, json_schema_extra={"readOnly": True})

    automatic: Optional[bool] = Field(None, json_schema_extra={"readOnly": True})
    frontend: Optional[str] = Field(None, json_schema_extra={"readOnly": True})

    current: Optional[float] = Field(None, json_schema_extra={"readOnly": True})
    refill: Optional[float] = Field(None, json_schema_extra={"readOnly": True})
    mode: Optional[str] = Field(None, json_schema_extra={"readOnly": True})
    message: Optional[str] = Field(None, json_schema_extra={"readOnly": True})

    feitlk: Optional[Literal[tuple(FrontendItlkStates)]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    pssitlk: Optional[Literal[tuple(FrontendItlkStates)]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    expitlk: Optional[Literal[tuple(FrontendItlkStates)]] = Field(
        None, json_schema_extra={"readOnly": True}
    )


class FrontendCallablesSchema(HardwareSchema):
    open: Optional[Any] = None
    close: Optional[Any] = None
    reset: Optional[Any] = None


class Frontend(HardwareObject):
    _type = "frontend"
    _state_ok = [FrontendStates[0], FrontendStates[1]]

    _properties = FrontendPropertiesSchema
    _callables = FrontendCallablesSchema
