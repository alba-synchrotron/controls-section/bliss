import logging
from typing import Any, Literal, Optional

from pydantic import Field
from .object import HardwareObject
from ...models.hardware import HardwareSchema


logger = logging.getLogger(__name__)

ActuatorStatues = ["IN", "OUT", "UNKNOWN", "ERROR"]


class ActuatorPropertiesSchema(HardwareSchema):
    state: Optional[Literal[tuple(ActuatorStatues)]] = Field(
        None, json_schema_extra={"readOnly": True}
    )


class ActuatorCallablesSchema(HardwareSchema):
    move_in: Optional[Any] = None
    move_out: Optional[Any] = None
    toggle: Optional[Any] = None


class Actuator(HardwareObject):
    _type = "actuator"
    _state_ok = [ActuatorStatues[0], ActuatorStatues[1]]

    _properties = ActuatorPropertiesSchema
    _callables = ActuatorCallablesSchema
