from typing import Any, List, Literal, Optional

from pydantic import Field
from .object import HardwareObject
from ...models.hardware import HardwareSchema

MotorStates = [
    "READY",
    "MOVING",
    "FAULT",
    "UNKNOWN",
    "DISABLED",
    "LOWLIMIT",
    "HIGHLIMIT",
]


class MotorPropertiesSchema(HardwareSchema):
    position: Optional[float] = None
    target: Optional[float] = None
    tolerance: Optional[float] = None
    acceleration: Optional[float] = None
    velocity: Optional[float] = None
    limits: Optional[List[float]] = None
    state: Optional[List[Literal[tuple(MotorStates)]]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    unit: Optional[str] = Field(None, json_schema_extra={"readOnly": True})
    offset: Optional[float] = None
    sign: Optional[int] = None


class MotorCallablesSchema(HardwareSchema):
    move: Optional[float] = None
    rmove: Optional[float] = None
    stop: Optional[Any] = None
    wait: Optional[Any] = None


class Motor(HardwareObject):
    _type = "motor"
    _state_ok = [MotorStates[0], MotorStates[1]]

    _properties = MotorPropertiesSchema
    _callables = MotorCallablesSchema

    def rmove(self, value):
        self.call("rmove", value)

    def move(self, value):
        self.call("move", value)

    def stop(self):
        self.call("stop", None)

    def wait(self):
        self.call("wait", None)
