import logging
from typing import Any, Literal, Optional

from pydantic import Field
from .object import HardwareObject
from ...models.hardware import HardwareSchema

logger = logging.getLogger(__name__)

BeamviewerStates = ["ON", "OFF", "UNKNOWN", "FAULT"]


class BeamviewerPropertiesSchema(HardwareSchema):
    state: Optional[Literal[tuple(BeamviewerStates)]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    foil: Optional[Literal["IN", "OUT", "UNKNOWN", "NONE"]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    led: Optional[Literal["ON", "OFF"]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    screen: Optional[Literal["IN", "OUT", "UNKNOWN"]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    diode_ranges: Optional[str] = Field(None, json_schema_extra={"readOnly": True})
    diode_range: Optional[str] = None


class BeamviewerCallablesSchema(HardwareSchema):
    led: Optional[bool] = None
    screen: Optional[bool] = None
    foil: Optional[bool] = None
    current: Optional[Any] = None


class Beamviewer(HardwareObject):
    _type = "beamviewer"
    _state_ok = [BeamviewerStates[0], BeamviewerStates[1]]

    _properties = BeamviewerPropertiesSchema
    _callables = BeamviewerCallablesSchema
