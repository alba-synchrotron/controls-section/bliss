import logging
from typing import Any, Literal, Optional

from pydantic import Field
from .object import HardwareObject
from ...models.hardware import HardwareSchema

logger = logging.getLogger(__name__)

ShutterStates = [
    "OPEN",
    "CLOSED",
    "AUTO",
    "MOVING",
    "DISABLED",
    "STANDBY",
    "FAULT",
    "UNKNOWN",
]


class ShutterPropertiesSchema(HardwareSchema):
    state: Optional[Literal[tuple(ShutterStates)]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    status: Optional[str] = Field(None, json_schema_extra={"readOnly": True})
    valid: Optional[bool] = None
    open_text: Optional[str] = Field(None, json_schema_extra={"readOnly": True})
    closed_text: Optional[str] = Field(None, json_schema_extra={"readOnly": True})


class ShutterCallablesSchema(HardwareSchema):
    open: Optional[Any] = None
    close: Optional[Any] = None
    toggle: Optional[Any] = None
    reset: Optional[Any] = None


class Shutter(HardwareObject):
    _type = "shutter"
    _state_ok = [ShutterStates[0], ShutterStates[1]]

    _properties = ShutterPropertiesSchema
    _callables = ShutterCallablesSchema

    def _call_toggle(self):
        if self.get("state") == "OPEN":
            self.call("close", None)
        else:
            self.call("open", None)
