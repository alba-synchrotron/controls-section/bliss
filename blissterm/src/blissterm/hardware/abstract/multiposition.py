import logging
from typing import Any, List, Literal, Optional

from pydantic import BaseModel, Field
from .object import HardwareObject
from ...models.hardware import HardwareSchema

logger = logging.getLogger(__name__)

MultipositionStates = ["MOVING", "READY", "UNKNOWN", "ERROR"]


class MultipositionAxis(BaseModel):
    axis: Optional[str] = None
    destination: Optional[float] = None
    tolerance: Optional[float] = None


class MultipositionPosition(BaseModel):
    position: Optional[str] = None
    description: Optional[str] = None
    target: Optional[List[MultipositionAxis]] = None


class MultipositionPropertiesSchema(HardwareSchema):
    position: Optional[str] = None
    positions: List[MultipositionPosition] = Field(json_schema_extra={"readOnly": True})
    state: Optional[Literal[tuple(MultipositionStates)]] = Field(
        None, json_schema_extra={"readOnly": True}
    )


class MultipositionCallablesSchema(HardwareSchema):
    move: Optional[str] = None
    stop: Optional[Any] = None


class Multiposition(HardwareObject):
    _type = "multiposition"
    _state_ok = [MultipositionStates[0], MultipositionStates[1]]

    _properties = MultipositionPropertiesSchema
    _callables = MultipositionCallablesSchema

    def move(self, value):
        self.call("move", value)

    def stop(self):
        self.call("stop", None)
