import logging
from typing import Literal, Optional

from pydantic import Field
from .object import HardwareObject
from ...models.hardware import HardwareSchema

logger = logging.getLogger(__name__)

LightStates = ["ON", "OFF", "STANDBY", "ERROR"]


class LightPropertiesSchema(HardwareSchema):
    state: Optional[Literal[tuple(LightStates)]] = Field(
        None, json_schema_extra={"readOnly": True}
    )
    temperature: Optional[float] = Field(None, json_schema_extra={"readOnly": True})
    intensity: Optional[float] = None


class Light(HardwareObject):
    _type = "light"
    _state_ok = [LightStates[0], LightStates[1], LightStates[2]]

    _properties = LightPropertiesSchema
