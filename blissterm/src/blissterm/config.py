from typing import Any, Dict, List

from blissdata.beacon.files import read_config
from bliss.config import get_sessions_list
from bliss.config.static import get_config
from .bliss_server import repls

from .core import CoreBase, CoreResource, doc
from .models.config import PublicConfigSchema


def get_layouts() -> Dict[str, List[Dict[str, Any]]]:
    """Get the layouts from beacon"""
    try:
        return read_config("beacon:///blissterm/layouts.yml")
    except (RuntimeError, ValueError):
        return {"layouts": []}


class ConfigResource(CoreResource):
    @doc(summary="Get current config", responses={"200": PublicConfigSchema})
    def get(self):
        """Get the current UI configuration from beacon

        This includes monitor settings, top and side items, and available layouts

        Configuration is located in:
        * beacon://blissterm/config.yml
        * beacon://blissterm/layouts.yml
        """
        bliss_config = get_config()
        session_list = get_sessions_list()
        layouts = get_layouts()

        # Re-read config file
        # TODO: Warn if config has changed and requires server restart
        config = read_config("beacon:///blissterm/config.yml")
        return PublicConfigSchema(
            beamline=bliss_config.root.get("beamline"),
            sessions=config["allowed_sessions"]
            if config.get("allowed_sessions")
            else session_list + ["__DEFAULT__"],
            activeSessions=list(repls.keys()),
            layouts=layouts["layouts"],
            **config,
        ).model_dump(exclude_unset=True)


class Config(CoreBase):
    def setup(self):
        self.register_route(ConfigResource, "")
