import { useCache, useController } from '@rest-hooks/react';
import type { HardwareTypes, HWObject } from '@esrf/daiquiri-lib';
import {
  MotorDefault,
  NoObject,
  Multiposition,
  ShutterDefault,
  Frontend,
  Property,
  Info,
} from '@esrf/daiquiri-lib';

import {
  HardwareCallFunctionEndpoint,
  HardwareUpdatePropertyEndpoint,
  getHardwareObject,
} from '../api/resources/Hardware';

export const objMap: HWObject.ComponentMapping = {
  motor: MotorDefault,
  shutter: ShutterDefault,
  multiposition: Multiposition,
  frontend: Frontend,
  // measurementgroup: MeasurementGroup,
};

export const globalVariants: HWObject.ComponentMapping = {
  property: Property,
  info: Info,
};

export default function HardwareObject(props: HWObject.HardwareObjectProps) {
  const obj = useCache(getHardwareObject, { id: props.id });
  const { fetch } = useController();

  function requestChange(request: HardwareTypes.HardwareChangeAction) {
    if (obj) {
      if (request.function) {
        void fetch(HardwareCallFunctionEndpoint, { id: obj.id }, request);
      } else {
        void fetch(HardwareUpdatePropertyEndpoint, { id: obj.id }, request);
      }
    }
    return null;
  }

  function addToast(data: any) {
    props.actions.addToast(data);
  }

  const { options = {} } = props;
  if (!obj) {
    return <NoObject id={props.id} options={options} />;
  }

  function getComp() {
    const { variant } = options;
    if (variant && variant in globalVariants) {
      return globalVariants[variant];
    }
    if (obj && obj.type in objMap) {
      return objMap[obj.type];
    }
    return undefined;
  }

  const { propertiesSchema, callablesSchema } = props;
  const schema = {
    properties: propertiesSchema,
    callables: callablesSchema,
  };

  const hardware = {
    ...obj,
    requestChange,
  };

  const Comp = getComp();
  if (!Comp) return <NoObject id={props.id} options={options} />;
  return (
    // @ts-expect-error
    <Comp
      key={`hw${obj.id}`}
      hardware={hardware}
      schema={schema}
      // @ts-expect-error
      options={options}
      addToast={addToast}
      operator
      disabled={!obj.online}
    />
  );
}
