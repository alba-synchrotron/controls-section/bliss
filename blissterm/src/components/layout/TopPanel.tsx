import { Suspense } from 'react';
import { useSuspense } from '@rest-hooks/react';
import { ConfigEndpoint } from '../../api/resources/Config';
import { renderYamlNode, YAMLErrorBoundary } from '@esrf/daiquiri-lib';

function TopPanel() {
  const config = useSuspense(ConfigEndpoint);
  return <>{config.topItems.map((node) => renderYamlNode(node, 'top'))}</>;
}

export default function TopPanelWrap() {
  return (
    <Suspense>
      <YAMLErrorBoundary>
        <TopPanel />
      </YAMLErrorBoundary>
    </Suspense>
  );
}
