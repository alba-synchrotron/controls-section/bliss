import { Suspense } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import useConfig from '../../hooks/useConfig';
import useSearchParamsObj from '../../hooks/useSearchParamsObj';

interface ButtonProps {
  onClick?: () => void;
  icon: string;
  text: string;
  disabled?: boolean;
}

function SidebarButton(props: ButtonProps) {
  return (
    <button
      className="sidebar-button"
      disabled={props.disabled}
      type="button"
      title={props.text}
      onClick={() => {
        if (props.onClick) props.onClick();
      }}
    >
      <span className="sidebar-icon">
        <i className={`fa ${props.icon} fa-2x fa-fw`} />
      </span>
    </button>
  );
}

function Sidebar() {
  const config = useConfig();
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const pathParts = pathname.split('/');
  const current = pathParts.length > 2 ? pathParts[2] : '';
  const searchParams = useSearchParamsObj();

  function changeLayout(acronym: string) {
    navigate(acronym ? `/layout/${acronym.toLowerCase()}` : '/');
  }

  return (
    <div className="sidebar shadow-sm">
      <div
        className={`sidebar-trigger ${
          current === '' && !searchParams.session ? 'active' : ''
        }`}
      >
        <SidebarButton
          icon="fa-home"
          text="Terminal"
          onClick={() => changeLayout('')}
        />
      </div>
      {config.activeSessions
        .filter((sessionName) => config.sessions.includes(sessionName))
        .map((sessionName) => (
          <div
            key={sessionName}
            className={`sidebar-trigger ${
              sessionName === searchParams.session ? 'active' : ''
            }`}
          >
            <SidebarButton
              text={`Session: ${sessionName}`}
              icon="fa-terminal"
              onClick={() => navigate(`/?session=${sessionName}`)}
            />
          </div>
        ))}
      {config.layouts.map((layout) => (
        <div
          key={layout.acronym}
          className={`sidebar-trigger ${
            layout.acronym === current ? 'active' : ''
          }`}
        >
          <SidebarButton
            icon={layout.icon}
            text={layout.name}
            onClick={() => changeLayout(layout.acronym)}
          />
        </div>
      ))}
    </div>
  );
}

export default function SidebarWrap() {
  return (
    <Suspense>
      <Sidebar />
    </Suspense>
  );
}
