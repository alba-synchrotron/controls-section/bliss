import type { PropsWithChildren } from 'react';
import type { NetworkError } from '@rest-hooks/react';
import { NetworkErrorBoundary } from '@rest-hooks/react';
import { Navbar, Container, Alert } from 'react-bootstrap';

interface Props extends PropsWithChildren {}

export default function AppErrorBoundary({ children }: Props) {
  return (
    <NetworkErrorBoundary fallbackComponent={AppError}>
      {children}
    </NetworkErrorBoundary>
  );
}

function ErrorPanel({ children }: Props) {
  return (
    <div className="loading-error">
      <figure className="text-center">
        <img src="/bliss.svg" alt="Bliss" />
      </figure>
      <p className="text-center">{children}</p>
    </div>
  );
}

function AppError({ error }: { error: NetworkError }) {
  return (
    <>
      <Navbar bg="primary" variant="dark" className="shadow">
        <Container fluid>
          <Navbar.Brand className="mr-auto">Bliss</Navbar.Brand>
        </Container>
      </Navbar>
      <div
        style={{
          gridArea: 'content',
          margin: '1rem',
        }}
      >
        <ErrorPanel>
          {error.message === 'Failed to fetch' ? (
            <>Could not connect to the blissterm server. Is it running?</>
          ) : (
            <Alert variant="danger">
              <p>Something went wrong:</p>
              <p>
                {error.message} {error.status}
              </p>
            </Alert>
          )}
        </ErrorPanel>
      </div>
    </>
  );
}
