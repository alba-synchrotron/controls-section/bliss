import { Suspense } from 'react';
import useConfig from '../../hooks/useConfig';
import { renderYamlNode, YAMLErrorBoundary } from '@esrf/daiquiri-lib';

function SidePanel() {
  const config = useConfig();
  return <>{config.sideItems.map((node) => renderYamlNode(node, 'side'))}</>;
}

export default function SidePanelWrap() {
  return (
    <Suspense>
      <YAMLErrorBoundary>
        <SidePanel />
      </YAMLErrorBoundary>
    </Suspense>
  );
}
