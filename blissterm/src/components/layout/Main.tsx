import { Suspense } from 'react';
import type { CSSProperties } from 'react';
import Console from '../Console';
import SidePanel from './SidePanel';
import TopPanel from './TopPanel';
import NetworkErrorPage from '../NetworkError';
import useConfig from '../../hooks/useConfig';

function Main() {
  const config = useConfig();

  const consoleStyle: CSSProperties = {};
  if (config.sideItems.length === 0 && config.topItems.length === 0) {
    consoleStyle.gridArea = 'topbar / console / console / sidebar';
  } else if (config.sideItems.length === 0) {
    consoleStyle.gridArea = 'console / console / console / sidebar';
  } else if (config.topItems.length === 0) {
    consoleStyle.gridArea = 'topbar / console / console / console';
  }

  const sidePanelStyle: CSSProperties = {};
  if (config.topItems.length === 0) {
    sidePanelStyle.gridArea = 'topbar / sidebar / sidebar / sidebar';
  }

  return (
    <div className="main">
      {config.topItems.length > 0 && (
        <div className="top-bar">
          <TopPanel />
        </div>
      )}
      <div className="panel console" style={consoleStyle}>
        <Console />
      </div>
      {config.sideItems.length > 0 && (
        <div className="side-bar" style={sidePanelStyle}>
          <SidePanel />
        </div>
      )}
    </div>
  );
}

export default function MainWrap() {
  return (
    <Suspense>
      <NetworkErrorPage>
        <Main />
      </NetworkErrorPage>
    </Suspense>
  );
}
