import { Suspense } from 'react';
import { useParams } from 'react-router-dom';
import { YAMLLayout, registerHardwareComponent } from '@esrf/daiquiri-lib';
import useConfig from '../../hooks/useConfig';
import HardwareGroup from '../HardwareGroup';

registerHardwareComponent('HardwareGroup', HardwareGroup);

function Layout() {
  const config = useConfig();
  const { acronym } = useParams();
  const layout = config.layouts.find((l) => l.acronym === acronym);
  if (!layout) return <p>No such layout</p>;
  return (
    <YAMLLayout
      id="root"
      layout={{
        type: '',
        name: layout.name,
        error: '',
        children: layout.children,
        _parentNode: null,
        _indexFromParent: null,
      }}
    />
  );
}

export default function LayoutWrap() {
  return (
    <Suspense>
      <Layout />
    </Suspense>
  );
}
