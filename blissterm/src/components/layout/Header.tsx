import { Suspense, useEffect } from 'react';
import { Container, Navbar } from 'react-bootstrap';
import { matchRoutes, useLocation } from 'react-router-dom';
import useConfig from '../../hooks/useConfig';
import MonitorPanel from '../MonitorPanel';

function Header() {
  const config = useConfig();

  const currentLocation = useLocation();

  // TODO: Route duplication...
  const someRoutes = [{ path: '/' }, { path: '/layout/:acronym' }];
  const matches = matchRoutes(someRoutes, currentLocation);

  let currentView = '';
  if (matches?.[0].params.acronym) {
    const layout = config.layouts.find(
      (l) => l.acronym === matches?.[0].params.acronym
    );
    if (layout) currentView = layout.name;
  }

  const title = `BLISS: ${config.beamline}`;

  useEffect(() => {
    document.title = `${title}${currentView ? ` » ${currentView}` : ''}`;
  }, [currentView, title]);
  return (
    <Navbar bg="primary" variant="dark" className="shadow">
      <Container fluid>
        <Navbar.Brand className="mr-auto">{title}</Navbar.Brand>
        <MonitorPanel monitors={config.monitor} />
      </Container>
    </Navbar>
  );
}

export default function HeaderWrap() {
  return (
    <Suspense>
      <Header />
    </Suspense>
  );
}
