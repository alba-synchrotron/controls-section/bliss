import type { PropsWithChildren } from 'react';

interface Props {
  style?: Record<string, any>;
}

export default function Panel(props: PropsWithChildren<Props>) {
  return (
    <div className="panel" style={props.style}>
      {props.children}
    </div>
  );
}
