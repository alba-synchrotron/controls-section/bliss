import { useRef, useState } from 'react';
import {
  Menu,
  MenuItem,
  Highlighter,
  Typeahead,
  Hint,
} from 'react-bootstrap-typeahead';
import type { TypeaheadInputProps } from 'react-bootstrap-typeahead/types/types';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import 'react-bootstrap-typeahead/css/Typeahead.bs5.css';

import {
  OverlayTrigger,
  Tooltip,
  InputGroup,
  Form,
  Badge,
  Button,
} from 'react-bootstrap';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { TypeIcon } from '@esrf/daiquiri-lib';

export interface MeasurementGroupSchema extends HardwareTypes.Hardware {
  properties: {
    enabled: string[];
    available: string[];
    active: boolean;
    states: string[];
    active_state: string;
  };
  fetchMeasurementGroup: () => Promise<any>;
}

interface CustomTypeaheadInputProps extends TypeaheadInputProps {
  [key: string]: any;
}

function RenderInput(inputProps: CustomTypeaheadInputProps, state: any) {
  const {
    inputRef,
    referenceElementRef,
    onAddSelection,
    onRemoveSelection,
    dorment,
    pending,
    ...otherInputProps
  } = inputProps;
  const fieldRef = useRef<HTMLInputElement>();

  const clearInput = () => {
    setTimeout(() => {
      // @ts-expect-error
      const setValue = Object.getOwnPropertyDescriptor(
        window.HTMLInputElement.prototype,
        'value'
      ).set;
      if (setValue) {
        setValue.call(fieldRef.current, '');
      }
      if (fieldRef.current) {
        fieldRef.current.dispatchEvent(new Event('input', { bubbles: true }));
      }
    }, 500);
  };

  const addSelection = () => {
    if (onAddSelection) {
      onAddSelection(state.results).then(() => {
        clearInput();
      });
    }
  };

  const removeSelection = () => {
    const selected = state.text
      ? [...state.selected].filter((option) => {
          return option.toLowerCase().indexOf(state.text.toLowerCase()) !== -1;
        })
      : [...state.selected];

    if (onRemoveSelection) {
      onRemoveSelection(selected).then(() => {
        clearInput();
      });
    }
  };

  return (
    <InputGroup>
      <Hint>
        {
          // @ts-expect-error
          <Form.Control
            style={{ width: '100%' }}
            {...otherInputProps}
            ref={(node: HTMLInputElement) => {
              fieldRef.current = node;
              inputRef(node);
              referenceElementRef(node);
            }}
            placeholder={dorment ? '-' : `${state.selected.length} enabled`}
          />
        }
      </Hint>
      <Button
        disabled={inputProps.disabled || state.results.length === 0}
        onClick={addSelection}
      >
        <i className="fa fa-plus" />
      </Button>
      <Button disabled={inputProps.disabled} onClick={removeSelection}>
        <i className="fa fa-minus" />
      </Button>
    </InputGroup>
  );
}

function withExtraProps(extraProps: { [prop: string]: any }) {
  return function (wrappedComponent: any) {
    function WithExtraProps(props: any, props2: any, props3: any) {
      return wrappedComponent(props, props2, props3, extraProps);
    }

    WithExtraProps.displayName = `WithExtraProps(${
      wrappedComponent.displayName || wrappedComponent.name || 'Component'
    })`;

    return WithExtraProps;
  };
}

function RenderMenu(results: any, menuProps: any, state: any, extraProps: any) {
  const selected = [...state.selected] // eslint-disable-line @typescript-eslint/require-array-sort-compare
    .filter((option) => {
      return option.toLowerCase().indexOf(state.text.toLowerCase()) !== -1;
    })
    .sort();

  const selectedItems = selected.map((opt) => (
    /* eslint-disable-next-line jsx-a11y/anchor-is-valid */
    <a
      href="#"
      className="dropdown-item"
      key={opt}
      onClick={() => state.onRemove(opt)}
    >
      <Highlighter search={state.text}>{opt}</Highlighter>
    </a>
  ));
  const items = [...state.results].sort(); // eslint-disable-line @typescript-eslint/require-array-sort-compare
  const availableItems = items.map((opt, i) => (
    <MenuItem key={opt} option={opt} position={i}>
      <Highlighter search={state.text}>{opt}</Highlighter>
    </MenuItem>
  ));

  return (
    <Menu {...{ ...menuProps, style: { ...menuProps.style, width: 350 } }}>
      {extraProps.pending && <Menu.Header>Loading...</Menu.Header>}
      {!extraProps.pending && (
        <>
          <Menu.Header>Enabled</Menu.Header>
          {selectedItems}
          {selectedItems.length === 0 && (
            <span className="dropdown-item disabled">No items enabled</span>
          )}

          <Menu.Header>Available</Menu.Header>
          {availableItems}
          {availableItems.length === 0 && (
            <span className="dropdown-item disabled">No items available</span>
          )}
        </>
      )}
    </Menu>
  );
}

export default function MeasurementGroup(
  props: HardwareTypes.HardwareWidgetProps<MeasurementGroupSchema>
) {
  const { hardware } = props;
  const { disabled = false } = props;
  const [pending, setPending] = useState(false);
  const [dorment, setDorment] = useState(true);

  function setSelection(selected: any) {
    const added = selected.filter(
      (x: string) => !hardware.properties.enabled.includes(x)
    );
    const removed = hardware.properties.enabled.filter(
      (x) => !selected.includes(x)
    );

    if (added.length > 0) {
      void hardware.requestChange({
        function: 'enable',
        value: added,
      });
    }

    if (removed.length > 0) {
      void hardware.requestChange({
        function: 'disable',
        value: removed,
      });
    }
  }

  function onAddSelection(selection: any) {
    return hardware.requestChange({
      function: 'enable',
      value: selection,
    });
  }

  function onRemoveSelection(selection: any) {
    return hardware.requestChange({
      function: 'disable',
      value: selection,
    });
  }

  function setActive() {
    return hardware.requestChange({
      function: 'set_active',
    });
  }

  function setState(state: any) {
    return hardware.requestChange({
      function: 'switch_state',
      value: state,
    });
  }

  function refreshMG() {
    setPending(true);
    hardware
      .fetchMeasurementGroup()
      .then(() => {
        setTimeout(() => {
          setPending(false);
          setDorment(false);
        }, 500);
      })
      .catch(() => {
        setTimeout(() => {
          setPending(false);
        }, 500);
      });
  }

  return (
    <div className="hw-component">
      <div className="hw-head">
        <TypeIcon
          name="Measurement Group"
          icon="fa-list-ol"
          online={hardware.properties.active}
          message="This group is"
          activeMessage="Active"
          inactiveMessage="Inactive"
        />
        <div className="name">
          {hardware.name}
          {pending && <i className="ms-1 fa fa-arrows-rotate fa-spin" />}
        </div>
        <Badge bg="success">{hardware.properties.active_state}</Badge>
        {!hardware.properties.active && (
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="active">Activate this measurement group</Tooltip>
            }
          >
            <Button
              size="sm"
              disabled={disabled}
              onClick={() => void setActive()}
              className="float-right ml-1"
            >
              <i className="fa fa-power-off" />
            </Button>
          </OverlayTrigger>
        )}
      </div>
      <div className="hw-content">
        <Typeahead
          onFocus={refreshMG}
          onMenuToggle={(isOpen) => {
            if (!isOpen) {
              setTimeout(() => {
                setDorment(true);
              }, 1000);
            }
          }}
          disabled={disabled}
          id={`typeahead_mg_${hardware.name}`}
          multiple
          flip
          positionFixed
          renderInput={RenderInput}
          renderMenu={withExtraProps({
            name: hardware.id,
            states: hardware.properties.states,
            activeState: hardware.properties.active_state,
            setState,
            pending,
          })(RenderMenu)}
          onChange={setSelection}
          inputProps={{
            // @ts-expect-error
            onAddSelection,
            onRemoveSelection,
            dorment,
          }}
          selected={hardware.properties.enabled}
          options={hardware.properties.available}
          selectHint={(shouldSelect, e) => {
            return e.key === 'Enter' || shouldSelect;
          }}
        />
      </div>
    </div>
  );
}
