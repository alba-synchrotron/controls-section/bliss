import { Col, Row, Container } from 'react-bootstrap';
import HardwareObject from './HardwareObject';

interface Props {
  // Wrap each object in a Col so it fills the parent width and each object will be evenly spaced
  even?: boolean;
  ids: { [name: string]: any; id: string }[];
}

export default function HardwareGroup(props: Props) {
  const objs = props.ids.map((obj) => {
    const { id, ...options } = obj;
    const wrapArgs: { [name: string]: any } = {};

    if (!props.even) wrapArgs.lg = 'auto';

    function requestChange() {
      console.log('request change');
    }

    function addToast() {
      console.log('add toast');
    }

    return (
      <Col {...wrapArgs} className="gx-0" key={`col${id}`}>
        <HardwareObject
          key={`chw${id}`}
          id={id}
          options={options}
          actions={{ requestChange, addToast }}
        />
      </Col>
    );
  });

  return (
    <Container fluid className="hw-group gx-0">
      <Row className="gx-0">{objs}</Row>
    </Container>
  );
}
