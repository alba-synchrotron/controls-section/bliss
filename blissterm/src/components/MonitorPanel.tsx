import { Suspense } from 'react';
import { useCache, useSuspense } from '@rest-hooks/react';
import type { HardwareTypes, MonitorPanelProps } from '@esrf/daiquiri-lib';
import { MonitorPanel, registerRuntimeHook } from '@esrf/daiquiri-lib';
import { getHardwareObject, HardwareResource } from '../api/resources/Hardware';

function useHardware(id: string | null) {
  // @ts-expect-error
  return useCache(getHardwareObject, { id }) as HardwareTypes.Hardware;
}

registerRuntimeHook('useHardware', useHardware);

function MonitorPanelMain(props: MonitorPanelProps) {
  useSuspense(HardwareResource.getList);
  return <MonitorPanel {...props} />;
}

export default function MonitorPanelWrap(props: MonitorPanelProps) {
  return (
    <Suspense fallback="Loading...">
      <MonitorPanelMain bg="light" margin="start" {...props} />
    </Suspense>
  );
}
