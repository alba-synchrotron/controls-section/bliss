import { useMemo, useState, useEffect, useCallback, Suspense } from 'react';
import { useNavigate, createSearchParams } from 'react-router-dom';
import { useController } from '@rest-hooks/react';
import { Form, Modal, Button, Alert } from 'react-bootstrap';
import { Terminal } from 'xterm';
import { CanvasAddon } from 'xterm-addon-canvas';
import { FitAddon } from 'xterm-addon-fit';
import 'xterm/css/xterm.css';

import MeasurementGroup from './MeasurementGroup';
import { SocketIO } from '../api/SocketIO';
import { TermSizeEndpoint } from '../api/resources/TermSize';
import { ConfigEndpoint } from '../api/resources/Config';
import useConfig from '../hooks/useConfig';
import useSearchParamsObj from '../hooks/useSearchParamsObj';

function HelpModal({
  show,
  content,
  onClose,
}: {
  show: boolean;
  content: string;
  onClose: () => void;
}) {
  return (
    <Modal show={show} onHide={onClose} size="lg">
      <Modal.Header>
        <Modal.Title>Help</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: content }}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onClose}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

function Console() {
  const [socketError, setSocketError] = useState<string>();
  const [socketConnected, setSocketConnected] = useState<boolean>();

  const { fetch } = useController();
  const searchParams = useSearchParamsObj();
  const fitAddon = useMemo(() => new FitAddon(), []);
  const socketIO = useMemo(
    () =>
      new SocketIO({
        onConnectErrorCallback(message) {
          setSocketError(message);
        },
        onConnectCallback() {
          setSocketConnected(true);
          setSocketError('');
        },
        onDisconnectCallback() {
          setSocketConnected(false);
        },
        namespace: 'console',
      }),
    []
  );

  const [term, setTerm] = useState<Terminal>();
  const [ready, setReady] = useState<boolean>(false);
  const [showHelp, setShowHelp] = useState<boolean>(false);
  const [helpContent, setHelpContent] = useState<string>('');
  const session_name = searchParams.session || '';

  console.log('term render');

  const handleResize = useCallback(async () => {
    if (!term) {
      console.log('handleResize no term yet');
      return;
    }
    fitAddon.fit();
    console.log('resize', term.cols, term.rows);

    try {
      await fetch(TermSizeEndpoint, {
        w: term.cols,
        h: term.rows,
        session_name,
      });
    } catch (error) {
      console.log('couldnt set terminal size', error);
    }
  }, [fitAddon, term, fetch, session_name]);

  useEffect(() => {
    console.log('term changed', term);
    if (term) void handleResize();
  }, [term, handleResize]);

  const terminalRef = useCallback(
    (node: HTMLDivElement) => {
      console.log('term ref loaded', node);
      if (!node) return;

      setTimeout(() => {
        const terminal = new Terminal({
          theme: { background: 'white', foreground: 'black', cursor: 'black' },
          convertEol: true,
        });
        terminal.attachCustomKeyEventHandler((ev) => {
          // prevent ctrl-d
          if (ev.ctrlKey && ev.keyCode === 68) {
            ev.preventDefault();
            return false;
          }
          return true;
        });
        terminal.loadAddon(new CanvasAddon());
        terminal.loadAddon(fitAddon);
        terminal.open(node);
        fitAddon.fit();

        socketIO.on('terminal_output', (data) => {
          if (typeof data === 'string') {
            terminal.write(data);
          } else {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
            terminal.write(new Uint8Array(data));
          }
        });
        terminal.onData((data) => {
          socketIO.emit('terminal_input', { input: data, session_name });
        });
        setReady(false);
        terminal.options.disableStdin = true;
        if (session_name === '') {
          setTerm(terminal);
        } else {
          socketIO.emit('attach', {
            session_name,
            w: terminal.cols,
            h: terminal.rows,
          });
          socketIO.on('ready', () => {
            setReady(true);
            void fetch(ConfigEndpoint);
            terminal.options.disableStdin = false;
          });

          socketIO.on('show_help', (helpDoc: string) => {
            setHelpContent(helpDoc);
            setShowHelp(true);
          });

          console.log('set term');
          setTerm(terminal);
        }
      }, 1000);
    },
    [fitAddon, session_name, socketIO, fetch]
  );

  useEffect(() => {
    console.log('term mount');
    return () => {
      console.log('term unmount');
      if (term) term.dispose();
      // setTerm(undefined);
    };

    // TODO: Render loop if term is included here, something not memo'd properly yet
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div
      // className="mb-1"
      style={{
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <div>
        <MeasurementGroup session_name={session_name} disabled={!ready} />
      </div>
      <div
        className="mt-1"
        ref={terminalRef}
        style={{
          flex: 1,
        }}
      >
        {!socketConnected && (
          <Alert variant="warning">Socket not connected yet</Alert>
        )}
        {socketError && (
          <Alert variant="danger">
            Could not connect to blissterm socket: `{socketError}`
          </Alert>
        )}
        <HelpModal
          show={showHelp}
          content={helpContent}
          onClose={() => setShowHelp(false)}
        />
      </div>
    </div>
  );
}

function SessionSelect() {
  const config = useConfig();
  const navigate = useNavigate();
  const searchParams = useSearchParamsObj();

  function setSession(sessionName: string) {
    const { session, ...rest } = searchParams;
    navigate({
      pathname: '',
      search: createSearchParams({ ...rest, session: sessionName }).toString(),
    });
  }

  return (
    <div>
      <Form>
        <Form.Control
          as="select"
          onChange={(e) => setSession(e.target.value)}
          defaultValue={searchParams.session}
        >
          <option key="-" value="" disabled={!!searchParams.session}>
            -- sessions --
          </option>
          {config.sessions.map((session) => (
            <option key={session} value={session}>
              {session}
            </option>
          ))}
        </Form.Control>
      </Form>
    </div>
  );
}

export default function ConsoleWrap() {
  // used to force remount on change
  const searchParams = useSearchParamsObj();
  const sessionName = searchParams.session || '';

  return (
    <div
      style={{
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <Suspense>
        {sessionName ? <Console key={sessionName} /> : <SessionSelect />}
      </Suspense>
    </div>
  );
}
