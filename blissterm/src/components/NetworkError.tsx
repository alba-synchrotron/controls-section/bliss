import { NetworkErrorBoundary } from '@rest-hooks/react';
import { forwardRef, useEffect, useState } from 'react';
import type { NetworkError } from '@rest-hooks/react';
import { Alert } from 'react-bootstrap';

export default function NetworkErrorPage({
  children,
}: {
  children: JSX.Element;
}) {
  return (
    <NetworkErrorBoundary fallbackComponent={ParsedError}>
      {children}
    </NetworkErrorBoundary>
  );
}

export function ParsedErrorMain({ error }: { error: NetworkError }) {
  const [json, setJson] = useState<Record<string, any>>({});

  useEffect(() => {
    setJson({});
    error.response
      ?.clone()
      .json()
      .then((jsonTmp: Record<string, any>) => setJson(jsonTmp))
      .catch((error_) => console.log('Couldnt load json', error_));
  }, [error]);

  if (error.status === 422) {
    const errors =
      Array.isArray(json.detail) &&
      json.detail?.map((errorDetail: Record<string, any>) => (
        <li key={errorDetail.loc.join('.')}>
          {errorDetail.loc.join('.')}: {errorDetail.msg}
        </li>
      ));

    return <span>Validation error(s): {errors}</span>;
  }

  if (json.error) {
    return (
      <span>
        {error.message} &raquo; {json.error}
      </span>
    );
  }
  return <span>An error occured: {error.message}</span>;
}

/**
 * Parsed rest-hooks NetworkError
 *   with ref forwarded (to scroll in to view)
 */
export const ParsedErrorRef = forwardRef(
  ({ error }: { error?: NetworkError }, ref: any) => {
    return (
      <>
        {error !== undefined && (
          <Alert variant="danger" ref={ref}>
            <ParsedErrorMain error={error} />
          </Alert>
        )}
      </>
    );
  }
);
ParsedErrorRef.displayName = 'ParsedErrorRef';

/**
 * Parsed rest-hooks NetworkError
 */
export function ParsedError({ error }: { error?: NetworkError }) {
  return (
    <>
      {error !== undefined && (
        <Alert variant="danger">
          <ParsedErrorMain error={error} />
        </Alert>
      )}
    </>
  );
}
