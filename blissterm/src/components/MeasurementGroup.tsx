import { Suspense } from 'react';
import { useController, useSuspense } from '@rest-hooks/react';
import type { HardwareTypes } from '@esrf/daiquiri-lib';
import { MeasurementGroupResource } from '../api/resources/MeasurementGroup';
import MeasurementGroup from './MeasurementGroupBase';
import NetworkErrorPage from './NetworkError';

function MG({
  session_name,
  disabled,
}: {
  session_name: string;
  disabled: boolean;
}) {
  function addToast() {
    console.log('add toast');
  }

  const { fetch } = useController();
  const measurementGroup = useSuspense(
    MeasurementGroupResource.get,
    disabled
      ? null
      : {
          session_name,
        }
  );

  const hardware = {
    name: 'ACTIVE_MG',
    id: 'ACTIVE_MG',
    alias: '',
    properties: {
      enabled: measurementGroup ? measurementGroup.enabled : [],
      available: measurementGroup ? measurementGroup.available : [],
      active: true,
      states: [],
      active_state: '',
    },
    online: false,
    type: 'measurementgroup',
    requestChange,
    fetchMeasurementGroup,
  };

  const schema = {
    properties: undefined,
    callables: undefined,
  };

  const options = {};

  function fetchMeasurementGroup() {
    return fetch(MeasurementGroupResource.get, { session_name });
  }

  function requestChange(
    action: HardwareTypes.HardwareChangeAction
  ): Promise<any> {
    if (session_name === '') {
      return Promise.resolve({});
    }

    if (action.function === 'enable') {
      return fetch(
        MeasurementGroupResource.partialUpdate,
        { session_name },
        {
          enabled: action.value,
        }
      );
    }

    if (action.function === 'disable') {
      return fetch(
        MeasurementGroupResource.partialUpdate,
        { session_name },
        { disabled: action.value }
      );
    }
    return Promise.resolve();
  }

  return (
    <MeasurementGroup
      hardware={hardware}
      // @ts-expect-error
      schema={schema}
      options={options}
      addToast={addToast}
      operator
      disabled={disabled}
      fetchMeasurementGroup={fetchMeasurementGroup}
    />
  );
}

export default function MeasurementGroupWrap({
  session_name,
  disabled,
}: {
  session_name: string;
  disabled: boolean;
}) {
  return (
    <Suspense>
      <NetworkErrorPage>
        <MG session_name={session_name} disabled={disabled} />
      </NetworkErrorPage>
    </Suspense>
  );
}
