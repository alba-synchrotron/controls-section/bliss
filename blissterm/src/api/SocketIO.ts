import { io } from 'socket.io-client';

interface SocketIOProps {
  onConnectCallback?: () => void;
  onDisconnectCallback?: () => void;
  onConnectErrorCallback?: (message?: string) => void;
  namespace: string;
}

export class SocketIO {
  public static baseUrl = '';
  private readonly ws;

  constructor(private readonly props: SocketIOProps) {
    this.props = props;
    const url =
      SocketIO.baseUrl.replace('/api', '') +
      (props.namespace ? `/${props.namespace}` : '');
    this.ws = io(url);
    this.ws.on('connect', this.onConnect.bind(this, props.namespace));
    this.ws.on('disconnect', this.onDisconnect.bind(this, props.namespace));
    this.ws.on(
      'connect_error',
      this.onConnectError.bind(this, props.namespace)
    );
  }

  on(signal: string, callback: (data: any) => void) {
    this.ws.on(signal, callback);
  }

  emit(signal: string, data: any) {
    this.ws.emit(signal, data);
  }

  onConnect(namespace: string) {
    console.log(`SocketIO Connected to namespace '${namespace}'`);
    if (this.props.onConnectCallback) this.props.onConnectCallback();
  }

  onDisconnect(namespace: string) {
    console.log(`SocketIO Connected from namespace '${namespace}'`);

    if (this.props.onDisconnectCallback) this.props.onDisconnectCallback();
  }

  onConnectError(namespace: string, err: Error) {
    console.log(`SocketIO Connection Error for namespace '${namespace}'`, err);
    if (this.props.onConnectErrorCallback) {
      this.props.onConnectErrorCallback(err.message);
    }
  }
}
