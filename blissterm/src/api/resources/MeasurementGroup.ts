import { Entity } from '@rest-hooks/rest';
import createBaseResource from './base/BaseResource';

class MeasurementGroupEntity extends Entity {
  session_name = '';
  enabled: string[] = [];
  available: string[] = [];
  disabled: string[] = [];

  pk() {
    return this.session_name;
  }
}

export const MeasurementGroupResource = createBaseResource({
  path: '/session/:session_name/active_mg',
  schema: MeasurementGroupEntity,
});
