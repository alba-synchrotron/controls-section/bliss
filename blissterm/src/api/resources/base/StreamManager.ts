import type { Manager, Middleware } from '@rest-hooks/core';
import type { EndpointInterface } from '@rest-hooks/endpoint';

import { SocketIO } from '../../SocketIO';

interface EventData {
  id: string;
  data: Record<string, any>;
}

export default class StreamManager implements Manager {
  protected declare middleware: Middleware;
  protected declare evtSource: WebSocket | EventSource;
  protected declare socketIO: SocketIO;

  constructor(protected endpoints: Record<string, EndpointInterface>) {
    this.socketIO = new SocketIO({ namespace: 'hardware' });

    this.middleware = (controller) => {
      this.socketIO.on('change', (eventData: EventData) => {
        // Horrible sync to allow multiple events to fire closely together
        //   Otherwise getResponse is returning stale data
        setTimeout(() => {
          try {
            const args: any[] = [];
            const current = controller.getResponse(
              this.endpoints.getList,
              // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
              ...args,
              controller.getState()
            );

            if (!(current.data && typeof current.data === 'object')) return;

            // @ts-expect-error
            const objectIndex = current.data.results.findIndex(
              (item: Record<string, any>) => item.id === eventData.id
            );
            const newData = { ...current.data };
            // @ts-expect-error
            newData.results[objectIndex].properties = {
              // @ts-expect-error
              ...newData.results[objectIndex].properties,
              ...eventData.data,
            };

            void controller.setResponse(
              this.endpoints.getList,
              // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
              ...args,
              newData
            );
          } catch (error) {
            console.error('Failed to handle message');
            console.error(error);
          }
        }, 1);
      });

      return (next) => async (action) => next(action);
    };
  }

  cleanup() {
    console.log('StreamManager cleanup');
  }

  getMiddleware() {
    return this.middleware;
  }
}
