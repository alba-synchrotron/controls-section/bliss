import type { RestGenerics } from '@rest-hooks/rest';
import { RestEndpoint } from '@rest-hooks/rest';
import config from '../../../config';

export class BaseEndpoint<
  O extends RestGenerics = any
> extends RestEndpoint<O> {
  public urlPrefix = config.baseUrl;
}
