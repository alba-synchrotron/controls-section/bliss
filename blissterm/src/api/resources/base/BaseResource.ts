import { createResource } from '@rest-hooks/rest';
import type { Schema } from '@rest-hooks/rest';
import type { EndpointExtraOptions } from '@rest-hooks/endpoint';
import { BaseEndpoint } from './BaseEndpoint';

export default function createBaseResource<U extends string, S extends Schema>({
  path,
  schema,
  endpointOptions,
}: {
  readonly path: U;
  readonly schema: S;
  readonly endpointOptions?: EndpointExtraOptions;
}) {
  return createResource({
    path,
    schema,
    Endpoint: BaseEndpoint,
    ...endpointOptions,
  });
}
