import { Query, schema, Entity } from '@rest-hooks/rest';

import type { HardwareTypes } from '@esrf/daiquiri-lib';
import createPaginatedResource from './base/PaginatedResource';
import { BaseEndpoint } from './base/BaseEndpoint';

class HardwareClass implements HardwareTypes.Hardware {
  name = '';
  id = '';
  alias: string | null = '';
  properties: Record<string, any> = {};
  online = false;
  type = '';
}

// eslint-disable-next-line new-cap
class HardwareEntity extends schema.Entity(HardwareClass) {
  pk() {
    return this.id;
  }
}
export const HardwareResource = createPaginatedResource({
  path: '/hardware/:id',
  schema: HardwareEntity,
});

export const getHardwareObject = new Query(
  new schema.All(HardwareEntity),
  (entries, { id }: { id: string }) => {
    const objects = [...entries].filter((object) => object.id === id);
    if (objects.length === 0) return undefined;
    return objects[0];
  }
);

class HardwareCallFunctionEntity extends Entity {
  id = '';
  function = '';
  value: any = '';
  response? = '';

  pk() {
    return this.id;
  }
}

export const HardwareCallFunctionEndpoint = new BaseEndpoint({
  path: '/hardware/:id',
  schema: HardwareCallFunctionEntity,
  method: 'POST',
  process(value, params) {
    return {
      id: params.id,
      ...value,
    };
  },
});

class HardwareUpdatePropertyEntity extends Entity {
  id = '';
  property = '';
  value: any = '';

  pk() {
    return this.id;
  }
}

export const HardwareUpdatePropertyEndpoint = new BaseEndpoint({
  path: '/hardware/:id',
  schema: HardwareUpdatePropertyEntity,
  method: 'PUT',
  process(value, params) {
    return {
      id: params.id,
      ...value,
    };
  },
});
