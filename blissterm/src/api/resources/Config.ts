import { Entity } from '@rest-hooks/rest';
import type { YamlNode } from '@esrf/daiquiri-lib';
import { BaseEndpoint } from './base/BaseEndpoint';

interface MonitorPanelItem {
  comparator: string;
  comparison: string | number[];
  name: string;
  overlay: string;
  value: string;
}

interface Layout {
  acronym: string;
  name: string;
  children: YamlNode[];
  icon: string;
}

class ConfigEntity extends Entity {
  beamline = '';
  sessions: string[] = [];
  activeSessions: string[] = [];
  monitor: MonitorPanelItem[] = [];
  layouts: Layout[] = [];
  topItems: YamlNode[] = [];
  sideItems: YamlNode[] = [];
  activeMG = true;
  pk() {
    return 'config';
  }
}

export const ConfigEndpoint = new BaseEndpoint({
  path: '/config',
  schema: ConfigEntity,
});
