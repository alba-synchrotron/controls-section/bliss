import { Entity } from '@rest-hooks/rest';
import { BaseEndpoint } from './base/BaseEndpoint';

class TermSizeEntity extends Entity {
  w = 0;
  h = 0;
  session_name = '';
  response = '';

  public pk() {
    return this.session_name;
  }
}

export const TermSizeEndpoint = new BaseEndpoint({
  path: '/console/term_size',
  schema: TermSizeEntity,
  method: 'POST',
  process(value, params) {
    return {
      response: value,
      session_name: params.session_name,
    };
  },
});
