# blissterm

![blissterm](images/mockup.png 'blissterm')

blissterm is a web shell for BLISS

## bliss API

blissterm provides a REST API that can be consumed by other applications to automate BLISS
